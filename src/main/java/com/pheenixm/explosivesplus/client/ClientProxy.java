package com.pheenixm.explosivesplus.client;

import java.lang.reflect.Method;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.vecmath.Matrix4d;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Project;
import org.lwjgl.util.vector.Matrix4f;

import com.google.common.base.Predicates;
import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.client.render.entity.RenderChaos;
import com.pheenixm.explosivesplus.client.render.entity.RenderCamera;
import com.pheenixm.explosivesplus.client.render.entity.RenderExplosionController;
import com.pheenixm.explosivesplus.client.render.entity.RenderFusedBomb;
import com.pheenixm.explosivesplus.client.render.entity.RenderMissile;
import com.pheenixm.explosivesplus.client.render.entity.RenderNova;
import com.pheenixm.explosivesplus.client.render.entity.RenderPhysicsBlock;
import com.pheenixm.explosivesplus.client.render.entity.RenderPredatorMissile;
import com.pheenixm.explosivesplus.client.render.entity.RenderShockwave;
import com.pheenixm.explosivesplus.client.render.entity.RenderStealthBomber;
import com.pheenixm.explosivesplus.client.render.entity.RenderValkyrie;
import com.pheenixm.explosivesplus.client.render.tile.RenderSilo;
import com.pheenixm.explosivesplus.client.render.tile.RenderCentrifuge;
import com.pheenixm.explosivesplus.client.render.tile.RenderGeneratorTier1;
import com.pheenixm.explosivesplus.client.render.tile.RenderItemPredator;
import com.pheenixm.explosivesplus.client.render.tile.RenderItemStealth;
import com.pheenixm.explosivesplus.common.CommonProxy;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.RegistryHandler;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.entities.EntityBiomeBuster;
import com.pheenixm.explosivesplus.common.entities.EntityFusedBomb;
import com.pheenixm.explosivesplus.common.entities.EntityMegaNuke;
import com.pheenixm.explosivesplus.common.entities.EntityMolecularDisruptor;
import com.pheenixm.explosivesplus.common.entities.EntityNovaBomb;
import com.pheenixm.explosivesplus.common.entities.EntityNuke;
import com.pheenixm.explosivesplus.common.entities.EntityPhysicsBlock;
import com.pheenixm.explosivesplus.common.entities.EntityShockwave;
import com.pheenixm.explosivesplus.common.entities.EntityStealthBomber;
import com.pheenixm.explosivesplus.common.entities.EntityTNTRain;
import com.pheenixm.explosivesplus.common.entities.EntityTheoryOfChaos;
import com.pheenixm.explosivesplus.common.entities.EntityViewer;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityControllable;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityPredatorMissile;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityValkyrie;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTheoryOfChaosController;
import com.pheenixm.explosivesplus.common.entities.missiles.EntityBallisticMissile;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.tiles.generator.Tier1Generator;
import com.pheenixm.explosivesplus.common.util.ModResourceLocation;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import ladylib.client.shader.ShaderRegistryEvent;
import ladylib.compat.EnhancedBusSubscriber;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.entity.RenderEntity;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.registry.IRegistry;
import net.minecraftforge.client.event.EntityViewRenderEvent.CameraSetup;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.event.MouseEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.client.event.sound.PlaySoundEvent;
import net.minecraftforge.client.event.sound.SoundEvent.SoundSourceEvent;
import net.minecraftforge.client.event.RenderPlayerEvent.Pre;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.PlaySoundAtEntityEvent;
import net.minecraftforge.event.entity.living.LivingSetAttackTargetEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.LeftClickBlock;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@EventBusSubscriber(Side.CLIENT)
@EnhancedBusSubscriber(side = Side.CLIENT)
public class ClientProxy implements CommonProxy {

	public static KeyBinding[] keyBindings;
	public static RenderView viewpointHandler;
	public static Minecraft mc = Minecraft.getMinecraft();
	
	public static int[] screenCoords = new int[2];
	
	
	
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// GUI STUFF GOES IN HERE TOO, DEAL WITH LATER
		System.out.println("Client proxy called");
		MinecraftForge.EVENT_BUS.register(this);
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		ExplosivesPlusHolder.PREDATOR.setTileEntityItemStackRenderer(new RenderItemPredator());
		ExplosivesPlusHolder.STEALTH.setTileEntityItemStackRenderer(new RenderItemStealth());
		registerKeyBindings();
		viewpointHandler = new RenderView();
	    MinecraftForge.EVENT_BUS.register(viewpointHandler);

		//TODO: Make this configurable
		if(!Minecraft.getMinecraft().getFramebuffer().isStencilEnabled()){
			Minecraft.getMinecraft().getFramebuffer().enableStencil();
		}
	}

	@Override
	public void postInit(FMLPostInitializationEvent event) {
		// TODO Auto-generated method stub

	}
	


	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event) {
		
        OBJLoader.INSTANCE.addDomain(ExplosivesPlus.MOD_ID);
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySilo.class, new RenderSilo());
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCentrifuge.class, new RenderCentrifuge());
        ClientRegistry.bindTileEntitySpecialRenderer(Tier1Generator.class, new RenderGeneratorTier1());

		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.NOVA));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.C4));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.BIOME));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.MEGA));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.CHAOS));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.RAIN));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.DISRUPT));

		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.CONCRETE));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.IRON_SLAB));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.EM_BLOCK));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.LINEAR_EM_BLOCK));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.LINEAR_EM_SLAB));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.IN_OUT_BLOCK));

		
		
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.CABLE));
		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.INJECTOR));

		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.DEBUGGER_TILE));

		registerModel(Item.getItemFromBlock(ExplosivesPlusHolder.CREEPER));

		registerModel(ExplosivesPlusHolder.DETONATOR);
		registerModel(ExplosivesPlusHolder.REPULSION);
		registerModel(ExplosivesPlusHolder.BOOMSTICK);
		registerModel(ExplosivesPlusHolder.DEBUGGER);
		
		registerModel(ExplosivesPlusHolder.REACTONITE);
		registerModel(ExplosivesPlusHolder.REFINED_REACTONITE);


		registerModel(ExplosivesPlusHolder.WARHEAD_BIOME);
		registerModel(ExplosivesPlusHolder.WARHEAD_MEGA);
		registerModel(ExplosivesPlusHolder.WARHEAD_NOVA);
		
		registerModel(ExplosivesPlusHolder.FUSELAGE_NORMAL);

		registerModel(ExplosivesPlusHolder.ENGINE_NORMAL);
		
		registerModel(ExplosivesPlusHolder.PREDATOR);
		registerModel(ExplosivesPlusHolder.STEALTH);


		registerRenderers();
	}

	private static void registerModel(Item item) {
		ModelLoader.setCustomModelResourceLocation(item, 0,
				new ModelResourceLocation(item.getRegistryName(), "inventory"));

	}

	protected static void registerBlockModel(Block block, int meta, String sub,
			String variant) {
		System.out.println(block.getRegistryName());
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block),
				meta, new ModelResourceLocation(block.getRegistryName() + sub,
						variant));
	}

	private static void registerRenderers() {
		

		System.out.println("Register called for Entities");
		RenderingRegistry.registerEntityRenderingHandler(EntityNovaBomb.class, manager -> new RenderFusedBomb(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityBiomeBuster.class, manager -> new RenderFusedBomb(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityMegaNuke.class, manager -> new RenderFusedBomb(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityNuke.class, manager -> new RenderFusedBomb(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityTNTRain.class, manager -> new RenderFusedBomb(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityTheoryOfChaos.class, manager -> new RenderFusedBomb(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityMolecularDisruptor.class, manager -> new RenderFusedBomb(manager));

		RenderingRegistry.registerEntityRenderingHandler(EntityNovaController.class, manager -> new RenderNova(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityExplosionController.class, manager -> new RenderExplosionController(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityBallisticMissile.class, manager -> new RenderMissile(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityPredatorMissile.class, manager -> new RenderPredatorMissile(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityStealthBomber.class, manager -> new RenderStealthBomber(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityViewer.class, manager -> new RenderCamera(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityShockwave.class, manager -> new RenderShockwave(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityPhysicsBlock.class, manager -> new RenderPhysicsBlock(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityTheoryOfChaosController.class, manager -> new RenderChaos(manager));
		RenderingRegistry.registerEntityRenderingHandler(EntityValkyrie.class, manager -> new RenderValkyrie(manager));

	}
	
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public static final void onModelBakeEvent(final ModelBakeEvent event) {
		final IRegistry<ModelResourceLocation, IBakedModel> registry = event.getModelRegistry();

		injectModels(registry);
	}

	@SideOnly(Side.CLIENT)
	private static final void injectModels(final IRegistry<ModelResourceLocation, IBakedModel> registry) {
		final ModResourceLocation[] models = {

				new ModResourceLocation(ExplosivesPlus.MOD_ID, "item/engine_normal.obj"),

				new ModResourceLocation(ExplosivesPlus.MOD_ID, "item/fuselage_normal.obj"),

				new ModResourceLocation(ExplosivesPlus.MOD_ID, "item/warhead_biome.obj"),

		};

		for (ModResourceLocation model : models) {
			try {
				/* modified from code made by Draco18s */
				ModelResourceLocation location = new ModelResourceLocation(model.toString());
				IBakedModel bakedModel = ModelRegistry.INSTANCE.getBakedModel(model);

				registry.putObject(location, bakedModel);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void registerKeyBindings() {
		// declare an array of key bindings
		keyBindings = new KeyBinding[2]; 
		  
		// instantiate the key bindings
		keyBindings[0] = new KeyBinding("key.zoom.desc", Keyboard.KEY_Z, "key.explosivesplus.category");
		keyBindings[1] = new KeyBinding("key.anything.desc", Keyboard.KEY_L, "key.explosivesplus.category");
		  
		// register all the key bindings
		for (int i = 0; i < keyBindings.length; ++i) 
		{
		    ClientRegistry.registerKeyBinding(keyBindings[i]);
		}

	}
	
	
	@SubscribeEvent
	public void updateCamera(RenderHandEvent event) {
		Minecraft mc = Minecraft.getMinecraft();
		if(mc.getRenderViewEntity() instanceof EntityViewer) {
			EntityPlayer player = mc.player;
			EntityViewer view = (EntityViewer) mc.getRenderViewEntity();
			
			if(player.rotationYaw > 360) player.rotationYaw -= 360;
			if(player.rotationYaw < 0) player.rotationYaw += 360;
			
			if(!view.isAngleLocked()) {
				view.rotationPitch = player.rotationPitch;
				view.rotationYaw = player.rotationYaw;
			} else {
				//If view is riding an entity, 
				//set their rotation/pitch to prescribed values
				if(view.isRiding() && view.getRidingEntity() instanceof EntityControllable) {
					EntityControllable control = (EntityControllable) view.getRidingEntity();
					view.rotationPitch = control.getLockPitch();
					view.rotationYaw = control.getLockYaw();
				}
			}
			
		}
	}
	
	
	/**
	 * Helps to stop problems from occurring with
	 * renderViewEntity
	 * @param event
	 */
	@SubscribeEvent
	public void playerInteract(PlayerInteractEvent event) {
		if(event.getSide() == Side.CLIENT) {
			if(Minecraft.getMinecraft().getRenderViewEntity() instanceof EntityViewer) {
				if(event.isCancelable()) {
					event.setCanceled(true);
				}
				System.out.println("Interact event called client-side");
			}
		}
	}
	
	/**
	 * Helps to stop problems from occurring with
	 * renderViewEntity
	 * @param event
	 */
	@SubscribeEvent
	public void onMouseClick(MouseEvent event) {
		if(Minecraft.getMinecraft().getRenderViewEntity() instanceof EntityViewer) {
			if(event.getButton() == 0) {
				event.setCanceled(true);
				System.out.println("Cancelled mouse event");
			}
		}
	}
	
	
	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=true)
	public static void onEvent(KeyInputEvent event)
	{
	    KeyBinding[] keyBindings = ClientProxy.keyBindings;
	    if (keyBindings[0].isPressed()) 
	    {
	    		//Key Z is down
	    		RenderUtils.isZooming = !RenderUtils.isZooming;
	    		System.out.println(RenderUtils.isZooming);
	    }
	    if (keyBindings[1].isPressed()) 
	    {
	    		//Key L is down
	    	
	    }
	}
	
	@SubscribeEvent public static void renderGameOverlay(RenderGameOverlayEvent.Post event) {

		GL11.glPushMatrix();
        mc.ingameGUI.drawString(mc.fontRenderer, "overlay", screenCoords[0], screenCoords[1], 0xFF00FF00);
        GL11.glPopMatrix();
	}
	
	/* For future use
	@SubscribeEvent
	public void onCameraSetup(EntityViewRenderEvent event) {
		
	}
	
	
	public void onFOVCall(FOVModifier event) {
		
	}
	
	
	*/
	
	/*
	@SubscribeEvent
	public void onRenderOverlayPre(RenderGameOverlayEvent.Pre event)
	{
		Minecraft mc = Minecraft.getMinecraft();
		if(RenderUtils.isZooming && event.getType()==RenderGameOverlayEvent.ElementType.CROSSHAIRS)
		{
			event.setCanceled(true);
			//Process zooming here
		}
	}
	*/
	
    @SubscribeEvent
    public void onClientWorldLoad(WorldEvent.Load event)
    {
        if(event.getWorld() instanceof WorldClient)
        {
            RenderView.cameraGlobalRender.setWorldAndLoadRenderers((WorldClient) event.getWorld());
        }
    }
    
    @SubscribeEvent
    public void onClientWorldUnload(WorldEvent.Unload event)
    {
        if(event.getWorld() instanceof WorldClient)
        {
            RenderView.clearAll();
        }
    }
    
	@SubscribeEvent
	public static void onRenderWorldLast(RenderWorldLastEvent event) {	
		RenderUtils.updateWorldMatrix();
		//TODO: Fix shaders by uncommenting this and adding a json
		//RenderUtils.renderShockwaves(event);
		//RenderUtils.batchRenderBlocks(event);
		RenderUtils.batchRender(event);
	}
	
    
    @SubscribeEvent
    public void onShaderRegistry(ShaderRegistryEvent event) {
        event.registerShader(RenderUtils.SHOCKWAVE_SHADER);
        System.out.println("Shaders registered");
    }


}
