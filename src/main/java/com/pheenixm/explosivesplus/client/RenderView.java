package com.pheenixm.explosivesplus.client;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import com.pheenixm.explosivesplus.client.gui.RenderGlobalTarget;
import com.pheenixm.explosivesplus.common.entities.EntityViewer;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.entity.Entity;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

/**
 * Must create an instance of this class for every renderer you want to 
 * be able to use it
 * @author RobMontgomery
 *
 */
public class RenderView {

	private static Minecraft mc = Minecraft.getMinecraft();
	public static final int RESOLUTION = 1024;
	private long renderTime;
	
	public static Map<EntityViewer, Integer> cameraMap = new ConcurrentHashMap<>();
	private static Collection<Integer> toRemove = Collections.synchronizedCollection(new HashSet<Integer>	());
	public static RenderGlobal cameraGlobalRender = new RenderGlobalTarget(mc);
	protected RenderGlobal renderGlobalBackup;
	private static Entity renderEntity;
	private static Entity backupEntity;

	
	public RenderView() {}
	
	public static boolean removeCamera(EntityViewer entity) {
		Integer remove = cameraMap.remove(entity);
		if(remove != null) {
			entity.framebuffer.deleteFramebuffer();
			return toRemove.add(remove);
		} else {
			return false;
		}
	}
	
	public static void clearAll() {
		cameraMap.clear();
	}
	
	/**
	 * Checks to see if our texture exists, 
	 * creates it if it does not
	 * @param entity
	 * @return true if the texture exists, false if it did not
	 */
	private boolean checkForTexture(EntityViewer entity) {
		if(cameraMap.containsKey(entity)) {
			return true;
		}
		int textureId = entity.framebuffer.framebufferTexture;
        cameraMap.put(entity, textureId);
        return false;
	}
	
	public void renderToGUI(EntityViewer entity, int left, int top, int width, int height) {
		GuiScreen screen = mc.currentScreen;
		//Automatically add the entity to the map
        if(!checkForTexture(entity)) {
        		return;
        }
        entity.setShouldRender(true);
        GlStateManager.pushMatrix();
        GlStateManager.enableBlend();
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);

        GlStateManager.bindTexture(entity.framebuffer.framebufferTexture);
        GlStateManager.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
        GlStateManager.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        
        GlStateManager.disableLighting();
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.enableRescaleNormal();



        //screen.drawTexturedModalRect(left, top, 0, 0, entity.getWidth() / 8, entity.getHeight() / 8);
        
        //1024 is the resolution
        //128 is the size
        //        screen.drawScaledCustomSizeModalRect(left, top, 0, 0, 1024, 1024, 128, 128, 1024, 1024);

        //16 blocks at 128 resolution at y = 69 for blocks at functionally y=69
        //32 blocks at 256 resolution at y = 69 for " "
        //Y level can be approximated as y = 154
        //Recommend y-level be set as a constant s/t this relationship holds relatively true
        //Fair conclusion, that resolution doubles
        //Thus, for the player at same level (roughly) of blocks, we can assume 
        //1024 -> 128 blocks ~ 8 chunks as long as player ~ same level as blocks
        //Therefore, on a 128x128 texture, each "pixel" can be approximated as one block
        screen.drawScaledCustomSizeModalRect(left, top, 0, 0, entity.getWidth(), entity.getHeight(), width, height, entity.getWidth(), entity.getHeight());
        
        GlStateManager.disableRescaleNormal();
        GlStateManager.disableBlend();
        GlStateManager.enableLighting();
        GlStateManager.popMatrix();
        
		//bufferbuilder.begin(7, DefaultVertexFormats.POSITION); //Has to be position
		
	}
	
	//TODO: Implement
	public void renderToTile() {} 
	
	private void alterSettings(EntityViewer view) {
        GameSettings settings = mc.gameSettings;
        //settings.fovSetting = something;
        
        //Making this 1 will force the camera to render
        settings.thirdPersonView = 0;
        
        settings.hideGUI = true;
        settings.mipmapLevels = 3;
        mc.displayHeight = view.getHeight();
        mc.displayWidth = view.getWidth();
	}
	
	@SubscribeEvent
	public void onRenderTick(TickEvent.RenderTickEvent event) {
		if(event.getPhase().equals(TickEvent.Phase.END)) {
			return;
		}
		
		if(!toRemove.isEmpty()) {
			for(int rem : toRemove) {
				GlStateManager.deleteTexture(rem);
			}
			toRemove.clear();
		}
		for(EntityViewer view : cameraMap.keySet()) {
			if(view != null && !view.isDead) {
				if(view.shouldRender()) {
					if(view.viewport == null || mc.player.canEntityBeSeen(view.viewport.getDetectionEntity())) {
						renderGlobalBackup = mc.renderGlobal;
						mc.renderGlobal = cameraGlobalRender;
						mc.world.removeEventListener(renderGlobalBackup);
						mc.world.addEventListener(cameraGlobalRender);
						
						view.framebuffer.bindFramebuffer(true);
						
						view.setupCamera();
						alterSettings(view);
						RenderUtils.isRenderingViewport = !RenderUtils.isRenderingViewport;
						RenderView.renderEntity = mc.player;

	                    int fps = Math.max(30, mc.gameSettings.limitFramerate);
	                    EntityRenderer entityRenderer = mc.entityRenderer;
	                    entityRenderer.renderWorld(event.renderTickTime, renderTime + (1000000000 / fps));

	                    GlStateManager.bindTexture(view.framebuffer.framebufferTexture);
						GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, 0, 0, view.getWidth(), view.getHeight(), 0);
						
						renderTime = System.nanoTime();
						
						RenderUtils.isRenderingViewport = !RenderUtils.isRenderingViewport;
						mc.renderGlobal = renderGlobalBackup;
						
						view.resetCamera();
						mc.world.removeEventListener(cameraGlobalRender);
						mc.world.addEventListener(renderGlobalBackup);

						RenderView.renderEntity = null;
						view.setShouldRender(false);

						view.framebuffer.unbindFramebuffer();
						view.framebuffer.framebufferRender(view.getWidth(), view.getHeight());
						mc.getFramebuffer().bindFramebuffer(true);
					}
				}
			} else {
				RenderView.removeCamera(view);
			}
		}
	}
	
    @SubscribeEvent
    public void onPrePlayerRender(RenderPlayerEvent.Pre event)
    {
        if(!RenderUtils.isRenderingViewport) return;

        if(event.getEntityPlayer() == renderEntity)
        {
            backupEntity = Minecraft.getMinecraft().getRenderManager().renderViewEntity;
            Minecraft.getMinecraft().getRenderManager().renderViewEntity = renderEntity;
        }
    }

    @SubscribeEvent
    public void onPostPlayerRender(RenderPlayerEvent.Post event)
    {
        if(!RenderUtils.isRenderingViewport) return;

        if(event.getEntityPlayer() == renderEntity)
        {
            Minecraft.getMinecraft().getRenderManager().renderViewEntity = backupEntity;
            renderEntity = null;
        }
    }

	
	
	//May do something nice with this
	public enum RenderType {
		GUI,
		TILE,
		ENTITY;
	}

}
