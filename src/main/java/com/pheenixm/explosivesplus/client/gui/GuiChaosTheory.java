package com.pheenixm.explosivesplus.client.gui;

import java.io.IOException;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageSeedUpdate;
import com.pheenixm.explosivesplus.common.tiles.TileEntityChaos;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;

public class GuiChaosTheory extends GuiScreen {

	private final TileEntityChaos chaos;
	public static final ResourceLocation TEXTURE = new ResourceLocation(ExplosivesPlus.MOD_ID, "textures/gui/map.png");
	
	private GuiTextField text;
	
	private String seed;
	private int guiLeft;
	private int guiTop;
	private int xSize = 176;
	private int ySize = 166;
	
	public GuiChaosTheory(TileEntityChaos tile) {
		super();
		chaos = tile;
	}

    @Override
    public void initGui() {
        super.initGui();
		text = new GuiTextField(0, fontRenderer, guiLeft + width/2 - 50, guiTop + height/2 - 12, 100, 24);
		seed = chaos.seed == null ? "" : chaos.seed;
		text.setText(seed);
    }
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
        this.guiLeft = (this.width - this.xSize) / 2;
        this.guiTop = (this.height - this.ySize) / 2;

		
		drawDefaultBackground();
		GlStateManager.pushMatrix();
		mc.getTextureManager().bindTexture(TEXTURE);
		ScaledResolution res = new ScaledResolution(mc);
		GlStateManager.color(1, 1, 1);
		this.drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		GlStateManager.popMatrix();
		
		text.drawTextBox();


	}
	
    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
    		super.mouseClicked(mouseX, mouseY, mouseButton);
    		text.mouseClicked(mouseX, mouseY, mouseButton);
    }

	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}
	
	@Override
    public void onGuiClosed() {
		
    }


    /** Call this method from you GuiScreen to process the keys into textbox. 
     * @throws IOException */
    @Override
    public void keyTyped(char typedChar, int keyCode) throws IOException 
    {
        super.keyTyped(typedChar, keyCode);
        text.textboxKeyTyped(typedChar, keyCode);		
        String oldSeed = seed;
        seed = text.getText();
        if(!oldSeed.equals(seed)) {
        		chaos.seed = seed;
        		PacketHandler.NETWORK.sendToServer(new MessageSeedUpdate(seed, chaos.getPos()));
        }
    }
    
    @Override
    public void updateScreen()
    {
        super.updateScreen();

        if (!text.isFocused())
        {
            this.text.setText(seed);
        }
    }


	
}
