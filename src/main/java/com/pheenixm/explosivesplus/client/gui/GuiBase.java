package com.pheenixm.explosivesplus.client.gui;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.gui.ContainerEPBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

//MINECRAFT WANTS THINGS AT 256X256 AND WILL SCALE DOWN TO ACCOMODATE
public abstract class GuiBase extends GuiContainer {

	private static final ResourceLocation TEXTURE_LOCATION = new ResourceLocation(ExplosivesPlus.MOD_ID, "textures/gui/base.png");
	public static final ResourceLocation COMPONENTS_LOCATION = new ResourceLocation(ExplosivesPlus.MOD_ID, "textures/gui/gui_components.png");
	
	public static final int SLOT_SPACING = 18;
	
	protected int mainX;
	protected int mainY; 
	
	protected float mousePosX;
	protected float mousePosY;

	
	public ContainerEPBase<TileEntityBase> container;
	
	public GuiBase(Container conta) {
		super(conta);
		if(conta instanceof ContainerEPBase<?>)	{
			container = (ContainerEPBase<TileEntityBase>) conta;
		}
		mainX = (this.width - this.xSize) / 2;
		mainY = (this.width - this.ySize) / 2;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks,
			int mouseX, int mouseY) {
        mc.renderEngine.bindTexture(getTexture());
        drawGuiBackground(partialTicks, mouseX, mouseY);
        
	}
	
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		drawGuiForeground(mouseX, mouseY);
	}
	
	public void drawSlot(int x, int y) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		Minecraft.getMinecraft().getTextureManager().bindTexture(COMPONENTS_LOCATION);
		this.drawTexturedModalRect(this.guiLeft + x, this.guiTop + y, 0, 0, 18, 18);
	}
	
	public void drawScaledTransparentSlot(int x, int y, float scale) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		Minecraft.getMinecraft().getTextureManager().bindTexture(COMPONENTS_LOCATION);
		int scaled = (int)(scale * 18);
		int offset = (scaled - 18)/2;
		Gui.drawScaledCustomSizeModalRect(this.guiLeft + x - offset, this.guiTop + y - offset, 0, 0, 18, 18, scaled, scaled, 256, 256);

	}
	
	public void drawScaledSlot(int x, int y, float scale) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		Minecraft.getMinecraft().getTextureManager().bindTexture(COMPONENTS_LOCATION);
		int scaled = (int)(scale * 18);
		int offset = (scaled - 18)/2;
		Gui.drawScaledCustomSizeModalRect(this.guiLeft + x - offset, this.guiTop + y - offset, 18, 0, 18, 18, scaled, scaled, 256, 256);

	}

	

	
	protected abstract void drawGuiBackground(float partialTicks, int mouseX, int mouseY);
	protected abstract void drawGuiForeground(int mouseX, int mouseY);
	protected abstract ResourceLocation getTexture();
	
    protected <G extends GuiButton> G addButton(G buttonIn)
    {
        this.buttonList.add(buttonIn);
        return buttonIn;
    }
    
    public void drawTooltip(int x, int y, String... toolTips)
    {
        if (toolTips != null)
        {
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            GL11.glDisable(GL11.GL_DEPTH_TEST);

            int var5 = 0;
            int var6;
            int var7;

            for (var6 = 0; var6 < toolTips.length; ++var6)
            {
                var7 = Minecraft.getMinecraft().fontRenderer.getStringWidth(toolTips[var6]);

                if (var7 > var5)
                {
                    var5 = var7;
                }
            }

            var6 = x + 12;
            var7 = y - 12;

            int var9 = 8;

            if (toolTips.length > 1)
            {
                var9 += 2 + (toolTips.length - 1) * 10;
            }

            if (this.guiTop + var7 + var9 + 6 > this.height)
            {
                var7 = this.height - var9 - this.guiTop - 6;
            }

            this.zLevel = 300;
            int var10 = -267386864;
            this.drawGradientRect(var6 - 3, var7 - 4, var6 + var5 + 3, var7 - 3, var10, var10);
            this.drawGradientRect(var6 - 3, var7 + var9 + 3, var6 + var5 + 3, var7 + var9 + 4, var10, var10);
            this.drawGradientRect(var6 - 3, var7 - 3, var6 + var5 + 3, var7 + var9 + 3, var10, var10);
            this.drawGradientRect(var6 - 4, var7 - 3, var6 - 3, var7 + var9 + 3, var10, var10);
            this.drawGradientRect(var6 + var5 + 3, var7 - 3, var6 + var5 + 4, var7 + var9 + 3, var10, var10);
            int var11 = 1347420415;
            int var12 = (var11 & 16711422) >> 1 | var11 & -16777216;
            this.drawGradientRect(var6 - 3, var7 - 3 + 1, var6 - 3 + 1, var7 + var9 + 3 - 1, var11, var12);
            this.drawGradientRect(var6 + var5 + 2, var7 - 3 + 1, var6 + var5 + 3, var7 + var9 + 3 - 1, var11, var12);
            this.drawGradientRect(var6 - 3, var7 - 3, var6 + var5 + 3, var7 - 3 + 1, var11, var11);
            this.drawGradientRect(var6 - 3, var7 + var9 + 2, var6 + var5 + 3, var7 + var9 + 3, var12, var12);

            for (int var13 = 0; var13 < toolTips.length; ++var13)
            {
                String var14 = toolTips[var13];

                Minecraft.getMinecraft().fontRenderer.drawStringWithShadow(var14, var6, var7, -1);
                var7 += 10;
            }

            this.zLevel = 0;

            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        }
    }
    
	
    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        this.drawDefaultBackground();
        this.mousePosX = (float)mouseX;
        this.mousePosY = (float)mouseY;
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }



}
