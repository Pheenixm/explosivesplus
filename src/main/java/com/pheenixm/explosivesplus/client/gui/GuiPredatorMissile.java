package com.pheenixm.explosivesplus.client.gui;

import java.awt.Color;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.client.ClientProxy;
import com.pheenixm.explosivesplus.common.entities.EntityTargeter;
import com.pheenixm.explosivesplus.common.gui.ContainerEPBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class GuiPredatorMissile extends GuiScreen {

	Minecraft mc;
	public static final ResourceLocation TEXTURE = new ResourceLocation(ExplosivesPlus.MOD_ID, "textures/gui/map.png");
	public static final ResourceLocation CROSSHAIR = new ResourceLocation(ExplosivesPlus.MOD_ID, "textures/gui/texture_sheet.png");

	private EntityTargeter target;
	
	public static final int TEXTURE_SIZE = 128;
	protected int leftBound;
	protected int rightBound;
	protected int topBound;
	protected int bottomBound;
	
	private int guiLeft;
	private int guiTop;
	private int xSize = 176;
	private int ySize = 166;
	
	protected int targetSpriteX;
	protected int targetSpriteY;
	protected boolean spriteBound;
	protected int mouseButton = -1;
	
	public GuiPredatorMissile(EntityTargeter view) {
		super();
		mc = Minecraft.getMinecraft();
		target = view;
        this.guiLeft = (this.width - this.xSize) / 2;
        this.guiTop = (this.height - this.ySize) / 2;
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		
		//This formula centers the texture
		leftBound = width/2 - TEXTURE_SIZE/2;
		rightBound = width/2 + TEXTURE_SIZE/2;
		topBound = height/2 - TEXTURE_SIZE/2;
		bottomBound = height/2 + TEXTURE_SIZE/2;

		
		GlStateManager.pushMatrix();
		mc.getTextureManager().bindTexture(TEXTURE);
		ScaledResolution res = new ScaledResolution(mc);
		GlStateManager.color(1, 1, 1);
		this.drawTexturedModalRect(width/2 - xSize/2, height/2 - ySize/2, 0, 0, xSize, ySize);

		//this.drawTexturedModalRect(width / 4, height / 4, 0, 0, width, height);
		
		
		//Draw the targeting viewpoint
		ClientProxy.viewpointHandler.renderToGUI(target, leftBound, topBound, TEXTURE_SIZE, TEXTURE_SIZE);
		
		//String tester
		//mc.fontRenderer.drawStringWithShadow(Integer.toString(target.getEntityId()), res.getScaledWidth() / 2 - mc.fontRenderer.getStringWidth(Integer.toString(target.getEntityId())) / 2, (res.getScaledHeight() / 2 - 166 / 2) + mc.fontRenderer.FONT_HEIGHT, Color.lightGray.getRGB());
		
		//Check mouse click state
		//This stuff for bombers and directionality
		/*
		switch(mouseButton) {
			case -1: {}
			case 0: {
				if(!spriteBound) {
					spriteBound = true;
				} else {
					//Get position, fire missile
					
				}
			}
			case 1: {
				if(spriteBound) {
					spriteBound = false;
				}
			}
		}*/
		
		mouseButton = getMouseState();
		if(mouseButton == 0) {
			//Get position, fire missile
			processLeftClick(mouseX, mouseY);
		}
		
		//Check the bounds of the mouse, render texture
		renderTargetingSprite(mouseX, mouseY);
		
		
		GlStateManager.popMatrix();
		
		//Draw buttons
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	protected void processLeftClick(int mouseX, int mouseY) {
		if(mouseX >= leftBound && mouseX <= rightBound 
				&& mouseY >= topBound && mouseY <= bottomBound) {
			//If in valid area, proceed
			int relX = mouseX - width/2;
			int relZ = mouseY - height/2;
			BlockPos interPos = target.getPosition().add(-relX, 0, relZ);
			int y = target.world.getHeight(interPos.getX(), interPos.getZ());
			BlockPos targetPos = new BlockPos(interPos.getX(), y, interPos.getZ());
			System.out.println(targetPos);
			//TODO: DO SOMETHING
			target.transmitFireControl(targetPos, new Vec3d(0,0,0));
			//close the gui
			mc.displayGuiScreen(null);
		}

	}
	
	protected void renderTargetingSprite(int mouseX, int mouseY) {
		if(mouseX < leftBound) {
			targetSpriteX = leftBound;
		} 
		if(mouseX >= leftBound && mouseX <= rightBound) {
			targetSpriteX = mouseX;
		}
		if(mouseX > rightBound) {
			targetSpriteX = rightBound;
		}
		if(mouseY < topBound) {
			targetSpriteY = topBound;
		}
		if(mouseY >= topBound && mouseY <= bottomBound) {
			targetSpriteY = mouseY;
		}
		if(mouseY > bottomBound) {
			targetSpriteY = bottomBound;
		}
		mc.getTextureManager().bindTexture(CROSSHAIR);
		GlStateManager.pushMatrix();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA.factor, GlStateManager.SourceFactor.ONE_MINUS_CONSTANT_ALPHA.factor);
		//this.drawTexturedModalRect(targetSpriteX - 16, targetSpriteY - 26, 0, 0, 64, 85);
		Gui.drawScaledCustomSizeModalRect(targetSpriteX - 8, targetSpriteY - 14, 0, 0, 64, 85, 16, 21, 512, 512);
		
		GlStateManager.popMatrix();
		//Gui.drawModalRectWithCustomSizedTexture(targetSpriteX - 9, targetSpriteY - 9, 0, 0, 18, 18, 4, 4);
		//Gui.drawScaledCustomSizeModalRect(targetSpriteX - 9/3, targetSpriteY - 9/3, 0, 0, 4, 4, 4, 4, 18*4, 18*4);
		//Don't know why this works, but it does
		//Gui.drawScaledCustomSizeModalRect(targetSpriteX - 9/3 + 1, targetSpriteY - 9/3 + 1, 0, 0, 3, 3, 3, 3, 18*3, 18*3);

	}
	
    protected int getMouseState() {
        
        if(Mouse.isButtonDown(1)) {
        		return 1;
        }
        if(Mouse.isButtonDown(0)) {
        		return 0;
        }
        //This statement checks if a button is being pressed
        //boolean isClickingButton = buttonList.get(0).isMouseOver() || buttonList.get(1).isMouseOver() || buttonList.get(2).isMouseOver();
        return -1;// && !isClickingButton;
    }


	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}

    /**
     * Called when the screen is unloaded. 
     * Set the targeter dead and transmit a kill code
     * with -1 as the y-coordinate
     */
	@Override
    public void onGuiClosed()
    {
    		if(!target.getShouldFire()) {
    			target.transmitFireControl(new BlockPos(0,-1,0), new Vec3d(0,0,0));
    			target.setDead();
    		}
    }


}
