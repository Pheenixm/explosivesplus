package com.pheenixm.explosivesplus.client.gui;

import org.lwjgl.opengl.GL11;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.gui.ContainerEPBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

public class GuiSilo extends GuiBase {

	private static final ResourceLocation TEXTURE_LOCATION = new ResourceLocation(ExplosivesPlus.MOD_ID, "textures/gui/silo.png");
	private ContainerEPBase<TileEntitySilo> container;
	
	public GuiSilo(Container conta) {
		super(conta);
		if(conta instanceof ContainerEPBase<?>)	{
			container = (ContainerEPBase<TileEntitySilo>) conta;
		}
		this.ySize = 221;
	}
	
	@Override
	protected void drawGuiBackground(float partialTicks, int mouseX,
			int mouseY) {
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);

	}

	@Override
	protected void drawGuiForeground(int mouseX, int mouseY) {
		String name = "Silo";
		fontRenderer.drawString(name, xSize / 2 - fontRenderer.getStringWidth(name) / 2, 6, 0x404040);
		fontRenderer.drawString(container.invPlayer.getDisplayName().getUnformattedText(), 8, ySize - 94, 0x404040);
		fontRenderer.drawString(String.valueOf(mouseX), xSize / 2 - fontRenderer.getStringWidth(name) / 2, 26, 0x404040);
		fontRenderer.drawString(String.valueOf(mouseY), xSize / 2 - fontRenderer.getStringWidth(name) / 2, 46, 0x404040);

		drawTooltip(xSize/2 + 50, ySize/2, "This is a test");
		
	}

	@Override
	protected ResourceLocation getTexture() {
		return TEXTURE_LOCATION;
	}

}
