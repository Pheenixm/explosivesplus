package com.pheenixm.explosivesplus.client.gui;

import javax.swing.colorchooser.ColorSelectionModel;

import org.lwjgl.opengl.GL11;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.gui.ContainerGenerator;
import com.pheenixm.explosivesplus.common.gui.ContainerEPBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntityGenerator;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.BossInfo.Color;

public class GuiGenerator extends GuiBase {

	private static final ResourceLocation TEXTURE_LOCATION = new ResourceLocation(ExplosivesPlus.MOD_ID, "textures/gui/template.png");
	private ContainerEPBase<TileEntityGenerator<?>> container;
	public static final int PROGRESS_BAR_SIZE = 160;
	public static final int PROGRESS_X = 8;
	public static final int PROGRESS_Y = 64;
	public static final int TEXTURE_X = 0;
	public static final int TEXTURE_Y = 166;
	
	public Slot input;
	
	public GuiGenerator(Container conta) {
		super(conta);
		if(conta instanceof ContainerEPBase<?>)	{
			container = (ContainerEPBase<TileEntityGenerator<?>>) conta;
			if(conta instanceof ContainerGenerator) {
				ContainerGenerator centi = (ContainerGenerator)container;
				input = centi.getSlot(centi.inputSlot);
			}
		}
	}
	
	@Override
	protected void drawGuiBackground(float partialTicks, int mouseX,
			int mouseY) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		//drawTooltip(xSize/2, ySize/2 + 5, Double.toString(progress) + "%");
		//int draw = container.tile.isProcessing() ? (int)(((double) container.tile.getCurrentRecipeTicksElapsed() / (double) container.tile.getCurrentRecipeTicks()) * PROGRESS_BAR_SIZE) : 0;
        //this.drawTexturedModalRect(guiLeft + PROGRESS_X, this.guiTop + PROGRESS_Y, TEXTURE_X, TEXTURE_Y, draw, PROGRESS_X);
        
		drawScaledSlot(input.xPos - 1, input.yPos - 1, 1.5F);
	}

	@Override
	protected void drawGuiForeground(int mouseX, int mouseY) {
		String name = "Generator";
		String energy = "FE: " + Integer.toString(container.tile.getEnergy().getEnergyStored());
		fontRenderer.drawStringWithShadow(name, xSize / 2 - fontRenderer.getStringWidth(name) / 2, 6, -1);
		//fontRenderer.drawString(container.invPlayer.getDisplayName().getUnformattedText(), 8, ySize - 94, 0x404040);
		fontRenderer.drawStringWithShadow(energy, xSize / 2  - 82, 16, -1);

		//drawTooltip(xSize/2, ySize/2 - 50, Integer.toString(container.tile.getEnergy().getEnergyStored()));

		mc.renderEngine.bindTexture(getTexture());

        
        
        //this.drawTexturedModalRect(8, this.guiTop + 27, 175, 0, draw, 8);
		//Gui.drawModalRectWithCustomSizedTexture(8, this.guiTop + 27, 175, 0, draw, 8, draw * 175, 8*8);
	}


	@Override
	protected ResourceLocation getTexture() {
		return TEXTURE_LOCATION;
	}


}
