package com.pheenixm.explosivesplus.client.render.entity;

import org.lwjgl.util.glu.Sphere;

import com.pheenixm.explosivesplus.common.entities.EntityFusedBomb;
import com.pheenixm.explosivesplus.common.entities.EntityShockwave;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class RenderShockwave extends Render<EntityShockwave> {

	public RenderShockwave(RenderManager renderManagerIn) {
        super(renderManagerIn);
        this.shadowSize = 0.0F;
	}
	
    public void doRender(EntityShockwave entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    		super.doRender(entity, x, y, z, entityYaw, partialTicks);
        float radius = entity.prevRadius + (entity.radius - entity.prevRadius) * partialTicks;
        
        GlStateManager.pushMatrix();
        
        GlStateManager.translate((float)x + 0.5F, (float)y + 0.5F, (float)z + 0.5F);
        
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.DST_ALPHA);
        GlStateManager.depthMask(false);

        
        GlStateManager.color(1.0F, 1.0F, 1.0F, 0.2F);
		Sphere s = new Sphere();
		s.setDrawStyle(org.lwjgl.util.glu.GLU.GLU_FILL);
		s.draw(radius, 64, 64);
		
        GlStateManager.color(0.0F, 0.0F, 0.0F, 0.2F);
		Sphere s1 = new Sphere();
		s1.setDrawStyle(org.lwjgl.util.glu.GLU.GLU_FILL);
		s1.draw(radius - 0.5F, 64, 64);


		
		GlStateManager.disableBlend();

        GlStateManager.popMatrix();
    }

    
	
	@Override
	protected ResourceLocation getEntityTexture(EntityShockwave entity) {
		return TextureMap.LOCATION_BLOCKS_TEXTURE;
	}

}
