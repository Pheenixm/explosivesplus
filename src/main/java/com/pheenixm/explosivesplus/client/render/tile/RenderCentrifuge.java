package com.pheenixm.explosivesplus.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;

public class RenderCentrifuge extends RenderMultiblock {

	long angle;
	
	public RenderCentrifuge() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void renderExtraComponent(TileEntityMultiblock t) {
		double x = pos.getX();
		double y = pos.getY();
		double z = pos.getZ();
		
		TileEntityCentrifuge c = null;
		if(t instanceof TileEntityCentrifuge) {
			c = (TileEntityCentrifuge)t;
		}

		state = state.withProperty(MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.DYNAMIC_RENDER);
		GlStateManager.pushMatrix();

		this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		RenderHelper.disableStandardItemLighting();
		GlStateManager.blendFunc(770, 771);
		GlStateManager.enableBlend();
		GlStateManager.disableCull();
		if (Minecraft.isAmbientOcclusionEnabled())
			GlStateManager.shadeModel(7425);
		else
			GlStateManager.shadeModel(7424);
		
		GlStateManager.translate(-getXOffset(t), 0, -getZOffset(t));
		GlStateManager.rotate(c.angle, 0, 1, 0);
		// Translate back to local view coordinates so that we can do the actual
		// rendering here
		GlStateManager.translate(-x , -y, -z );
        
		
		Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldrenderer = tessellator.getBuffer();

		tessellator.getBuffer().begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK);
		Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelRenderer().renderModel(world,
				getBlockBakedModel(state), state, pos, worldrenderer, true);
        worldrenderer.setTranslation(0, 0, 0);
		tessellator.draw();
		

		RenderHelper.enableStandardItemLighting();

		GlStateManager.disableBlend();
		GlStateManager.popMatrix();

	}

	@Override
	public double getXOffset(TileEntityMultiblock tile) {
		// TODO Auto-generated method stub
		return -0.5;
	}

	@Override
	public double getZOffset(TileEntityMultiblock tile) {
		// TODO Auto-generated method stub
		return -0.5;
	}

}
