package com.pheenixm.explosivesplus.client.render.entity;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Sphere;

import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTheoryOfChaosController;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityFallingBlocks;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import net.minecraft.block.BlockLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.culling.ICamera;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderChaos extends Render<EntityTheoryOfChaosController> {

	private Tessellator tessellator;
	private BufferBuilder bufferbuilder;

	public RenderChaos(RenderManager renderManagerIn) {
        super(renderManagerIn);
        this.shadowSize = 0.5F;
	}
	
    public void doRender(EntityTheoryOfChaosController entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	/*
		controller = entity;
        GL11.glPushMatrix();
        
        tessellator = Tessellator.getInstance();
        bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.BLOCK);

        GlStateManager.disableLighting();
        if (this.renderOutlines)
        {
            GlStateManager.enableColorMaterial();
        }
		for(FauxServerEntityFallingBlocks block : entity.fallingBlocks.values()) {
			float diffX = (float) (block.posX - entity.posX);
			float diffY = (float) (block.posY - entity.posY);
			float diffZ = (float) (block.posZ - entity.posZ);
			
			float blockX = (float) x + diffX;
			float blockY = (float) y + diffY;
			float blockZ = (float) z + diffZ;
			if(block.block.getBlock() instanceof BlockLiquid) {
				renderFluid(blockX, blockY, blockZ, block);
			} else {
				renderBlock(blockX, blockY, blockZ, block);
			}
		}
		bufferbuilder.setTranslation(0, 0, 0);

		//GlStateManager.translate(x, y, z);
        tessellator.draw();
        
        GlStateManager.enableLighting();
        GL11.glPopMatrix();

		super.doRender(entity, x, y, z, entityYaw, partialTicks);
    	//BREAK
		controller = entity;
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, z);
		RenderUtils.renderBlackhole(6);
		GlStateManager.popMatrix();
		
        GL11.glPushMatrix();
		for(FauxServerEntityFallingBlocks block : entity.fallingBlocks.values()) {
			float diffX = (float) (block.posX - entity.posX);
			float diffY = (float) (block.posY - entity.posY);
			float diffZ = (float) (block.posZ - entity.posZ);
			
			float blockX = (float) x + diffX;
			float blockY = (float) y + diffY;
			float blockZ = (float) z + diffZ;
			if(block.block.getBlock() instanceof BlockLiquid) {
				renderFluid(blockX, blockY, blockZ, block);
			} else {
				renderBlock(blockX, blockY, blockZ, block);
			}


		}
        GL11.glPopMatrix();

		super.doRender(entity, x, y, z, entityYaw, partialTicks);*/
    }

    private void renderFluid(float x, float y, float z,
			FauxEntityFallingBlocks block) {
        final IBlockState blockState = block.block;
        final BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();

        GlStateManager.pushMatrix();
        GlStateManager.translate((float)  - 0.5, (float) y, (float) z - 0.5);
        this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);

        blockrendererdispatcher.renderBlockBrightness(blockState, blockState.getAmbientOcclusionLightValue());

        GlStateManager.popMatrix();
		
	}
    
	public void renderBlock(double x, double y, double z, FauxEntityFallingBlocks entity) {
        final IBlockState blockState = entity.block;
        World world = controller.getEntityWorld();
        if (blockState != world.getBlockState(new BlockPos(controller)) && blockState.getRenderType() != EnumBlockRenderType.INVISIBLE)
        {
            this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);


            BlockPos blockpos = new BlockPos(0,128,0);
            bufferbuilder.setTranslation(x, y-128, z);
            BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();
            blockrendererdispatcher.getBlockModelRenderer().renderModelFlat(world, blockrendererdispatcher.getModelForState(blockState), blockState, blockpos, bufferbuilder, false, MathHelper.getPositionRandom(new Vec3i(controller.posX, controller.posY, controller.posZ)));
            if (this.renderOutlines)
            {
                GlStateManager.disableOutlineMode();
                GlStateManager.disableColorMaterial();
            }
        }
        
    }

    /*
	public void renderBlock(double x, double y, double z, FauxServerEntityFallingBlocks entity) {
        final IBlockState blockState = entity.block;
        World world = controller.getEntityWorld();
        if (blockState != world.getBlockState(new BlockPos(controller)) && blockState.getRenderType() != EnumBlockRenderType.INVISIBLE)
        {
            this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
            GlStateManager.pushMatrix();
            GlStateManager.disableLighting();
            GlStateManager.enableRescaleNormal();
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder bufferbuilder = tessellator.getBuffer();
            if (this.renderOutlines)
            {
                GlStateManager.enableColorMaterial();
            }

            bufferbuilder.begin(7, DefaultVertexFormats.BLOCK);
            BlockPos blockpos = new BlockPos(0,128,0);
            GlStateManager.translate((float) x - 0.5, (float) y - 128, (float) z - 0.5);
            GlStateManager.scale(entity.scale, entity.scale, entity.scale);	

            BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();
            blockrendererdispatcher.getBlockModelRenderer().renderModelFlat(world, blockrendererdispatcher.getModelForState(blockState), blockState, blockpos, bufferbuilder, false, MathHelper.getPositionRandom(new Vec3i(controller.posX, controller.posY, controller.posZ)));
            tessellator.draw();

            if (this.renderOutlines)
            {
                GlStateManager.disableOutlineMode();
                GlStateManager.disableColorMaterial();
            }

            GlStateManager.disableRescaleNormal();
            GlStateManager.enableLighting();
            GlStateManager.popMatrix();
        }
        
    }*/
    
	public boolean shouldRender(EntityTheoryOfChaosController livingEntity, ICamera camera, double camX, double camY, double camZ) {
		return true;
	}

    

	private EntityTheoryOfChaosController controller;
    
	@Override
	protected ResourceLocation getEntityTexture(EntityTheoryOfChaosController entity) {
		return TextureMap.LOCATION_BLOCKS_TEXTURE;
	}

}
