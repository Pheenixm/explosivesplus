package com.pheenixm.explosivesplus.client.render.entity;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.client.render.models.ModelPredatorMissile;
import com.pheenixm.explosivesplus.client.render.models.ModelStealthBomber;
import com.pheenixm.explosivesplus.common.entities.EntityFusedBomb;
import com.pheenixm.explosivesplus.common.entities.EntityStealthBomber;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityPredatorMissile;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTheoryOfChaosController;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.culling.ICamera;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderStealthBomber extends Render<EntityStealthBomber> {

	private final ModelStealthBomber model = new ModelStealthBomber(); 
    private final ResourceLocation TEXTURE = new ResourceLocation(ExplosivesPlus.MOD_ID,"textures/entity/stealth.png");

	public RenderStealthBomber(RenderManager renderManagerIn) {
        super(renderManagerIn);
        this.shadowSize = 10.0F;
	}
	
    public void doRender(EntityStealthBomber entity, double x, double y, double z, float entityYaw, float partialTicks)
    {

        GlStateManager.pushMatrix();
        GlStateManager.translate((float)x, (float)y, (float)z);
        Minecraft.getMinecraft().getTextureManager().bindTexture(TEXTURE);
        GlStateManager.rotate(entity.rotationYaw - 90, 0, 1, 0);
        GlStateManager.scale(0.25, 0.25, 0.25);
        model.setRotationAngles(0, 0, 0, entity.rotationYaw, 0, 0.25F, entity);
    		model.render(entity, 0, 0, 0, 0, 0, 1);
    		GlStateManager.popMatrix();
    		
    		super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }
    		
	public boolean shouldRender(EntityTheoryOfChaosController livingEntity, ICamera camera, double camX, double camY, double camZ) {
		return true;
	}



	@Override
	protected ResourceLocation getEntityTexture(EntityStealthBomber entity) {
		return TEXTURE;
	}

}