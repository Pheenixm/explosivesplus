package com.pheenixm.explosivesplus.client.render.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelStealthBomber extends ModelBase
{
  //fields
    ModelRenderer Wing1;
    ModelRenderer Wing2;
    ModelRenderer Shape1;
    ModelRenderer Shape2;
  
  public ModelStealthBomber()
  {
    textureWidth = 512;
    textureHeight = 512;
    
      Wing1 = new ModelRenderer(this, 0, 0);
      Wing1.addBox(-64F, -2F, 0F, 64, 4, 192);
      Wing1.setRotationPoint(0F, 0F, 0F);
      Wing1.setTextureSize(512, 512);
      Wing1.mirror = true;
      setRotation(Wing1, 0F, 0.7853982F, 0F);
      Wing2 = new ModelRenderer(this, 0, 0);
      Wing2.addBox(0F, -2F, 0F, 64, 4, 192);
      Wing2.setRotationPoint(0F, 0F, 0F);
      Wing2.setTextureSize(512, 512);
      Wing2.mirror = true;
      setRotation(Wing2, 0F, -0.8028515F, 0F);
      Shape1 = new ModelRenderer(this, 0, 256);
      Shape1.addBox(-4F, -2F, -4F, 64, 4, 64);
      Shape1.setRotationPoint(0F, 0F, 90F);
      Shape1.setTextureSize(512, 512);
      Shape1.mirror = true;
      setRotation(Shape1, 0F, -0.7853982F, 0F);
      Shape2 = new ModelRenderer(this, 256, 400);
      Shape2.addBox(-16F, -4F, -32F, 32, 8, 64);
      Shape2.setRotationPoint(0F, -3F, 64F);
      Shape2.setTextureSize(512, 512);
      Shape2.mirror = true;
      setRotation(Shape2, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Wing1.render(f5);
    Wing2.render(f5);
    Shape1.render(f5);
    Shape2.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}

}
