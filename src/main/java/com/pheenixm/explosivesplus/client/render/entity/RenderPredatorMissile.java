package com.pheenixm.explosivesplus.client.render.entity;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.client.render.models.ModelPredatorMissile;
import com.pheenixm.explosivesplus.common.entities.EntityFusedBomb;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityPredatorMissile;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) braces deadcode 

public class RenderPredatorMissile extends Render<EntityPredatorMissile> {

	private final ModelPredatorMissile model = new ModelPredatorMissile(); 
    private final ResourceLocation TEXTURE = new ResourceLocation(ExplosivesPlus.MOD_ID,"textures/entity/missile.png");

	public RenderPredatorMissile(RenderManager renderManagerIn) {
        super(renderManagerIn);
        this.shadowSize = 0.5F;
	}
	
    public void doRender(EntityPredatorMissile entity, double x, double y, double z, float entityYaw, float partialTicks)
    {

        GlStateManager.pushMatrix();
        GlStateManager.translate((float)x, (float)y, (float)z);
        Minecraft.getMinecraft().getTextureManager().bindTexture(TEXTURE);
        GlStateManager.rotate(180, 0, 0, 1);
        GlStateManager.scale(0.25, 0.25, 0.25);
    		model.render(entity, 0, 0, 0, 0, 0, 1);
    		GlStateManager.popMatrix();
    		
    		super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }
    		



	@Override
	protected ResourceLocation getEntityTexture(EntityPredatorMissile entity) {
		return TEXTURE;
	}

}