package com.pheenixm.explosivesplus.client.render.entity;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Sphere;

import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityFallingBlocks;

import net.minecraft.block.BlockLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.culling.ICamera;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderNova extends Render<EntityNovaController> {

	private Tessellator tessellator;
	private BufferBuilder bufferbuilder;

	
	public RenderNova(RenderManager renderManagerIn) {
        super(renderManagerIn);
        this.shadowSize = 0.5F;
	}
	
	/*
	public void batchRender(World world) {
		
		double d0 = entityIn.lastTickPosX + (entityIn.posX - entityIn.lastTickPosX) * (double)partialTicks;
        double d1 = entityIn.lastTickPosY + (entityIn.posY - entityIn.lastTickPosY) * (double)partialTicks;
        double d2 = entityIn.lastTickPosZ + (entityIn.posZ - entityIn.lastTickPosZ) * (double)partialTicks;
        float f = entityIn.prevRotationYaw + (entityIn.rotationYaw - entityIn.prevRotationYaw) * partialTicks;
        this.renderEntity(entityIn, d0 - this.renderPosX, d1 - this.renderPosY, d2 - this.renderPosZ, f, partialTicks, p_188388_3_);

	}*/
	
    public void doRender(EntityNovaController entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	/*
//		GlStateManager.depthMask(true);
		controller = entity;
        GL11.glPushMatrix();
        
        tessellator = Tessellator.getInstance();
        bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.BLOCK);

        GlStateManager.disableLighting();
        if (this.renderOutlines)
        {
            GlStateManager.enableColorMaterial();
        }
		for(FauxServerEntityFallingBlocks block : entity.fallingBlocks.values()) {
			float diffX = (float) (block.posX - entity.posX);
			float diffY = (float) (block.posY - entity.posY);
			float diffZ = (float) (block.posZ - entity.posZ);
			
			float blockX = (float) x + diffX;
			float blockY = (float) y + diffY;
			float blockZ = (float) z + diffZ;
			if(block.block.getBlock() instanceof BlockLiquid) {
				renderFluid(blockX, blockY, blockZ, block);
			} else {
				renderBlock(blockX, blockY, blockZ, block);
			}
		}
		bufferbuilder.setTranslation(0, 0, 0);

		//GlStateManager.translate(x, y, z);
        tessellator.draw();
        
        GlStateManager.enableLighting();
        GL11.glPopMatrix();

		super.doRender(entity, x, y, z, entityYaw, partialTicks);*/
    }

    private void renderFluid(float x, float y, float z,
			FauxEntityFallingBlocks block) {
        final IBlockState blockState = block.block;
        final BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();

        GlStateManager.pushMatrix();
        GlStateManager.translate((float)  - 0.5, (float) y, (float) z - 0.5);
        this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);

        blockrendererdispatcher.renderBlockBrightness(blockState, blockState.getAmbientOcclusionLightValue());

        GlStateManager.popMatrix();
		
	}

	public void renderBlock(double x, double y, double z, FauxEntityFallingBlocks entity) {
        final IBlockState blockState = entity.block;
        World world = controller.getEntityWorld();
        if (blockState != world.getBlockState(new BlockPos(controller)) && blockState.getRenderType() != EnumBlockRenderType.INVISIBLE)
        {
            this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);


            BlockPos blockpos = new BlockPos(0,64,0);
            bufferbuilder.setTranslation(x, y-64, z);
            BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();
            blockrendererdispatcher.getBlockModelRenderer().renderModelFlat(world, blockrendererdispatcher.getModelForState(blockState), blockState, blockpos, bufferbuilder, false, MathHelper.getPositionRandom(new Vec3i(controller.posX, controller.posY, controller.posZ)));
            if (this.renderOutlines)
            {
                GlStateManager.disableOutlineMode();
                GlStateManager.disableColorMaterial();
            }
        }
        
    }
    
	@Override
    public boolean shouldRender(EntityNovaController livingEntity, ICamera camera, double camX, double camY, double camZ)
    {
		return true;
    }
    

	private EntityNovaController controller;
    
	@Override
	protected ResourceLocation getEntityTexture(EntityNovaController entity) {
		return TextureMap.LOCATION_BLOCKS_TEXTURE;
	}

}
