package com.pheenixm.explosivesplus.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.generator.Tier1Generator;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class RenderGeneratorTier1 extends RenderMultiblock {

	long angle;
	int ticks;
	Vec3d vec;
	
	public RenderGeneratorTier1() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void renderExtraComponent(TileEntityMultiblock t) {
		double x = pos.getX();
		double y = pos.getY();
		double z = pos.getZ();
		
		Tier1Generator c = null;
		if(t instanceof Tier1Generator) {
			c = (Tier1Generator)t;
		}
		if(c == null) {
			return;
		}
		int half = c.getProcessingTime()/2;

		state = state.withProperty(MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.DYNAMIC_RENDER);
		
		
		
		if(ticks % c.getProcessingTime() == 0 && ticks > 0) {
			ticks = 0;
			vec = c.getItemPos().scale(-1);
		} else {
			//TODO: Normalize ticks for FPS
				ticks++;
		}
		
		if(ticks <= half) {
			ItemStack stack = c.getFuel();
			if(!stack.isEmpty() && vec != null) {
				float scale = (float)ticks/half;
				Vec3d pos2 = vec.scale(scale);   
				
				RenderHelper.enableStandardItemLighting();
	            GlStateManager.enableLighting();
	            GlStateManager.pushMatrix();
	            // Translate to the center of the block and .9 points higher
	            GlStateManager.translate(pos2.x + 0.5 - vec.x, pos2.y - vec.y + 0.5,pos2.z + 0.5 -vec.z);
	            //GlStateManager.scale(.4f, .4f, .4f);
	
	            Minecraft.getMinecraft().getRenderItem().renderItem(stack, ItemCameraTransforms.TransformType.NONE);
	
	            GlStateManager.popMatrix();
			}
		}
		
		//GlStateManager.pushMatrix();

		/*
		this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		RenderHelper.disableStandardItemLighting();
		GlStateManager.blendFunc(770, 771);
		GlStateManager.enableBlend();
		GlStateManager.disableCull();
		if (Minecraft.isAmbientOcclusionEnabled())
			GlStateManager.shadeModel(7425);
		else
			GlStateManager.shadeModel(7424);
		
		GlStateManager.translate(-getXOffset(), 0, -getZOffset());
		GlStateManager.rotate(c.angle, 0, 1, 0);
		// Translate back to local view coordinates so that we can do the actual
		// rendering here
		GlStateManager.translate(-x , -y, -z );
        
		
		Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldrenderer = tessellator.getBuffer();

		tessellator.getBuffer().begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK);
		Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelRenderer().renderModel(world,
				getBlockBakedModel(state), state, pos, worldrenderer, true);
        worldrenderer.setTranslation(0, 0, 0);
		tessellator.draw();
		

		RenderHelper.enableStandardItemLighting();

		GlStateManager.disableBlend();
		GlStateManager.popMatrix();
		*/
	}

	@Override
	public double getXOffset(TileEntityMultiblock tile) {
		switch(tile.facing) {
			case SOUTH:
				return 0.5;
			case EAST: 
				return 0.5;
			case NORTH:
				return -0.5;
			case WEST:
				return -0.5;
		    default:
		    		return 0.5;
		}
	}

	@Override
	public double getZOffset(TileEntityMultiblock tile) {
		switch(tile.facing) {
			case SOUTH:
				return -0.5;
			case EAST: 
				return 0.5;
			case NORTH:
				return 0.5;
			case WEST:
				return -0.5;
		    default:
		    		return 0.5;
		}
	}

}
