package com.pheenixm.explosivesplus.client.render.tile;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Sphere;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.client.ModelRegistry;
import com.pheenixm.explosivesplus.client.gui.GuiStealthBomber;
import com.pheenixm.explosivesplus.client.render.models.ModelPredatorLaptop;
import com.pheenixm.explosivesplus.common.entities.missiles.EntityBallisticMissile;
import com.pheenixm.explosivesplus.common.items.StealthBomber;
import com.pheenixm.explosivesplus.common.util.ModResourceLocation;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntityItemStackRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class RenderItemStealth extends TileEntityItemStackRenderer {

	public static final float RADIUS = 0.05F;
	public static final ModelPredatorLaptop laptop = new ModelPredatorLaptop();
	public static final ModelPredatorLaptop laptopFirstPerson = new ModelPredatorLaptop();

    private final ResourceLocation TEXTURE = new ResourceLocation(ExplosivesPlus.MOD_ID, "textures/items/stealth_laptop.png");
	private Minecraft mc = Minecraft.getMinecraft();
    
	@Override
	public void renderByItem(ItemStack stack, float partialTicks) {
		if(stack.isEmpty()) {
			System.out.println("Serious problem");
			return;
		}
		
		EntityPlayer player = Minecraft.getMinecraft().player;
		

		Item item = stack.getItem();
		if(item instanceof StealthBomber && player != null) {
			StealthBomber missile = (StealthBomber)item;
			renderMissile(stack, missile, player);
		}
		
		super.renderByItem(stack, partialTicks);
	}

	
	
	private void renderMissile(ItemStack stack, StealthBomber missile, EntityPlayer player) {
		float useTime = player.isHandActive() ? (StealthBomber.ANIMATION_TIME - player.getItemInUseCount()) : 0;
		useTime /= StealthBomber.ANIMATION_TIME;
		float angle = useTime * 90F;
		angle *= 0.0174533F;
		
        GlStateManager.pushMatrix();
        GlStateManager.translate(0.5, 0.5, 0.5);
        GlStateManager.rotate(180, 1, 0, 0);
        if(stack.isOnItemFrame()) {
        	 	GlStateManager.rotate(180, 0, 1, 0);
        }

        Minecraft.getMinecraft().getTextureManager().bindTexture(TEXTURE);
        
        
        if(player.getHeldItemMainhand() == stack || player.getHeldItemOffhand() == stack) {
	    		if(mc.currentScreen instanceof GuiStealthBomber) {
	    			laptop.screen.rotateAngleX = -90F;
	    		} else {
	    			//We want an exact address in memory
	            if(player.getActiveItemStack() == stack) {
	            		//TODO: Expand on this animation
	            		laptop.screen.rotateAngleX = -angle;
	            		//TODO: Make this more generic, for all targeting screens
	            } else {
	            		laptop.screen.rotateAngleX = 0;
	            }
	    		}
        } else {
	    		GlStateManager.rotate(90, 1, 0, 0);
	    		GlStateManager.rotate(180, 0, 1, 0);
	    		GlStateManager.translate(0, 0, -0.25);
        }
                
        laptop.render(null, 0, 0, 0, 0, 0, 0.02F);
    		GlStateManager.popMatrix();
	}

}
