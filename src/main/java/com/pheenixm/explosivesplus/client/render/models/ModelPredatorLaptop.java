package com.pheenixm.explosivesplus.client.render.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * PredatorMissileLaptop - Pheenixm
 * Created using Tabula 7.0.0
 */
public class ModelPredatorLaptop extends ModelBase {
    public ModelRenderer keyboard;
    public ModelRenderer armrest;
    public ModelRenderer peg1;
    public ModelRenderer peg2;
    public ModelRenderer shape5;
    public ModelRenderer screen;

    public ModelPredatorLaptop() {
        this.textureWidth = 512;
        this.textureHeight = 512;
        this.armrest = new ModelRenderer(this, 112, 0);
        this.armrest.mirror = true;
        this.armrest.setRotationPoint(12.0F, -1.0F, 0.0F);
        this.armrest.addBox(0.0F, 0.0F, 0.0F, 8, 4, 24, 0.0F);
        this.peg1 = new ModelRenderer(this, 0, 0);
        this.peg1.setRotationPoint(-3.0F, 1.5F, -4.0F);
        this.peg1.addBox(0.0F, 0.0F, 0.0F, 1, 1, 4, 0.0F);
        this.shape5 = new ModelRenderer(this, 0, 0);
        this.shape5.setRotationPoint(-2.0F, 1.5F, -4.0F);
        this.shape5.addBox(0.0F, 0.0F, 0.0F, 5, 1, 2, 0.0F);
        this.keyboard = new ModelRenderer(this, 0, 0);
        this.keyboard.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.keyboard.addBox(-20.0F, 0.0F, 0.0F, 32, 3, 24, 0.0F);
        this.screen = new ModelRenderer(this, 176, 0);
        this.screen.setRotationPoint(0.0F, 0.0F, 24.0F);
        this.screen.addBox(-20.0F, -1.0F, -24.0F, 32, 1, 24, 0.0F);
        this.peg2 = new ModelRenderer(this, 10, 0);
        this.peg2.setRotationPoint(3.0F, 1.5F, -4.0F);
        this.peg2.addBox(0.0F, 0.0F, 0.0F, 1, 1, 4, 0.0F);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) { 
        this.armrest.render(f5);
        this.peg1.render(f5);
        this.shape5.render(f5);
        this.keyboard.render(f5);
        this.screen.render(f5);
        this.peg2.render(f5);
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
