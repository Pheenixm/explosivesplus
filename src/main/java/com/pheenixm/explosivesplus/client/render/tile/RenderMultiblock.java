package com.pheenixm.explosivesplus.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.client.ModelRegistry;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.util.ModResourceLocation;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.model.IModel;

public abstract class RenderMultiblock extends TileEntitySpecialRenderer<TileEntityMultiblock> {

	public RenderMultiblock() {
		super();
	}
	

	private IBakedModel bakedModel;
	protected static BlockRendererDispatcher blockRenderer;
	BlockPos pos;
	World world;
	IBlockState state;

	protected IBakedModel getBlockBakedModel(IBlockState state) {
		if (blockRenderer == null)
			blockRenderer = Minecraft.getMinecraft().getBlockRendererDispatcher();
		bakedModel = blockRenderer.getBlockModelShapes().getModelForState(state);
		return bakedModel;
	}

	protected IBakedModel getItemBakedModel(ItemStack stack) {

		String path = getPath(stack);
		ModResourceLocation loc = new ModResourceLocation(ExplosivesPlus.MOD_ID, path);
		IBakedModel bakedModel = ModelRegistry.INSTANCE.getBakedModel(loc);
		return bakedModel;

	}

	protected String getPath(ItemStack stack) {
		String registry = stack.getItem().getRegistryName().toString();
		String[] splitter = registry.split(":");
		return "item/" + splitter[splitter.length - 1] + ".obj";
	}

	@Override
	public void render(TileEntityMultiblock tile, double x, double y, double z, float partialTicks, int destroyStage,
			float alpha) {
		if (!tile.validMultiblock || !tile.isTileLoaded()) {
			return;
		}

		world = tile.getWorld();
		pos = tile.getPos();

		if (world == null || pos == null || world.getBlockState(pos) == null) {
			return;
		}

		state = world.getBlockState(pos);

		if(!state.getPropertyKeys().contains(MultiblockComponent.MULTI_BLOCK))
			return;
		
		if (state.getValue(MultiblockComponent.MULTI_BLOCK) != MultiTypeEnum.CENTER) {
			// Stops the block from rendering, unless it is the center
			return;
		}

		GlStateManager.pushMatrix();

		// Translate to the location of our tile entity
		GlStateManager.translate(x, y, z);
		// GlStateManager.disableRescaleNormal();
		// OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit,
		// 240f, 240f);

		renderTile(tile);
		// handle.render(0xFFFFFFFF);
		renderExtraComponent(tile);

		GlStateManager.popMatrix();

	}

	public abstract void renderExtraComponent(TileEntityMultiblock tile);
	public abstract double getXOffset(TileEntityMultiblock tile);
	public abstract double getZOffset(TileEntityMultiblock tile);

	public void renderTile(TileEntityMultiblock tile) {

		double x = pos.getX() + getXOffset(tile);
		double y = pos.getY();
		double z = pos.getZ() + getZOffset(tile);

		state = state.withProperty(MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.RENDER);
		GlStateManager.pushMatrix();

		this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		RenderHelper.disableStandardItemLighting();
		GlStateManager.blendFunc(770, 771);
		GlStateManager.enableBlend();
		GlStateManager.disableCull();
		if (Minecraft.isAmbientOcclusionEnabled())
			GlStateManager.shadeModel(7425);
		else
			GlStateManager.shadeModel(7424);

		// Translate back to local view coordinates so that we can do the actual
		// rendering here
		GlStateManager.translate(0, 0, 0);
		
		
		if (tile.facing != null && tile.facing != EnumFacing.UP) {
			float angle = tile.facing == EnumFacing.NORTH || tile.facing == EnumFacing.SOUTH ? tile.facing.getHorizontalAngle() + 90 : tile.facing.getHorizontalAngle() - 90;
			GlStateManager.rotate(angle, 0.0F, 1.0F, 0.0F);
		}


		GlStateManager.translate(-x, -y, -z);


		Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldrenderer = tessellator.getBuffer();

		tessellator.getBuffer().begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK);
		Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelRenderer().renderModel(world,
				getBlockBakedModel(state), state, pos, worldrenderer, true);
        worldrenderer.setTranslation(0, 0, 0);
		tessellator.draw();
		

		RenderHelper.enableStandardItemLighting();

		GlStateManager.disableBlend();
		GlStateManager.popMatrix();

	}

	/**
	 * TODO: Is this necessary?
	 */
	@Override
	public boolean isGlobalRenderer(TileEntityMultiblock tile) {
		return true;
	}

	public void renderItemStack(ItemStack stack, double x, double y, double z, float pitch, float yaw, float roll) {
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();

		bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);

		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder worldrenderer = tessellator.getBuffer();

		worldrenderer.begin(GL11.GL_QUADS, DefaultVertexFormats.ITEM);
		worldrenderer.setTranslation(-x, -y, -z);
		worldrenderer.color(255, 255, 255, 255);
		for (BakedQuad bakedquad : getItemBakedModel(stack).getQuads(null, null, 0)) {
			worldrenderer.addVertexData(bakedquad.getVertexData());
		}
		worldrenderer.setTranslation(0, 0, 0);

		tessellator.draw();

		GlStateManager.disableRescaleNormal();
		GlStateManager.enableLighting();
		GlStateManager.popMatrix();
	}

}
