package com.pheenixm.explosivesplus.client.render.entity;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.client.render.models.ModelPredatorMissile;
import com.pheenixm.explosivesplus.common.entities.EntityFusedBomb;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityValkyrie;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) braces deadcode 

public class RenderValkyrie extends Render<EntityValkyrie> {

	private final ModelPredatorMissile model = new ModelPredatorMissile(); 
    private final ResourceLocation TEXTURE = new ResourceLocation(ExplosivesPlus.MOD_ID,"textures/entity/missile.png");

	public RenderValkyrie(RenderManager renderManagerIn) {
        super(renderManagerIn);
        this.shadowSize = 0.5F;
	}
	
	@Override
    public void doRender(EntityValkyrie entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
		Vec3d flightVec = new Vec3d(entity.posX - entity.prevPosX, entity.posY - entity.prevPosY, entity.posZ - entity.prevPosZ);
		flightVec = flightVec.normalize();
		
		
        GlStateManager.pushMatrix();
        GlStateManager.translate((float)x, (float)y, (float)z);
        Minecraft.getMinecraft().getTextureManager().bindTexture(TEXTURE);

        GlStateManager.scale(0.25, 0.25, 0.25);
        //RenderGlStateManager.rotate(90, 0, 0, 1);
		for(ModelRenderer box : model.boxList) {
			//box.offsetX = (float) (flightVec.x) * 2 * 16;
			//box.offsetZ = (float) (flightVec.z) * 2 * 16;
			box.offsetX =  -(float) (16 * flightVec.x);
			box.offsetZ = (float)(16 * flightVec.z);
			box.rotateAngleY = 0;
			box.rotateAngleX = 180 * 0.0174533F;// (entity.rotationYaw + 90) * 0.0174533F;
			box.rotationPointX = 0;
			box.rotationPointZ = 0;

		}

        
		//model.setRotationAngles(0F, 0F, 0F, entity.rotationYaw, entity.rotationPitch, 0.25F, entity);
    		model.render(entity, 0, 0, 0, entity.rotationYaw, entity.rotationPitch, 1);
    		GlStateManager.popMatrix();
    		
    		super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }
    		



	@Override
	protected ResourceLocation getEntityTexture(EntityValkyrie entity) {
		return TEXTURE;
	}

}