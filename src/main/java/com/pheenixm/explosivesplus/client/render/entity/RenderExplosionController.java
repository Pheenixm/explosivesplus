package com.pheenixm.explosivesplus.client.render.entity;

import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.ResourceLocation;

public class RenderExplosionController extends Render<EntityExplosionController> {

	public RenderExplosionController(RenderManager renderManagerIn) {
		super(renderManagerIn);
		this.shadowSize = 0.5F;

	}

	public void doRender(EntityExplosionController entity, double x, double y, double z,
			float entityYaw, float partialTicks) {
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
		IBlockState block = entity.parentBlock;
		if(block == null) { 
			System.out.println("What is wrong");
			block = ExplosivesPlusHolder.MEGA.getDefaultState().withProperty(ExplosivesBlock.EXPLODE, false);
		}
		if(block.getBlock().getDefaultState().equals(ExplosivesPlusHolder.CONCRETE.getDefaultState())) {
			//Makes certain explosions not render
			return;
		}
		BlockRendererDispatcher blockrendererdispatcher = Minecraft
				.getMinecraft().getBlockRendererDispatcher();
		GlStateManager.pushMatrix();
		GlStateManager.translate((float) x, (float) y + 0.5F, (float) z);


		
		this.bindEntityTexture(entity);
		GlStateManager.rotate(-90.0F, 0.0F, 1.0F, 0.0F);
		GlStateManager.translate(-0.5F, -0.5F, 0.5F);
		blockrendererdispatcher.renderBlockBrightness(block,
				entity.getBrightness());
		GlStateManager.translate(0.0F, 0.0F, 1.0F);

		if (this.renderOutlines) {
			GlStateManager.enableColorMaterial();
			GlStateManager.enableOutlineMode(this.getTeamColor(entity));
			blockrendererdispatcher.renderBlockBrightness(block, 1.0F);
			GlStateManager.disableOutlineMode();
			GlStateManager.disableColorMaterial();
		}
		GlStateManager.disableTexture2D();
		GlStateManager.disableLighting();
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA,
				GlStateManager.DestFactor.DST_ALPHA);
		GlStateManager.color(1.0F, 1.0F, 1.0F, 0.9F);
		GlStateManager.doPolygonOffset(-3.0F, -3.0F);
		GlStateManager.enablePolygonOffset();
		blockrendererdispatcher.renderBlockBrightness(block, 1.0F);
		GlStateManager.doPolygonOffset(0.0F, 0.0F);
		GlStateManager.disablePolygonOffset();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.disableBlend();
		GlStateManager.enableLighting();
		GlStateManager.enableTexture2D();

		GlStateManager.popMatrix();
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityExplosionController entity) {
		return TextureMap.LOCATION_BLOCKS_TEXTURE;
	}

}
