package com.pheenixm.explosivesplus.client.render.entity;

import com.pheenixm.explosivesplus.common.entities.EntityPhysicsBlock;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityFallingBlocks;

import net.minecraft.block.BlockLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderPhysicsBlock extends Render<EntityPhysicsBlock> {

	World world;
	
	public RenderPhysicsBlock(RenderManager renderManagerIn) {
        super(renderManagerIn);
        this.shadowSize = 0.5F;
	}
	
    public void doRender(EntityPhysicsBlock entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	if (entity.getBlock() != null)
        {
            IBlockState iblockstate = entity.getBlock();

            if (iblockstate.getRenderType() == EnumBlockRenderType.MODEL)
            {
                World world = entity.getWorldObj();

                if (iblockstate != world.getBlockState(new BlockPos(entity)) && iblockstate.getRenderType() != EnumBlockRenderType.INVISIBLE)
                {
                    this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
                    float scale = entity.scale;

                    GlStateManager.pushMatrix();
                    GlStateManager.disableLighting();
                    Tessellator tessellator = Tessellator.getInstance();
                    BufferBuilder bufferbuilder = tessellator.getBuffer();

                    if (this.renderOutlines)
                    {
                        GlStateManager.enableColorMaterial();
                        GlStateManager.enableOutlineMode(this.getTeamColor(entity));
                    }
                    
                    GlStateManager.translate((float) x - 0.5, (float) y  - 128, (float) z - 0.5);



                    GlStateManager.pushMatrix();
                    GlStateManager.enableRescaleNormal();
                    
                    
	                    bufferbuilder.begin(7, DefaultVertexFormats.BLOCK);  
	                    GlStateManager.scale(scale, scale, scale);	
	                    //GlStateManager.translate(x,y,z);
	                    BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();
	                    blockrendererdispatcher.getBlockModelRenderer().renderModel(world, blockrendererdispatcher.getModelForState(iblockstate), iblockstate, new BlockPos(0,128,0), bufferbuilder, false, MathHelper.getPositionRandom(entity.getOrigin()));
	                    tessellator.draw();
                    
                    GlStateManager.disableRescaleNormal();
                    GlStateManager.popMatrix();
                    
                    if (this.renderOutlines)
                    {
                        GlStateManager.disableOutlineMode();
                        GlStateManager.disableColorMaterial();
                    }
                    GlStateManager.enableLighting();
                    GlStateManager.popMatrix();
                    
                    
                    super.doRender(entity, x, y, z, entityYaw, partialTicks);
                }
            }
        }


    }

    private void renderFluid(double x, double y, double z,
			IBlockState blockState, EntityPhysicsBlock entity) {
        final BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();

        GlStateManager.pushMatrix();
        GlStateManager.translate((float) x, (float) y + 0.5F, (float) z);


        this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);

        GlStateManager.rotate(-90.0F, 0.0F, 1.0F, 0.0F);
        GlStateManager.translate(-0.5F, -0.5F, 0.5F);
        blockrendererdispatcher.renderBlockBrightness(blockState, blockState.getAmbientOcclusionLightValue());
        GlStateManager.translate(0.0F, 0.0F, 1.0F);

        GlStateManager.popMatrix();
		
	}

	public void renderBlock(double x, double y, double z, IBlockState blockState, EntityPhysicsBlock entity) {
        if (blockState.getRenderType() != EnumBlockRenderType.INVISIBLE)
        {
            final BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();

            GlStateManager.pushMatrix();
            GlStateManager.translate((float) x, (float) y + 0.5F, (float) z);


            this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);

            GlStateManager.rotate(-90.0F, 0.0F, 1.0F, 0.0F);
            GlStateManager.translate(-0.5F, -0.5F, 0.5F);
            blockrendererdispatcher.renderBlockBrightness(blockState, entity.getBrightness());
            GlStateManager.translate(0.0F, 0.0F, 1.0F);

            GlStateManager.popMatrix();
        }
        
    }
    
    
    

    
	@Override
	protected ResourceLocation getEntityTexture(EntityPhysicsBlock entity) {
		return TextureMap.LOCATION_BLOCKS_TEXTURE;
	}

}
