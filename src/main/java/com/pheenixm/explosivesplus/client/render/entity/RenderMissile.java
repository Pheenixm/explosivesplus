package com.pheenixm.explosivesplus.client.render.entity;

import org.lwjgl.opengl.GL11;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.client.ModelRegistry;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.entities.missiles.EntityBallisticMissile;
import com.pheenixm.explosivesplus.common.items.components.Engine;
import com.pheenixm.explosivesplus.common.items.components.Fuselage;
import com.pheenixm.explosivesplus.common.items.components.Warhead;
import com.pheenixm.explosivesplus.common.util.ModResourceLocation;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.ForgeHooksClient;

public class RenderMissile extends Render<EntityBallisticMissile> {

	private ItemStack war;
	private ItemStack fuse;
	private ItemStack engine;
	World world;
	private IBakedModel bakedModel;
    protected static ItemModelMesher itemRenderer;

	/*
	 * TODO: Fix rotation issues
	 */
	public RenderMissile(RenderManager renderManagerIn) {
		super(renderManagerIn);
		this.shadowSize = 0.5F;
		
	}
	
    private IBakedModel getBakedModel(ItemStack stack) {
    		
    		String path = getPath(stack);
    		ModResourceLocation loc = new ModResourceLocation(ExplosivesPlus.MOD_ID, path);
    		IBakedModel bakedModel = ModelRegistry.INSTANCE.getBakedModel(loc);
        return bakedModel;
        
    }
    
    private String getPath(ItemStack stack) {
    		String registry = stack.getItem().getRegistryName().toString();
    		String[] splitter = registry.split(":");
    		return "item/" + splitter[splitter.length - 1] + ".obj";
    }


	public void doRender(EntityBallisticMissile entity, double x, double y, double z,
			float entityYaw, float partialTicks) {
		war  = entity.getWarhead();
		fuse = entity.getFuselage();
		engine = entity.getEngine();
		world = entity.getEntityWorld();
		
        GlStateManager.pushAttrib();
        GlStateManager.pushMatrix();
        
        float yaw = entity.prevRotationYaw + (entity.rotationYaw - entity.prevRotationYaw) * partialTicks - 90;
        float pitch = entity.prevRotationPitch + (entity.rotationPitch - entity.prevRotationPitch) * partialTicks - 90;  
        
        float offset = 0.5F;
        
        
        GlStateManager.translate((float) x, (float) y, (float) z);
        GlStateManager.translate(offset, offset, offset);
        GlStateManager.translate(0, -entity.height/2, 0);
        
        GlStateManager.rotate(yaw, 0.0F, 1.0F, 0.0F);
        GlStateManager.rotate(pitch, 0.0F, 0.0F, 1.0F);


		if(war.getItem() instanceof Warhead) {
			renderWarhead(entity,x,y,z, yaw, pitch);
		}
		if(fuse.getItem() instanceof Fuselage) {
			renderFuselage(entity,x,y,z, yaw, pitch);
		}
		if(engine.getItem() instanceof Engine) {
			renderEngine(entity,x,y,z, yaw, pitch);
		}
		

		
        GlStateManager.popMatrix();
        GlStateManager.popAttrib();

        super.doRender(entity, x, y, z, entityYaw, partialTicks);

	}

	private void renderEngine(EntityBallisticMissile entity, double x, double y, double z, float yaw, float pitch) {
        GlStateManager.pushMatrix();
        GlStateManager.enableRescaleNormal();
        GlStateManager.translate(0,1-0.3,0);



        bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldrenderer = tessellator.getBuffer();
        
        worldrenderer.begin(GL11.GL_QUADS, DefaultVertexFormats.ITEM);
        worldrenderer.setTranslation(-x, -y, -z);
        worldrenderer.color(255, 255, 255, 255);
        for (BakedQuad bakedquad : getBakedModel(engine).getQuads(null, null, 0))
        {
            worldrenderer.addVertexData(bakedquad.getVertexData());
        }
        worldrenderer.setTranslation(0, 0, 0);

        tessellator.draw();
        
        GlStateManager.disableRescaleNormal();
        GlStateManager.enableLighting();
        GlStateManager.popMatrix();
	}

	private void renderFuselage(EntityBallisticMissile entity, double x, double y, double z, float yaw, float pitch) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(0,1,0);
        GlStateManager.enableRescaleNormal();


        bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
        
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldrenderer = tessellator.getBuffer();
        
        worldrenderer.begin(GL11.GL_QUADS, DefaultVertexFormats.ITEM);
        worldrenderer.setTranslation(-x, -y, -z);
        worldrenderer.color(255, 255, 255, 255);
        for (BakedQuad bakedquad : getBakedModel(fuse).getQuads(null, null, 0))
        {
            worldrenderer.addVertexData(bakedquad.getVertexData());
        }
        worldrenderer.setTranslation(0, 0, 0);

        tessellator.draw();

        
        GlStateManager.disableRescaleNormal();
        GlStateManager.enableLighting();
        GlStateManager.popMatrix();

		
	}

	private void renderWarhead(EntityBallisticMissile entity, double x, double y, double z, float yaw, float pitch) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(0,16.04F,0);
        GlStateManager.rotate(-67.5F, 0, 1, 0);
        GlStateManager.enableRescaleNormal();

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldrenderer = tessellator.getBuffer();
        
        worldrenderer.begin(GL11.GL_QUADS, DefaultVertexFormats.ITEM);
        worldrenderer.setTranslation(-x, -y, -z);
        worldrenderer.color(255, 255, 255, 255);
        for (BakedQuad bakedquad : getBakedModel(war).getQuads(null, null, 0))
        {
            worldrenderer.addVertexData(bakedquad.getVertexData());
        }
        worldrenderer.setTranslation(0, 0, 0);

        tessellator.draw();
		
        
        GlStateManager.disableRescaleNormal();
        GlStateManager.enableLighting();
        GlStateManager.popMatrix();


	}

	@Override
	protected ResourceLocation getEntityTexture(EntityBallisticMissile entity) {
		return TextureMap.LOCATION_BLOCKS_TEXTURE;
	}
	

	

}
