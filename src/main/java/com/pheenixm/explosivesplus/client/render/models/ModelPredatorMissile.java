package com.pheenixm.explosivesplus.client.render.models;
//Exported java file
//Keep in mind that you still need to fill in some blanks
// - ZeuX

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelPredatorMissile extends ModelBase
{
	public ModelPredatorMissile()
	{
		missile1 = new ModelRenderer(this, 12, 20);
		missile1.addBox(-0.5F, 18F, -0.5F, 1, 1, 1, 0F);
		missile1.setRotationPoint(0F, 0F, 0F);
		missile1.rotateAngleX = 0F;
		missile1.rotateAngleY = 0F;
		missile1.rotateAngleZ = 0F;
		missile1.mirror = false;
		missiel2 = new ModelRenderer(this, 12, 22);
		missiel2.addBox(-1.033333F, 17F, -1F, 2, 1, 2, 0F);
		missiel2.setRotationPoint(0F, 0F, 0F);
		missiel2.rotateAngleX = 0F;
		missiel2.rotateAngleY = 0F;
		missiel2.rotateAngleZ = 0F;
		missiel2.mirror = false;
		missile3 = new ModelRenderer(this, 12, 25);
		missile3.addBox(-1.5F, 16F, -1.5F, 3, 1, 3, 0F);
		missile3.setRotationPoint(0F, 0F, 0F);
		missile3.rotateAngleX = 0F;
		missile3.rotateAngleY = 0F;
		missile3.rotateAngleZ = 0F;
		missile3.mirror = false;
		missile4 = new ModelRenderer(this, 16, 0);
		missile4.addBox(-2F, 0F, -2F, 4, 16, 4, 0F);
		missile4.setRotationPoint(0F, 0F, 0F);
		missile4.rotateAngleX = 0F;
		missile4.rotateAngleY = 0F;
		missile4.rotateAngleZ = 0F;
		missile4.mirror = false;
		missile5 = new ModelRenderer(this, 0, 0);
		missile5.addBox(-2F, -16F, -2F, 4, 16, 4, 0F);
		missile5.setRotationPoint(0F, 0F, 0F);
		missile5.rotateAngleX = 0F;
		missile5.rotateAngleY = 0F;
		missile5.rotateAngleZ = 0F;
		missile5.mirror = false;
		missile6 = new ModelRenderer(this, 0, 22);
		missile6.addBox(0F, 5F, -5F, 0, 5, 3, 0F);
		missile6.setRotationPoint(0F, 0F, 0F);
		missile6.rotateAngleX = 0F;
		missile6.rotateAngleY = 0F;
		missile6.rotateAngleZ = 0F;
		missile6.mirror = false;
		missile7 = new ModelRenderer(this, 0, 20);
		missile7.addBox(2F, 5F, 0F, 3, 5, 0, 0F);
		missile7.setRotationPoint(0F, 0F, 0F);
		missile7.rotateAngleX = 0F;
		missile7.rotateAngleY = 0F;
		missile7.rotateAngleZ = 0F;
		missile7.mirror = false;
		missile8 = new ModelRenderer(this, 6, 20);
		missile8.addBox(-5F, 5F, 0F, 3, 5, 0, 0F);
		missile8.setRotationPoint(0F, 0F, 0F);
		missile8.rotateAngleX = 0F;
		missile8.rotateAngleY = 0F;
		missile8.rotateAngleZ = 0F;
		missile8.mirror = false;
		missile9 = new ModelRenderer(this, 6, 22);
		missile9.addBox(0F, 5F, 2F, 0, 5, 3, 0F);
		missile9.setRotationPoint(0F, 0F, 0F);
		missile9.rotateAngleX = 0F;
		missile9.rotateAngleY = 0F;
		missile9.rotateAngleZ = 0F;
		missile9.mirror = false;
		missile10 = new ModelRenderer(this, 42, 5);
		missile10.addBox(-7F, -16F, 0F, 5, 7, 0, 0F);
		missile10.setRotationPoint(0F, 0F, 0F);
		missile10.rotateAngleX = 0F;
		missile10.rotateAngleY = 0F;
		missile10.rotateAngleZ = 0F;
		missile10.mirror = false;
		missile11 = new ModelRenderer(this, 52, 0);
		missile11.addBox(0F, -16F, 2F, 0, 7, 5, 0F);
		missile11.setRotationPoint(0F, 0F, 0F);
		missile11.rotateAngleX = 0F;
		missile11.rotateAngleY = 0F;
		missile11.rotateAngleZ = 0F;
		missile11.mirror = false;
		missile12 = new ModelRenderer(this, 42, 12);
		missile12.addBox(2F, -16F, 0F, 5, 7, 0, 0F);
		missile12.setRotationPoint(0F, 0F, 0F);
		missile12.rotateAngleX = 0F;
		missile12.rotateAngleY = 0F;
		missile12.rotateAngleZ = 0F;
		missile12.mirror = false;
		missile13 = new ModelRenderer(this, 32, 0);
		missile13.addBox(0F, -16F, -7F, 0, 7, 5, 0F);
		missile13.setRotationPoint(0F, 0F, 0F);
		missile13.rotateAngleX = 0F;
		missile13.rotateAngleY = 0F;
		missile13.rotateAngleZ = 0F;
		missile13.mirror = false;
	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		missile1.render(f5);
		missiel2.render(f5);
		missile3.render(f5);
		missile4.render(f5);
		missile5.render(f5);
		missile6.render(f5);
		missile7.render(f5);
		missile8.render(f5);
		missile9.render(f5);
		missile10.render(f5);
		missile11.render(f5);
		missile12.render(f5);
		missile13.render(f5);
	}
	
	//f3 rotYaw
	//f4 rotPitch
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity ent)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, ent);
	}
	
	//fields
	public ModelRenderer missile1;
	public ModelRenderer missiel2;
	public ModelRenderer missile3;
	public ModelRenderer missile4;
	public ModelRenderer missile5;
	public ModelRenderer missile6;
	public ModelRenderer missile7;
	public ModelRenderer missile8;
	public ModelRenderer missile9;
	public ModelRenderer missile10;
	public ModelRenderer missile11;
	public ModelRenderer missile12;
	public ModelRenderer missile13;
}

