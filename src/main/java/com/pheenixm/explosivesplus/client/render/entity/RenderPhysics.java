package com.pheenixm.explosivesplus.client.render.entity;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Sphere;

import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityFallingBlocks;

import net.minecraft.block.BlockLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderPhysics extends Render<EntityNovaController> {

	public RenderPhysics(RenderManager renderManagerIn) {
        super(renderManagerIn);
        this.shadowSize = 0.5F;
	}
	
    public void doRender(EntityNovaController entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
		controller = entity;
        GL11.glPushMatrix();
        GlStateManager.translate((float) x, (float) y + 0.5F, (float) z);
        
		for(FauxEntityFallingBlocks block : entity.fallingBlocks.values()) {
			float diffX = (float) (entity.posX - block.posX);
			float diffY = (float) (entity.posY - block.posY);
			float diffZ = (float) (entity.posZ - block.posZ);
			
			float blockX = (float) x - diffX;
			float blockY = (float) y - diffY;
			float blockZ = (float) z - diffZ;
			if(block.block.getBlock() instanceof BlockLiquid) {
				renderFluid(blockX, blockY, blockZ, block);
			} else {
				renderBlock(blockX, blockY, blockZ, block);
			}


		}
        GL11.glPopMatrix();

        
    }

    private void renderFluid(float blockX, float blockY, float blockZ,
			FauxEntityFallingBlocks block) {
        final IBlockState blockState = block.block;
        final BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();

        GlStateManager.pushMatrix();
        GlStateManager.translate((float) blockX, (float) blockY + 0.5F, (float) blockZ);


        this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);

        GlStateManager.rotate(-90.0F, 0.0F, 1.0F, 0.0F);
        GlStateManager.translate(-0.5F, -0.5F, 0.5F);
        blockrendererdispatcher.renderBlockBrightness(blockState, blockState.getAmbientOcclusionLightValue());
        GlStateManager.translate(0.0F, 0.0F, 1.0F);

        GlStateManager.popMatrix();
		
	}

	public void renderBlock(double x, double y, double z, FauxEntityFallingBlocks entity) {
        final IBlockState blockState = entity.block;
        World world = controller.getEntityWorld();
        if (blockState != world.getBlockState(new BlockPos(controller)) && blockState.getRenderType() != EnumBlockRenderType.INVISIBLE)
        {
            this.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
            GlStateManager.pushMatrix();
            GlStateManager.disableLighting();
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder bufferbuilder = tessellator.getBuffer();

            if (this.renderOutlines)
            {
                GlStateManager.enableColorMaterial();
            }

            bufferbuilder.begin(7, DefaultVertexFormats.BLOCK);
            BlockPos blockpos = new BlockPos(entity.posX, controller.getEntityBoundingBox().maxY, entity.posZ);
            GlStateManager.translate((float)(x - (double)blockpos.getX() - 0.5D), (float)(y - (double)blockpos.getY()), (float)(z - (double)blockpos.getZ() - 0.5D));
            BlockRendererDispatcher blockrendererdispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();
            blockrendererdispatcher.getBlockModelRenderer().renderModel(world, blockrendererdispatcher.getModelForState(blockState), blockState, blockpos, bufferbuilder, false, MathHelper.getPositionRandom(new Vec3i(controller.posX, controller.posY, controller.posZ)));
            tessellator.draw();

            if (this.renderOutlines)
            {
                GlStateManager.disableOutlineMode();
                GlStateManager.disableColorMaterial();
            }

            GlStateManager.enableLighting();
            GlStateManager.popMatrix();
        }
        
    }
    
    
    

	private EntityNovaController controller;
    
	@Override
	protected ResourceLocation getEntityTexture(EntityNovaController entity) {
		return TextureMap.LOCATION_BLOCKS_TEXTURE;
	}

}
