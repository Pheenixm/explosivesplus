package com.pheenixm.explosivesplus.common.util;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;

public class Position {

	public Position(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}
	
	public double getDistance(double i, double j, double k) {
		return Math.sqrt(Math.pow(i-x, 2) + Math.pow(j-y,2) + Math.pow(k-z, 2));	
	}
	
    public Position add(double x, double y, double z)
    {
        return x == 0 && y == 0 && z == 0 ? this : new Position(this.getX() + x, this.getY() + y, this.getZ() + z);
    }
    
    public Position add(Position vec)
    {
        return this.add(vec.getX(), vec.getY(), vec.getZ());
    }



	private double x;
	private double y;
	private double z;

}
