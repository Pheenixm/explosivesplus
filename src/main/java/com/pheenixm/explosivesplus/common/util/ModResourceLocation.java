package com.pheenixm.explosivesplus.common.util;

import net.minecraft.util.ResourceLocation;

/**
 * 
 * @author Cadiboo, adapted by Pheenixm
 *
 */
public class ModResourceLocation extends ResourceLocation {

	public ModResourceLocation(final String resourceName) {
		super(resourceName);
	}

	public ModResourceLocation(final String resourceDomain, final String resourcePath) {
		super(resourceDomain, resourcePath);
	}

	public ModResourceLocation(final ResourceLocation resourceLocation) {
		super(resourceLocation.getNamespace(), resourceLocation.getPath());
	}

	@Override
	public String toString() {
		if (this.namespace.equals("")) {
			return this.path;
		}
		return super.toString();
	}

}