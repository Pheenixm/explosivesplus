package com.pheenixm.explosivesplus.common.util;

import java.util.HashMap;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.pheenixm.explosivesplus.ExplosivesPlus;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.Ticket;
import net.minecraftforge.common.ForgeChunkManager.Type;

public class Utils {

	public Utils() {
		// TODO Auto-generated constructor stub
	}

	private static Logger l; // You need to import the right logger. Make sure
								// to import the org.apache.logging.log4j.Logger
								// interface.

	public static Logger getLogger() {
		if (l == null) {
			l = LogManager.getFormatterLogger(ExplosivesPlus.MOD_ID);
		}
		return l;
	}

	public static int[] vec3iToArray(Vec3i vec) {
		int[] array = {vec.getX(), vec.getY(), vec.getZ()};
		return array;
	}

	public static Vec3i arrayToVec3i(int[] args) {
		return new Vec3i(args[0], args[1], args[2]);
	}

	public static double getAverageHeight(World world, int x, int z, int width, int height) {
		double yAverage = 0;
		for (int i = -width / 2; i <= width / 2; i++) {
			for (int j = -height / 2; j <= height / 2; j++) {
				yAverage += world.getHeight(x + i, z + j);
			}
		}

		return (double) yAverage / (width * height);
	}

	public static boolean doesInterceptExist(Vec3d startPos, Vec3d endPos, Vec3d centerPos, int radius) {
		Vec3d relStart = startPos.subtract(centerPos);
		Vec3d relEnd = endPos.subtract(centerPos);
		double ax = relStart.x;
		double ay = relStart.z;
		double bx = relEnd.x;
		double by = relEnd.z;
		double a = ax*ax+ay*ay-2*ax*ay+bx*bx-2*ay*by+by*by;
		double b = -2*ax*ax-2*ay*ay+2*ax*bx+2*ay*by;
		double c = ax*ax+ay*ay-(double)(radius*radius);
		return b*b-4*a*c >= 0;
	}

	protected static long stringToSeed(String s) {
		if (s == null) {
			return 0;
		}
		long hash = 0;
		for (char c : s.toCharArray()) {
			hash = 31L * hash + c;
		}
		return hash;
	}

	public static Random getRandSeed(String seed) {
		Random rand = new Random(stringToSeed(seed));
		return rand;
	}

}
