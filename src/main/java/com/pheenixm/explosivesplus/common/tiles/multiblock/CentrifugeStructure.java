package com.pheenixm.explosivesplus.common.tiles.multiblock;

import java.util.ArrayList;
import java.util.HashMap;

import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;

import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class CentrifugeStructure extends MultiblockStructure {

	private IBlockState electro;
	private IBlockState lid;
	
	public CentrifugeStructure(World w, ArrayList<IBlockState> mats, TileEntityCentrifuge te) {
		super(w, mats, te);
	}
	
	public CentrifugeStructure(MultiblockStructure struc, ArrayList<IBlockState> mats) {
		super(struc, mats);
	}


	@Override
	public HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> defineStructure() {
		HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> defs = new HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>>(1);
		HashMap<Integer, HashMap<Integer, BlockContainer>> def = new HashMap<Integer, HashMap<Integer, BlockContainer>>();
		addBlockToLevel(def, new BlockPos(0,0,0), electro);
		addBlockToLevel(def, new BlockPos(0,1,0), lid);
		addBlockToLevel(def, new BlockPos(0,2,0), lid);

		defs.put(EnumFacing.UP.getIndex(), def);
		this.sizeDef = 3;
		
		return defs;
	}
	
	private void addBlockToLevel(HashMap<Integer, HashMap<Integer, BlockContainer>> def, BlockPos pos, IBlockState state) {
		HashMap<Integer, BlockContainer> level = new HashMap<Integer, BlockContainer>();
		BlockContainer base = new BlockContainer(pos, state);
		level.put(getHashFromPos(pos), base);
		def.put(pos.getY(), level);
	}

	@Override
	public void defineMaterials(ArrayList<IBlockState> mats) {
		electro = mats.get(0);
		lid = mats.get(1	);

	}

	@Override
	public boolean hasValidTile(BlockPos childPos, boolean checkTiles) {
		IBlockState state = world.getBlockState(childPos);
		if(state.getBlock().hasTileEntity(state)) {
			TileEntity te = world.getTileEntity(childPos);
			if(te instanceof TileEntityMultiblock) {
				TileEntityMultiblock silo = (TileEntityMultiblock) te;
				if(checkTiles && silo.getMaster().equals(tile)) {
					return true;
				}
				if(!checkTiles && !silo.validMultiblock) {
					return true;
				}
				System.out.println("No valid tile?");
			}
		} else {
			System.out.println("This block just doesn't have a tile");
		}
		return false;
	}

}
