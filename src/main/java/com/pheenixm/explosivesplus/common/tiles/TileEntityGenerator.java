package com.pheenixm.explosivesplus.common.tiles;

import java.util.List;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.api.energy.EnergyBuffer;
import com.pheenixm.explosivesplus.common.EnumTier;
import com.pheenixm.explosivesplus.common.coms.tiles.TileEntityCable;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageEnergyUpdate;
import com.pheenixm.explosivesplus.common.network.messages.MessageGeneratorUpdate;
import com.pheenixm.explosivesplus.common.tiles.generator.Tier1Generator;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public abstract class TileEntityGenerator<T extends EntityMob> extends TileEntityMultiblock implements ITickable{
	
	public EnumTier tier;
	public int mobId;
	
	public EnergyBuffer buffer;
	public static final int INTERNAL_STORAGE = 100000;
	public int energyPerTick;
	
	private int ticks;
	
	public ItemStackHandler in = new ItemStackHandler(1);
	
	/**
	 * Position within the current processing cycle; counts down to 0
	 */
	public int currentTicks = getProcessingTime();
	public boolean spawnParticlesTick; 
	

	

	public TileEntityGenerator(EnumTier tier) {
		super();
		this.tier = tier;
	}
	
	@Override
	public void onLoad() {
		super.onLoad();
		mobId = -1;
		buffer = new EnergyBuffer(100000);
		energyPerTick = EnumTier.getGeneratorPowerPerTickFromTier(tier);
	}

	@Override
	@Nullable
	public TileEntityGenerator<?> getMaster() {
		if(master instanceof TileEntityGenerator) {
			return (TileEntityGenerator<?>) super.getMaster();
		} else {
			return null;
		}
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		//This one should only be used for rendering purposes
		//Prevents network capabilities from breaking client-side
		if(world.isRemote && validMultiblock) {
			return capability == CapabilityEnergy.ENERGY || capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
		}
 		if(master == null || master.centerPosition == null || master.core == null) {
			return super.hasCapability(capability, facing);
		}
		if(capability == CapabilityEnergy.ENERGY) {
 			if(this.getPos().equals(getTop())) {
 				return validMultiblock;
 			}
		}
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return validMultiblock;
		}
 		return super.hasCapability(capability, facing);
	}
	
    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
    		if(master == null || master.centerPosition == null || master.core == null) {
    			return super.getCapability(capability, facing);
    		}
        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(getMaster().in);	
        }
        if(capability == CapabilityEnergy.ENERGY) {
        		if(this.getPos().equals(getTop())) {
        			return CapabilityEnergy.ENERGY.cast(getMaster().getEnergy());
        		}
        }
        return super.getCapability(capability, facing);
    }

    private BlockPos getTop() {
		if(master == null || master.centerPosition == null || master.core == null) {
			return new BlockPos(0,0,0);
		}
    		BlockPos centerPos = master.centerPosition;
		return new BlockPos(centerPos.getX(),master.core.getyMax(),centerPos.getZ());
    }


	@Override
	public NBTTagCompound serialize(NBTTagCompound tag, boolean isPacket) {
		tag = super.serialize(tag, isPacket);
		tag.setTag("inv", in.serializeNBT());
		tag.setTag("buffer", buffer.serializeNBT());
		return tag;
	}
	
	@Override
	//TODO: Fix energy storage bug
	public void deserialize(NBTTagCompound tag, boolean isPacket) {
 		super.deserialize(tag, isPacket);
		if(buffer == null) {
			//There is another way of doing this, if this fails
			buffer = new EnergyBuffer(INTERNAL_STORAGE);
			
			in.deserializeNBT(tag.getCompoundTag("inv"));  
			buffer.deserializeNBT(tag.getCompoundTag("buffer"));
		}
	}
	
	@Override
	public void update() {
		if(!world.isRemote) {
			spawnParticlesTick = false;
			ticks++;
			//Try to empty buffer
			pushEnergy();
			
			if(mobId > -1) {
				//Mob is present, can continue in generator workflow
				EntityMob mob = getMobFromId();
				if(mob == null) {
					mobId = -1;
					return;
				}
				/*
				if(!mob.isAIDisabled()) {
					//Ensure the mob is not targeting anything
					mob.setAttackTarget((EntityLivingBase)null);
				}*/
				ItemStack fuel = getFuel();
				if(isGeneratorSupplied(fuel)) {
					//Power the generator
					buffer.receiveEnergy(energyPerTick, false);
					if(ticks % 40 == 0)  {
						getMobFromId().getJumpHelper().doJump();
					}
					
					if(currentTicks == 0) {
						//Remove fuel from generator, reset counter
						in.extractItem(0, fuel.getCount(), false);
						currentTicks = getProcessingTime();
						mob.setVelocity(0, 1, 0);

						//Spawn new itemstack
						//TODO: Fix
						//spawnItem(fuel);
						
						//Create explosion particles
						//Need to play sound as well
						spawnParticlesTick = true;
						
					} else {
						currentTicks--;
					}	
					updateValues();
				} else {
					currentTicks = getProcessingTime();
					
				}
	
				
			} else {
				//Mob is not present, check for it
				mobId = checkForMob();
			}
		}
	}	
	
	private void pushEnergy() {
		BlockPos sink = getTop().offset(EnumFacing.UP);
		if(sink.equals(new BlockPos(0,1,0))) {
			return;
		}
		TileEntity tile = world.getTileEntity(sink);
		IEnergyStorage en = null;
		if(tile != null && tile.hasCapability(CapabilityEnergy.ENERGY, EnumFacing.DOWN)) {
			en = CapabilityEnergy.ENERGY.cast(tile.getCapability(CapabilityEnergy.ENERGY, EnumFacing.DOWN));
		}
		if(en != null) {
			if(en.canReceive()) {
				int send = en.receiveEnergy(energyPerTick, true);
				if(buffer.canExtract()) {
					int out = buffer.extractEnergy(send, true);
					en.receiveEnergy(out, false);
				}
			}
		}
	}
	
	private void updateValues() {
		MessageGeneratorUpdate message = new MessageGeneratorUpdate(buffer, this, mobId, spawnParticlesTick);
		PacketHandler.NETWORK.sendToAllAround(message, 
				new NetworkRegistry.TargetPoint(world.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ(), 64));
	}
	
	public void updateClient(int e, int c, boolean anim, BlockPos center) {
		if(buffer == null) {
			buffer = new EnergyBuffer(e, c);
		} else {
			buffer.setEnergy(e);
			buffer.setCapacity(c);
		}
		if(anim) {
			world.spawnParticle(EnumParticleTypes.EXPLOSION_LARGE, center.getX() + 0.5, center.getY(), center.getZ() + 0.5, 1.0D, 0.0D, 0.0D);
		}
		spawnParticlesTick = anim;
	}


	
	private void spawnItem(ItemStack fuel) {
		EntityItem item = new EntityItem(world, getItemPos().x, getItemPos().y, getItemPos().z, fuel.copy());
		//TODO: Check velocity
		item.setVelocity(0, 0.1, 0);
		item.setInfinitePickupDelay();
		item.setNoGravity(true);
		item.noClip = true;
		item.lifespan = currentTicks/2;
		world.spawnEntity(item);
	}
	
	/**
	 * Get a mob from its id
	 * @return The mob if one exists, null otherwise
	 */
	protected T getMobFromId() {
		Entity ent = world.getEntityByID(mobId);
		if(ent instanceof EntityMob) {
			return (T) ent;
		}
		return null;
	}
	
	/**
	 * Check the passed in EntityMob against the generator-specific mob
	 * @param mob the Entity to check against
	 * @return true is the entity passed in is the correct type, false otherwise
	 */
	protected boolean correctMob(EntityMob mob) {
		return getGeneratorMob().getClass().isInstance(mob);
	}

	
	/**
	 * Checks for a mob inside the generator's bounding box
	 * @return the int id of the mob, if it exists. -1 otherwise
	 */
	protected int checkForMob() {
		List<? extends EntityMob> list = world.getEntitiesWithinAABB(getGeneratorMob().getClass(), getBoundingBox());
		if(!list.isEmpty()) {
			for(EntityMob ent : list) {
				if(correctMob(ent)) {
					//ent.setAttackTarget((EntityLivingBase)null);
					//ent.setNoAI(true);
					return ent.getEntityId();
				}
			}
		}
		return -1;
	}
	
	protected boolean isGeneratorSupplied(ItemStack fuel) {
		ItemStack stack = in.getStackInSlot(0);
		if(!stack.isEmpty()) {
			if(fuel.getItem().equals(stack.getItem())) {
				int amountFuel = fuel.getCount();
				if(in.extractItem(0, amountFuel, true).getCount() >= amountFuel) {
					//We have enough fuel to power the generator
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Get the bounding box of the generator, in which valid mobs may exist
	 * @return The bounding box in which a mob may be found
	 */
	protected abstract AxisAlignedBB getBoundingBox();
	
	/**
	 * Get the mob that generates power for this generator
	 * @return
	 */
	protected abstract T getGeneratorMob();
	
	/**
	 * Get an itemstack containing the valid fuel for this generator
	 * @return An itemstack containing the valid fuel and the amount needed per unit time
	 */
	public abstract ItemStack getFuel();
	
	/**
	 * Get the processing time for this generator
	 * @return the time to process a unit amount of fuel
	 */
	public abstract int getProcessingTime();
	
	public abstract Vec3d getItemPos();

	public IItemHandler getInventory() {
		return in;
	}

	public EnergyBuffer getEnergy() {
		return buffer;
	}

}
