package com.pheenixm.explosivesplus.common.tiles;

import java.util.ArrayList;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.energy.EnergyBuffer;
import com.pheenixm.explosivesplus.api.recipes.RecipeCentrifuge;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.GuiHandler;
import com.pheenixm.explosivesplus.common.GuiHandler.GuiEnum;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkEndpointCentrifuge;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkEndpointMultiblock;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageEnergyUpdate;
import com.pheenixm.explosivesplus.common.tiles.multiblock.CentrifugeStructure;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockStructure;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import scala.actors.threadpool.Arrays;

//TODO: Add my own APIs to streamline inventory saving and syncing
//TODO: Make energy OPTIONAL if no energy mod exists
//TODO: FIx animation
public class TileEntityCentrifuge extends TileEntityMultiblock implements ITickable {



	public static final int POWER_LIMIT = 100000;
    public static final int ENERGY_PER_TICK = 2;
	
	private ItemStackHandler in = new ItemStackHandler(1);
	private ItemStackHandler out = new ItemStackHandler(1);
	
	private EnergyBuffer energy = new EnergyBuffer(POWER_LIMIT);

	private int currentRecipeTicks;
	private int currentRecipeTicksElapsed;
	private boolean isProcessing;
	private int currentCount;
	private ItemStack currentOutput = ItemStack.EMPTY;
	private ItemStack currentInput = ItemStack.EMPTY;
	private int lastEnergy;
	private boolean[] lastProcessing = new boolean[2];
	
	public long angle = 0;
	
	protected CapabilityNetworkEndpointCentrifuge endpoint;
	
	//TODO: Modify
	public static final IBlockState mats[] = {
			ExplosivesPlusHolder.EM_BLOCK.getDefaultState(),
			ExplosivesPlusHolder.CONCRETE.getDefaultState(),
			ExplosivesPlusHolder.EM_BLOCK.getDefaultState()
					.withProperty(MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.CENTER),
			ExplosivesPlusHolder.CONCRETE.getDefaultState()
					.withProperty(MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.CENTER),
			ExplosivesPlusHolder.EM_BLOCK.getDefaultState().withProperty(
					MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.STRUCTURE),
			ExplosivesPlusHolder.CONCRETE.getDefaultState().withProperty(
					MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.STRUCTURE)};

	
	public TileEntityCentrifuge() {
		super();
		//TODO: REMOVE TESTING METHOD
		energy.receiveEnergy(10000, false);
	}

	
	@Override
	public void onInventoryClosed() {
		System.out.println(in.getStackInSlot(0));
		System.out.println(out.getStackInSlot(0));
	}

	@Override
	protected MultiblockStructure setStructure() {
		ArrayList<IBlockState> materials = new ArrayList<IBlockState>(
				Arrays.asList(mats));
		return new CentrifugeStructure(world, materials, this);
	}

	@Override
	public boolean onMultiTileActivated(TileEntityMultiblock tile,
			EntityPlayer player, EnumHand hand, EnumFacing side, float xHit,
			float yHit, float zHit) {
		if(tile.validMultiblock) {
			if(player.isSneaking()) {
			} else {
				if(!world.isRemote) {
					player.openGui(ExplosivesPlus.instance, GuiEnum.CENTRI.ordinal(), world, pos.getX(), pos.getY(), pos.getZ());
					System.out.println(getMaster().getNetworkEndpoint().getSharedUid().toString());
					System.out.println(getMaster().getNetworkEndpoint().getStatus());
				} 
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onMultiTileClicked(TileEntityMultiblock tile,
			EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state,
			EntityLivingBase placer, ItemStack stack) {
		this.onBlockPlaced(worldIn, pos, state, placer, stack);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		//This one should only be used for rendering purposes
		if(world.isRemote && validMultiblock) {
			return capability == CapabilityEnergy.ENERGY;
		}
 		if(master == null || master.centerPosition == null || master.core == null) {
			return super.hasCapability(capability, facing);
		}
 		if(capability == CapabilityEnergy.ENERGY) {
 			return validMultiblock;
 		} else {
 			if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
	        		BlockPos centerPos = master.centerPosition;
	        		BlockPos bottom = new BlockPos(centerPos.getX(),master.core.getyMin(),centerPos.getZ());
	        		BlockPos top = new BlockPos(centerPos.getX(),master.core.getyMax(),centerPos.getZ());
	        		if(this.getPos().equals(top) || this.getPos().equals(bottom)) {
	        			return validMultiblock;
	        		}
 			}
 		}
 		
 		return super.hasCapability(capability, facing);
	}
	
    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
    		if(master == null || master.centerPosition == null || master.core == null) {
    			return super.getCapability(capability, facing);
    		}
        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
        {
        		BlockPos centerPos = master.centerPosition;
        		BlockPos bottom = new BlockPos(centerPos.getX(),master.core.getyMin(),centerPos.getZ());
        		BlockPos top = new BlockPos(centerPos.getX(),master.core.getyMax(),centerPos.getZ());
        		if(this.getPos().equals(bottom)) {
        			return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(getMaster().out);
        		}
        		if(this.getPos().equals(top)) {
        			return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(getMaster().in);
        		}

        			
        }
        if(capability == CapabilityEnergy.ENERGY) {
        		return CapabilityEnergy.ENERGY.cast(getMaster().energy);
        }

        return super.getCapability(capability, facing);
    }

	
	protected void invalidateMultiblock() {
		super.invalidateMultiblock();
		if(this == master) {
				if(in.getStackInSlot(0) != null) {
					Block.spawnAsEntity(world, this.centerPosition.add(new BlockPos(0,1,0)), in.getStackInSlot(0));
					in.setStackInSlot(0, ItemStack.EMPTY);
				}
				if(out.getStackInSlot(0) != null) {
					Block.spawnAsEntity(world, this.centerPosition.add(new BlockPos(0,1,0)), out.getStackInSlot(0));
					out.setStackInSlot(0, ItemStack.EMPTY);
				}

		}
	}
	
	@Override
	@Nullable
	public TileEntityCentrifuge getMaster() {
		if(master instanceof TileEntityCentrifuge) {
			return (TileEntityCentrifuge) super.getMaster();
		} else {
			return null;
		}
	}



	@Override
	public void deserialize(NBTTagCompound tag, boolean isPacket) {
		super.deserialize(tag, isPacket);
		in.deserializeNBT(tag.getCompoundTag("input"));
		out.deserializeNBT(tag.getCompoundTag("output"));

		if(energy == null) {
			//There is another way of doing this, if this fails
			energy = new EnergyBuffer(POWER_LIMIT);
			energy.deserializeNBT(tag.getCompoundTag("energy"));
		}
		setCurrentRecipeTicks(tag.getInteger("currentRecipeTicks"));
		setCurrentRecipeTicksElapsed(tag.getInteger("currentElapsed"));
		setProcessing(tag.getBoolean("isProcessing"));
		setCurrentCount(tag.getInteger("currentCount"));
		
	}
	
	@Override
	public NBTTagCompound serialize(NBTTagCompound tag, boolean isPacket) {
		tag = super.serialize(tag, isPacket);
		tag.setTag("input", in.serializeNBT());
		tag.setTag("output", out.serializeNBT());
		tag.setTag("energy", energy.serializeNBT());
		tag.setInteger("currentRecipeTicks", this.getCurrentRecipeTicks());
		tag.setInteger("currentElapsed", this.getCurrentRecipeTicksElapsed());
		tag.setBoolean("isProcessing", isProcessing);
		tag.setInteger("currentCount", getCurrentCount());
		return tag;
	}
	

	@Override
	public void update() {
		if(world.isRemote) {
			if(isMaster && isProcessing) {
				angle = (System.currentTimeMillis() / 5) % 360;
			}
			/*
			if(!isProcessing && lastEnergy != energy.getEnergyStored()) {
				//Energy has changed, but we aren't processing...so we're probably processing
				angle = (System.currentTimeMillis() / 5) % 360;
			}*/
		}
		lastProcessing[1] = lastProcessing[0];
		lastProcessing[0] = isProcessing;
		
		if(!world.isRemote && validMultiblock && super.getMaster().equals(this)) {
			if(lastEnergy != energy.getEnergyStored() || (lastProcessing[1] != isProcessing)) {
				updateValues();
			} 
			
			if(energy.extractEnergy(ENERGY_PER_TICK, true) > 0) {
				ItemStack input = in.getStackInSlot(0);

				//If we have energy, begin processing
				if(input.isEmpty()) {
					setProcessing(false);
				}
				if(isProcessing) {
					if(canProcess()) {
						//Remove requisite amount of energy, increment counter
						energy.extractEnergy(ENERGY_PER_TICK, false);
	
						if(currentRecipeTicksElapsed >= currentRecipeTicks) {
							//Process recipe
							//TODO: Add implementation
							processItem();
							setCurrentInput(ItemStack.EMPTY);
							setCurrentCount(0);
							setProcessing(false);
							
						} else {
							currentRecipeTicksElapsed++;
						}
						
					}
					//If we can't process, do nothing. 
				} else {
					//Start processing next item
					if(canProcess()) {
						setCurrentRecipeTicksElapsed(0);
						setCurrentRecipeTicks(RecipeCentrifuge.getCentrifugeTicks(input));
						setProcessing(true);
						
					} else {
						setCurrentOutput(ItemStack.EMPTY);
						setCurrentInput(ItemStack.EMPTY);
					}
				}
			} else {
				if(this.isProcessing) {
					this.isProcessing = false;
					updateValues();
				}
			}
		} 
		lastEnergy = energy.getEnergyStored();

	}
		
	private void updateValues() {
		MessageEnergyUpdate message = new MessageEnergyUpdate(energy, this, isProcessing);
		PacketHandler.NETWORK.sendToAllAround(message, 
				new NetworkRegistry.TargetPoint(world.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ(), 64));
	}


	//TODO: Optimize the coding here, it's unnecessarily intensive
	private boolean canProcess() {
		ItemStack input = in.getStackInSlot(0);
		if(currentInput.isEmpty()) {
			setCurrentInput(input);
		}
		if(RecipeCentrifuge.hasRecipe(input)) {
			setCurrentCount(RecipeCentrifuge.getInputCount(input));
			if(in.extractItem(0, currentCount, true).getCount() >= currentCount) {
				//If we have enough input to process, continue on
				ItemStack output = out.getStackInSlot(0);
				currentOutput = RecipeCentrifuge.getCentrifugeOutput(input);
				if(output.isEmpty()) {
					//If there is no output
					return true;
				} else {
					if(output.isItemEqual(currentOutput)) {
						if(out.insertItem(0, currentOutput, true).isEmpty()) {
							//If the items are the same, and we can stack them
							return true;
						}
					}
				}
			}
		} 
		return false;
	}
	
	public void processItem() {
		ItemStack output = out.getStackInSlot(0);
		if(output.isEmpty()) {
			//If output slot is empty, make this simple
			out.insertItem(0, currentOutput, false);
		} else {
			if(output.isItemEqual(currentOutput)) {
				out.insertItem(0, currentOutput, false);
			}
		}
		in.extractItem(0, RecipeCentrifuge.getInputCount(currentInput), false);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public net.minecraft.util.math.AxisAlignedBB getRenderBoundingBox() {
		IBlockState state = world.getBlockState(pos);
		if (state.getPropertyKeys().contains(MultiblockComponent.MULTI_BLOCK)) {
			if (state.getValue(MultiblockComponent.MULTI_BLOCK) == MultiTypeEnum.CENTER) {
				BlockPos minPos = pos.subtract(new BlockPos(0, 1, 0));
				BlockPos maxPos = pos.add(new BlockPos(1, 3, 1));
				return new AxisAlignedBB(minPos, maxPos);
			} else {
				return super.getRenderBoundingBox();
			}
		} else {
			return super.getRenderBoundingBox();
		}

	}
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// 												GETTERS AND SETTERS
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	
	public int getCurrentRecipeTicks() {
		return currentRecipeTicks;
	}


	public void setCurrentRecipeTicks(int currentRecipeTicks) {
		this.currentRecipeTicks = currentRecipeTicks;
	}


	public int getCurrentRecipeTicksElapsed() {
		return currentRecipeTicksElapsed;
	}


	public void setCurrentRecipeTicksElapsed(int currentRecipeTicksElapsed) {
		this.currentRecipeTicksElapsed = currentRecipeTicksElapsed;
	}


	public boolean isProcessing() {
		return isProcessing;
	}


	public void setProcessing(boolean isProcessing) {
		this.isProcessing = isProcessing;
	}


	public ItemStack getCurrentOutput() {
		return currentOutput;
	}


	public void setCurrentOutput(ItemStack currentOutput) {
		this.currentOutput = currentOutput;
	}


	public ItemStack getCurrentInput() {
		return currentInput;
	}


	public void setCurrentInput(ItemStack currentInput) {
		this.currentInput = currentInput;
	}


	public int getCurrentCount() {
		return currentCount;
	}


	public void setCurrentCount(int currentCount) {
		this.currentCount = currentCount;
	}


	public IItemHandler[] getInventory() {
		return new IItemHandler[]{in, out};
	}

    public ItemStackHandler getIn() {
		return in;
	}


	public void setIn(ItemStackHandler in) {
		this.in = in;
	}


	public ItemStackHandler getOut() {
		return out;
	}


	public void setOut(ItemStackHandler out) {
		this.out = out;
	}


	public EnergyBuffer getEnergy() {
		return energy;
	}


	public void setEnergy(EnergyBuffer energy) {
		this.energy = energy;
	}


	public void updateEnergy(int e, int c) {
		if(energy == null) {
			energy = new EnergyBuffer(e, c);
		} else {
			lastEnergy = energy.getEnergyStored();
			energy.setEnergy(e);
			energy.setCapacity(c);
		}
	}


	@Override
	public CapabilityNetworkEndpointMultiblock getNetworkEndpoint() {
		return endpoint;
	}


	@Override
	public void setNetworkEndpoint() {
		endpoint = new CapabilityNetworkEndpointCentrifuge(world, this);
	}


}
