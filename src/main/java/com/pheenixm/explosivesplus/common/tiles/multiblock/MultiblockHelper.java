package com.pheenixm.explosivesplus.common.tiles.multiblock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntityPluripotent;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.tiles.generator.Tier1Generator;

import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

//TODO: Size in here is always zero: why? 
public class MultiblockHelper extends MultiblockStructure {
	
	private int offset = 0;
	public int structureIndex = 0;
	
	public static HashSet<Integer> validSizes = new HashSet<Integer>();
	
	public MultiblockHelper(World w, ArrayList<IBlockState> mats, TileEntityMultiblock te) {
		super(w, mats, te);
	}
	public MultiblockHelper(MultiblockStructure from,
			ArrayList<IBlockState> materials) {
		super(from, materials);
	}
	/**
	 * Add all possible structures to this structure's map
	 * We will use these to verify our structure
	 */
	@Override
	public HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> defineStructure() {
		HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> defs = new HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>>(1);
		
		ArrayList<IBlockState> materials = new ArrayList<IBlockState>(
				Arrays.asList(TileEntitySilo.mats));
		MultiblockStructure struc = new SiloStructure(world, materials, null);
		struc.defineMaterials(materials);
		addStrucToDefs(defs, struc);

		materials = new ArrayList<IBlockState>(Arrays.asList(TileEntityCentrifuge.mats));
		struc = new CentrifugeStructure(world, materials, null);
		struc.defineMaterials(materials);
		addStrucToDefs(defs, struc);
		
		materials = new ArrayList<IBlockState>(Arrays.asList(Tier1Generator.mats));
		struc = new GenTier1Structure(world, materials, null);
		struc.defineMaterials(materials);
		addStrucToDefs(defs, struc);
		
		//TODO: ADD GENERATORS

		return defs;
	}
	
	private void addStrucToDefs(HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> defs, MultiblockStructure struc) {
		HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> specDefs = struc.defineStructure();
		for(int i = offset; i < offset + 6; i++) {
			HashMap<Integer, HashMap<Integer, BlockContainer>> singleDef = specDefs.get(i % 6);
			defs.put(i, singleDef);
		}
		
		validSizes.add(struc.sizeDef);
		
		offset += 6;
	}

	@Override
	public boolean isStructureValid() {
		boolean isValid = false;
		Iterator<Integer> iter = structureDef.keySet().iterator();
		while(!isValid && iter.hasNext()) {
			int index = iter.next();
			HashMap<Integer, HashMap<Integer, BlockContainer>> definition = structureDef.get(index);
			//This check only works if the structures are different heights. Need to fix this
			//TODO: IMPROVE THIS

			if(definition != null && definition.size() == (yMax - yMin + 1)) {
				isValid = true;
				facing = EnumFacing.byIndex(index);
				structureIndex = index;
				
				for(int i : relativeStructure.keySet()) {
					HashMap<Integer, BlockContainer> level = relativeStructure.get(i);
					HashMap<Integer, BlockContainer> defLevel = definition.get(i);
					if(defLevel == null) {
						//This level isn't mapped in the structure definition
						if(level.size() > 0) {
							isValid = false;
						} else {
							relativeStructure.remove(i);
							structure.remove(i+ yMin);
						}
					}
		
					for(int key : level.keySet()) {
						BlockContainer blockReal = level.get(key);
						BlockContainer blockDef = defLevel.get(key);
						
						
						if(blockDef == null || blockReal == null || !isValid(blockDef, blockReal)) {
							//System.out.println("Block is invalid at " + blockReal.pos + " with state " + blockReal.state);
							if(blockDef != null) {
								//System.out.println("Block is invalid at " + blockDef.pos + " with state " + blockDef.state);
							}
							//Either the block doesn't exist in the definition
							//Or the block is and invalid type
							//TODO: Alert player that block is invalid type
							isValid = false;
						}
					} 
				}
			}
		}
		return isValid;
	}

	@Override
	public void defineMaterials(ArrayList<IBlockState> mats) {

	}

	@Override
	public boolean hasValidTile(BlockPos childPos, boolean checkTiles) {
		IBlockState state = world.getBlockState(childPos);
		if(state.getBlock().hasTileEntity(state)) {
			TileEntity te = world.getTileEntity(childPos);
			if(te instanceof TileEntityMultiblock) {
				TileEntityMultiblock pluri = (TileEntityMultiblock) te;
				if(checkTiles && pluri.getMaster().equals(tile)) {
					return true;
				}
				if(!checkTiles && !pluri.validMultiblock) {
					return true;
				}
			}
		}
		return false;
	}
}
