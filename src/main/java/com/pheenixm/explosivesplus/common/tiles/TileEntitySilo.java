package com.pheenixm.explosivesplus.common.tiles;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.GuiHandler;
import com.pheenixm.explosivesplus.common.GuiHandler.GuiEnum;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkEndpointMultiblock;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkEndpointSilo;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityRegistry;
import com.pheenixm.explosivesplus.common.entities.missiles.EntityBallisticMissile;
import com.pheenixm.explosivesplus.common.entities.missiles.FauxEntityBallisticMissile;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockStructure;
import com.pheenixm.explosivesplus.common.tiles.multiblock.SiloStructure;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public class TileEntitySilo extends TileEntityMultiblock implements ITickable {

	public static final IBlockState mats[] = {ExplosivesPlusHolder.CONCRETE.getDefaultState(),
			ExplosivesPlusHolder.IRON_SLAB.getDefaultState(),
			ExplosivesPlusHolder.CONCRETE.getDefaultState()
					.withProperty(MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.CENTER),
			ExplosivesPlusHolder.IRON_SLAB.getDefaultState()
					.withProperty(MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.CENTER),
			ExplosivesPlusHolder.CONCRETE.getDefaultState().withProperty(
					MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.STRUCTURE),
			ExplosivesPlusHolder.IRON_SLAB.getDefaultState().withProperty(
					MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.STRUCTURE)};
	// Add variables to contain missile, etc.
	//TODO: Bind this to the capability
	private ItemStackHandler inventory = new ItemStackHandler(3);

	public WeakReference<FauxEntityBallisticMissile> missile;
	public int missileId;

	
	
	// This value increases to about 135 as the silo opens
	// Should be controlled by the silo's missile
	public float siloAngle = 0;
	
	public boolean isLaunching = false;
	
	protected CapabilityNetworkEndpointSilo endpoint;

	public TileEntitySilo() {
		super();
	}
	
	@Override
	protected MultiblockStructure setStructure() {
		ArrayList<IBlockState> materials = new ArrayList<IBlockState>(
				Arrays.asList(mats));
		return new SiloStructure(world, materials, this);
	}

	@Override
	public boolean onMultiTileActivated(TileEntityMultiblock tile,
			EntityPlayer player, EnumHand hand, EnumFacing side, float xHit,
			float yHit, float zHit) {
		if(tile.validMultiblock) {
			if(player.isSneaking()) {
			} else {
				if(!world.isRemote) {
					player.openGui(ExplosivesPlus.instance, GuiEnum.SILO.ordinal(), world, pos.getX(), pos.getY(), pos.getZ());
					System.out.println(getMaster().getNetworkEndpoint().getSharedUid().toString());
				} 
			}
			return true;
		} else {
			return false;
		}
	}

	private void initializeMissile() {
		if(missileId == 0) {
			System.out.println("No missile present");
			if(!world.isRemote) {
				missile = new WeakReference<FauxEntityBallisticMissile>(new FauxEntityBallisticMissile(world, this, new Vec3d(BlockPos.ORIGIN)));
				FauxEntityRegistry.getRegistry(world).spawnEntity(missile	.get());
				missileId = missile.get().entityId;
			}
		} else {
			if(FauxEntityRegistry.getRegistry(world).entityMap.isEmpty()) {
				FauxEntityRegistry.getRegistry(world);
				FauxEntityRegistry.load(world);
			}
			missile = new WeakReference<FauxEntityBallisticMissile>(FauxEntityRegistry.getRegistry(world).<FauxEntityBallisticMissile>getEntity(missileId));
		}
	}

	@Override
	public void onMultiTileClicked(TileEntityMultiblock tile,
			EntityPlayer player) {

	}
	

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state,
			EntityLivingBase placer, ItemStack stack) {
		this.onBlockPlaced(worldIn, pos, state, placer, stack);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public net.minecraft.util.math.AxisAlignedBB getRenderBoundingBox() {
		IBlockState state = world.getBlockState(pos);
		if (state.getPropertyKeys().contains(MultiblockComponent.MULTI_BLOCK)) {
			if (state.getValue(MultiblockComponent.MULTI_BLOCK) == MultiTypeEnum.CENTER) {
				BlockPos minPos = pos.subtract(new BlockPos(2, 0, 2));
				BlockPos maxPos = pos.add(new BlockPos(2, 20, 2));
				return new AxisAlignedBB(minPos, maxPos);
			} else {
				return super.getRenderBoundingBox();
			}
		} else {
			return super.getRenderBoundingBox();
		}

	}
	
	
	public AxisAlignedBB getBoundingBox() {
		BlockPos center = super.getMaster().centerPosition;
		if(!world.isRemote) {
			center = getMaster().centerPosition;
			BlockPos minPos = center.subtract(new BlockPos(2, 0, 2));
			BlockPos maxPos = center.add(new BlockPos(2, 20, 2));
			return new AxisAlignedBB(minPos, maxPos);
		} else {
			return this.getTile(center).getRenderBoundingBox();
		}
	}

	
	@Override
	@Nullable
	public TileEntitySilo getMaster() {
		if(master instanceof TileEntitySilo) {
			return (TileEntitySilo) super.getMaster();
		} else {
			return null;
		}
	}
	
	@Override
	protected void validateMultiblock() {
		super.validateMultiblock();
		//Most convenient place to perform this
		if(this == super.getMaster()) {
			System.out.println("Initializing missile");
			initializeMissile();
		}
	}
	
	@Override
	protected void invalidateMultiblock() {
		super.invalidateMultiblock();
		if(this == master) {
			for(int i = 0; i < inventory.getSlots(); i++) {
				if(inventory.getStackInSlot(i) != null && !inventory.getStackInSlot(i).isEmpty()) {
					Block.spawnAsEntity(world, this.centerPosition.add(new BlockPos(0,1,0)), inventory.getStackInSlot(i));
					inventory.setStackInSlot(i, ItemStack.EMPTY);
				}
			}
			if(missile != null && !missile.get().launched) {
				//Only kill the missile if it is still in the silo
				missile.get().setDead();
			}
		}
	}

	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? validMultiblock : super.hasCapability(capability, facing);
	}
	
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
    		if(master == null || master.centerPosition == null || master.core == null) {
    			return super.getCapability(capability, facing);
    		}
        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
        {
        		if(this.getPos() == master.getPos()) {
        			return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(inventory);
        		} 	
        }
        return super.getCapability(capability, facing);
    }

	public ItemStackHandler getInventory() {
		if(inventory == null) {
			inventory = new ItemStackHandler(3);
			System.out.println("Null inventory");
		}
		return inventory;
	}

	public void setInventory(ItemStackHandler inventory) {
		this.inventory = inventory;
	}

	@Override
	public void deserialize(NBTTagCompound tag, boolean isPacket) {
		super.deserialize(tag, isPacket);
		inventory.deserializeNBT(tag.getCompoundTag("inventory"));
		isLaunching = tag.getBoolean("launching");
		siloAngle = tag.getFloat("angle");
		missileId = tag.getInteger("entId");
	}
	
	@Override
	public NBTTagCompound serialize(NBTTagCompound tag, boolean isPacket) {
		tag = super.serialize(tag, isPacket);
		tag.setInteger("entId", missileId);
		tag.setTag("inventory", inventory.serializeNBT());
		tag.setBoolean("launching", this.isLaunching);
		tag.setFloat("angle", siloAngle);
		return tag;
	}

	@Override
	/**
	 * Only ever called on master
	 */
	public void onInventoryClosed() {
		if(world.isRemote) {
			//We don't want this doing anything client-side
			return;
		}
		if(missile == null || missile.get().isDead) {
			this.initializeMissile();
		}
		if(!world.isRemote) {
			missile.get().setWarhead(inventory.getStackInSlot(0));
			missile.get().setFuselage(inventory.getStackInSlot(1));
			missile.get().setEngine(inventory.getStackInSlot(2));
			
			handleLaunch(new BlockPos(getPos().getX() - 256, 64, getPos().getZ()+256));
		}
	}
	
	public void handleLaunch(BlockPos target) {
		if(missile.get().canLaunch()) {
			//missile.setLaunched(true);
			if(target != null) {
				missile.get().updateTarget(target.getX(), target.getZ());
			}
			missile.get().launched = true;
			missile = null;
			missileId = 0;
		}

	}

	@Override
	/**
	 * This should ONLY ever be called on the master
	 */
	public CapabilityNetworkEndpointMultiblock getNetworkEndpoint() {
		return endpoint;
	}

	@Override
	public void setNetworkEndpoint() {
		endpoint = new CapabilityNetworkEndpointSilo(world, this);
	}

}
