package com.pheenixm.explosivesplus.common.tiles;

import com.pheenixm.explosivesplus.common.blocks.C4;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityC4 extends TileEntity {

	private boolean detonate;
	private String username = "";

	public TileEntityC4() {
		detonate = false;
	}

	public void detonate(World world, BlockPos pos) {
		if(!world.isRemote) {			

			if(world.getBlockState(pos).getBlock() instanceof C4)
			{
				C4 block = (C4)world.getBlockState(pos).getBlock();
				block.detonate(world, pos);
				this.world.removeTileEntity(this.pos);
				this.invalidate();
			}
		}
	}

	
	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);
		detonate = nbttagcompound.getBoolean("detonate");
		username = nbttagcompound.getString("username");
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbttagcompound) {
		super.writeToNBT(nbttagcompound);
		nbttagcompound.setBoolean("detonate", detonate);
		nbttagcompound.setString("username", username);
		return nbttagcompound;
	}
	
	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
	{
	    return (oldState.getBlock() != newState.getBlock());
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}
}
