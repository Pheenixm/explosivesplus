package com.pheenixm.explosivesplus.common.tiles.multiblock;

import java.util.ArrayList;
import java.util.HashMap;

import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.tiles.generator.Tier1Generator;

import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class GenTier1Structure extends MultiblockStructure {

	private IBlockState concrete;
	private IBlockState linear;
	private IBlockState iron_slab;
	private IBlockState io_block;
	private IBlockState linear_slab;
	private int strucSize = 0;

	
	public GenTier1Structure(World w, ArrayList<IBlockState> mats, Tier1Generator te) {
		super(w, mats, te);
	}
	
	public GenTier1Structure(MultiblockStructure struc, ArrayList<IBlockState> mats) {
		super(struc, mats);
	}


	@Override
	public HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> defineStructure() {
		HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> defs = new HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>>(4);
		strucSize = 0;
		//West is default
		HashMap<Integer, HashMap<Integer, BlockContainer>> defSouth = new HashMap<Integer, HashMap<Integer, BlockContainer>>();
		buildLevel0(defSouth);
		buildLevel1(defSouth);
		buildLevel2(defSouth);
		buildLevel3(defSouth);
		
		HashMap<Integer, HashMap<Integer, BlockContainer>> defWest = rotateClockwise(defSouth);
		HashMap<Integer, HashMap<Integer, BlockContainer>> defNorth = rotateClockwise(defWest);
		HashMap<Integer, HashMap<Integer, BlockContainer>> defEast = rotateClockwise(defNorth);

		defs.put(EnumFacing.WEST.getIndex(), defWest);
		defs.put(EnumFacing.NORTH.getIndex(), defNorth);
		defs.put(EnumFacing.EAST.getIndex(), defEast);
		defs.put(EnumFacing.SOUTH.getIndex(), defSouth);

		this.sizeDef = strucSize;
		
		return defs;
	}
	


	private HashMap<Integer, HashMap<Integer, BlockContainer>> rotateClockwise( HashMap<Integer, HashMap<Integer, BlockContainer>> def) {
		HashMap<Integer, HashMap<Integer, BlockContainer>> rotation = new HashMap<Integer, HashMap<Integer, BlockContainer>>();
		for(int level : def.keySet()) {
			HashMap<Integer, BlockContainer> lvl = new HashMap<Integer, BlockContainer>();
			for(int key : def.get(level).keySet()) {
				HashMap<Integer, BlockContainer> oldLevel = def.get(level);
				if(oldLevel != null  && !oldLevel.isEmpty() && oldLevel.containsKey(key)) {
					BlockContainer oldCont = oldLevel.get(key);
					if(oldCont != null && oldCont.pos != null) {
						BlockPos newPos = oldCont.pos.rotate(Rotation.CLOCKWISE_90);
						BlockContainer newCont = new BlockContainer(newPos, def.get(level).get(key).state);
						lvl.put(this.getHashFromPos(newPos), newCont);
					}
				}
			}
			rotation.put(level, lvl);
		}
		return rotation;
	}

	private void buildLevel3(HashMap<Integer, HashMap<Integer, BlockContainer>> defWest) {
		HashMap<Integer, BlockContainer> level = new HashMap<Integer, BlockContainer>();
		for(int i = -1; i <= 1; i++) {
			for(int j = -1; j <= 1; j++) {
				BlockPos pos = new BlockPos(i, 3, j);
				BlockContainer base = null;
				int key = this.getHashFromPos(pos);


				if(i == 0 && j == 0) {
					base = new BlockContainer(pos, linear);
				} else {
					base = new BlockContainer(pos, linear_slab);
				}
				if(base != null) {
					level.put(key, base);
					strucSize++;
				}
			}
		}
		defWest.put(3, level);
	}

	private void buildLevel2(HashMap<Integer, HashMap<Integer, BlockContainer>> defWest) {
		HashMap<Integer, BlockContainer> level = new HashMap<Integer, BlockContainer>();
		for(int i = -1; i <= 1; i++) {
			for(int j = -1; j <= 1; j++) {
				BlockPos pos = new BlockPos(i, 2, j);
				BlockContainer base = null;
				int key = this.getHashFromPos(pos);
				if(j == 1) {
					if((i == 1 || i == -1)) {
						base = new BlockContainer(pos, concrete);
					} else {
						base = new BlockContainer(pos, io_block);
					}
				} else {
					if(i == 0) {
						if(j == -1) {
							base = new BlockContainer(pos, iron_slab);
						}
					} else {
						base = new BlockContainer(pos, linear);
					}
				}
				if(base != null) {
					level.put(key, base);
					strucSize++;
				}
			}
		}
		defWest.put(2, level);
	}

	private void buildLevel1(HashMap<Integer, HashMap<Integer, BlockContainer>> defWest) {
		HashMap<Integer, BlockContainer> level = new HashMap<Integer, BlockContainer>();
		for(int i = -1; i <= 1; i++) {
			for(int j = -1; j <= 1; j++) {
				BlockPos pos = new BlockPos(i, 1, j);
				BlockContainer base = null;
				int key = this.getHashFromPos(pos);


				if(j == 1) {
					base = new BlockContainer(pos, concrete);
				} else {
					if(i == 0) {
						if(j == -1) {
							base = new BlockContainer(pos, iron_slab);
						}
					} else {
						base = new BlockContainer(pos, linear);
					}
				}
						
				if(base != null) {
					level.put(key, base);
					strucSize++;
				}
			}
		}
		defWest.put(1, level);
	}

	private void buildLevel0(HashMap<Integer, HashMap<Integer, BlockContainer>> defWest) {
		HashMap<Integer, BlockContainer> level = new HashMap<Integer, BlockContainer>();
		for(int i = -1; i <= 1; i++) {
			for(int j = -1; j <= 1; j++) {
				BlockPos pos = new BlockPos(i, 0, j);
				BlockContainer base = new BlockContainer(pos, linear_slab);
				int key = this.getHashFromPos(pos);
				level.put(key, base);
				strucSize++;
			}
		}
		defWest.put(0, level);
		
	}

	@Override
	public void defineMaterials(ArrayList<IBlockState> mats) {
		concrete = mats.get(0);
		linear = mats.get(1);
		iron_slab = mats.get(2);
		io_block = mats.get(3);
		linear_slab = mats.get(4);
	}

	@Override
	public boolean hasValidTile(BlockPos childPos, boolean checkTiles) {
		IBlockState state = world.getBlockState(childPos);
		if(state.getBlock().hasTileEntity(state)) {
			TileEntity te = world.getTileEntity(childPos);
			if(te instanceof TileEntityMultiblock) {
				TileEntityMultiblock silo = (TileEntityMultiblock) te;
				if(checkTiles && silo.getMaster().equals(tile)) {
					return true;
				}
				if(!checkTiles && !silo.validMultiblock) {
					return true;
				}
				System.out.println("No valid tile?");
			}
		} else {
			System.out.println("This block just doesn't have a tile");
		}
		return false;
	}

}
