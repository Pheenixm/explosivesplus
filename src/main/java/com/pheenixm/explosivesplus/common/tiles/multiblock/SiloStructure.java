package com.pheenixm.explosivesplus.common.tiles.multiblock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class SiloStructure extends MultiblockStructure {

	// TODO: FIX THIS
	// TODO: Implement in such a way that controllers extend structural blocks,
	// s/t they are recognized as valid block types for structure
	private IBlockState concrete = null;//BlockStructural.class instance
	private IBlockState steel = null;//BlockAccessory.class instance
	private int strucSize = 0;

	public SiloStructure(World w, ArrayList<IBlockState> mats, TileEntitySilo te) {
		super(w, mats, te);
	}
	
	public SiloStructure(MultiblockStructure struc, ArrayList<IBlockState> mats) {
		super(struc, mats);
	}


	@Override
	/**
	 * Structure in relative coords
	 */
	public HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> defineStructure() {
		HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> defs = new HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>>(1);
		HashMap<Integer, HashMap<Integer, BlockContainer>> def = new HashMap<Integer, HashMap<Integer, BlockContainer>>();
		strucSize = 0;
		for (int i = 0; i <= 20; i++) {
			switch (i) {
				case 0 :
					buildBase(def, i);
					break;
				case 20 :
					buildTop(def, i);
					break;
				default :
					buildWalls(def, i);
			}

		}
		this.sizeDef = strucSize;
		defs.put(EnumFacing.UP.getIndex(), def);
		return defs;
	}

	private void buildWalls(
			HashMap<Integer, HashMap<Integer, BlockContainer>> def, int i) {
		HashMap<Integer, BlockContainer> level = new HashMap<Integer, BlockContainer>();
		for(int x = -2; x <= 2; x++) {
			for(int z = -2; z <= 2; z++) {
				if(/*Math.abs(x * z) != 4 &&*/ (Math.abs(x) > 1 || Math.abs(z) > 1)) {
					BlockPos pos = new BlockPos(x, i, z);
					BlockContainer base = new BlockContainer(pos, concrete);
					int key = this.getHashFromPos(pos);
					level.put(key, base);
					strucSize++;
				}
			}
		}
		def.put(i, level);
	}

	private void buildTop(
			HashMap<Integer, HashMap<Integer, BlockContainer>> def, int i) {
		HashMap<Integer, BlockContainer> level = new HashMap<Integer, BlockContainer>();
		for (int x = -2; x <= 2; x++) {
			for (int z = -2; z <= 2; z++) {
				if (Math.abs(x * z) != 4) {
					BlockPos pos = new BlockPos(x, i, z);
					BlockContainer base = new BlockContainer(pos, steel);
					int key = this.getHashFromPos(pos);
					level.put(key, base);
					strucSize++;
				}
			}
		}
		def.put(i, level);
	}

	private void buildBase(
			HashMap<Integer, HashMap<Integer, BlockContainer>> def, int i) {
		HashMap<Integer, BlockContainer> level = new HashMap<Integer, BlockContainer>();
		for (int x = -2; x <= 2; x++) {
			for (int z = -2; z <= 2; z++) {
				//if (Math.abs(x * z) != 4) {
					BlockPos pos = new BlockPos(x, i, z);
					BlockContainer base = new BlockContainer(pos, concrete);
					int key = this.getHashFromPos(pos);
					level.put(key, base);
					strucSize++;
				//}
			}
		}
		def.put(i, level);
	}

	@Override
	public void defineMaterials(ArrayList<IBlockState> mats) {
		concrete = mats.get(0);
		steel = mats.get(1);
	}

	@Override
	//TODO: This returns false when it shouldn't
	//silo.getMaster().equals returns false when it shouldn't
	public boolean hasValidTile(BlockPos childPos, boolean checkTiles) {
		IBlockState state = world.getBlockState(childPos);
		if(state.getBlock().hasTileEntity(state)) {
			TileEntity te = world.getTileEntity(childPos);
			if(te instanceof TileEntityMultiblock) {
				TileEntityMultiblock silo = (TileEntityMultiblock) te;
				if(checkTiles && silo.getMaster().equals(tile)) {
					return true;
				}
				if(!checkTiles && !silo.validMultiblock) {
					return true;
				}
				System.out.println("No valid tile?");
			}
		} else {
			System.out.println("This block just doesn't have a tile");
		}
		return false;
	}
	
}