package com.pheenixm.explosivesplus.common.tiles.generator;

import java.util.ArrayList;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.EnumTier;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.GuiHandler.GuiEnum;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkEndpointCentrifuge;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkEndpointGenerator;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkEndpointMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntityGenerator;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.tiles.multiblock.GenTier1Structure;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockStructure;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk.EnumCreateEntityType;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import scala.actors.threadpool.Arrays;

public class Tier1Generator extends TileEntityGenerator<EntityCreeper> {

	public static final IBlockState mats[] = {
			ExplosivesPlusHolder.CONCRETE.getDefaultState(),
			ExplosivesPlusHolder.LINEAR_EM_BLOCK.getDefaultState(),
			ExplosivesPlusHolder.IRON_SLAB.getDefaultState(),
			ExplosivesPlusHolder.IN_OUT_BLOCK.getDefaultState(),
			ExplosivesPlusHolder.LINEAR_EM_SLAB.getDefaultState()};
	private CapabilityNetworkEndpointGenerator endpoint;
	
	public Tier1Generator() {
		super(EnumTier.TIER_1);
	}

	@Override
	public AxisAlignedBB getBoundingBox() {
		BlockPos center = super.getMaster().centerPosition;
		if(!world.isRemote) {
			if(getMaster() == null || getMaster().core == null) {
				return new AxisAlignedBB(pos, pos.add(1,1,1));
			}
			center = getMaster().core.centerPos;
			BlockPos minPos = center.subtract(new BlockPos(1, 0, 1));
			BlockPos maxPos = center.add(new BlockPos(1, 3, 1));
			return new AxisAlignedBB(minPos, maxPos);
		} else {
			return this.getTile(center).getRenderBoundingBox();
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public net.minecraft.util.math.AxisAlignedBB getRenderBoundingBox() {
		IBlockState state = world.getBlockState(pos);
		if (state.getPropertyKeys().contains(MultiblockComponent.MULTI_BLOCK)) {
			if (state.getValue(MultiblockComponent.MULTI_BLOCK) == MultiTypeEnum.CENTER) {
				BlockPos minPos = pos.subtract(new BlockPos(1, 0, 1));
				BlockPos maxPos = pos.add(new BlockPos(1, 3, 1));
				return new AxisAlignedBB(minPos, maxPos);
			} else {
				return super.getRenderBoundingBox();
			}
		} else {
			return super.getRenderBoundingBox();
		}

	}
	
	@Override
	@Nullable
	public Tier1Generator getMaster() {
		if(master instanceof Tier1Generator) {
			return (Tier1Generator) super.getMaster();
		} else {
			return null;
		}
	}



	@Override
	protected EntityCreeper getGeneratorMob() {
		return new EntityCreeper(world);
	}

	@Override
	public ItemStack getFuel() {
		return new ItemStack(Items.GUNPOWDER, 1);
	}

	@Override
	public int getProcessingTime() {
		return 100;
	}

	@Override
	public Vec3d getItemPos() {
		if(getMaster() == null) {
			return new Vec3d(0,0,0);
		} else {
			return new Vec3d(0, 2.5, 0);
			/*
			BlockPos center = getMaster().core.centerPos;
			switch(facing) {
				case SOUTH:
					return center.add(new BlockPos(0.5,2,0.5));
				case WEST:
					return center.add(new BlockPos(-0.5,2,-0.5).rotate(Rotation.CLOCKWISE_90));
				case NORTH:
					return center.add(new BlockPos(-0.5,2,-0.5).rotate(Rotation.CLOCKWISE_180));
				case EAST:
					return center.add(new BlockPos(-0.5,2,-0.5).rotate(Rotation.COUNTERCLOCKWISE_90));
				default:
					return new BlockPos(0,-1,0);
			}*/
		}
	}

	@Override
	public void onInventoryClosed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected MultiblockStructure setStructure() {
		ArrayList<IBlockState> materials = new ArrayList<IBlockState>(Arrays.asList(mats));
		return new GenTier1Structure(world, materials, this);
	}

	@Override
	public boolean onMultiTileActivated(TileEntityMultiblock tile, EntityPlayer player, EnumHand hand, EnumFacing side,
			float xHit, float yHit, float zHit) {
		if(tile.validMultiblock) {
			if(player.isSneaking()) {
			} else {
				if(!world.isRemote) {
					player.openGui(ExplosivesPlus.instance, GuiEnum.GENERATOR.ordinal(), world, pos.getX(), pos.getY(), pos.getZ());
					System.out.println(getMaster().getNetworkEndpoint().getSharedUid().toString());
					System.out.println(getMaster().getNetworkEndpoint().getStatus());
				} 
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onMultiTileClicked(TileEntityMultiblock tile, EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CapabilityNetworkEndpointMultiblock getNetworkEndpoint() {
		return endpoint;
	}

	@Override
	public void setNetworkEndpoint() {
		endpoint = new CapabilityNetworkEndpointGenerator(world, this);
		
	}

	@Override
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		this.onBlockPlaced(worldIn, pos, state, placer, stack);
	}

}
