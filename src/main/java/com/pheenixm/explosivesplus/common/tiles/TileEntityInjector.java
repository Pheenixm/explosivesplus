package com.pheenixm.explosivesplus.common.tiles;

import java.util.UUID;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import com.pheenixm.explosivesplus.api.energy.EnergyBuffer;
import com.pheenixm.explosivesplus.common.blocks.PowerInjector;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.tiles.TileEntityCable;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

//TODO: Add !world.isRemote checks
@Deprecated
public class TileEntityInjector extends TileEntityBase implements ITickable {

	public EnumFacing facing;
	private TileEntityCable tileInjector;
	private TileEntity otherTile;
	private EnergyBuffer buffer = new EnergyBuffer(TRANSFER_RATE);
	private UUID networkId = UUID.randomUUID();
	private int ticksInWorld;

	public static final int TRANSFER_RATE = 100;
	public static final int UPDATE_FREQ = 100;

	public TileEntityInjector() {
	}

	@Override
	public void deserialize(NBTTagCompound tag, boolean isPacket) {
		facing = EnumFacing.byIndex(tag.getInteger("facing"));
        CapabilityEnergy.ENERGY.readNBT(buffer, null, tag.getTag("storedEnergy"));
	}

	@Override
	public NBTTagCompound serialize(NBTTagCompound tag, boolean isPacket) {
		tag.setInteger("facing", facing.getIndex());
		tag.setTag("storedEnergy", CapabilityEnergy.ENERGY.writeNBT(buffer, null));
		return tag;
	}

	@Override
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		checkForCable();
		facing = worldIn.getBlockState(pos).getValue(PowerInjector.FACING);
		System.out.println(facing);
	}

	public void neighborChanged(IBlockState state, IBlockAccess world, BlockPos pos, Block blockIn, BlockPos fromPos) {
		checkForCable();
	}

	protected void checkForCable() {
		if (!world.isRemote && pos != null) {
			TileEntity te = world.getTileEntity(pos.offset(facing));
			if (te instanceof TileEntityCable) {
				tileInjector = (TileEntityCable) te;
				otherTile = null;
				networkId = tileInjector.getCapability(CapabilityNetwork.COMPONENT, null).getSharedUid();
				tileInjector.updateEnergyRegistry();
			} else {
				if (te == null) {
					if (tileInjector != null) {
						// There was a cable tile there, not anymore though
						tileInjector = null;
					}
				}
				otherTile = te;
			}
		}
	}

	@Override
	public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing face) {
		//Controls connection to cables and exposes buffer
		if (face == facing || face == facing.getOpposite()) {
			return capability == CapabilityEnergy.ENERGY;
		}
		return super.hasCapability(capability, face);
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing face) {
		if ((face == facing.getOpposite() || face == facing) && capability == CapabilityEnergy.ENERGY) {
			return (T) buffer;
		}
		return super.getCapability(capability, face);
	}

	@Override
	public void onLoad() {
		//checkForCable();
		facing = world.getBlockState(pos).getValue(PowerInjector.FACING);
		System.out.println(world.getBlockState(pos.offset(facing)).getBlock().getLocalizedName());
		buffer.setEnergy(1000);
	}

	@Override
	/**
	 * Prioritizes tiles with the lowest energy
	 */
	public void update() {
		ticksInWorld++;
		if (!world.isRemote) {
			if(tileInjector == null && otherTile == null && ticksInWorld % UPDATE_FREQ == 0) {
				checkForCable();
			}
			if (buffer.getEnergyStored() > 0) {
				IEnergyStorage store = null;
				if (otherTile != null && otherTile.hasCapability(CapabilityEnergy.ENERGY, facing.getOpposite())) {
					store = otherTile.getCapability(CapabilityEnergy.ENERGY, facing.getOpposite());
				}
				if (tileInjector != null && !tileInjector.isInvalid()) {
					// Check for network changes, update energy registry
					if(!tileInjector.energyRegistry.isEmpty()) {
						store = tileInjector.getLowestEnergy();
					} 
				}
				if (store != null && store.canReceive() && buffer.canExtract()) {
					if(store.receiveEnergy(buffer.extractEnergy(TRANSFER_RATE, true), true) > 0) {
						store.receiveEnergy(buffer.extractEnergy(TRANSFER_RATE, false), false);
					}
				}
			}
		}
	}

}
