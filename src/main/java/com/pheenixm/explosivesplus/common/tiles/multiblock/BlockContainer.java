package com.pheenixm.explosivesplus.common.tiles.multiblock;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;

public class BlockContainer {

	public BlockPos pos;
	public IBlockState state;
	
	public BlockContainer(BlockPos posi, IBlockState stat) {
		pos = posi;
		state = stat;
	}

	
}
