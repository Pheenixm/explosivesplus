package com.pheenixm.explosivesplus.common.tiles;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityChaos extends TileEntityBase {

	public String seed;
	
	public TileEntityChaos() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void deserialize(NBTTagCompound tag, boolean isPacket) {
		if(tag.hasKey("seed")) {
			seed = tag.getString("seed");
		}
	}

	@Override
	public NBTTagCompound serialize(NBTTagCompound tag, boolean isPacket) {
		if(seed != null) {
			tag.setString("seed", seed);
			System.out.println(seed);
		}
		return tag;
	}

	@Override
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		// TODO Auto-generated method stub
		
	}

}
