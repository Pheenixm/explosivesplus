package com.pheenixm.explosivesplus.common.tiles;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkEndpointMultiblock;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockHelper;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockStructure;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityPluripotent extends TileEntityMultiblock {

	//TODO: Modify
	public static final IBlockState mats[] = {
			ExplosivesPlusHolder.EM_BLOCK.getDefaultState(),
			ExplosivesPlusHolder.IRON_SLAB.getDefaultState(),
			ExplosivesPlusHolder.CONCRETE.getDefaultState(),
			ExplosivesPlusHolder.LINEAR_EM_BLOCK.getDefaultState(),
			ExplosivesPlusHolder.LINEAR_EM_SLAB.getDefaultState(),
			ExplosivesPlusHolder.IN_OUT_BLOCK.getDefaultState()
			
	};

	
	public TileEntityPluripotent() {
		super();
	}
	
	//TODO: FIX INFINITE RECURSION
	public void onBlockPlaced(World worldIn, BlockPos pos, IBlockState state,
			EntityLivingBase placer, ItemStack stack) {
		if(!world.isRemote) {
			super.onBlockPlaced(worldIn, pos, state, placer, stack);
			if(validMultiblock) {
				return;
			} else {
				System.out.println("Not valid");
			}
			LinkedList<TileEntityPluripotent> neighbors = new LinkedList<TileEntityPluripotent>();
			TileEntityMultiblock superior = null;
			for(BlockPos childPos : MultiblockStructure.getChildren(pos)) {
				IBlockState childState = world.getBlockState(childPos);
				if(childState.getBlock().hasTileEntity()) {
					TileEntity te = world.getTileEntity(childPos);
					if(te instanceof TileEntityMultiblock) {
						if(te instanceof TileEntityPluripotent) {
							neighbors.add((TileEntityPluripotent)te);
						} else {
							TileEntityMultiblock multi = (TileEntityMultiblock) te;
							if(!multi.validMultiblock && !multi.getMaster().validMultiblock) {
								superior = multi;
							}
						}
					}
				}
			}
			if(superior != null && !superior.validMultiblock && !superior.getMaster().validMultiblock) {
				TileEntityMultiblock subterior = this.joinSuperior(superior, pos);
				subterior.onBlockPlaced(worldIn, pos, state, placer, stack);
				if(!neighbors.isEmpty()) {
					for(TileEntityPluripotent plur : neighbors) {
						//Will recursively add all pluripotents to the proper tile
						plur.onBlockPlaced(worldIn, pos, state, placer, stack);
					}
				}
				return;
			}
		}
	}

	
	private TileEntityMultiblock joinSuperior(TileEntityMultiblock superior, BlockPos pos) {
		Class<?> clazz = superior.getClass();
		Object superClass = null;
		try {
			Constructor<?> ctor = clazz.getConstructor();
			superClass = ctor.newInstance(new Object[] {});
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(superClass instanceof TileEntityMultiblock) {
			TileEntityMultiblock multi = (TileEntityMultiblock)superClass;
			world.removeTileEntity(pos);
			world.setTileEntity(pos, multi);
			//TODO: Am I missing anything here?
			return multi;
		}
		return null;
	}

	/**
	 * Searches the structure to validate it Also builds the structure's
	 * relative coords if it is valid size
	 * 
	 * @return true if the structure is valid, false otherwise
	 */
	@Override
	protected boolean searchAndValidate(boolean checkTiles) {
		boolean valid = false;
		master.core.search(master.getPos(), true, checkTiles);
		System.out.println(core.sizeDef);
		if (master.isValidSize()) {
			master.core.setCenterAndRelativeCoords();
			valid = master.core.isStructureValid();
			MultiblockHelper castedCore = (MultiblockHelper)(master.core);
			//TODO: Investigate how this can be further abstracted
			int structureId = castedCore.structureIndex;
			tile_id = MultiEnum.getEnumFromStructureId(structureId);
			System.out.println("Is structure valid?: " + valid);
			facing = master.core.facing;
		} else {
			tile_id = MultiEnum.PLURIPOTENT;
		}
		return valid;
	}
	
	@Override
	protected boolean isValidSize() {
		if (core == null) {
			// Edge case
			System.out.println("Core is null, for some reason");
			return false;
		}
		return MultiblockHelper.validSizes.contains(core.getSize());
	}


	@Override
	public void onInventoryClosed() {}
	
	
	@Override
	protected MultiblockStructure setStructure() {
		ArrayList<IBlockState> materials = new ArrayList<IBlockState>(
				Arrays.asList(TileEntityPluripotent.mats));
		return new MultiblockHelper(world, materials, this);
	}
	

	@Override
	public boolean onMultiTileActivated(TileEntityMultiblock tile,
			EntityPlayer player, EnumHand hand, EnumFacing side, float xHit,
			float yHit, float zHit) {
		return false;
	}
	

	@Override
	public void onMultiTileClicked(TileEntityMultiblock tile,
			EntityPlayer player) {}

	@Override
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state,
			EntityLivingBase placer, ItemStack stack) {
		this.onBlockPlaced(worldIn, pos, state, placer, stack);
	}

	@Override
	public CapabilityNetworkEndpointMultiblock getNetworkEndpoint() {
		return null;
	}

	@Override
	public void setNetworkEndpoint() {
		// TODO Auto-generated method stub
		
	}
}
