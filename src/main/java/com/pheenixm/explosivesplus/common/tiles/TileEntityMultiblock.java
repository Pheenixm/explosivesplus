package com.pheenixm.explosivesplus.common.tiles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkEndpointMultiblock;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkEndpointSilo;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageTileReplaced;
import com.pheenixm.explosivesplus.common.tiles.multiblock.BlockContainer;
import com.pheenixm.explosivesplus.common.tiles.multiblock.CentrifugeStructure;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockStructure;
import com.pheenixm.explosivesplus.common.tiles.multiblock.SiloStructure;
import com.pheenixm.explosivesplus.common.util.Utils;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.SPacketChunkData;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.capabilities.Capability;
import scala.actors.threadpool.Arrays;

public abstract class TileEntityMultiblock extends TileEntityBase {

	protected TileEntityMultiblock master;
	protected BlockPos masterPos;
	public boolean isMaster;

	public BlockPos centerPosition;

	public MultiblockStructure core;
	public boolean validMultiblock;
	private NBTTagCompound toDeserialize;
	public EnumFacing facing = EnumFacing.UP;
	public MultiEnum tile_id;


	//TODO: Make invalidation convert back to pluripotent forms
	//TODO: Fix pluripotent joining, and placing next to complete blocks
	public TileEntityMultiblock() {
	}

	@Override
	public void onLoad() {
		if(!world.isRemote) {
			if (masterPos != null && this.master == null) {
				// We have retrieved masterPos yet, but master has not been set
				this.master = this.getMasterFromPos();
	
				if (this.isMaster && toDeserialize != null) {
					NBTTagCompound struc = (NBTTagCompound) toDeserialize
							.getTag("structure");
					this.setMaster(this);
					// Recreate core and read data into it
					core = setStructure();
					core.deserialize(struc);
				}
			}
		} else {
			//Client side
			if(world.getBlockState(pos).getValue(MultiblockComponent.MULTI_BLOCK) != MultiTypeEnum.INVALID) {
				validMultiblock = true;
			}
			
			if (masterPos != null && this.master == null) {
				this.setMaster(this.getMasterFromPos());
			}
			if(toDeserialize != null && toDeserialize.hasKey("center")) {
					centerPosition = new BlockPos(Utils.arrayToVec3i(toDeserialize.getIntArray("center")));
					System.out.println("Center is " + centerPosition);
			}
		}
		if(isMaster && validMultiblock) {
			setNetworkEndpoint();
		}

	}

	public void onBlockPlaced(World worldIn, BlockPos pos, IBlockState state,
			EntityLivingBase placer, ItemStack stack) {
		if(!world.isRemote) {
			tile_id = world.getBlockState(pos).getValue(MultiblockComponent.MULTI_TILE);
			validMultiblock = false;
			isMaster = checkNeighbors(this.pos);
			if (isMaster) {
				// If this is the first multiblock, set it as master
				this.setMaster(this);
				core = setStructure();
				validMultiblock = this.searchAndValidate(true);
				System.out.println("Should form structure");
			} else {
				// Else, alert the "master" multiblock to this change, and
				// update the structure
				System.out.println("Should join structure");
	
				getMaster().notifyChange(this, pos, true);
			}	

		} else {
			System.out.println("Called client side");
		}
	}
	

	// COMMON LOGIC

	/**
	 * Check the neighbors for any blocks that are part of an incomplete
	 * structure Runs setMaster function
	 * 
	 * @param pos
	 * @return true if no other blocks are found, false otherwise
	 */
	private boolean checkNeighbors(BlockPos pos) {
		System.out.println("Placed at " + pos);
		BlockPos[] children = MultiblockStructure.getChildren(pos);

		Queue<BlockPos> validBlocks = new LinkedList<BlockPos>();
		for (BlockPos child : children) {
			TileEntity tile = world.getTileEntity(child);
			if (tile instanceof TileEntityMultiblock) {
				TileEntityMultiblock neighbor = (TileEntityMultiblock) tile;
				if (!neighbor.validMultiblock && neighbor.getMaster() != null) {
					validBlocks.offer(child);
				}

			}
		}
		if(validBlocks.isEmpty()) {
			//There is no neighbor
			setMaster(this);
			return true;
		}
		TileEntityMultiblock neighbor = (TileEntityMultiblock) world
				.getTileEntity(validBlocks.poll());
		if (validBlocks.isEmpty()) {
			//There was only one neighbor
			this.setMaster(neighbor.getMaster());
			return false;
		}
		//There are multiple neighbors
		int masterSize = neighbor.getMaster().core.getSize();
		int currentSize = masterSize;
		while (!validBlocks.isEmpty()) {
			BlockPos child = validBlocks.poll();
			TileEntityMultiblock neb = (TileEntityMultiblock) world.getTileEntity(child);
			currentSize = neb.getMaster().core.getSize();
			
			if(currentSize <= masterSize) {
				//Neighbor is part of another, smaller (or equal) structure. Join it to this one
				MultiblockStructure struc = neb.getMaster().core;
				for(int iter : struc.structure.keySet()) {
					HashMap<Integer, BlockPos> level = struc.structure.get(iter);
					for(BlockPos blocky : level.values()) {
						TileEntityMultiblock local = getTile(blocky);
						if(local.isMaster && !local.getMaster().equals(neighbor.getMaster())) {
							local.isMaster = false;
							local.core = null;
						}
						local.setMaster(neighbor.getMaster());
					}
				}
			}
			if(currentSize > masterSize) {
				//Neighbor was part of a larger structure, set up this structure to join it
				masterSize = currentSize;
				neighbor = neb;
				validBlocks.offer(child);
			}
		}
		this.setMaster(neighbor.getMaster());
		return false;
	}

	/**
	 * Gets the instance of this tile based on the passed in block pos
	 * Also checks if the tile is an instance of this one
	 * @param posi The BlockPos to check
	 * @return the tile if there is one, null otherwise
	 */
	@Nullable
	protected TileEntityMultiblock getTile(BlockPos posi) {
		TileEntity ent = world.getTileEntity(posi);
		if(ent instanceof TileEntityMultiblock) {
			return getTile(ent);
		}
		return null;
	}
	
	/**
	 * Casts a TileEntity to TileEntityMultiblock
	 * @param te
	 * @return
	 */
	protected TileEntityMultiblock getTile(TileEntity te) {
		return (TileEntityMultiblock) te;
	}

	
	/**
	 * Called whenever the containing block is destroyed
	 * @param pos
	 *            The position of the containing block
	 */
	public void onBlockDestroyed(BlockPos destroyed) {
		getMaster().core.removeBlockFromStructure(destroyed);
		
		//Ensures master is moved prior to this check
		getMaster().notifyChange(this, destroyed, false);			

		
		Queue<BlockPos> strucs = new LinkedList<BlockPos>();
		BlockPos[] children = MultiblockStructure.getChildren(destroyed);

		for(BlockPos child : children) {
			TileEntity chi = world.getTileEntity(child);
			if(chi instanceof TileEntityMultiblock) {
				strucs.offer(child);
			}
		}
		
		if(strucs.isEmpty()) {
			//There are no neighbors
			return;
		}
		

		while(!strucs.isEmpty()) {
			BlockPos checkedPos = strucs.poll();
			TileEntityMultiblock chill = getTile(checkedPos);
			//Is there a path to the master from this neighbor?
			boolean pathExists = checkedPos == getMaster().pos ? true : pathTo(checkedPos, chill.getMaster().pos);
			if(!pathExists) {
				//No path to master
				chill.splitMasterTo();
			} else {
				//Else, a path exists
			}
		}
		
	}
		
	/**
	 * Executes a DFS to determine if there is a valid path of 
	 * structural blocks between the two BlockPos objects
	 * @param toBlock the block to path from
	 * @param goalPos the block to path to
	 * @return true if a path exists, false otherwise
	 */
	public boolean pathTo(BlockPos toBlock, BlockPos goalPos) {
		HashMap<Integer, Boolean> visited = new HashMap<Integer, Boolean>();
		//Ensure we won't search over the destroyed block
		visited.put(this.pos.hashCode(), true);
		return pathTo(toBlock, goalPos, visited);
	}
	
	/**
	 * DFS Utility method
	 * @param toBlock the currently examined block to path from
	 * @param goalPos the block to path to
	 * @param visited the list of visited blocks
	 * @return true if a path exists, false otherwise
	 */
    private boolean pathTo(BlockPos toBlock, BlockPos goalPos, HashMap<Integer, Boolean> visited) {
        // Mark the current node as visited and print it
    		setVisited(visited, toBlock);
    		if(toBlock.equals(goalPos)) {
    			return true;
    		}
    		
        // Recur for all the vertices adjacent to this vertex
        BlockPos[] children = MultiblockStructure.getChildren(toBlock);
    		boolean result = false;
        for(BlockPos child : children) {
        		if(!visited.containsKey(child.hashCode())
        				&& getMaster().core.isValidMaterial(world.getBlockState(child))
        				&& getMaster().core.hasValidTile(child, true)
        				&& !result) {
        			result = pathTo(child, goalPos, visited);
        		}
        }
        
        return result;
    }
    
    /**
     * Sets the passed in position as visited on the visited map
     * @param visited
     * @param pos
     */
	private void setVisited(HashMap<Integer, Boolean> visited, BlockPos pos) {
		visited.put(pos.hashCode(), true);
	}



	@Override
	public void deserialize(NBTTagCompound tag, boolean isPacket) {
		// Common between null world and valid world
		BlockPos mas = new BlockPos(Utils.arrayToVec3i(tag.getIntArray("masterPos")));
		this.setMasterPos(mas);

		validMultiblock = tag.getBoolean("valid");
		isMaster = tag.getBoolean("isMaster");
		facing = EnumFacing.byIndex(tag.getInteger("facing"));
		if(tag.hasKey("tile_id")) {
			tile_id = MultiEnum.get(tag.getInteger("tile_id"));
		}

		if (world != null) {
			if (!world.isRemote) {
				// Server side info

			} else {
				// Client side info
				if(isPacket) {
					//This is an update packet
					if (masterPos != null && this.master == null) {
						setMaster(this.getMasterFromPos());
					}
					if(tag.hasKey("center")) {
						centerPosition = new BlockPos(Utils.arrayToVec3i(tag.getIntArray("center")));
						System.out.println("Center is " + centerPosition);
					}
				}
			}
		} else {
			// World is null, we've only just loaded in. We want to deserialize
			// this all later
			// So we'll save the tag to an instance variable
			if (toDeserialize == null) {
				// Store this tag for retrieval of world specific info later
				toDeserialize = tag;
			}
		}
	}

	@Override
	public NBTTagCompound serialize(NBTTagCompound tag, boolean isPacket) {

		// Common to client and server
		tag.setBoolean("valid", validMultiblock);
		tag.setBoolean("isMaster", isMaster);
		tag.setInteger("facing", facing.getIndex());
		tag.setInteger("tile_id", tile_id.ordinal());

		if(master != null) {
			int[] mas = Utils.vec3iToArray(masterPos);
			tag.setIntArray("masterPos", mas);
		}
		
		// Server only
		if (world != null && !world.isRemote) {
			if (this.isMaster && core != null) {
				NBTTagCompound struc = core.serialize();
				tag.setTag("structure", struc);
				
				centerPosition = core.centerPos;
				int[] cent = Utils.vec3iToArray(centerPosition);
				tag.setIntArray("center", cent);
			}
		} else {
			// Client only
		}
		return tag;
	}

	/**
	 * @return BlockPos of master
	 */
	@Nullable
	public BlockPos getMasterPos() {
		return this.masterPos;
	}

	/**
	 * Store masterPosition for later use
	 * 
	 * @param masterPos
	 */
	public void setMasterPos(BlockPos poz) {
		masterPos = poz;
	}

	public void invalidate() {
		if (master == null) {
			// Edge case
			return;
		}
		if (master != this) {
			getMaster().invalidate();
		} else {
			// Any special code for invalidating masters goes here
		}

	}

	@Override
	public void markContainingBlockForUpdate(@Nullable IBlockState newState) {
		getMaster().notifyChange(this, this.getPos(), true);
	}

	@Nullable
	public TileEntityMultiblock getMaster() {
		if(master == null) {
			System.out.println("Called with a null master");
			master = getMasterFromPos();
		}
		return master;
	}

	public boolean hasMaster() {
		return master != null;
	}

	// MASTER LOGIC
	public abstract void onInventoryClosed();
	
	protected abstract MultiblockStructure setStructure();

	public abstract boolean onMultiTileActivated(TileEntityMultiblock tile,
			EntityPlayer player, EnumHand hand, EnumFacing side, float xHit,
			float yHit, float zHit);

	public abstract void onMultiTileClicked(TileEntityMultiblock tile,
			EntityPlayer player);

	public TileEntityMultiblock getMasterFromPos() {
		return (TileEntityMultiblock) world.getTileEntity(masterPos);
	}

	/**
	 * Sets the master from the passed in tile entity
	 * 
	 * @param mast
	 *            the tile entity to set as the master
	 */
	public void setMaster(TileEntityMultiblock mast) {
		this.master = mast;
		this.masterPos = mast.pos;
		isMaster = this == mast;
	}

	/**
	 * Moves the master to the first available block in the structure Called by
	 * notifyChange
	 * Also in charge of multiplying master if necessary
	 * 
	 * @param oldMaster
	 *            the old master block
	 * @param pos
	 */
	public void moveMaster(TileEntityMultiblock oldMaster, BlockPos pos) {
		System.out.println("Moving master");
		ArrayList<BlockPos> blocks = core.getStructureAsList(core.structure);
		if (blocks.isEmpty()) {
			// List is empty, entire multiblock is destroyed
			return;
		}
		TileEntityMultiblock newMaster = null;
		BlockPos[] children = MultiblockStructure.getChildren(pos);

		for(BlockPos child : children) {
			TileEntity chi = world.getTileEntity(child);
			if(chi instanceof TileEntityMultiblock && newMaster == null) {
				TileEntityMultiblock chillen = getTile(chi);
				if(chillen.master.equals(oldMaster)) {
					newMaster = chillen;
				}
			}
		}
		if(newMaster == null) {
			//The multiblock is destroyed
			return;
		}
		MultiblockStructure struct = oldMaster.core;
		newMaster.core = struct;

		newMaster.setMaster(newMaster);
		oldMaster.isMaster = false;

		// Update master reference in all blocks in structure
		for (BlockPos posit : blocks) {
			TileEntityMultiblock tile = (TileEntityMultiblock) world
					.getTileEntity(posit);
			tile.setMaster(newMaster);
		}
		newMaster.searchAndValidate(true);
	}
	
	/**
	 * Splits the tile off onto its own structure
	 * and sets it as master
	 */
	public void splitMasterTo() {
		this.isMaster = true;
		this.setMaster(this);
		core = setStructure();
		validMultiblock = searchAndValidate(false);
		
		for(BlockPos neighbor : core.getStructureAsList(core.structure)) {
			TileEntityMultiblock neb = getTile(neighbor);
			neb.getMaster().core.removeBlockFromStructure(neb.pos);
			neb.setMaster(this);
		}
		
		validMultiblock = searchAndValidate(true);
		core.setCenterAndRelativeCoords();

	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == CapabilityNetwork.ENDPOINT ? validMultiblock && getMaster() != null && getMaster().getNetworkEndpoint() != null : super.hasCapability(capability, facing);
	}
	
	@Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
    		if(capability == CapabilityNetwork.ENDPOINT && validMultiblock && getMaster() != null) {
    			return CapabilityNetwork.ENDPOINT.cast(getMaster().getNetworkEndpoint());
    		}
    		return super.getCapability(capability, facing);
    }
	
	public abstract CapabilityNetworkEndpointMultiblock getNetworkEndpoint();
	public abstract void setNetworkEndpoint();
	
	protected boolean isValidSize() {
		if (core == null) {
			// Edge case
			System.out.println("Core is null, for some reason");
			return false;
		}
		return core.getSize() == core.sizeDef;
	}

	/**
	 * Searches the structure to validate it Also builds the structure's
	 * relative coords if it is valid size
	 * 
	 * @return true if the structure is valid, false otherwise
	 */
	protected boolean searchAndValidate(boolean checkTiles) {
		boolean valid = false;
		master.core.search(master.pos, true, checkTiles);
		System.out.println(core.sizeDef);
		if (getMaster().isValidSize()) {
			getMaster().core.setCenterAndRelativeCoords();
			valid = getMaster().core.isStructureValid();
			System.out.println("Is structure valid?: " + valid);
			facing = getMaster().core.facing;
		}
		return valid;
	}

	protected void invalidateMultiblock() {
		if (this.isMaster) {
			
			if(getNetworkEndpoint() != null) {
				getNetworkEndpoint().onComponentDestroyed(pos);
			}
			
			IBlockState state = null;
			if(!world.isAirBlock(getMaster().getPos())) {
			// Set centerPos
				if(!this.getBlockType().isAir(this.getBlockType().getDefaultState(), world, getMaster().getPos())) {
					state = this.getBlockType().getDefaultState()
							.withProperty(MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.INVALID)
							.withProperty(MultiblockComponent.MULTI_TILE, tile_id);

				// Hopefully won't produce unexpected behavior
					world.setBlockState(getMaster().getPos(), state);
				}
			}
			// Call loop
			for (int i : core.structure.keySet()) {
				HashMap<Integer, BlockPos> level = core.structure.get(i);
				for (BlockPos blockPos : level.values()) {
					TileEntityMultiblock tile = (TileEntityMultiblock) world
							.getTileEntity(blockPos);
					
					if (!tile.isMaster && !world.isAirBlock(blockPos)) {
						state = world.getBlockState(blockPos).withProperty(
								MultiblockComponent.MULTI_BLOCK, MultiTypeEnum.INVALID)
								.withProperty(MultiblockComponent.MULTI_TILE, tile_id);
						tile.invalidateMultiblock();
						world.setBlockState(blockPos, state);
					}

					
					
				}
			}
		}
		// Sanity check
		validMultiblock = false;
	}

	protected void validateMultiblock() {
		if (this.isMaster) {
			// Call loop
			centerPosition = getMaster().core.centerPos;
			getMaster().validMultiblock = true;
			TileEntityMultiblock masterDiffer = MultiblockDict.getTileForStructure(tile_id); 
			copyTileEntity(masterDiffer, getMaster(), null);
			masterDiffer.setMaster(masterDiffer);
			masterDiffer.isMaster = true;
			world.removeTileEntity(masterPos);
			world.setTileEntity(masterPos, masterDiffer);
			MessageTileReplaced masterMess = new MessageTileReplaced(masterDiffer.pos, masterDiffer.pos, masterDiffer.tile_id, masterDiffer.facing);
			PacketHandler.NETWORK.sendToAll(masterMess);


			for (int i : getMaster().core.structure.keySet()) {
				HashMap<Integer, BlockPos> level = core.structure.get(i);
				for (int key : level.keySet()) {
					BlockPos blockPos = level.get(key);
					TileEntityMultiblock tile = (TileEntityMultiblock) world
							.getTileEntity(blockPos);

					tile.validMultiblock = true;

					TileEntityMultiblock differentiated = MultiblockDict.getTileForStructure(tile_id);
					copyTileEntity(differentiated, tile, masterDiffer);
					differentiated.facing = masterDiffer.facing;
					world.removeTileEntity(blockPos);
					world.setTileEntity(blockPos, differentiated);

					// This code is unnecessary, but I may need it someday
					// BlockPos relativePos =
					// blockPos.subtract(master.core.centerPos);
					// BlockContainer container =
					// master.core.relativeStructure.get(i -
					// master.core.centerPos.getY()).get(master.core.getHashFromPos(relativePos));

					// Creating the object in here ensures blocks retain their
					// identity
					if (blockPos.equals(centerPosition)) {
						IBlockState centerState = world.getBlockState(blockPos)
								.withProperty(MultiblockComponent.MULTI_BLOCK,
										MultiTypeEnum.CENTER)
								.withProperty(MultiblockComponent.MULTI_TILE, tile_id);
						// container.state = centerState;
						world.setBlockState(blockPos, centerState);
					} else {
						IBlockState slaveState = world.getBlockState(blockPos)
								.withProperty(MultiblockComponent.MULTI_BLOCK,
										MultiTypeEnum.STRUCTURE)
								.withProperty(MultiblockComponent.MULTI_TILE, tile_id);
						// container.state = slaveState;
						world.setBlockState(blockPos, slaveState);
					}
					MessageTileReplaced message = new MessageTileReplaced(blockPos, masterDiffer.pos, differentiated.tile_id, differentiated.facing);
					PacketHandler.NETWORK.sendToAll(message);
					
					//sendUpdates(blockPos);
				}
			}
		}
		// Sanity check
		validMultiblock = true;
		
		reloadChunks();
	}
	
	private void sendUpdates(BlockPos pos) {
		world.markBlockRangeForRenderUpdate(pos, pos);
		world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 3);
		world.scheduleBlockUpdate(pos,this.getBlockType(),0,0);
		markDirty();
	}

	
	public void reloadChunks() {
		HashSet<Chunk> chunks = new HashSet<Chunk>(4);
		Chunk xMzM = world.getChunk(master.core.getxMax(), master.core.getzMax());
		chunks.add(xMzM);
		forceChunkReload(xMzM);
		

		Chunk xMzm = world.getChunk(master.core.getxMax(), master.core.getzMin());
		if(chunks.add(xMzm)) {
			forceChunkReload(xMzm);
		}

		Chunk xmzM = world.getChunk(master.core.getxMin(), master.core.getzMax());
		if(chunks.add(xmzM)) {
			forceChunkReload(xmzM);
		}

		Chunk xmzm = world.getChunk(master.core.getxMin(), master.core.getzMin());
		if(chunks.add(xmzm)) {
			forceChunkReload(xmzm);
		}

	}
	
	public void forceChunkReload(Chunk chunk) {
		
		WorldServer ws = (WorldServer) world;
		SPacketChunkData packet = new SPacketChunkData(chunk,
				65535);

		for (EntityPlayer play : ws.playerEntities) {
			EntityPlayerMP entityplayermp = (EntityPlayerMP) play;
			entityplayermp.connection.sendPacket(packet);
			ws.getEntityTracker().sendLeashedEntitiesInChunk(
					entityplayermp, chunk);
		}
	}
	

	/**
	 * Copies the data from one tile to another
	 * @param to The tile to copy over to
	 * @param from The tile to copy from
	 */
	public void copyTileEntity(TileEntityMultiblock to, TileEntityMultiblock from, TileEntityMultiblock mast) {
		if(from.isMaster && !world.isRemote) {
			to.core = MultiblockDict.getStructure(tile_id, from.core);
			to.core.tile = to;
			to.tile_id = tile_id;
			to.centerPosition = from.centerPosition;
		}
		
		to.isMaster = from.isMaster;
		to.facing = from.facing;
		to.validMultiblock = from.validMultiblock;
		if(from.masterPos != null) {
			to.setMasterPos(from.getMasterPos());
		}
		if(mast != null) {
			to.master = mast;
			to.tile_id = mast.tile_id;
		}
		
		if(to instanceof ITickable) {
			world.tickableTileEntities.add(to);
		}
	}

	/**
	 * Only called for master TE Will also set validMultiblock Called when a
	 * block is created or destroyed
	 * 
	 * @param tileEntitySlave
	 *            Tile entity notifying the master of change Can be the master
	 *            itself
	 * @param pos
	 *            the position of the tile entity
	 */
	public void notifyChange(TileEntityMultiblock tile, BlockPos pos,
			boolean added) {
		boolean wasValid = validMultiblock;
		boolean isValid = master.searchAndValidate(true);

		if (isValid) {
			// Multiblock is currently valid
			if (!wasValid) {
				// Multiblock has just been successfully created
				// Propagate changes along other tile entities
				validMultiblock = isValid;
				validateMultiblock();
			} else {
				// An existing multiblock has had a notification for some reason
				// Nothing in the block has changed, we can ignore this
			}
		} else {
			// Multiblock is currently invalid
			if (wasValid) {
				// Multiblock previously existed, is now destroyed
				validMultiblock = isValid;
				if (tile.isMaster && world.isAirBlock(pos)) {
					moveMaster(tile, pos);
					
				}
				// Need to call on new master, in case we have moved it
				master.invalidateMultiblock();
				// Move master from here, if it has been destroyed
			}
			// Nothing gets called if a block is just added? Or deleted?
			if (!added) {
				// If we removed the master of an incomplete structure, we need
				// to move it
				if (tile.isMaster) {
					moveMaster(tile, pos);
				}
			} else {
				// If we just added a block
				// Verify the master of the block
			}
		}
	}

}
