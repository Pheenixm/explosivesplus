package com.pheenixm.explosivesplus.common.tiles;

import javax.annotation.Nullable;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

public abstract class TileEntityBase extends TileEntity {


	@Override
	public void readFromNBT(NBTTagCompound tag) {
		super.readFromNBT(tag);
		this.deserialize(tag, false);
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound tag) {
		super.writeToNBT(tag);
		return this.serialize(tag, false);
	}

	@Override
	public SPacketUpdateTileEntity getUpdatePacket() {
		NBTTagCompound tag = getUpdateTag();
		return new SPacketUpdateTileEntity(this.pos, 0, tag);
	}

	@Override
	public NBTTagCompound getUpdateTag() {
		NBTTagCompound update = super.getUpdateTag();
		update = super.writeToNBT(update);
		NBTTagCompound send = serialize(update, true);
		return send;
	}

	@Override
	public void onDataPacket(NetworkManager network,
			SPacketUpdateTileEntity packet) {
		super.onDataPacket(network, packet);
		this.handleUpdateTag(packet.getNbtCompound());
	}
	
	@Override
    public void handleUpdateTag(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
		this.deserialize(tag, true);
    }


	@Override
	public boolean shouldRefresh(World world, BlockPos pos,
			IBlockState oldState, IBlockState newState) {
		return (oldState.getBlock() != newState.getBlock());
	}

	@Override
	public boolean hasCapability(Capability<?> capability,
			@Nullable EnumFacing facing) {
		return super.hasCapability(capability, facing);
	}

	@Override
	public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
		return super.getCapability(capability, facing);
	}

	public void markContainingBlockForUpdate(@Nullable IBlockState newState) {
		markBlockForUpdate(getPos(), newState);
	}

	public void markBlockForUpdate(BlockPos pos,
			@Nullable IBlockState newState) {
		IBlockState state = world.getBlockState(pos);
		if (newState == null) {
			newState = state;
		}
		world.notifyBlockUpdate(pos, state, newState, 3);
		world.notifyNeighborsOfStateChange(pos, newState.getBlock(), true);
	}

	public void markForUpdate(@Nullable IBlockState state) {
		markBlockForUpdate(this.getPos(), state);
	}
	
    public void reloadRender() {
        world.markBlockRangeForRenderUpdate(getPos(), getPos());
    }
	// TODO: Implement rotation and mirroring?
	
	public boolean isTileLoaded() {
		return world.isBlockLoaded(this.getPos());
	}
	
	//TODO: Implement configurable distance
	@Override
	public double getMaxRenderDistanceSquared()
	{
		return super.getMaxRenderDistanceSquared()*
				6;
	}

	
	public abstract void deserialize(NBTTagCompound tag, boolean isPacket);
	public abstract NBTTagCompound serialize(NBTTagCompound tag,
			boolean isPacket);
	public abstract void onPlacedBy(World worldIn, BlockPos pos, IBlockState state,
			EntityLivingBase placer, ItemStack stack);
}
