package com.pheenixm.explosivesplus.common.tiles.multiblock;

import java.util.ArrayList;

import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntityPluripotent;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.tiles.generator.Tier1Generator;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.IStringSerializable;
import scala.actors.threadpool.Arrays;

public class MultiblockDict {

	public MultiblockDict() {}

	/**
	 * Gets the tile for a given structure
	 * based on {@link}structureId
	 * @return the Tile for that differentiated structure 
	 */
    	public static TileEntityMultiblock getTileForStructure(MultiEnum multi) {
    		switch(multi) {
    			case PLURIPOTENT:
    				return new TileEntityPluripotent();
    			case SILO:
    				return new TileEntitySilo();
    			case CENTRIFUGE:
    				return new TileEntityCentrifuge();
    			case GEN_TIER_1:
    				return new Tier1Generator();
    			default: {
    				System.out.println("Default called for some reason...");
    				return new TileEntityPluripotent();
    			}
    		}
    	}
    	
    	public static MultiblockStructure getStructure(MultiEnum multi, MultiblockStructure from) {
    		ArrayList<IBlockState> materials = new ArrayList<IBlockState>();
    		switch(multi) {
    			case PLURIPOTENT: {
    				materials = new ArrayList<IBlockState>(Arrays.asList(TileEntityPluripotent.mats));
    				return new MultiblockHelper(from, materials);
    			}
    			case SILO: {
    				materials = new ArrayList<IBlockState>(Arrays.asList(TileEntitySilo.mats));
    				return new SiloStructure(from, materials);
    			}
    			case CENTRIFUGE: {
    				materials = new ArrayList<IBlockState>(Arrays.asList(TileEntityCentrifuge.mats));
    				return new CentrifugeStructure(from, materials);
    			}
    			case GEN_TIER_1: {
    				materials = new ArrayList<IBlockState>(Arrays.asList(Tier1Generator.mats));
    				return new GenTier1Structure(from, materials);
    			}
    			default: {
    				materials = new ArrayList<IBlockState>(Arrays.asList(TileEntitySilo.mats));
    				System.out.println("Default called for some reason...");
    				return new MultiblockHelper(from, materials);
    			}
    		}
    	}

	public static int mapPropertiesToValue(MultiTypeEnum type, MultiEnum multi) {
		switch(multi) {
			case PLURIPOTENT: {
				switch(type) {
					case INVALID:
						return 0;
					case CENTER: 
						return 1;
					case STRUCTURE:
						return 2;
					case RENDER:
						return 3;
					default:
						return 0;	
				}
			}
			case SILO: {
				switch(type) {
					case INVALID:
						return 4;
					case CENTER: 
						return 5;
					case STRUCTURE:
						return 6;
					case RENDER:
						return 7;
					default:
						return 4;	
				}
			}
			case CENTRIFUGE: {
				switch(type) {
					case INVALID:
						return 8;
					case CENTER: 
						return 9;
					case STRUCTURE:
						return 10;
					case RENDER:
						return 11;
					default:
						return 8;	
				}
			}
			case GEN_TIER_1: {
				switch(type) {
					case INVALID:
						return 12;
					case CENTER: 
						return 13;
					case STRUCTURE:
						return 14;
					case RENDER:
						return 15;
					default:
						return 12;	
				}
			}
			default: {
				return 0;
			}
		}
	}
	
	public static MultiTypeEnum mapValueToTypeProperty(int value) {
		if(value == 0 || value == 4 || value == 8 || value == 12) {
			return MultiTypeEnum.INVALID;
		}
		if(value == 1 || value == 5 || value == 9 || value == 13) {
			return MultiTypeEnum.CENTER;
		}
		if(value == 2 || value == 6 || value == 10 || value == 14) {
			return MultiTypeEnum.STRUCTURE;
		}
		if(value == 3 || value == 7 || value == 11 || value == 15) {
			return MultiTypeEnum.RENDER;
		}
		return MultiTypeEnum.INVALID;
	}
	
	public static MultiEnum mapValueToMultiProperty(int value) {
		if(value < 4) {
			return MultiEnum.PLURIPOTENT;
		}
		if(value >= 4 && value < 8) {
			return MultiEnum.SILO;
		}
		if(value >= 8 && value < 12) {
			return MultiEnum.CENTRIFUGE;
		}
		if(value >= 12 && value < 16) {
			return MultiEnum.GEN_TIER_1;
		}
		return MultiEnum.PLURIPOTENT;
	}
    	
    public enum MultiEnum implements IStringSerializable {
        PLURIPOTENT("pluripotent"),
        SILO("silo"),
        CENTRIFUGE("centrifuge"),
        GEN_TIER_1("gen_tier_1");
    		

        private final String name;

        MultiEnum(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return name;
        }

        @Override
        public String getName()
        {
            return name;
        }
        
        public static MultiEnum get(int meta)
        {
            return meta >= 0 && meta < values().length ? values()[meta] : PLURIPOTENT;
        }
        
	    	public static MultiEnum getEnumFromStructureId(int structureId) {
	    		if(structureId < 0) {
	    			return PLURIPOTENT;
	    		}
	    		if(structureId >= 0 && structureId < 6) {
	    			return SILO;
	    		}
	    		if(structureId >= 6 && structureId < 12) {
	    			return CENTRIFUGE;
	    		}
	    		if(structureId >= 12 && structureId < 18) {
	    			return GEN_TIER_1;
	    		}
	    		return PLURIPOTENT;
	    	}

        public static MultiEnum[] values = values();

    }
}
