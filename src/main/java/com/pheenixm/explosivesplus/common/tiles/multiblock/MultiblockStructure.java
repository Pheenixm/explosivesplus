package com.pheenixm.explosivesplus.common.tiles.multiblock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import com.pheenixm.explosivesplus.common.blocks.multiblocks.AccessoryBlock;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.StructuralBlock;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityFallingBlocks;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.util.Utils;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;

public abstract class MultiblockStructure {

	protected World world;
	protected ArrayList<IBlockState> materials;
	protected int xMin;
	protected int xMax;
	protected int yMin;
	protected int yMax;
	protected int zMin;
	protected int zMax;
	private int size;
	public TileEntityMultiblock tile;
	
	public EnumFacing facing = EnumFacing.UP;
	
	public BlockPos centerPos;
	
	public final HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> structureDef;
	//Guaranteed to be defined by defining the structure
	public int sizeDef;

	
	/**
	 * Dynamically changing HashMap of blocks in search area
	 * Updated every time a block near an existing block is changed
	 */
	public HashMap<Integer, HashMap<Integer, BlockPos>> structure;
	
	/**
	 * Use this for comparing the instance structure to the real
	 * Only form after sufficient blocks exist in structure
	 */
	public HashMap<Integer, HashMap<Integer, BlockContainer>> relativeStructure;


	public MultiblockStructure(World w, ArrayList<IBlockState> mats, TileEntityMultiblock te) {
		world = w;
		tile = te;
		materials = mats;
		defineMaterials(mats);
		structureDef = defineStructure();
		structure = new HashMap<Integer, HashMap<Integer, BlockPos>>();
	}
	
	public MultiblockStructure(MultiblockStructure struc, ArrayList<IBlockState> mats) {
		world = struc.world;
		tile = struc.tile;
		materials = mats;
		defineMaterials(mats);
		structureDef = defineStructure();
		structure = struc.structure;
		xMin = struc.xMin;
		xMax = struc.xMax;
		yMin = struc.yMin;
		yMax = struc.yMax;
		zMin = struc.zMin;
		zMax = struc.zMax;
		setSize(struc.getSize());
		centerPos = struc.centerPos;
		facing = struc.facing;
		size = struc.size;
	}

	/**
	 * Called when a block in the multiblock changes, or when its neighbor
	 * changes Uses existing structure list to
	 * TODO: Can this just be a void? 
	 * @param bPos
	 * @return
	 */
	public synchronized HashMap<Integer, HashMap<Integer, BlockPos>> search(BlockPos bPos, boolean local, boolean checkTiles) {
		Queue<BlockPos> queue = new LinkedList<BlockPos>();
		HashMap<Integer, Boolean> visited = new HashMap<Integer, Boolean>();
		queue.add(bPos);
		setVisited(visited, bPos);
		setSize(0);
		
		//Adds lag, but increases reliability. Reusability is overrated
		HashMap<Integer, HashMap<Integer, BlockPos>> struc = new HashMap<Integer, HashMap<Integer, BlockPos>>();
		
		
		while (!queue.isEmpty()) {
			BlockPos pos = queue.remove();
			int key = pos.getY();
			int key2 = this.getHashFromPos(pos);
			if (struc.containsKey(key)
					&& !struc.get(key).containsKey(key2)) {
				struc.get(key).put(key2, pos);
				setSize(getSize() + 1);
			} else {
					HashMap<Integer, BlockPos> blocks = new HashMap<Integer, BlockPos>(32);
					blocks.put(key2, pos);
					struc.put(key, blocks);
					setSize(getSize() + 1);
			}
			
			for (BlockPos childPos : getChildren(pos)) {

				if (!visited.containsKey(childPos.hashCode())
						&& isValidMaterial(world.getBlockState(childPos)) 
						&& hasValidTile(childPos, checkTiles)) {
					setVisited(visited, childPos);
					queue.add(childPos);
				}
			}
		}
		System.out.println("Structure size is " + getSize());
		if(local) {
			structure = struc;
		}
		return struc;
	}

	private void setVisited(HashMap<Integer, Boolean> visited, BlockPos pos) {
		visited.put(pos.hashCode(), true);
	}
	
	
	
	public ArrayList<BlockPos> getStructureAsList(HashMap<Integer, HashMap<Integer, BlockPos>> struc) {
		ArrayList<BlockPos> blocks = new ArrayList<BlockPos>();
		for(int i : struc.keySet()) {
			HashMap<Integer, BlockPos> level = struc.get(i);
			for(BlockPos pos : level.values()) {
				blocks.add(pos);
			}
		}
		return blocks;
	}
	
	/**
	 * 
	 * @return the size of structure
	 */
	public int getSize() {
		return size;
	}

	public boolean isValidMaterial(IBlockState block) {
		for (IBlockState blahk : materials) {
			if (block.getBlock().getDefaultState()
					.equals(blahk.getBlock().getDefaultState())) {
				return true;
			}
		}
		return false;
	}

	public static BlockPos[] getChildren(BlockPos pos) {
		BlockPos[] children = {
				new BlockPos(pos.getX() + 1, pos.getY(), pos.getZ()),
				new BlockPos(pos.getX(), pos.getY(), pos.getZ() + 1),
				new BlockPos(pos.getX(), pos.getY() - 1, pos.getZ()),
				new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ()),
				new BlockPos(pos.getX() - 1, pos.getY(), pos.getZ()),
				new BlockPos(pos.getX(), pos.getY(), pos.getZ() - 1)};
		return children;
	}

	/**
	 * Only call when there are enough blocks in the structure to designate it valid
	 */
	public void setCenterAndRelativeCoords() {		
		//Likely a waste of overhead, but I don't expect to call this often
		relativeStructure = new HashMap<Integer, HashMap<Integer, BlockContainer>>();
		
		ArrayList<Integer> yCoords = new ArrayList<Integer>(structure.keySet());
		yCoords.sort(null);
		if(yCoords.isEmpty()) {
			return;
		}
		yMin = yCoords.get(0);
		yMax = yCoords.get(yCoords.size() - 1);
		
		//Calculate x and z relative to lowest y level
		HashMap<Integer, BlockPos> lowestLevel = structure.get(yMin);
		ArrayList<Integer> xCoords = new ArrayList<Integer>(lowestLevel.size()); 
		ArrayList<Integer> zCoords = new ArrayList<Integer>(lowestLevel.size());
		for(BlockPos pos : lowestLevel.values()) {
			xCoords.add(pos.getX());
			zCoords.add(pos.getZ());
		}
		xCoords.sort(null);
		zCoords.sort(null);
		
		xMin = xCoords.get(0);
		xMax = xCoords.get(xCoords.size() - 1);
		
		zMin = zCoords.get(0);
		zMax = zCoords.get(zCoords.size() - 1);

		int centerX = xMin + ((xMax - xMin) / 2);
		int centerY = yMin;
		int centerZ = zMin + ((zMax - zMin) / 2);
		
		//Set center position
		centerPos = new BlockPos(centerX, centerY, centerZ);
		
		//Now, build relative coordinates
		for(int iter : structure.keySet()) {
			HashMap<Integer, BlockPos> level = structure.get(iter);
			HashMap<Integer, BlockContainer> newLevel = new HashMap<Integer, BlockContainer>(level.size());
			for(BlockPos pos : level.values()) {
				BlockPos relativePos = pos.subtract(centerPos);
				BlockContainer block = new BlockContainer(relativePos, world.getBlockState(pos));
				newLevel.put(this.getHashFromPos(relativePos), block);
			}
			relativeStructure.put(iter-yMin, newLevel);
		}
	}

	/**
	 * Used to serialize
	 * @return
	 */
	private ArrayList<BlockContainer> getListFromRelativeStructure() {
		ArrayList<BlockContainer> blocks = new ArrayList<BlockContainer>(512);
		setCenterAndRelativeCoords();
		for(int i : relativeStructure.keySet()) {
			for(BlockContainer block : relativeStructure.get(i).values()) {
				blocks.add(block);
			}
		}
		return blocks;
	}
	
	public abstract HashMap<Integer, HashMap<Integer, HashMap<Integer, BlockContainer>>> defineStructure();
	public abstract void defineMaterials(ArrayList<IBlockState> mats);
	public abstract boolean hasValidTile(BlockPos childPos, boolean checkTiles);

	public int getHashFromPos(BlockPos pos) {
		return pos.hashCode();
	}
	
	/**
	 * Used for serialization/deserialization
	 */
	private void buildStructureFromRelativeAndCenter() {
		if(structure == null) {
			structure = new HashMap<Integer, HashMap<Integer, BlockPos>>();
		}
		if(relativeStructure == null) { 
			//Edge case handling
			return;
		}
		for(int iter : relativeStructure.keySet()) {
			HashMap<Integer, BlockContainer> level = relativeStructure.get(iter);
			HashMap<Integer, BlockPos> newLevel = new HashMap<Integer, BlockPos>(level.size());
			for(BlockContainer block : level.values()) {
				BlockPos pos = centerPos.add(block.pos);
				newLevel.put(this.getHashFromPos(pos), pos);
			}
			structure.put(iter+yMin, newLevel);
		}

	}
	
	public boolean removeBlockFromStructure(BlockPos pos) {
		if(structure.containsKey(pos.getY())) {
			int key = this.getHashFromPos(pos);
			if(structure.get(pos.getY()).containsKey(key)) {
				return structure.get(pos.getY()).remove(key, pos);
				//TODO: Trigger automatic update of relativePos following this event
			}
		}
		return false;
	}
	
	/**
	 * Called to check validity of structure
	 * ONLY call after building the relative structure map
	 * @return
	 */
	public boolean isStructureValid() {
		boolean isValid = false;
		Iterator<Integer> iter = structureDef.keySet().iterator();
		while(!isValid && iter.hasNext()) {
			int index = iter.next();
			HashMap<Integer, HashMap<Integer, BlockContainer>> definition = structureDef.get(index);
			isValid = true;
			facing = EnumFacing.byIndex(index);
			
			for(int i : relativeStructure.keySet()) {
				HashMap<Integer, BlockContainer> level = relativeStructure.get(i);
				HashMap<Integer, BlockContainer> defLevel = definition.get(i);
				if(defLevel == null) {
					//This level isn't mapped in the structure definition
					if(level.size() > 0) {
						isValid = false;
					} else {
						relativeStructure.remove(i);
						structure.remove(i+ yMin);
					}
				}
	
				for(int key : level.keySet()) {
					BlockContainer blockReal = level.get(key);
					BlockContainer blockDef = defLevel.get(key);
					
					if(blockDef == null || !isValid(blockDef, blockReal)) {
						System.out.println("Block is invalid at " + blockReal.pos + " with state " + blockReal.state);
						if(blockDef != null) {
							System.out.println("Block is invalid at " + blockDef.pos + " with state " + blockDef.state);
						}
						//Either the block doesn't exist in the definition
						//Or the block is and invalid type
						//TODO: Alert player that block is invalid type
						isValid = false;
					}
				}
			}

		}
		return isValid;
	}
	
	//TODO: Fix this method
	protected boolean isValid(BlockContainer def, BlockContainer real) {
		if(def == null) {
			System.out.println("def is null");
		}
		if(def.state == null) {
			System.out.println("def state is null");
		}
		IBlockState defState = def.state.getBlock().getDefaultState();
		IBlockState realState = real.state.getBlock().getDefaultState();
		return defState.equals(realState);
	}
	
	/*
	@Deprecated
	private boolean isOf(BlockContainer sup, BlockContainer sub){
		Class clazz = sup.state.getBlock().getClass();
		Object obj = sub.state.getBlock();
	    return clazz.isInstance(obj);
	}*/


	public NBTTagCompound serialize() {
		if(world == null) {
			System.out.println("Null world");
		}
		if(relativeStructure == null) {
			setCenterAndRelativeCoords();
		}
		// Write structure to NBT
		NBTTagCompound compound = new NBTTagCompound();
		if (structure == null) {
			System.out.println("null structure???");
			return compound;
		}
		if(relativeStructure == null) {
			System.out.println("Wierd error");
			return compound;
		}

		NBTTagList positions = new NBTTagList();
		
		//We will serialize the relative structure
		//then rebuild the structure from it and center
		//on reloading it
		for (BlockContainer block : getListFromRelativeStructure()) {
			NBTTagCompound tag = new NBTTagCompound();
			tag.setIntArray("pos", Utils.vec3iToArray(block.pos));
			tag.setInteger("block", Block.getStateId(block.state));
			positions.appendTag(tag);
		}
		int[] center = Utils.vec3iToArray(centerPos);
		System.out.println(center);
		compound.setIntArray("center", center);
		int[] minVals = {xMin, yMin, zMin};
		int[] maxVals = {xMax, yMax, zMax};
		compound.setIntArray("max", maxVals);
		compound.setIntArray("min", minVals);
		
		compound.setTag("relative_struc", positions);
		
		compound.setInteger("facing", facing.getIndex());
		compound.setInteger("size", size);
		
		return compound;
	}
	
	public void deserialize(NBTTagCompound compound) {
		NBTTagList positions = compound.getTagList("relative_struc", Constants.NBT.TAG_COMPOUND);
		int[] center = compound.getIntArray("center");
		this.centerPos = new BlockPos(center[0],center[1],center[2]);
		int[] mins = compound.getIntArray("min");
		this.xMin = mins[0];
		this.yMin = mins[1];
		this.zMin = mins[2];
		
		int[] maxes = compound.getIntArray("max");
		this.xMax = maxes[0];
		this.yMax = maxes[1];
		this.zMax = maxes[2]	;
		
		this.facing = EnumFacing.byIndex(compound.getInteger("facing"));
		this.size = compound.getInteger("size");
		
		if(relativeStructure == null) {
			relativeStructure = new HashMap<Integer, HashMap<Integer, BlockContainer>>();
		}

		for(NBTBase cast : positions) {
			NBTTagCompound tag = (NBTTagCompound) cast;
			BlockPos pos = new BlockPos(Utils.arrayToVec3i(tag.getIntArray("pos")));
			BlockContainer block = new BlockContainer(pos, Block.getStateById(tag.getInteger("block")));
			int key = this.getHashFromPos(pos);
			if(relativeStructure.containsKey(pos.getY())) {
				relativeStructure.get(pos.getY()).put(key, block);
			} else {
				HashMap<Integer, BlockContainer> level = new HashMap<Integer, BlockContainer>();
				level.put(key, block);
				relativeStructure.put(pos.getY(), level);
			}
		}
		
		//Build structure from relative and center
		this.buildStructureFromRelativeAndCenter();
	}

	public int getxMin() {
		return xMin;
	}

	public void setxMin(int xMin) {
		this.xMin = xMin;
	}

	public int getxMax() {
		return xMax;
	}

	public void setxMax(int xMax) {
		this.xMax = xMax;
	}

	public int getyMin() {
		return yMin;
	}

	public void setyMin(int yMin) {
		this.yMin = yMin;
	}

	public int getyMax() {
		return yMax;
	}

	public void setyMax(int yMax) {
		this.yMax = yMax;
	}

	public int getzMin() {
		return zMin;
	}

	public void setzMin(int zMin) {
		this.zMin = zMin;
	}

	public int getzMax() {
		return zMax;
	}

	public void setzMax(int zMax) {
		this.zMax = zMax;
	}

	public void setSize(int size) {
		this.size = size;
	}
	

}
