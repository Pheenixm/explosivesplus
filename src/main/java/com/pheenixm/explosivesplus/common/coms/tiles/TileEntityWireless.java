package com.pheenixm.explosivesplus.common.coms.tiles;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.api.coms.INetworkComponent;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.NetworkHelper;
import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
//Adding to network works
public class TileEntityWireless extends TileEntityBase {

	public CapabilityNetworkHandlerWireless<TileEntityWireless> wireless = new CapabilityNetworkHandlerWireless<TileEntityWireless>(this, WIRELESS_RANGE);
	public static final int WIRELESS_RANGE = 256;
	private boolean firstInit = true;
	//private int ticks = 0;
	
	public TileEntityWireless() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onLoad() {
		wireless.tile = this;
		//First init being read wrong
		if(!firstInit) {
			wireless.init(world);
		} else {
			firstInit = false;
		}
	}

	@Override
	public void deserialize(NBTTagCompound tag, boolean isPacket) {
		if(!isPacket) {
			CapabilityNetwork.HANDLER.readNBT(wireless, null, tag.getTag("wireless"));
			firstInit = tag.getBoolean("first");
		}
	}

	@Override
	public NBTTagCompound serialize(NBTTagCompound tag, boolean isPacket) {
		if(!isPacket) {
			tag.setTag("wireless", CapabilityNetwork.HANDLER.writeNBT(wireless, null));
			tag.setBoolean("first", firstInit);
		}
		return tag;
	}

	@Override
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		wireless.onPlacedBy(worldIn, pos, state, placer, stack);
		wireless.init(worldIn);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability,
			@Nullable EnumFacing facing) {
		return capability == CapabilityNetwork.HANDLER || super.hasCapability(capability, facing);
	}

	@Override
	public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
		if(capability == CapabilityNetwork.HANDLER) {
			return CapabilityNetwork.HANDLER.cast(wireless);
		}
		return super.getCapability(capability, facing);
	}

	/*
	@Override
	public void update() {
		if(ticks < 20) {
			ticks++;
		}
		if(ticks == 20) {
			//wireless.updateNeighbors(world, pos);
			ticks++;
		}
		
	}*/


}
