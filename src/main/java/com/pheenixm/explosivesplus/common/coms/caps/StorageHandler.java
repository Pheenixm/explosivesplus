package com.pheenixm.explosivesplus.common.coms.caps;

import com.pheenixm.explosivesplus.api.coms.INetworkComponent;
import com.pheenixm.explosivesplus.api.coms.INetworkEndpoint;
import com.pheenixm.explosivesplus.api.coms.INetworkHandler;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

/**
 * Empty class used as placeholder for handler storage
 * @author RobMontgomery
 *
 */
public class StorageHandler implements IStorage<INetworkHandler> {

	public StorageHandler() {
	}

	@Override
	public NBTBase writeNBT(Capability<INetworkHandler> capability, INetworkHandler instance, EnumFacing side) {
		return instance.serializeNBT();
	}

	@Override
	public void readNBT(Capability<INetworkHandler> capability, INetworkHandler instance, EnumFacing side,
			NBTBase nbt) {		
		instance.deserializeNBT((NBTTagCompound) nbt);
	}

}
