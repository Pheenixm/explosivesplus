package com.pheenixm.explosivesplus.common.coms.caps;

import com.pheenixm.explosivesplus.api.coms.INetworkComponent;
import com.pheenixm.explosivesplus.api.coms.INetworkEndpoint;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

/**
 * Empty class used as placeholder for endpoint storage
 * @author RobMontgomery
 *
 */
public class StorageEndpoint implements IStorage<INetworkEndpoint> {

	public StorageEndpoint() {
	}

	@Override
	public NBTBase writeNBT(Capability<INetworkEndpoint> capability, INetworkEndpoint instance, EnumFacing side) {
		return null;
	}

	@Override
	public void readNBT(Capability<INetworkEndpoint> capability, INetworkEndpoint instance, EnumFacing side,
			NBTBase nbt) {		
	}

}
