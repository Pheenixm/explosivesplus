package com.pheenixm.explosivesplus.common.coms.tiles;

import java.util.List;

import com.pheenixm.explosivesplus.api.coms.EnumCommand;
import com.pheenixm.explosivesplus.common.coms.NetworkHelper;
import com.pheenixm.explosivesplus.common.coms.caps.CapabilityNetworkEndpoint;

import gigaherz.graph2.Graph;
import gigaherz.graph2.GraphObject;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class CapabilityNetworkEndpointTile extends CapabilityNetworkEndpoint {

	protected TileEntity tile;
	
	public CapabilityNetworkEndpointTile(World world, TileEntity tile) {
		super(world);
		this.tile = tile;
	}
	
	@Override
	public List<GraphObject> getNeighbors(IBlockAccess world, BlockPos pos) {
		return NetworkHelper.getNeighbors(world, pos);
	}

	@Override
	public void updateNeighbors(IBlockAccess world, BlockPos pos) {
        Graph graph = networkHandler.getGraph();
        if (graph != null)
        {
            graph.addDirectedEdges(networkHandler, getNeighbors(world, pos));
        }
	}

	///
	/// 		MUST BE HANDLED BY CHILDREN
	///
	
	@Override
	public void handleMessage(EnumCommand command, Object[] args) {
		
	}

	@Override
	public String getStatus() {
		return null;
	}


}
