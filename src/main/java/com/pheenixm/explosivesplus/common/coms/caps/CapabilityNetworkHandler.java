package com.pheenixm.explosivesplus.common.coms.caps;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.pheenixm.explosivesplus.api.coms.INetworkHandler;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoder;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoderData;
import com.pheenixm.explosivesplus.common.coms.NetworkHelper;
import com.pheenixm.explosivesplus.common.coms.WirelessRegistry;
import com.pheenixm.explosivesplus.common.util.Utils;

import gigaherz.graph2.Graph;
import gigaherz.graph2.GraphObject;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.Constants;

/**
 * Placeholder for INetworkHandler capability
 * @author RobMontgomery
 *
 */
public class CapabilityNetworkHandler implements INetworkHandler {

	protected NetworkEncoder networkHandler = new NetworkEncoder();
	protected int range;
	public HashSet<BlockPos> endpoints;
	protected World world;
	protected NetworkEncoderData data;
	public EntityPlayer placer;
	public HashSet<UUID> whitelist;
	public HashSet<UUID> blacklist;

	
	public CapabilityNetworkHandler() {
		range = -1;
		endpoints = new HashSet<BlockPos>();
		//Need to set world after this super call
	}
	
	@Override
	public GraphObject getNetworkHandler() {
		return networkHandler;
	}


	@Override
	public void onComponentDestroyed(BlockPos pos) {
        Graph graph = networkHandler.getGraph();
        if (graph != null) {
            graph.remove(networkHandler);
        }
        NetworkHelper.removeWirelessNode(pos, world);
        //TODO: Do I need to remove values as well? 
	}

	@Override
	public UUID getSharedUid() {
		return networkHandler.getGraph().<NetworkEncoderData>getContextData().getUid();
	}

	/**
	 * Provides a copy of the network's whitelist, which will not be affected by changes to the network
	 */
	@Override
	@SuppressWarnings(value = {"unchecked"})
	public HashSet<UUID> getPlayerWhitelist(boolean update) {
		if(update || whitelist == null) {
			whitelist = (HashSet<UUID>) ((HashSet<UUID>) networkHandler.getGraph().<NetworkEncoderData>getContextData().getWhitelist()).clone();
		}
		return whitelist;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<UUID> getPlayerBlacklist(boolean update) {
		if(update || blacklist == null) {
			blacklist = (HashSet<UUID>) ((HashSet<UUID>) networkHandler.getGraph().<NetworkEncoderData>getContextData().getBlacklist()).clone();
		}
		return blacklist;
	}

	
	@Override
	public boolean setPlayerWhitelists(HashSet<UUID> whitelist, HashSet<UUID> blacklist) {
		return  networkHandler.getGraph().<NetworkEncoderData>getContextData().mergeToWhitelist(whitelist, blacklist);
	}

	@Override
	public int getWirelessRange() {
		return WirelessRegistry.getRegistry(world).hasSatellite ? Integer.MAX_VALUE : range;
	}
	
	@Override
	/**
	 * This must be called when the status of all nodes is reset
	 */
	public void updateEndpointList() {
		try {
			endpoints = NetworkHelper.getNetworkEndpoints(world, networkHandler);
		}
		catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
	}
	
	//This stores a list of machines/handlers and their status
	//As well as the current whitelist
	@Override
	public NBTTagCompound encodeNodeList() {
		NBTTagCompound tag = new NBTTagCompound();
		NBTTagList ends = new NBTTagList();
		NBTTagList whitelist = new NBTTagList();
		NBTTagList blacklist = new NBTTagList();
		boolean flag = !networkHandler.getGraph().<NetworkEncoderData>getContextData().getWhitelist().isEmpty();
		for(BlockPos node : endpoints) {
			NBTTagCompound position = new NBTTagCompound();
			position.setIntArray("pos", Utils.vec3iToArray(node));
			ends.appendTag(position);
		}
		if(getPlayerWhitelist(flag) != null) {
			for(UUID id : getPlayerWhitelist(flag)) {
				NBTTagCompound uuid = new NBTTagCompound();
				uuid.setString("white", id.toString());
				whitelist.appendTag(uuid);
			}
		}
		if(getPlayerBlacklist(flag) != null) {
			for(UUID id : getPlayerBlacklist(flag)) {
				NBTTagCompound uuid = new NBTTagCompound();
				uuid.setString("black", id.toString());
				blacklist.appendTag(uuid);
			}	
		}
		tag.setTag("endpoints", ends);
		tag.setTag("whitelist", whitelist);
		tag.setTag("blacklist", blacklist);
		return tag;
	}

	@Override
	public void decodeNodeList(NBTTagCompound tag) {
		NBTTagList ends = tag.getTagList("endpoints", Constants.NBT.TAG_COMPOUND);
		NBTTagList whitelist = tag.getTagList("whitelist", Constants.NBT.TAG_COMPOUND);
		NBTTagList blacklist = tag.getTagList("blacklist", Constants.NBT.TAG_COMPOUND);
		HashSet<UUID> whiteSet = new HashSet<UUID>();
		HashSet<UUID> blackSet = new HashSet<UUID>();
		if(endpoints == null) {
			endpoints = new HashSet<BlockPos>();
		}
		for(NBTBase comp : ends) {
			if(comp instanceof NBTTagCompound) {
				NBTTagCompound com = (NBTTagCompound)comp;
				endpoints.add(new BlockPos(Utils.arrayToVec3i(com.getIntArray("pos"))));
			}
		}
		for(NBTBase comp : whitelist) {
			if(comp instanceof NBTTagCompound) {
				NBTTagCompound white = (NBTTagCompound)comp;
				whiteSet.add(UUID.fromString(white.getString("white")));
			}
		}
		for(NBTBase comp : blacklist) {
			if(comp instanceof NBTTagCompound) {
				NBTTagCompound black = (NBTTagCompound)comp;
				blackSet.add(UUID.fromString(black.getString("black")));
			}
		}
		if(this.whitelist == null || this.whitelist.size() <= whiteSet.size()) {
			this.whitelist = whiteSet; 
		}
		if(this.blacklist == null || this.blacklist.size() <= blackSet.size()) {
			this.blacklist = blackSet;
		}
		if(data == null || !this.whitelist.equals(data.getWhitelist()) || !this.blacklist.equals(data.getBlacklist())) {
			data = new NetworkEncoderData(whiteSet, blackSet);
		}
	}



	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound tag = new NBTTagCompound();
		tag.setTag("nodes", encodeNodeList());
		if(networkHandler != null && networkHandler.getPosition() != null) {
			tag.setIntArray("position", Utils.vec3iToArray(networkHandler.getPosition()));
		}
		return tag;
	}

	@Override
	public void deserializeNBT(NBTTagCompound tag) {
		decodeNodeList((NBTTagCompound)tag.getTag("nodes"));
		networkHandler.setPosition(Utils.arrayToVec3i(tag.getIntArray("position")));
		//
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == CapabilityNetwork.HANDLER;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		return capability == CapabilityNetwork.HANDLER ? CapabilityNetwork.HANDLER.cast(this) : null;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//									NEED TO BE IMPLEMENTED BY CHILDREN
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	
	@Override
	public List<GraphObject> getNeighbors(IBlockAccess world, BlockPos pos) {
		//TODO: Figure out how to implement this
		return null;
	}

	@Override
	public void updateNeighbors(IBlockAccess world, BlockPos pos) {
		//TODO: Implement this
	}

	@Override
	public boolean joinExternalToHandlerNetwork() {
		return false;
	}

	@Override
	public boolean joinHandlerToExternalNetwork(BlockPos transmitter) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(World wor) {
		world = wor;
		
	}




}
