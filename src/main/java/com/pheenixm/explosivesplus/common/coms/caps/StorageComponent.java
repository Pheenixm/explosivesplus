package com.pheenixm.explosivesplus.common.coms.caps;

import com.pheenixm.explosivesplus.api.coms.INetworkComponent;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

/**
 * Empty class used as placeholder for component storage
 * @author RobMontgomery
 *
 */
public class StorageComponent implements IStorage<INetworkComponent> {

	public StorageComponent() {
	}

	@Override
	public NBTBase writeNBT(Capability<INetworkComponent> capability, INetworkComponent instance, EnumFacing side) {
		return null;
	}

	@Override
	public void readNBT(Capability<INetworkComponent> capability, INetworkComponent instance, EnumFacing side,
			NBTBase nbt) {		
	}

}
