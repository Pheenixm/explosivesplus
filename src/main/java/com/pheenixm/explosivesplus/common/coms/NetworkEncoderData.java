package com.pheenixm.explosivesplus.common.coms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import gigaherz.graph2.Mergeable;
import net.minecraft.entity.player.EntityPlayer;

/**
 * Cables should be agnostic, join only when
 * called upon by network handler
 * @author RobMontgomery
 *
 */
public class NetworkEncoderData implements Mergeable<NetworkEncoderData> {
	
	private final UUID uid;
	private Set<UUID> whitelist;
	private Set<UUID> blacklist;

	
	public NetworkEncoderData() {
		this(UUID.randomUUID(), new HashSet<UUID>(), new HashSet<UUID>());
	}
	
	public NetworkEncoderData(Set<UUID> white, Set<UUID> black) {
		this(UUID.randomUUID(), white, black);
	}
	
	public NetworkEncoderData(UUID id, Set<UUID> white, Set<UUID> black) {
		uid = id;
		whitelist = white;
		blacklist = black;
	}

	
	/**
	 * Adds an id to the whitelist
	 * @param id the id to add
	 * @return true if the id is not in the list, false if it is
	 */
	public boolean addToWhitelist(UUID id) {
		return blacklist.contains(id) ? blacklist.remove(id) && whitelist.add(id) : whitelist.add(id);
	}
	
	public boolean removeFromWhitelist(UUID id) {
		return whitelist.remove(id) && blacklist.add(id);
	}
	
	public Set<UUID> getWhitelist() {
		return whitelist;
	}
		
	public Set<UUID> getBlacklist() {
		return blacklist;
	}
	
	/**
	 * Adds all ids in the passed in set to the whitelist
	 * and removes them from the blacklist
	 * Used by network handlers to force the whitelist to a specified value
	 * Additionally adds all names on the blacklist and removes them from the white
	 * @param white the whitelist to merge in
	 * @param black the blacklist to merge in
	 * @return the sucess of adding the whitelist
	 */
	public boolean mergeToWhitelist(HashSet<UUID> white, HashSet<UUID> black) {
		blacklist.removeAll(white);
		blacklist.addAll(black);
		white.removeAll(black);
		whitelist.removeAll(black);
		return whitelist.addAll(white);
	}

	
	@Override
	/**
	 * Should prevent edge-cases where players who have previously been removed from 
	 * a network are added back into it when an unloaded component of that network is loaded
	 */
	public NetworkEncoderData mergeWith(NetworkEncoderData other) {
		blacklist.addAll(other.blacklist);
		whitelist.addAll(other.whitelist);
		whitelist.removeAll(blacklist);
		return new NetworkEncoderData(whitelist, blacklist);
	}

	@Override
	public NetworkEncoderData copy() {
		return new NetworkEncoderData(whitelist, blacklist);
	}

	public UUID getUid() {
		return uid;
	}
	
}
