package com.pheenixm.explosivesplus.common.coms.caps;

import java.util.List;
import java.util.UUID;

import com.pheenixm.explosivesplus.api.coms.EnumCommand;
import com.pheenixm.explosivesplus.api.coms.INetworkEndpoint;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoder;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoderData;
import com.pheenixm.explosivesplus.common.coms.NetworkHelper;

import gigaherz.graph2.Graph;
import gigaherz.graph2.GraphObject;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

public class CapabilityNetworkEndpoint implements INetworkEndpoint {
	
	protected NetworkEncoder networkHandler;
	protected World world;

	public CapabilityNetworkEndpoint() {

	}

	
	public CapabilityNetworkEndpoint(World world) {
		this.world = world;
		networkHandler = new NetworkEncoder();
	}


	@Override
	public GraphObject getNetworkHandler() {
		return networkHandler;
	}

	@Override
	public void onComponentDestroyed(BlockPos pos) {
        Graph graph = networkHandler.getGraph();
        if (graph != null) {
            graph.remove(networkHandler);
        }
        NetworkHelper.removeWirelessNode(pos, world);		
	}

	@Override
	public UUID getSharedUid() {
		return networkHandler.getGraph().<NetworkEncoderData>getContextData().getUid();
	}
	

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == CapabilityNetwork.ENDPOINT;
	}


	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		return capability == CapabilityNetwork.ENDPOINT ? CapabilityNetwork.ENDPOINT.cast(this) : null;
	}

	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound tag = new NBTTagCompound();
		return tag;
	}


	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		
	}
	
	@Override
	public void updateNeighbors(IBlockAccess world, BlockPos pos) {
        Graph graph = networkHandler.getGraph();
        if (graph != null)
        {
            graph.addDirectedEdges(networkHandler, getNeighbors(world, pos));
        }
	}


	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//									NEED TO BE IMPLEMENTED BY CHILDREN
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	
	@Override
	public List<GraphObject> getNeighbors(IBlockAccess world, BlockPos pos) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void handleMessage(EnumCommand command, Object[] args) {
		
	}

	@Override
	public String getStatus() {
		// TODO Auto-generated method stub
		return null;
	}


}
