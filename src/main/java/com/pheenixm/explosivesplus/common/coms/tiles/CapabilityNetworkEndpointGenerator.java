package com.pheenixm.explosivesplus.common.coms.tiles;

import com.pheenixm.explosivesplus.api.coms.EnumCommand;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoderData;
import com.pheenixm.explosivesplus.common.coms.caps.CapabilityNetworkEndpoint;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityGenerator;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;

import gigaherz.graph2.ConcurrentGraph;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class CapabilityNetworkEndpointGenerator extends CapabilityNetworkEndpointMultiblock {

	
	public CapabilityNetworkEndpointGenerator(World world, TileEntityGenerator<?> tile) {
		super(world, tile);
		init();
	}

	public void init() {
		ConcurrentGraph.integrate(networkHandler, getNeighbors(tile.getWorld(), tile.getPos()), (graph) -> new NetworkEncoderData());
        networkHandler.setPosition(tile.getPos());
	}
	
	protected TileEntityGenerator<?> getTile() {
		return (TileEntityGenerator<?>)tile;
	}

	
	@Override
	public void handleMessage(EnumCommand command, Object[] args) {
		switch(command) {

		}
	}
	
	@Override
	public String getStatus() {
		return "standing by";
	}

	
}
