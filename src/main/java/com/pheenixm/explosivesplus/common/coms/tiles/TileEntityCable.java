package com.pheenixm.explosivesplus.common.coms.tiles;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import com.pheenixm.explosivesplus.api.energy.EnergyBuffer;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoder;
import com.pheenixm.explosivesplus.common.coms.NetworkHelper;
import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntityGenerator;
import com.pheenixm.explosivesplus.common.tiles.TileEntityInjector;

import gigaherz.graph2.GraphObject;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

//Problem with energy is simple: it only goes to one area, because most generators don't renew their output constantly
//Only reliable thing to do is to design an injector
//OR have an intermediary
public class TileEntityCable extends TileEntityBase {

	
    private boolean loaded = false;
    public boolean isInjector;
    public CapabilityNetworkComponentTile<TileEntityCable> component;
    
    public ArrayList<Pair<BlockPos, EnumFacing>> energyRegistry;
    private ArrayList<IEnergyStorage> tempList;
	
	public TileEntityCable() {}
	
	@Override
	public void deserialize(NBTTagCompound tag, boolean isPacket) {
	}

	@Override
	public NBTTagCompound serialize(NBTTagCompound tag, boolean isPacket) {
		return tag;
	}

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //					GRAPH API
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	
	@Override
	public boolean hasCapability(Capability<?> capability,
			@Nullable EnumFacing facing) {
		return capability == CapabilityNetwork.COMPONENT || capability == CapabilityEnergy.ENERGY || super.hasCapability(capability, facing);
	}

	@Override
	public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
		if(capability == CapabilityNetwork.COMPONENT) {
			return CapabilityNetwork.COMPONENT.cast(component);
		}
		if(capability == CapabilityEnergy.ENERGY) {
			if(!this.isInjector) {
				updateEnergyRegistry();
			}
			if(energyRegistry.isEmpty()) {
				return CapabilityEnergy.ENERGY.cast(new EnergyBuffer(0,0));
			} else {
				return CapabilityEnergy.ENERGY.cast(getLowestEnergy());
			}
		}
		return super.getCapability(capability, facing);
	}
	
	public IEnergyStorage getLowestEnergy() {
		if(tempList == null) {
			tempList = new ArrayList<IEnergyStorage>();
		}
		if(!tempList.isEmpty()) {
			tempList.clear();
		}
		if(energyRegistry == null || energyRegistry.isEmpty()) {
			return null;
		}
		Iterator<Pair<BlockPos, EnumFacing>> iter = energyRegistry.iterator();
		while(iter.hasNext()) {
			Pair<BlockPos,EnumFacing> pair = iter.next();
			IEnergyStorage storage = getStorageFromPos(pair.getLeft(), pair.getRight());
			if(storage == null) {
				iter.remove();
			} else {
				tempList.add(storage);
			}
		}
		tempList.sort(energyComparator);
		return tempList.get(0);
	}
		
	private IEnergyStorage getStorageFromPos(BlockPos pos, EnumFacing facing) {
		TileEntity tile = world.getTileEntity(pos);
		if(!(tile instanceof TileEntityCable) && !(tile instanceof TileEntityInjector) && tile != null) {
			return tile.getCapability(CapabilityEnergy.ENERGY, facing);
		}
		return null;
	}
	
	public void updateEnergyRegistry() {
		isInjector = true;
		if(!world.isRemote) {
			energyRegistry = new ArrayList<Pair<BlockPos, EnumFacing>>();
			HashSet<BlockPos> visited = new HashSet<BlockPos>();
			Queue<BlockPos> queue = new ArrayDeque<BlockPos>();
			queue.add(pos);
			visited.add(pos);
			BlockPos checkPos = pos;
			BlockPos oldPos = pos;
			while (!queue.isEmpty()) {
				oldPos = checkPos;
				checkPos = queue.remove();
				BlockPos vec = oldPos.subtract(checkPos);
				EnumFacing facing = EnumFacing.getFacingFromVector(vec.getX(), vec.getY(), vec.getZ());
				TileEntity tile = world.getTileEntity(checkPos);
				
				if(!(tile instanceof TileEntityCable) && !(tile instanceof TileEntityInjector) && !(tile instanceof TileEntityGenerator) && tile != null) {
					energyRegistry.add(Pair.of(checkPos, facing));
				}
				//This will allow for chunk-loading
				for (EnumFacing face : EnumFacing.VALUES) {
					BlockPos childPos = checkPos.offset(face);
					TileEntity childTile = world.getTileEntity(childPos);
					if (!visited.contains(childPos) && ((childTile instanceof TileEntityCable ||  (childTile != null && childTile.hasCapability(CapabilityEnergy.ENERGY, facing))))) {
						visited.add(childPos);
						queue.add(childPos);
					}
				}
			}
		}
	}
	

	//TODO: Update for cabels
	@Override
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		if(!loaded && world != null) {
			initialize();
		}
		for(EnumFacing face : EnumFacing.VALUES) {
			this.onNeighborChange(pos.offset(face));
		}
	}
	
	@Override
	public void onLoad() {
		if(!loaded && world != null) {
			initialize();
		}
		if(isInjector) {
			//TODO: Run this automatically?
			updateEnergyRegistry();
		}
	}
	
    @Override
    public void validate()
    {
        super.validate();
    }
    
    @Override
    public void invalidate()
    {
        super.invalidate();
    }
    

    public void initialize() {
        loaded = true;
		component = new CapabilityNetworkComponentTile<TileEntityCable>(this);
    }

	public void onNeighborChange(BlockPos neighbor) {
		if(!world.isRemote) {
			TileEntity nebTile = world.getTileEntity(neighbor);
			BlockPos relPos = this.getPos().subtract(neighbor);
			EnumFacing face = EnumFacing.getFacingFromVector(relPos.getX(), relPos.getY(), relPos.getZ());
			//Neighbor was just destroyed
			boolean flag = world.isAirBlock(neighbor);
			if(flag || (nebTile != null && (nebTile.hasCapability(CapabilityEnergy.ENERGY, face) || nebTile.hasCapability(CapabilityNetwork.COMPONENT, face)))) {
				CapabilityNetworkComponentTile<?> net = CapabilityNetwork.COMPONENT.cast(this.getCapability(CapabilityNetwork.COMPONENT, null));
				for(GraphObject go : net.getNetworkHandler().getGraph().acquireObjects()) {
					if(go instanceof NetworkEncoder) {
						NetworkEncoder enc = (NetworkEncoder)go;
						BlockPos pos = new BlockPos(enc.getPosition());
						if(pos != null) {
							TileEntity tile = world.getTileEntity(pos);
							if(tile != null && tile instanceof TileEntityCable) {
								TileEntityCable otherCable = (TileEntityCable)tile;
								if(otherCable.isInjector) {
									otherCable.updateEnergyRegistry();
								}
							}
						}
						//TODO: Update to include entities?
					}
				}
				net.getNetworkHandler().getGraph().releaseObjects();
			}
		}
	}

    public static Comparator<IEnergyStorage> energyComparator = new Comparator<IEnergyStorage>() {

    		public int compare(IEnergyStorage s1, IEnergyStorage s2) {
    			int amt1 = s1.getEnergyStored();
    			int amt2 = s2.getEnergyStored();
		
    			//ascending order
    			return amt1 > amt2 ? 1 : amt1 == amt2 ? 0 : -1;
		}
	};



}
