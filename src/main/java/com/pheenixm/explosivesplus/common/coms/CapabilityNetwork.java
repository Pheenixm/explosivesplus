package com.pheenixm.explosivesplus.common.coms;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.coms.INetworkComponent;
import com.pheenixm.explosivesplus.api.coms.INetworkEndpoint;
import com.pheenixm.explosivesplus.api.coms.INetworkHandler;
import com.pheenixm.explosivesplus.common.coms.caps.StorageComponent;
import com.pheenixm.explosivesplus.common.coms.caps.*;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = ExplosivesPlus.MOD_ID)
public class CapabilityNetwork {

	@CapabilityInject(INetworkComponent.class)
    public static Capability<INetworkComponent> COMPONENT = null;
	
	@CapabilityInject(INetworkEndpoint.class)
	public static Capability<INetworkEndpoint> ENDPOINT = null;
	
	@CapabilityInject(INetworkHandler.class)
	public static Capability<INetworkHandler> HANDLER = null;
	
    public static void register() {
    		CapabilityManager.INSTANCE.register(INetworkComponent.class, new StorageComponent(), () -> new CapabilityNetworkComponent());
    		CapabilityManager.INSTANCE.register(INetworkEndpoint.class, new StorageEndpoint(), () -> new CapabilityNetworkEndpoint());
    		CapabilityManager.INSTANCE.register(INetworkHandler.class, new StorageHandler(), () -> new CapabilityNetworkHandler());

    }
	
}
