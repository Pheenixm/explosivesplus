package com.pheenixm.explosivesplus.common.coms;

import gigaherz.graph2.Graph;
import gigaherz.graph2.GraphObject;
import net.minecraft.util.math.Vec3i;

public class NetworkEncoder implements GraphObject {

    private Graph graph = null;
    private Vec3i position = null;

	
	@Override
	public Graph getGraph() {
		return graph;
	}

	@Override
	public void setGraph(Graph g) {
		graph = g;
	}
	
	public Vec3i getPosition() {
		return position;
	}
	
	public void setPosition(Vec3i pos) {
		this.position = pos;
	}

}
