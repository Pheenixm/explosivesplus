package com.pheenixm.explosivesplus.common.coms;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;
import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.coms.INetworkEndpoint;
import com.pheenixm.explosivesplus.api.coms.INetworkHandler;
import com.pheenixm.explosivesplus.api.coms.INetworkedBlock;
import com.pheenixm.explosivesplus.common.coms.caps.CapabilityNetworkEndpoint;
import com.pheenixm.explosivesplus.common.coms.caps.CapabilityNetworkHandler;
import com.pheenixm.explosivesplus.common.coms.tiles.CapabilityNetworkHandlerWireless;
import com.pheenixm.explosivesplus.common.coms.tiles.TileEntityCable;
import com.pheenixm.explosivesplus.api.coms.INetworkComponent;

import gigaherz.graph2.Graph;
import gigaherz.graph2.GraphObject;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class NetworkHelper {

	
	
	public static boolean hasNetworkCapability(TileEntity tile, EnumFacing facing) {
		if(tile == null) {
			return false;
		}
		return tile.hasCapability(CapabilityNetwork.COMPONENT, facing)
				|| tile.hasCapability(CapabilityNetwork.ENDPOINT, facing) 
				|| tile.hasCapability(CapabilityNetwork.HANDLER, facing);
	}
	
	
	public static UUID getPlayerUID(EntityPlayer player) {
		return EntityPlayer.getUUID(player.getGameProfile());
	}

 
	/**
	 * Returns a list of wireless neighbors the node making the call should connect to.
	 * Additionally, registers any new nodes with the wireless registry
	 * @param world The world this network is in
	 * @param network The network handler for the INetworkHandler impl making this call
	 * @param range The range of the node making this call
	 * @return A list of neighbors that the node making this call should connect to
	 */
	public static List<GraphObject> getWirelessInRange(World world, CapabilityNetworkHandlerWireless<?> cap, int range) {
		ArrayList<GraphObject> nodes = new ArrayList<GraphObject>();
		NetworkEncoder network = (NetworkEncoder) cap.getNetworkHandler();
		if(world == null) {
			throw new IllegalArgumentException("World should not be null!");
		}
		//This value hasn't been initialized yet
		//Need to use entityplayer who placed it		
		BlockPos origin = cap.tile.getPos();
		WirelessRegistry reg = WirelessRegistry.getRegistry(world);
		ArrayDeque<BlockPos> list = WirelessRegistry.getNeighbors(world, origin);
		if(!list.isEmpty()) {
			//Our node is already in the wireless registry (previously serialized), retrieve its neighbors from there
			for(BlockPos pos : list) {
				TileEntity tile = world.getTileEntity(pos);
				//TODO: Update this for facings[?]
				if(tile != null && tile.hasCapability(CapabilityNetwork.HANDLER, null)) {
					INetworkHandler handler = tile.getCapability(CapabilityNetwork.HANDLER, null);
					if(handler.getNetworkHandler() != null && handler.getNetworkHandler().getGraph() != null) {
						boolean flag = false;
						//May have to create a hashSet with a local whitelist for each capability
						for(UUID uid : cap.getPlayerWhitelist(false)) {
							if(handler.getPlayerWhitelist(false).contains(uid)) {
								flag = true;
							}
						}
						if(flag) {
							nodes.add(handler.getNetworkHandler());
						}
					}
				}
			}
		} else {
			//Our node is not already in the wireless registry
			//Even if there are no nodes in range, we still want the key added to the map
			//TODO: Clientside, the wireless registry DOES NOT PERSIST
			//MEANING, YOU CANNOT RELY ON CLIENT_SIDE NETWORKS
			reg.initializePos(origin);
			if(cap.placer == null) 	{
				//Edge case for when there is only one wireless node on a network
				return nodes;
			}
			UUID uid = NetworkHelper.getPlayerUID(cap.placer);
			
			for(BlockPos pos : reg.wirelessMap.keySet()) {
				if(origin.getDistance(pos.getX(), pos.getY(), pos.getZ()) <= range) { 
					//We are in range, now we check if there's a tile there
					TileEntity tile = world.getTileEntity(pos);
					//TODO: Update this for facings[?]
					if(tile != null && tile.hasCapability(CapabilityNetwork.HANDLER, null) && tile != cap.tile) {
						INetworkHandler handler = tile.getCapability(CapabilityNetwork.HANDLER, null);
						//We have to check if they're on the same graph
						if(handler.getPlayerWhitelist(true).contains(uid)) {
							nodes.add(handler.getNetworkHandler());
							reg.connectNodeToNeighbor(pos, origin);
						}
					}
				}
			}
		}
		return nodes;
	}
	
	/**
	 * Iterates over a network and adds all tiles with the endpoint capability to a HashSet
	 * @param world The world to search in
	 * @param network The network to search over
	 * @return A HashSet containing all tiles in this network with the endpoint capability
	 * TODO: Update this to include wireless endpoints
	 */
	public static HashSet<BlockPos> getNetworkEndpoints(World world, NetworkEncoder network) {
		HashSet<BlockPos> endpoints = new HashSet<BlockPos>();
		if(world == null) {
			throw new IllegalArgumentException("World should not be null!");
		}
		Graph graph = network.getGraph();
		for(GraphObject go : graph.acquireObjects()) {
			if(go instanceof NetworkEncoder) {
				NetworkEncoder enc = (NetworkEncoder)go;
				BlockPos pos = new BlockPos(enc.getPosition());
				TileEntity tile = world.getTileEntity(pos);
				//TODO: Update this for facings[?]
				if(tile != null && tile.hasCapability(CapabilityNetwork.ENDPOINT, null)) {
					endpoints.add(pos);
				}
				//TODO: Update to include entities?
			}
		}
		graph.releaseObjects();
		return endpoints;
	}
	
	
	
	
	private static boolean isPosInGraph(World world, BlockPos otherCable, INetworkComponent component) {
		if(world.getTileEntity(otherCable) == null) {
			return false;
		}
		INetworkComponent otherComp = world.getTileEntity(otherCable).getCapability(CapabilityNetwork.COMPONENT, null);
		return component.getNetworkHandler().getGraph().contains(otherComp.getNetworkHandler());
	}
	
	/**
	 * Get the facing that our cable is entering the sink at
	 * @param world
	 * @param sinkPos
	 * @return
	 */
	private static EnumFacing getCableFacing(World world, BlockPos sinkPos) {
		for(EnumFacing f : EnumFacing.VALUES) {
			if(world.getBlockState(sinkPos.offset(f)).getBlock() instanceof INetworkedBlock) {
				return f;
			}
		}
		return null;
	}
	

	/**
	 * Gets a list of the neighbors of the passed in tile
	 * This method is designed to be use in building a network
	 * This method will need slight modification for wireless networks
	 * Also populates the energyRegistry
	 * @param world
	 * @param pos
	 * @return
	 */
	public static List<GraphObject> getNeighbors(IBlockAccess world, BlockPos pos) {
		List<GraphObject> neighbors = Lists.newArrayList();
		for (EnumFacing f : EnumFacing.VALUES) {
			//This call will hot-load any neighbors that are adjacent
			TileEntity tile = world.getTileEntity(pos.offset(f));
			if(tile != null) {
				boolean added = false;
				if(tile.hasCapability(CapabilityNetwork.COMPONENT, null) && !added) {
					INetworkComponent capGraph = tile.getCapability(CapabilityNetwork.COMPONENT, null);
					if(capGraph != null && capGraph.getNetworkHandler() != null && capGraph.getNetworkHandler().getGraph() != null) {
						neighbors.add(capGraph.getNetworkHandler());
						added = true;
					}
				} 
				if(tile.hasCapability(CapabilityNetwork.ENDPOINT, null) && !added) {
					INetworkComponent capGraph = tile.getCapability(CapabilityNetwork.ENDPOINT, null);
					if(capGraph != null && capGraph.getNetworkHandler() != null && capGraph.getNetworkHandler().getGraph() != null) {
						neighbors.add(capGraph.getNetworkHandler());
						added = true;
					}
				} 
				if(tile.hasCapability(CapabilityNetwork.HANDLER, null) && !added) {
					INetworkComponent capGraph = tile.getCapability(CapabilityNetwork.HANDLER, null);
					if(capGraph != null && capGraph.getNetworkHandler() != null && capGraph.getNetworkHandler().getGraph() != null) {
						neighbors.add(capGraph.getNetworkHandler());
						added = true;
					}
				} 
			}
		}
		return neighbors;

	}
	
	
	
	/**
	 * Called by a radar station to check if an entity was created by a player on the whitelist
	 * @param player The player who created the entity being checked
	 * @param handler The network handler of the radar station making the call
	 * @return true if the entity is on the same network as the station, false otherwise
	 */
	@Deprecated
	public static boolean isPlayerWhitelisted(EntityPlayer player, NetworkEncoder handler) {
		Graph graph = handler.getGraph();
		World world = player.getEntityWorld();
		for(GraphObject enc : graph.acquireObjects()) {
			if(enc instanceof NetworkEncoder) {
				NetworkEncoder encoder = (NetworkEncoder)enc;
				TileEntity tile = world.getTileEntity(new BlockPos(encoder.getPosition()));
				//Check needed for edge cases of entities on the network
				//Don't need to bother with entities yet, because there will generally always be a
				//network handler on the network taking the form of a tile
				if(tile instanceof INetworkHandler) {
					//If tile is null, we never reach this code
					INetworkHandler net = (INetworkHandler) tile;
					return net.getPlayerWhitelist(false).contains(EntityPlayer.getUUID(player.getGameProfile()));
				}
			}
		}
		return false;
	}
		
	
	/**
	 * Call to load chunks
	 * @param world
	 * @param nextPos
	 * @param chunks
	 * @return true if a chunk had to be loaded, false otherwise
	 */
	public static boolean loadChunks(World world, BlockPos nextPos, HashSet<ChunkPos> chunks) {
		if(!world.isBlockLoaded(nextPos)) {
			//If chunk is not loaded, need to load it
			if(!world.isRemote) {
				ChunkPos chunkPos = new ChunkPos(nextPos);
				ExplosivesPlus.instance.forceChunkLoad(world, chunkPos);
				chunks.add(chunkPos);
			}
			return true;
		} else {
			return false;
		}
	}
	
	public static void unloadChunk(World world, ChunkPos chun, HashSet<ChunkPos> chunks) {
		ExplosivesPlus.instance.releaseChunkLoad(world, chun);
		chunks.remove(chun);
	}


	public static void removeWirelessNode(BlockPos pos, World world) {
		ArrayDeque<BlockPos> list = WirelessRegistry.getRegistry(world).wirelessMap.remove(pos);
		if(list != null && !list.isEmpty()) {
			for(BlockPos key : list) {
				WirelessRegistry.getNeighbors(world, key).remove(pos);
			}
		}
	}


}
