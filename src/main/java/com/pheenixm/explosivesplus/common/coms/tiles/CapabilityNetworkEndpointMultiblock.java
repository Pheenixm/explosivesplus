package com.pheenixm.explosivesplus.common.coms.tiles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pheenixm.explosivesplus.api.coms.INetworkEndpoint;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.NetworkHelper;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;

import gigaherz.graph2.GraphObject;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class CapabilityNetworkEndpointMultiblock extends CapabilityNetworkEndpointTile {

	public CapabilityNetworkEndpointMultiblock(World world, TileEntityMultiblock tile) {
		super(world, tile);
	}
	
	protected TileEntityMultiblock getTile() {
		return (TileEntityMultiblock)tile;
	}

	@Override
	public List<GraphObject> getNeighbors(IBlockAccess wor, BlockPos pos) {
		ArrayList<GraphObject> neighbors = new ArrayList<GraphObject>();
		World world = (World)wor;
		if(!world.isRemote) {
			for(int i : getTile().getMaster().core.structure.keySet()) {
				HashMap<Integer, BlockPos> level = getTile().getMaster().core.structure.get(i);
				for (int key : level.keySet()) {
					BlockPos blockPos = level.get(key);
					neighbors.addAll(NetworkHelper.getNeighbors(world, blockPos));
				}
			}
			INetworkEndpoint mast = getTile().getMaster().getCapability(CapabilityNetwork.ENDPOINT, null);
			if(mast != null && mast.getNetworkHandler() != null) {
				//Removes all instances of the master endpoint capability from the neighbors list
				while(neighbors.remove(mast.getNetworkHandler())) {}
			}
		}
		return neighbors;
	}


}
