package com.pheenixm.explosivesplus.common.coms.tiles;

import com.pheenixm.explosivesplus.api.coms.EnumCommand;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoderData;
import com.pheenixm.explosivesplus.common.coms.caps.CapabilityNetworkEndpoint;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;

import gigaherz.graph2.ConcurrentGraph;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class CapabilityNetworkEndpointSilo extends CapabilityNetworkEndpointMultiblock {

	protected TileEntitySilo tile; 
	
	public CapabilityNetworkEndpointSilo(World world, TileEntitySilo tile) {
		super(world, tile);
		this.tile = tile;
		init();
	}

	public void init() {
		ConcurrentGraph.integrate(networkHandler, getNeighbors(tile.getWorld(), tile.getPos()), (graph) -> new NetworkEncoderData());
        networkHandler.setPosition(tile.getPos());
	}

	
	@Override
	public void handleMessage(EnumCommand command, Object[] args) {
		switch(command) {
			case LAUNCH:
				handleLaunch();
			case TARGET:
				handleTarget(args);
		}
	}
	
	@Override
	public String getStatus() {
		if(tile.missile == null) {
			return "Missing missile!";
		}
		if(tile.isLaunching) {
			return "LAUNCHING";
		}
		if(tile.missile.get().canLaunch()) {
			return "Ready to launch, standing by";
		} else {
			return "Missile requires further assembly";
		}
	}

	protected void handleLaunch() {
		if(!world.isRemote) {
			tile.handleLaunch(null);
		}
	}
	
	protected void handleTarget(Object[] args) {
		if(!world.isRemote) {
			BlockPos pos = null;
			if(args.length == 1 && args[0] instanceof BlockPos) {
				pos = (BlockPos)args[0];
			}
			if(args.length == 2) {
				pos = new BlockPos((Integer)args[0], 0, (Integer)args[1]);
			}
			if(tile.missile != null) {
				tile.missile.get().updateTarget(pos.getX(), pos.getZ());
			}
		}
	}

}
