package com.pheenixm.explosivesplus.common.coms.caps;

import java.util.List;
import java.util.UUID;

import com.pheenixm.explosivesplus.api.coms.INetworkComponent;

import gigaherz.graph2.GraphObject;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

/**
 * Default capability for INetworkComponent
 * @author RobMontgomery
 *
 */
public class CapabilityNetworkComponent implements INetworkComponent {

	public CapabilityNetworkComponent() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public UUID getSharedUid() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GraphObject getNetworkHandler() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<GraphObject> getNeighbors(IBlockAccess world, BlockPos pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateNeighbors(IBlockAccess world, BlockPos pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onComponentDestroyed(BlockPos pos) {
		// TODO Auto-generated method stub
		
	}

}
