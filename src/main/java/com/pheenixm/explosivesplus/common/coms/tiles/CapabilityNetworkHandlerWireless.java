package com.pheenixm.explosivesplus.common.coms.tiles;

import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoder;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoderData;
import com.pheenixm.explosivesplus.common.coms.NetworkHelper;
import com.pheenixm.explosivesplus.common.coms.caps.CapabilityNetworkHandler;

import gigaherz.graph2.ConcurrentGraph;
import gigaherz.graph2.Graph;
import gigaherz.graph2.GraphObject;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;


public class CapabilityNetworkHandlerWireless<T extends TileEntity> extends CapabilityNetworkHandler {
		
	public TileEntity tile;
	
	public CapabilityNetworkHandlerWireless(TileEntity tile, int wirelessRange) {
		super();
		range = wirelessRange; 
		this.tile = tile;
		world = tile.getWorld();
	}

	@Override
	public void init(World worl) {
		if(worl != null) {
			super.init(worl);
			ConcurrentGraph.integrate(networkHandler, getNeighbors(tile.getWorld(), tile.getPos()), (graph) -> this.data);
			networkHandler.setPosition(tile.getPos());
		}
	}
	
	@Override
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		if(worldIn != null) {
			this.world = worldIn;
			HashSet<UUID> white = new HashSet<UUID>();
			white.add(NetworkHelper.getPlayerUID((EntityPlayer)placer));
			this.data = new NetworkEncoderData(white, new HashSet<UUID>());
			this.placer = (EntityPlayer) placer;
		}
	}

	
	
	@Override
	public List<GraphObject> getNeighbors(IBlockAccess world, BlockPos pos) {
		List<GraphObject> list = NetworkHelper.getNeighbors(world, pos);
		//This call adds all wireless neighbors to the list
		list.addAll(NetworkHelper.getWirelessInRange((World) world, this, getWirelessRange()));
		return list;
	}

	@Override
	public void updateNeighbors(IBlockAccess world, BlockPos pos) {
        Graph graph = networkHandler.getGraph();
        if (graph != null)
        {
            graph.addDirectedEdges(networkHandler, getNeighbors(world, pos));
        }
	}

	@Override
	public boolean joinExternalToHandlerNetwork() {
		return false;
	}

	@Override
	public boolean joinHandlerToExternalNetwork(BlockPos transmitter) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
