package com.pheenixm.explosivesplus.common.coms.tiles;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.common.collect.Lists;
import com.pheenixm.explosivesplus.api.coms.INetworkComponent;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoder;
import com.pheenixm.explosivesplus.common.coms.NetworkEncoderData;
import com.pheenixm.explosivesplus.common.coms.NetworkHelper;

import gigaherz.graph2.ConcurrentGraph;
import gigaherz.graph2.Graph;
import gigaherz.graph2.GraphObject;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

/**
 * @author RobMontgomery
 *
 * @param <T>
 */
public class CapabilityNetworkComponentTile<T extends TileEntity> implements INetworkComponent, ICapabilityProvider {

	NetworkEncoder networkHandler = new NetworkEncoder();
	private TileEntity tile;
	
	public CapabilityNetworkComponentTile(T tile) {
		this.tile = tile;
		init();
	}
	
	public void init() {
		ConcurrentGraph.integrate(networkHandler, getNeighbors(tile.getWorld(), tile.getPos()), (graph) -> new NetworkEncoderData());
        networkHandler.setPosition(tile.getPos());
	}

	@Override
	public UUID getSharedUid() {
		return networkHandler.getGraph().<NetworkEncoderData>getContextData().getUid();
	}

	@Override
	public GraphObject getNetworkHandler() {
		return networkHandler;
	}

	@Override
	public List<GraphObject> getNeighbors(IBlockAccess world, BlockPos pos) {
		return NetworkHelper.getNeighbors(world, pos);
	}

    @Override
    /**
     * This method gets called EVERY time there is a change in the graph
     * Means we can map endpoints and energy sinks to graphs
     */
    public void updateNeighbors(IBlockAccess world, BlockPos pos)
    {
        Graph graph = networkHandler.getGraph();
        if (graph != null)
        {
            graph.addDirectedEdges(networkHandler, getNeighbors(world, pos));
        }
    }

	@Override
	public void onComponentDestroyed(BlockPos pos) {
        Graph graph = networkHandler.getGraph();
        if (graph != null) {
            graph.remove(networkHandler);
        }
        //TODO: Handle removal of endpoints and energy in this method
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == CapabilityNetwork.COMPONENT;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		return capability == CapabilityNetwork.COMPONENT ? (T) this : null;

	}


}
