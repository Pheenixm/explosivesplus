package com.pheenixm.explosivesplus.common.coms;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.radar.RadarRegistry;
import com.pheenixm.explosivesplus.common.radar.RadarSavedData;
import com.pheenixm.explosivesplus.common.util.Utils;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.storage.MapStorage;
import net.minecraft.world.storage.WorldSavedData;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

//TODO: Must add refences from receiver to transmitter and vice-versa
public class WirelessRegistry extends WorldSavedData {

	
	private static final String DATA_NAME = ExplosivesPlus.MOD_ID + "_WIRELESS_Data";

	/**
	 * On initializing, each transmitter grabs its reference from this map and connects to the receivers on its list
	 */
	public Map<BlockPos, ArrayDeque<BlockPos>> wirelessMap = new ConcurrentHashMap<>();
	public boolean hasSatellite;

	private World world;

	
	public WirelessRegistry(String name) {
		super(name);
	}

	public static void load(World world) {
		if(world == null) {
			System.out.println("How was this called with a null world?");
		}
		MapStorage storage = world.getPerWorldStorage();
		WirelessRegistry instance = (WirelessRegistry) storage.getOrLoadData(WirelessRegistry.class, DATA_NAME);
		if (instance == null) {
			instance = new WirelessRegistry(DATA_NAME);
			storage.setData(DATA_NAME, instance);
		}
		instance.world = world;
		//Can iterate through list here if you so desire
		System.out.println("Created wireless registry");
	}
	
	//TODO: Modify this
	public static WirelessRegistry getRegistry(World world) {
		MapStorage storage = world.getPerWorldStorage();
		WirelessRegistry instance = (WirelessRegistry) storage.getOrLoadData(WirelessRegistry.class, DATA_NAME);
		if (instance == null) {
			instance = new WirelessRegistry(DATA_NAME);
			storage.setData(DATA_NAME, instance);
			instance.world = world;
		}
		return instance;
	}
	
	/**
	 * Called on a per-node basis to get its wireless neighbors
	 * @param world The world in
	 * @param base The position of the tile making this call
	 * @return an ArrayDeque of the tile's neighbors
	 */
	public static ArrayDeque<BlockPos> getNeighbors(World world, BlockPos base) {
		if(getRegistry(world).wirelessMap.containsKey(base)) {
			return getRegistry(world).wirelessMap.get(base);
		} else {
			return new ArrayDeque<BlockPos>();
		}
	}

	/**
	 * This method needs to add the wireless to a registry
	 * @param reciever
	 * @param transmitter
	 */
	public void connectNodeToNeighbor(BlockPos neighbor, BlockPos base) {
		//Some of this code is redundant
		initializePos(neighbor);
		initializePos(base);
		wirelessMap.get(base).add(neighbor);
		wirelessMap.get(neighbor).add(base);
		markDirty();
	}
	
	public void initializePos(BlockPos key) {
		if(!wirelessMap.containsKey(key)) {
			wirelessMap.put(key, new ArrayDeque<BlockPos>());
			markDirty();
		}
	}
		
	@Override
	public void readFromNBT(NBTTagCompound tag) {
		NBTTagList list = tag.getTagList("wirelessList", Constants.NBT.TAG_COMPOUND);
		for(NBTBase uncast : list) {
			NBTTagCompound com = (NBTTagCompound)uncast;
			BlockPos rPos = new BlockPos(Utils.arrayToVec3i(com.getIntArray("rPos")));
			BlockPos tPos = new BlockPos(Utils.arrayToVec3i(com.getIntArray("tPos")));
			//TODO: Put to list
			this.connectNodeToNeighbor(rPos, tPos);
		}
		hasSatellite = tag.getBoolean("sat");
	}

	/**
	 * Receivers are neighbors, transmitters are each node
	 * Map nodes to its neighbors
	 */
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound tag) {
		NBTTagList fullMap = new NBTTagList();
		for(BlockPos transmitter : WirelessRegistry.getRegistry(world).wirelessMap.keySet()) {
			ArrayDeque<BlockPos> receivers = WirelessRegistry.getNeighbors(world, transmitter);
			for(BlockPos rPos : receivers) {
				//Should prevent invalid entries from being saved
				//TODO: Add cases for entities
				if(world.getTileEntity(rPos) != null ) {
					NBTTagCompound comp = new NBTTagCompound();
					comp.setIntArray("rPos", Utils.vec3iToArray(rPos));
					comp.setIntArray("tPos", Utils.vec3iToArray(transmitter));
					fullMap.appendTag(comp);
				}
			}
		}
		tag.setTag("wirelessList", fullMap);
		tag.setBoolean("sat", hasSatellite);
		return tag;
	}	
	
	
	@SubscribeEvent 
	public static void onWorldLoaded(WorldEvent.Load event) {
		World world = event.getWorld();
		if(world != null && !world.isRemote) {
			WirelessRegistry.load(world);
		}
	}

 




}
