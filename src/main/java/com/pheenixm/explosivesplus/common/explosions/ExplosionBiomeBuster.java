package com.pheenixm.explosivesplus.common.explosions;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.ServerProxy;
import com.pheenixm.explosivesplus.common.SoundHandler;
import com.pheenixm.explosivesplus.common.entities.EntityBiomeBuster;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.util.Utils;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockOldLeaf;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.multiplayer.ChunkProviderClient;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.ChunkProviderServer;

public class ExplosionBiomeBuster extends Explosion {

	private static final int RANGE = 16;
	private final String biome;

	public ExplosionBiomeBuster(World worldIn, Entity entity, double x,
			double y, double z) {
		super(worldIn, entity, x, y, z, RANGE);
		biome = world.getBiome(blockPosition).getBiomeName();
	}

	@Override
	public HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> getAffectedChunksAndBlocks() {
		setBusting();
		HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> affected = new HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>>();

		Queue<BlockPos> queue = new ArrayDeque<BlockPos>();
		HashMap<Integer, Boolean> visited = new HashMap<Integer, Boolean>();
		queue.add(blockPosition);
		setVisited(visited, blockPosition);

		while (!queue.isEmpty() && affected.size() <= maximum_chunks) {
			BlockPos pos = queue.remove();
			ChunkPos chunkPos = new ChunkPos(pos);
			checkAndForceLoad(pos, chunkPos);
			ArrayList<BlockPos> affectedColumn = getColumn(pos);
			affected = addToHashMap(affected, affectedColumn, pos, chunkPos);
			for (BlockPos childPos : getChildren(pos)) {
				ChunkPos childChunk = new ChunkPos(childPos);
				if (!visited.containsKey(childPos.hashCode())
						&& isBiome(childPos) && shouldLoad(childPos)) {
					setVisited(visited, childPos);
					queue.add(childPos);
				}
			}

		}
		System.out.println(affected.size() + " chunks affected");
		if (ExplosivesPlus.ticketList != null) {
			System.out.println(
					"Loaded " + ExplosivesPlus.ticketList.size() + " chunks");
		}
		return affected;
	}

	private void setBusting() {
		if(!ServerProxy.isBusting) {
			ServerProxy.isBusting = true;
		}	
	}

	private void setNotBusting() {
		if(ServerProxy.isBusting) {
			ServerProxy.isBusting = false;
		}
	}
	private boolean shouldLoad(BlockPos childPos) {
		if (!world.isRemote) {
			return true;
		} else {
			if (world.isBlockLoaded(childPos)) {
				return true;
			}
		}
		return false;
	}

	private void setVisited(HashMap<Integer, Boolean> visited, BlockPos pos) {
		visited.put(pos.hashCode(), true);
	}

	private BlockPos[] getChildren(BlockPos pos) {
		BlockPos[] children = {
				new BlockPos(pos.getX() + 1, pos.getY(), pos.getZ()),
				new BlockPos(pos.getX(), pos.getY(), pos.getZ() + 1),
				new BlockPos(pos.getX() - 1, pos.getY(), pos.getZ()),
				new BlockPos(pos.getX(), pos.getY(), pos.getZ() - 1)};
		return children;
	}

	private boolean isBiome(BlockPos pos) {
		return world.getBiome(pos).getBiomeName().equals(biome);
	}

	// get all blocks in a Y-column, defined by x and z position
	private ArrayList<BlockPos> getColumn(BlockPos xyPos) {
		ArrayList<BlockPos> yColumn = new ArrayList<BlockPos>();
		for (int y = 0; y < world.getHeight(); y++) {
			BlockPos iterPos = new BlockPos(xyPos.getX(), y, xyPos.getZ());
			if (!world.isAirBlock(iterPos)) {
				yColumn.add(iterPos);
			}
		}
		return yColumn;
	}

	@Override
	public synchronized void processBlocks(Chunk chunk) {
		setBusting();
		ArrayList<BlockPos> blocks = this.getAffectedBlocksByChunk()
				.get(chunk.getPos());
		for (BlockPos blockPos : blocks) {
			IBlockState air = Blocks.AIR.getDefaultState();
			IBlockState oldState = chunk.getWorld().getBlockState(blockPos);


			if (affectBedrock) {
				chunk.setBlockState(blockPos, air);
				chunk.getWorld().markBlockRangeForRenderUpdate(blockPos,
						blockPos.offset(EnumFacing.UP));
			} else {
				if (oldState.getBlockHardness(chunk.getWorld(),
						blockPos) != -1) {
					// If block is not unbreakable
					chunk.setBlockState(blockPos, air);
					chunk.getWorld().markBlockRangeForRenderUpdate(blockPos,
							blockPos.offset(EnumFacing.UP));

				}

			}

		}
		chunk.markDirty();
	}

	@Override
	public boolean doPostExplosion() {
		playEffects();
		setNotBusting();
		for (int i = 0; i < 20000; i++) {
			BlockPos pos = postProcessingQueue.poll();
			if (pos != null) {
				world.setBlockToAir(pos);
			} else {
				return true;
			}
		}
		return false;
	}

	@Override
	public void playEffects() {
		System.out.println("Playing sound?");
		controller.playSound(SoundHandler.BIOME_BUST, 256, (1.0F + (this.world.rand.nextFloat() - this.world.rand.nextFloat()) * 0.2F) * 0.7F);
		ExplosionHelper.spawnShockwave(world, x, y, z, 20);
	}

	@Override
	public EntityExplosionController initiate(IBlockState parent, boolean lag) {
		if(lag) {
			EntityBiomeBuster buster = new EntityBiomeBuster(world, x, y, z, detonator);
			buster.setFuse(1);
			world.spawnEntity(buster);
			return null;
		} else {
			EntityExplosionController controller = new EntityExplosionController(world, null, x, y, z, this, parent, false);
			setController(controller);
			world.spawnEntity(controller);
			return controller;
		}
	}
}
