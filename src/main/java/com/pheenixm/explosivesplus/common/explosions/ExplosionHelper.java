package com.pheenixm.explosivesplus.common.explosions;

import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.entities.EntityShockwave;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class ExplosionHelper {

	public static EntityExplosionController createExplosion(World world, EntityPlayer player, double x,
			double y, double z, Explosions exp, boolean lag, Integer...integers ) {
		switch(exp) {
			case BIOME: {
				if (!world.isRemote) {
					ExplosionBiomeBuster explosion = new ExplosionBiomeBuster(world, player, x, y, z);
					return explosion.initiate(ExplosivesPlusHolder.BIOME.getDefaultState(), lag); 
				}
				return null;
			}
			case CLASSIC: {
				int size = 20;
				int rays = 16;
				if(integers.length == 1) {
					size = integers[0];
				}
				if(integers.length == 2) {
					size = integers[0];
					rays = integers[1];
				}
				ExplosionClassic explosion = new ExplosionClassic(world, player, x, y, z, size, rays);
				return explosion.initiate(ExplosivesPlusHolder.CONCRETE.getDefaultState(), lag);
			}
			case CLASSIC_MULTI: {
				int size = 20;
				int rays = 16;
				if(integers.length == 1) {
					size = integers[0];
				}
				if(integers.length == 2) {
					size = integers[0];
					rays = integers[1];
				}
				if(!world.isRemote) {
					ExplosionClassicMulti explosion = new ExplosionClassicMulti(world, player, x, y, z, size, rays);
					return explosion.initiate(ExplosivesPlusHolder.CONCRETE.getDefaultState(), lag);
				}
				return null;
			}
			case MEGA: {
				int size = ExplosionMega.DEFAULT_SIZE;
				if(integers.length > 0) {
					size = integers[0];
				}
				if(!world.isRemote) {
					ExplosionMega explosion = new ExplosionMega(world, player, x, y, z, size);
					return explosion.initiate(ExplosivesPlusHolder.MEGA.getDefaultState(), lag);
				}
				return null;
			}
			case NOVA: {
				int size = EntityNovaController.DEFAULT_RADIUS;
				if(integers.length > 0) {
					size = integers[0];
				}
				ExplosionNova explosion = new ExplosionNova(world, player, x, y, z, size);
				return explosion.initiate(ExplosivesPlusHolder.NOVA.getDefaultState(), lag);
			}
			case CHAOS: {
				ExplosionChaos explosion = new ExplosionChaos(world, player, x, y, z);
				return explosion.initiate(ExplosivesPlusHolder.CHAOS.getDefaultState(), lag);
			}
			case TNT_RAIN: {
				int layers = ConfigHandler.TNT_RAIN_LAYERS;
				double height = ConfigHandler.TNT_RAIN_HEIGHT;
				if(integers.length > 0) {
					layers = integers[0];
					if(integers.length > 1) {
						height = integers[1];
					}
				}
				ExplosionTNTRain explosion = new ExplosionTNTRain(world, player, x, height, z, layers);
				return explosion.initiate(ExplosivesPlusHolder.RAIN.getDefaultState(), lag);
			}
			case DISRUPTOR: {
				int size = ConfigHandler.MOLECULAR_DISRUPTOR_RANGE;
				if(integers.length > 0) {
					size = integers[0];
				}
				ExplosionMolecularDisruptor disrupt = new ExplosionMolecularDisruptor(world, player, x, y, z, size);
				return disrupt.initiate(ExplosivesPlusHolder.DISRUPT.getDefaultState(), lag);
			}
			default:
				return null;
		}
		
	}
	
	public static void spawnShockwave(World world, double x, double y, double z, int... args) {
		int size = -1;
		int time = -1;
		if(args.length == 1) {
			size = args[0];
		}
		if(args.length == 2) {
			size = args[0];
			time = args[1];
		}
		if(!world.isRemote) {
			EntityShockwave shock = new EntityShockwave(world, x, y, z);
			shock.setSizeAndTime(size < 0 ? ConfigHandler.SHOCKWAVE_DEFAULT_SIZE : size, time < 0 ? ConfigHandler.SHOCKWAVE_DEFAULT_TIME : time);
			world.spawnEntity(shock);
		}
	}
	
	public enum Explosions {
		BIOME,
		CLASSIC,
		CLASSIC_MULTI,
		MEGA,
		TNT_RAIN,
		CHAOS,
		NOVA,
		DISRUPTOR;
	}

}
