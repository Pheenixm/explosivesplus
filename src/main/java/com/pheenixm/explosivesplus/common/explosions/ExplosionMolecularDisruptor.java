package com.pheenixm.explosivesplus.common.explosions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.entities.EntityMolecularDisruptor;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityBatchExplosionController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class ExplosionMolecularDisruptor extends Explosion {
	
	public ExplosionMolecularDisruptor(World worldIn, Entity entity, double x, double y, double z, int size) {
		super(worldIn, entity, x, y, z, size);
	}

	@Override
	public HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> getAffectedChunksAndBlocks() {
		return null;
	}

	@Override
	public void processBlocks(Chunk chunk) {}

	@Override
	public boolean doPostExplosion() {
		if(!world.isRemote) {
			EntityBatchExplosionController batch = new EntityBatchExplosionController(world, x, y, z, ConfigHandler.MOLECULAR_DISRUPTOR_SIZE);
			List<Entity> ents = world.getEntitiesWithinAABBExcludingEntity(detonator, new AxisAlignedBB((double)x - radius,(double) y - radius,(double) z- radius, 
					(double)x + radius, (double)y + radius,(double) z + radius));
			for(Entity ent : ents) {
				if(ent instanceof EntityLiving && (ent.posY >= 32 || world.canSeeSky(ent.getPosition()))) {
					batch.addExplosionAtPoint(ent.getPosition(), Explosions.CLASSIC);
				}
			}
			world.spawnEntity(batch);
		}
		
		playEffects();
		return true;
	}

	@Override
	public void playEffects() {
		//TODO: Add sci-fi effect sounds
	}

	@Override
	public EntityExplosionController initiate(IBlockState parent, boolean lag) {
		if(lag) {
			EntityMolecularDisruptor disrupt = new EntityMolecularDisruptor(world, x, y, z, detonator);
			disrupt.setFuse(1);
			world.spawnEntity(disrupt);
			return null;
		} else {
			EntityExplosionController controller = new EntityExplosionController(world, detonator, x, y, z, this, parent, true);
			setController(controller);
			world.spawnEntity(controller);
			return controller;
		}
	}

}
