package com.pheenixm.explosivesplus.common.explosions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.chunker.ChunkerProcessor;
import com.pheenixm.explosivesplus.common.entities.EntityBiomeBuster;
import com.pheenixm.explosivesplus.common.entities.EntityNuke;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.EnchantmentProtection;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.network.play.server.SPacketChunkData;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;

//TODO: Implement damaging of entities
public class ExplosionClassic extends Explosion {

	public int raysPerAxis = 16;

	public ExplosionClassic(World worldIn, Entity entity, double x, double y,
			double z, int size, int rays) {
		super(worldIn, entity, x, y, z, size);
		raysPerAxis = rays;
	}

	@Override
	public HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> getAffectedChunksAndBlocks() {
		HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> affected = new HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>>();

		for (int j = 0; j < raysPerAxis; ++j) {
			for (int k = 0; k < raysPerAxis; k++) {
				for (int l = 0; l < raysPerAxis; l++) {
					if (j == 0 || j == raysPerAxis - 1 || k == 0
							|| k == raysPerAxis - 1 || l == 0
							|| l == raysPerAxis - 1) {
						double d0 = (double) ((float) j
								/ (this.raysPerAxis - 1F) * 2.0F - 1.0F);
						double d1 = (double) ((float) k
								/ (this.raysPerAxis - 1F) * 2.0F - 1.0F);
						double d2 = (double) ((float) l
								/ (this.raysPerAxis - 1F) * 2.0F - 1.0F);
						double d3 = Math.sqrt(d0 * d0 + d1 * d1 + d2 * d2);
						d0 = d0 / d3;
						d1 = d1 / d3;
						d2 = d2 / d3;
						float f = radius
								* (0.7F + this.world.rand.nextFloat() * 0.6F);
						double d4 = this.x;
						double d6 = this.y;
						double d8 = this.z;

						for (float f1 = 0.3F; f > 0.0F; f -= 0.22500001F) {
							BlockPos blockpos = new BlockPos(d4, d6, d8);
							IBlockState iblockstate = this.world
									.getBlockState(blockpos);

							if (iblockstate.getMaterial() != Material.AIR) {
								float f2 = iblockstate.getBlock()
										.getExplosionResistance(world, blockpos,
												this.getController(), null);
								f -= (f2 + 0.3F) * 0.3F;
							}

							if (f > 0.0F) {
								buildList(blockpos, affected);
							}

							d4 += d0 * 0.30000001192092896D;
							d6 += d1 * 0.30000001192092896D;
							d8 += d2 * 0.30000001192092896D;
						}
					}
				}

			}
		}

		System.out.println("Affected size is " + affected.size());
		return affected;
	}

	private void buildList(BlockPos blockPos,
			HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> affected) {
		ChunkPos chunk = new ChunkPos(blockPos);
		BlockPos basePos = new BlockPos(blockPos.getX(), 64, blockPos.getY());
		if (affected.containsKey(chunk)) {
			if (!affected.get(chunk).containsKey(basePos)) {
				// We have to create the array
				ArrayList<BlockPos> blocker = new ArrayList<BlockPos>(256);
				affected.get(chunk).put(basePos, blocker);
			}

		} else {
			HashMap<BlockPos, ArrayList<BlockPos>> map = new HashMap<BlockPos, ArrayList<BlockPos>>();
			ArrayList<BlockPos> blocker = new ArrayList<BlockPos>(256);
			map.put(basePos, blocker);
			affected.put(chunk, map);
		}
		affected.get(chunk).get(basePos).add(blockPos);

	}

	@Override
	public synchronized void processBlocks(Chunk chunk) {
		ArrayList<BlockPos> blocks = this.getAffectedBlocksByChunk()
				.get(chunk.getPos());
		for (BlockPos blockPos : blocks) {
			if (blockPos.getY() > 0) {
				IBlockState airState = Blocks.AIR.getDefaultState();

				world.setBlockState(blockPos, airState, 3);
				world.markBlockRangeForRenderUpdate(blockPos,
						blockPos.offset(EnumFacing.UP));
			}
		}

	}

	@Override
	public boolean doPostExplosion() {
		if (!world.isRemote) {
			HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> aff = getAffectedChunksAndBlocks();
			HashMap<ChunkPos, ArrayList<BlockPos>> affectedByChunk = toChunkMaps(aff);
			for (ChunkPos cPos : affectedByChunk.keySet()) {
				Chunk chun = world.getChunk(cPos.x, cPos.z);
				processBlocks(chun)	;
				
			}
		}
		playEffects();
		return true;
	}

	@Override
	public void playEffects() {
		processKnockback();
		ExplosionHelper.spawnShockwave(world, x, y, z, radius);
		
        this.world.playSound((EntityPlayer)null, this.x, this.y, this.z, SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 4.0F, (1.0F + (this.world.rand.nextFloat() - this.world.rand.nextFloat()) * 0.2F) * 0.7F);

        if (this.radius >= 2.0F)
        {
            this.world.spawnParticle(EnumParticleTypes.EXPLOSION_HUGE, this.x, this.y, this.z, 1.0D, 0.0D, 0.0D);
        }
        else
        {
            this.world.spawnParticle(EnumParticleTypes.EXPLOSION_LARGE, this.x, this.y, this.z, 1.0D, 0.0D, 0.0D);
        }
    }


	@Override
	public EntityExplosionController initiate(IBlockState parent, boolean lag) {
		if(lag) {
			EntityNuke nuke = new EntityNuke(world, x, y, z, detonator, true);
			nuke.setSize(this.radius);
			nuke.setFuse(1);
			world.spawnEntity(nuke);
			return null;
		} else {
			EntityExplosionController controller = new EntityExplosionController(
					world, detonator, x, y, z, this, parent, true);
			setController(controller);
			world.spawnEntity(controller);
			return controller;
		}
	}

}
