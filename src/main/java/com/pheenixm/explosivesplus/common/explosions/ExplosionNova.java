package com.pheenixm.explosivesplus.common.explosions;

import java.util.ArrayList;
import java.util.HashMap;

import com.pheenixm.explosivesplus.common.entities.EntityNovaBomb;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class ExplosionNova extends Explosion {

	public ExplosionNova(World worldIn, Entity entity, double x, double y, double z, int size) {
		super(worldIn, entity, x, y, z, size);
	}

	@Override
	public HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> getAffectedChunksAndBlocks() {
		return new HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>>();
	}

	@Override
	/**
	 * Nova bomb performs no multi-threaded operations
	 * so ignore 
	 */
	public void processBlocks(Chunk chunk) {}

	@Override
	public boolean doPostExplosion() {
		EntityNovaController nova = new EntityNovaController(world, x, y, z, radius);
		world.spawnEntity(nova);
		ExplosionHelper.spawnShockwave(world, x, y, z, radius, 10);
		return true;
	}

	@Override
	public void playEffects() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EntityExplosionController initiate(IBlockState parent, boolean lag) {
		if(lag) {
			EntityNovaBomb nova = new EntityNovaBomb(world, x, y, z, detonator);
			nova.setFuse(1);
			world.spawnEntity(nova);
			return null;
		} else {
			EntityExplosionController controller = new EntityExplosionController(world, detonator, x, y, z, this, parent, true);
			setController(controller);
			world.spawnEntity(controller);	
			return controller;
		}
	}

}
