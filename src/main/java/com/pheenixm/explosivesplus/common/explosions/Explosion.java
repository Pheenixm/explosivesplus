package com.pheenixm.explosivesplus.common.explosions;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;

import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.EnchantmentProtection;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class Explosion {

	protected Entity detonator;
	protected EntityExplosionController controller;
	public World world;
	public double x;
	public double y;
	public double z;
	public BlockPos blockPosition;
	protected HashMap<ChunkPos, ArrayList<BlockPos>> affectedBlocksByChunk;
	public HashMap<ChunkPos, ArrayList<BlockPos>> chunkTracker;
	protected boolean affectBedrock = false;
	protected int radius = 64;
	//TODO: make this configurable
	protected int maximum_chunks = 2000;

	public boolean initialInjectionComplete = false;
	public Queue<BlockPos> postProcessingQueue = new ArrayDeque<BlockPos>();
	
	/**
	 * 
	 * @param worldIn
	 * @param entity
	 * @param x
	 * @param y
	 * @param z
	 * @param size Pass in -1 to use default size
	 */
	public Explosion(World worldIn, Entity entity, double x, double y, double z, int size) {
		this.x = x;
		this.y = y;
		this.z = z;
		world = worldIn;
		detonator = entity;
		blockPosition = new BlockPos(MathHelper.floor(x), MathHelper.floor(y), MathHelper.floor(z));
		postProcessingQueue = new ArrayDeque<BlockPos>();
		if(size > 0) {
			radius = size;
		}
	}

	public abstract HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> getAffectedChunksAndBlocks();
	
	public abstract void processBlocks(Chunk chunk);
	
	/**
	 * Perform post-multi threading operations
	 * - if you want to avoid traditional multi-threading and use direct queuing
	 * or perform single-threaded operations, perform them here
	 * @return true if the post-explosion operation was a success, false otherwise
	 */
	public abstract boolean doPostExplosion();
	
	public abstract void playEffects();
	
	public abstract EntityExplosionController initiate(IBlockState parent, boolean lag);
	
	
	public HashMap<ChunkPos, ArrayList<BlockPos>> toChunkMaps(HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> map) {
		HashMap<ChunkPos, ArrayList<BlockPos>> affected = new HashMap<ChunkPos, ArrayList<BlockPos>>();
		if(map.isEmpty()) {
			return affected;
		}
		for(ChunkPos pos : map.keySet()) {
			ArrayList<BlockPos> blocks = new ArrayList<BlockPos>();
			HashMap<BlockPos, ArrayList<BlockPos>> submap = map.get(pos);
			for(ArrayList<BlockPos> array : submap.values()) {
				blocks.addAll(array);
			}
			affected.put(pos, blocks);
			
		}
		setAffectedBlocksByChunk(affected);
		chunkTracker = (HashMap<ChunkPos, ArrayList<BlockPos>>) this.affectedBlocksByChunk.clone();
		return affected;
	}
	
	// Add array list to existing key value, or create new key
	public synchronized HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> addToHashMap(
			HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> affected,
			ArrayList<BlockPos> yColumn, BlockPos pos, ChunkPos chunkPos) {
		HashMap<BlockPos, ArrayList<BlockPos>> blocks = null;
		if (!affected.containsKey(chunkPos)) {
			// initialize Block HashMap for this chunk
			blocks = new HashMap<BlockPos, ArrayList<BlockPos>>();
			blocks.put(pos, yColumn);
			affected.put(chunkPos, blocks);
		} else {
			blocks = affected.get(chunkPos);
			if (!blocks.containsKey(pos)) {
				if (blocks.putIfAbsent(pos, yColumn) != null)
					System.out.println(
							"Something is wrong, this shouldn't ever be called");
			}
		}
		return affected;
	}
	
	protected void checkAndForceLoad(BlockPos pos, ChunkPos chunkPos) {
		if (!world.isBlockLoaded(pos)) {
			if (!world.isRemote) {
				ExplosivesPlus.instance.forceChunkLoad(world, chunkPos);
			}
		}

	}

	protected void processKnockback() {
		int f3 = this.radius * 2;
        int k1 = MathHelper.floor(this.x - (double)f3 - 1.0D);
        int l1 = MathHelper.floor(this.x + (double)f3 + 1.0D);
        int i2 = MathHelper.floor(this.y - (double)f3 - 1.0D);
        int i1 = MathHelper.floor(this.y + (double)f3 + 1.0D);
        int j2 = MathHelper.floor(this.z - (double)f3 - 1.0D);
        int j1 = MathHelper.floor(this.z + (double)f3 + 1.0D);
        List<Entity> list = this.world.getEntitiesWithinAABBExcludingEntity(this.controller, new AxisAlignedBB((double)k1, (double)i2, (double)j2, (double)l1, (double)i1, (double)j1));

        Vec3d vec3d = new Vec3d(this.x, this.y, this.z);

        for (int k2 = 0; k2 < list.size(); ++k2)
        {
            Entity entity = list.get(k2);

            if (!entity.isImmuneToExplosions())
            {
                double d12 = entity.getDistance(this.x, this.y, this.z) / (double)f3;

                if (d12 <= 1.0D)
                {
                    double d5 = entity.posX - this.x;
                    double d7 = entity.posY + (double)entity.getEyeHeight() - this.y;
                    double d9 = entity.posZ - this.z;
                    double d13 = (double)MathHelper.sqrt(d5 * d5 + d7 * d7 + d9 * d9);

                    if (d13 != 0.0D)
                    {
                        d5 = d5 / d13;
                        d7 = d7 / d13;
                        d9 = d9 / d13;
                        double d14 = (double)this.world.getBlockDensity(vec3d, entity.getEntityBoundingBox());
                        double d10 = (1.0D - d12) * d14;
                        entity.attackEntityFrom(DamageSource.GENERIC, (float)((int)((d10 * d10 + d10) / 2.0D * 7.0D * (double)f3 + 1.0D)));
                        double d11 = d10;

                        if (entity instanceof EntityLivingBase)
                        {
                            d11 = EnchantmentProtection.getBlastDamageReduction((EntityLivingBase)entity, d10);
                        }

                        entity.motionX += d5 * d11;
                        entity.motionY += d7 * d11;
                        entity.motionZ += d9 * d11;

                        /* Disabled pending fix
                         * TODO: REENABLE THIS
                        if (entity instanceof EntityPlayer)
                        {
                            EntityPlayer entityplayer = (EntityPlayer)entity;

                            if (!entityplayer.isSpectator() && (!entityplayer.isCreative() || !entityplayer.capabilities.isFlying))
                            {
                                this.playerKnockbackMap.put(entityplayer, new Vec3d(d5 * d10, d7 * d10, d9 * d10));
                            }
                        }
                        */
                    }
                }
            }
        }	
    }


	

	public HashMap<ChunkPos, ArrayList<BlockPos>> getAffectedBlocksByChunk() {
		return affectedBlocksByChunk;
	}

	public void setAffectedBlocksByChunk(HashMap<ChunkPos, ArrayList<BlockPos>> affectedBlocksByChunk) {
		this.affectedBlocksByChunk = affectedBlocksByChunk;
	}
	
	public void setController(EntityExplosionController control) {
		this.controller = control;
	}
	
	public EntityExplosionController getController() {
		return this.controller;
	}
	
	public void removeAffectedChunk(Chunk chunk) {
		chunkTracker.remove(chunk.getPos());
	}

	public boolean isAffectBedrock() {
		return affectBedrock;
	}

	public void setAffectBedrock(boolean affectBedrock) {
		this.affectBedrock = affectBedrock;
	}
}
