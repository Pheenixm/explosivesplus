package com.pheenixm.explosivesplus.common.explosions;

import java.util.ArrayList;
import java.util.HashMap;

import com.pheenixm.explosivesplus.common.entities.EntityMegaNuke;
import com.pheenixm.explosivesplus.common.entities.EntityNuke;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ExplosionMega extends Explosion {

	public static final int DEFAULT_SIZE = 96;

	public ExplosionMega(World worldIn, Entity entity, double x, double y,
			double z, int size) {
		super(worldIn, entity, x, y, z, size);
	}

	@Override
	public HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> getAffectedChunksAndBlocks() {

		HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> affected = new HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>>();

		for (int i1 = -radius; i1 < radius; i1++) {
			for (int k1 = -radius; k1 < radius; k1++) {
				BlockPos keyPos = blockPosition.add(i1, 0, k1);
				ChunkPos keyChunk = new ChunkPos(keyPos);
				ArrayList<BlockPos> yColumn = new ArrayList<BlockPos>(256);
				checkAndForceLoad(keyPos, keyChunk);
				for (int j1 = -radius; j1 < radius; j1++) {
					BlockPos pos = blockPosition.add(i1, j1, k1);
					double dist = blockPosition.getDistance(pos.getX(),
							pos.getY(), pos.getZ());
					if (dist <= radius) {
						if (!world.isAirBlock(pos)) {
							yColumn.add(pos);
						}
					} else {
						if (dist < radius + 1 && dist > radius) {
							// ADDS RANDOMNESS
							if (world.rand.nextInt(3) > 0
									&& !world.isAirBlock(pos)) {
								yColumn.add(pos);
							}
						}
					}
				}
				if (yColumn.size() > 0) {
					this.addToHashMap(affected, yColumn, keyPos,
							new ChunkPos(keyPos));
				}
			}
		}
		return affected;
	}

	@Override
	public synchronized void processBlocks(Chunk chunk) {
		ArrayList<BlockPos> blocks = this.getAffectedBlocksByChunk().get(chunk.getPos());
		for (BlockPos blockPos : blocks) {
			if (blockPos.getY() > 0) {
				double dist = 1;
				IBlockState airState = Blocks.AIR.getDefaultState();
				IBlockState fireState = Blocks.FIRE.getDefaultState();
				IBlockState oldState = chunk.getWorld().getBlockState(blockPos);

				if (affectBedrock) {
					if (dist <= radius) {
						chunk.setBlockState(blockPos, airState);
						chunk.getWorld().markBlockRangeForRenderUpdate(blockPos,blockPos.offset(EnumFacing.UP));
					} else {
						if (dist < radius + 1 && dist > radius) {
							chunk.setBlockState(blockPos, fireState);
							chunk.getWorld().markBlockRangeForRenderUpdate(blockPos,blockPos.offset(EnumFacing.UP));
						}
					}

				} else {
					if (oldState.getBlockHardness(chunk.getWorld(),
							blockPos) != -1) {
						if (dist <= radius) {
							chunk.setBlockState(blockPos, airState);
							chunk.getWorld().markBlockRangeForRenderUpdate(blockPos,blockPos.offset(EnumFacing.UP));
						} else {
							if (dist < radius + 1 && dist > radius) {
								chunk.setBlockState(blockPos, fireState);
								chunk.getWorld().markBlockRangeForRenderUpdate(blockPos,blockPos.offset(EnumFacing.UP));
							}
						}
					}
				}
			}
		}

	}
	
	//TODO: IMPLEMENT THIS
	public void playEffects() {
		world.spawnParticle(EnumParticleTypes.EXPLOSION_HUGE, x, y, z, 0, 0, 0);
	}
	

	@Override
	public boolean doPostExplosion() {
		playEffects();
		return true;
	}

	@Override
	public EntityExplosionController initiate(IBlockState parent, boolean lag) {
		if(world.isRemote) {
			System.out.println("Shouldn't be called");
		}
		if(lag) {
			EntityMegaNuke nuke = new EntityMegaNuke(world, x, y, z, detonator);
			nuke.setFuse(1);
			world.spawnEntity(nuke);
			return null;
		} else {
			EntityExplosionController controller = new EntityExplosionController(world, detonator, x, y, z, this, parent, false);
			setController(controller);
			world.spawnEntity(controller);	
			return controller;
		}
	}

}
