package com.pheenixm.explosivesplus.common.explosions;

import java.util.ArrayList;
import java.util.HashMap;

import com.pheenixm.explosivesplus.common.entities.EntityTNTRain;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTNTRainController;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class ExplosionTNTRain extends Explosion {

	public ExplosionTNTRain(World worldIn, Entity entity, double x, double y, double z, int layers) {
		super(worldIn, entity, x, y, z, layers);
	}

	@Override
	public HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> getAffectedChunksAndBlocks() {
		return new HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>>();
	}

	@Override
	/**
	 * TNT Rain performs no multi-threaded operations
	 * so ignore 
	 */
	public void processBlocks(Chunk chunk) {}

	@Override
	public boolean doPostExplosion() {
		//Hijack radius var to hold layers
		EntityTNTRainController rain = new EntityTNTRainController(world, x, y, z, radius);
		world.spawnEntity(rain);
		return true;
	}

	@Override
	public void playEffects() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EntityExplosionController initiate(IBlockState parent, boolean lag) {
		if(lag) {
			EntityTNTRain rain = new EntityTNTRain(world, x, y, z, detonator);
			rain.setFuse(1);
			world.spawnEntity(rain);
			return null;
		} else {
			EntityExplosionController controller = new EntityExplosionController(world, detonator, x, y, z, this, parent, true);
			setController(controller);
			world.spawnEntity(controller);	
			return controller;
		}
	}

}
