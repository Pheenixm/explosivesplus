package com.pheenixm.explosivesplus.common.explosions;

import java.util.ArrayList;
import java.util.HashMap;

import com.pheenixm.explosivesplus.common.entities.EntityTheoryOfChaos;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTheoryOfChaosController;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class ExplosionChaos extends Explosion {

	private String seed;
	
	public ExplosionChaos(World worldIn, Entity entity, double x, double y, double z, String...strings) {
		super(worldIn, entity, x, y, z, -1);
		if(strings.length > 0) {
			seed = strings[0];
		} else {
			seed = null;
		}
	}

	@Override
	public HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> getAffectedChunksAndBlocks() {
		return new HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>>();
	}

	@Override
	/**
	 * Nova bomb performs no multi-threaded operations
	 * so ignore 
	 */
	public void processBlocks(Chunk chunk) {}

	@Override
	public boolean doPostExplosion() {
		EntityTheoryOfChaosController chaos = new EntityTheoryOfChaosController(world, x, y, z, seed);
		world.spawnEntity(chaos);
		return true;
	}

	@Override
	public void playEffects() {
		
	}

	@Override
	public EntityExplosionController initiate(IBlockState parent, boolean lag) {
		if(lag) {
			EntityTheoryOfChaos chaos = new EntityTheoryOfChaos(world, x, y, z, detonator, seed);
			chaos.setFuse(1);
			world.spawnEntity(chaos);
			return null;
		}
		EntityExplosionController controller = new EntityExplosionController(world, detonator, x, y, z, this, parent, true);
		setController(controller);
		world.spawnEntity(controller);	
		return controller;
	}

}
