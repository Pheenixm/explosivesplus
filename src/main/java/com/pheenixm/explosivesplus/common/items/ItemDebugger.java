package com.pheenixm.explosivesplus.common.items;

import java.util.List;

import org.lwjgl.util.glu.Sphere;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.client.RenderView;
import com.pheenixm.explosivesplus.client.gui.GuiChaosTheory;
import com.pheenixm.explosivesplus.client.gui.GuiPredatorMissile;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.entities.EntityPhysicsBlock;
import com.pheenixm.explosivesplus.common.entities.EntityTargeter;
import com.pheenixm.explosivesplus.common.entities.EntityTargeter.EnumTargetingType;
import com.pheenixm.explosivesplus.common.entities.EntityViewer;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityPredatorMissile;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityValkyrie;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTheoryOfChaosController;
import com.pheenixm.explosivesplus.common.explosions.ExplosionBiomeBuster;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class ItemDebugger extends Item {

	public ItemDebugger(String itemName) {
		setItemName(this, itemName);
		setCreativeTab(ExplosivesPlus.tab);
	}

	public static void setItemName(Item item, String itemName) {
		item.setRegistryName(ExplosivesPlus.MOD_ID, itemName);
		item.setTranslationKey(item.getRegistryName().toString());
	}

	public int getMaxItemUseDuration(ItemStack itemstack) {
		return 1;
	}

	public EnumActionResult onItemUse(EntityPlayer player, World world,
			BlockPos pos, EnumHand hand, EnumFacing facing, float hitX,
			float hitY, float hitZ) {
			System.out.println(world.getBlockState(pos));
			TileEntity ent = world.getTileEntity(pos);
			if (ent instanceof TileEntityMultiblock) {
				TileEntityMultiblock mult = (TileEntityMultiblock) ent;
				//player.sendStatusMessage(new TextComponentString(
				//		mult.getMaster()
				//		+ "\n " + pos.toString()), true);
				System.out.println(mult.getMaster());
			}
		return EnumActionResult.PASS;
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World world,
			EntityPlayer player, EnumHand handIn) {

		ItemStack itemstack = player.getHeldItem(handIn);
		/*
		if(!world.isRemote) {
			EntityTheoryOfChaosController block = new EntityTheoryOfChaosController(world, 
					player.posX + 0.5D, player.posY, player.posZ + 0.5D, player.getName());
			//Gives upward velocity
			world.spawnEntity(block);
		}*/
		if(!world.isRemote) {
			EntityValkyrie predator = new EntityValkyrie(world, player, 
					player.posX + 5, 
					player.posY, 
					player.posZ + 5);
			world.spawnEntity(predator); 
		}
		
		/*
		if(!world.isRemote) {
			EntityTargeter pig = new EntityTargeter(world, player.posX, player.posY, player.posZ, player, EnumTargetingType.PREDATOR);
			pig.setFreeCam(true);
			pig.setLocationAndAngles(player.posX, player.posY + EntityTargeter.Y_OFFSET, player.posZ, 0, 90);
			pig.setHeight(RenderView.RESOLUTION);
			pig.setWidth(RenderView.RESOLUTION);
			pig.setAngleLocked(true);
			world.spawnEntity(pig);
		}*/
		/*		
		if(!world.isRemote) {
			EntityPlayerMP play = (EntityPlayerMP)player;
			EntityCreeper piggie = new EntityCreeper(world);
			piggie.setLocationAndAngles(player.posX, player.posY + 2, player.posZ, 0, 90);
			world.spawnEntity(piggie);
			play.setSpectatingEntity(piggie);
		}*/
		 
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
	}
}
