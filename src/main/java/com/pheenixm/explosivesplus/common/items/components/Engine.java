package com.pheenixm.explosivesplus.common.items.components;

public class Engine extends ItemComponent {

	private final EngineTiers tier;
	
	public Engine(String itemName, EngineTiers tier) {
		super(itemName);
		this.tier = tier;
	}

	public EngineTiers getTier() {
		return tier;
	}

	public enum EngineTiers {
		NORMAL(1, 0.5, 0.0F),
		EXTENDED(5, 0.25, 0.0F),
		UNLIMITED(12.5, 0.2, 0.7F);
		
		public static final int TICKS_PER_SECOND = 20;
		public final double acceleration;
		public final double velocity;
		public final float offset;
		
		EngineTiers(double vel, double acc, float off) {
			this.acceleration = acc;
			this.velocity = vel;
			this.offset = off;
		}
		
		public double getVelocityRealTime() {
			return velocity * TICKS_PER_SECOND;
		}
		
		public double getAccelerationRealTime() {
			return acceleration * TICKS_PER_SECOND;
		}

		
	}
}
