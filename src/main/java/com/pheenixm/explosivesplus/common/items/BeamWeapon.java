package com.pheenixm.explosivesplus.common.items;

import java.util.List;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;

import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class BeamWeapon extends Item {

	protected static final int RANGE = 256;
	
	public BeamWeapon(String itemName) {
		setItemName(this, itemName);
		setCreativeTab(ExplosivesPlus.tab);
		this.setMaxStackSize(1);
		this.setMaxDamage(64);
	}

	public static void setItemName(Item item, String itemName) {
		item.setRegistryName(ExplosivesPlus.MOD_ID, itemName);
		item.setTranslationKey(item.getRegistryName().toString());
	}

	@Override
	public int getMaxItemUseDuration(ItemStack itemstack) {
		return getUseDuration();
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World world,
			EntityPlayer player, EnumHand handIn) {
		ItemStack itemstack = player.getHeldItem(handIn);

        if (!player.capabilities.isCreativeMode)
        {
            damageItem(itemstack, player);
        }
		RayTraceResult ray = this.rayTrace(world, player, true, RANGE);
		if(world.isRemote) {
			playEffects(player, ray);
		} else {
			processHit(world, player, ray);
		}
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
	}

	protected RayTraceResult rayTrace(World world, EntityPlayer player,
			boolean useLiquids, double range) {
		float f = player.rotationPitch;
		float f1 = player.rotationYaw;
		double d0 = player.posX;
		double d1 = player.posY + (double) player.getEyeHeight();
		double d2 = player.posZ;
		Vec3d vec3d = new Vec3d(d0, d1, d2);
		float f2 = MathHelper.cos(-f1 * 0.017453292F - (float) Math.PI);
		float f3 = MathHelper.sin(-f1 * 0.017453292F - (float) Math.PI);
		float f4 = -MathHelper.cos(-f * 0.017453292F);
		float f5 = MathHelper.sin(-f * 0.017453292F);
		float f6 = f3 * f4;
		float f7 = f2 * f4;
		Vec3d vec3d1 = vec3d.add((double) f6 * range, (double) f5 * range,
				(double) f7 * range);
		return world.rayTraceBlocks(vec3d, vec3d1, useLiquids, !useLiquids,
				false);
	}

    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn)
    {
		tooltip.add(TextFormatting.DARK_RED + I18n.format("gui.tooltip." + stack.getTranslationKey()));	
    }

	
	public abstract void playEffects(EntityPlayer player, RayTraceResult ray);
	public abstract void damageItem(ItemStack itemstack, EntityPlayer player);
	public abstract int getUseDuration();
	public abstract void processHit(World world, EntityPlayer player, RayTraceResult ray);

}
