package com.pheenixm.explosivesplus.common.items.components;

import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;


public class Warhead extends ItemComponent {

	private final Explosions explosive;
	
	public Warhead(String itemName, Explosions expl) {
		super(itemName);
		explosive = expl;
	}

	public Explosions getExplosive() {
		return explosive;
	}




}
