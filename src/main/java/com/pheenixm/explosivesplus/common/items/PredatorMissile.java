package com.pheenixm.explosivesplus.common.items;

import java.util.List;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.client.RenderView;
import com.pheenixm.explosivesplus.common.entities.EntityTargeter;
import com.pheenixm.explosivesplus.common.entities.EntityViewer;
import com.pheenixm.explosivesplus.common.entities.EntityTargeter.EnumTargetingType;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityPredatorMissile;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.explosions.ExplosionBiomeBuster;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.util.Utils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PredatorMissile extends Item {

	public static int ANIMATION_TIME = 60;
	
	public PredatorMissile(String itemName) {
		setItemName(this, itemName);
		setCreativeTab(ExplosivesPlus.tab);
	}

	public static void setItemName(Item item, String itemName) {
		item.setRegistryName(ExplosivesPlus.MOD_ID, itemName);
		item.setTranslationKey(item.getRegistryName().toString());
	}

	public int getMaxItemUseDuration(ItemStack itemstack) {
		return 60;
	}

	public EnumActionResult onItemUse(EntityPlayer player, World world,
			BlockPos pos, EnumHand hand, EnumFacing facing, float hitX,
			float hitY, float hitZ) {
		return EnumActionResult.PASS;
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World world,
			EntityPlayer player, EnumHand handIn) {
		
		player.setActiveHand(handIn);
		ItemStack stack = player.getHeldItem(handIn);

		
		if(!world.isRemote) {
			System.out.println(player.getItemInUseCount());
		}
		 
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
	}
	


	@Override
    public ItemStack onItemUseFinish(ItemStack stack, World world, EntityLivingBase entity) {
		EntityPlayer player = null;
		if(entity instanceof EntityPlayer) {
			player = (EntityPlayer)entity;
			double yHeight = Utils.getAverageHeight(world, (int)Math.floor(player.posX), (int)Math.floor(player.posZ), 128, 128);
			if(!world.isRemote) {
				EntityTargeter targeter = new EntityTargeter(world, 
						player.posX, 
						yHeight + EntityTargeter.Y_OFFSET,
						player.posZ, player, EnumTargetingType.PREDATOR);
				targeter.setWidthAndHeight(RenderView.RESOLUTION, RenderView.RESOLUTION);
				targeter.setAngleLocked(true);
				world.spawnEntity(targeter);

			}
		}
		return stack;
    }
	
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn)
    {
		tooltip.add(TextFormatting.DARK_RED + I18n.format("gui.tooltip." + stack.getTranslationKey()));	
    }

}
