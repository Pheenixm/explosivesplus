package com.pheenixm.explosivesplus.common.items;

import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class RepulsionBeam extends BeamWeapon {

	public RepulsionBeam(String itemName) {
		super(itemName);
	}

	@Override
	public void playEffects(EntityPlayer player, RayTraceResult hitVector) {
		// TODO: Implement this
	}

	@Override
	public void damageItem(ItemStack itemstack, EntityPlayer player) {
		itemstack.damageItem(1, player);
	}

	@Override
	public int getUseDuration() {
		return 1;
	}

	@Override
	public void processHit(World world, EntityPlayer player,
			RayTraceResult ray) {
		if (!world.isRemote) {
			if (ray != null) {
				EntityNovaController nova = new EntityNovaController(world,
						ray.hitVec.x, ray.hitVec.y, ray.hitVec.z, EntityNovaController.DEFAULT_RADIUS / 3);
				nova.setFuse(10);
				nova.convertBlocks(EntityNovaController.DEFAULT_RADIUS / 2);
				nova.ticksExisted = 10;
				world.spawnEntity(nova);
			}
		}
	}

}
