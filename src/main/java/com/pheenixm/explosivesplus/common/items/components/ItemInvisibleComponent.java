package com.pheenixm.explosivesplus.common.items.components;

import com.pheenixm.explosivesplus.ExplosivesPlus;

import net.minecraft.item.Item;

public class ItemInvisibleComponent extends Item {
 
	public ItemInvisibleComponent(String itemName) {
		setItemName(this, itemName);
	}

	public static void setItemName(Item item, String itemName) {
		item.setRegistryName(ExplosivesPlus.MOD_ID, itemName);
		item.setTranslationKey(item.getRegistryName().toString());
	}

}
