package com.pheenixm.explosivesplus.common.items;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.annotation.Nullable;

import org.lwjgl.input.Keyboard;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.tiles.TileEntityC4;

import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Detonator extends Item {

	public Detonator(String itemName) {
		setItemName(this, itemName);
		setCreativeTab(ExplosivesPlus.tab);
	}

	public static void setItemName(Item item, String itemName) {
		item.setRegistryName(ExplosivesPlus.MOD_ID, itemName);
		item.setTranslationKey(item.getRegistryName().toString());
	}

	@Override
	public int getMaxItemUseDuration(ItemStack itemstack) {
		return 1;
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World world,
			EntityPlayer player, EnumHand handIn) {
		ItemStack itemstack = player.getHeldItem(handIn);

		if (!world.isRemote) {
			if (player.isSneaking()) {
				// detonateBombs(world, player);
			} else {
				detonate(world, player);
			}
		}
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
	}

	public void detonate(World world, EntityPlayer player) {
		if (world.isRemote)
			return;
		Iterator<TileEntity> tileEntities = new CopyOnWriteArrayList<TileEntity>(world.loadedTileEntityList).iterator();
		while(tileEntities.hasNext()) {
			TileEntity entity = tileEntities.next();
			if (entity instanceof TileEntityC4) {
				TileEntityC4 c4 = (TileEntityC4) entity;
				if (player.getGameProfile().getName()
						.equals(c4.getUsername())) {
					c4.detonate(world, entity.getPos());
				}
			}
		}
	}
	
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn)
    {
		tooltip.add(TextFormatting.DARK_AQUA + I18n.format("gui.tooltip." + stack.getTranslationKey()));	
    }

}
