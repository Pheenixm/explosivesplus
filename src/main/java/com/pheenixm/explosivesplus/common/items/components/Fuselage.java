package com.pheenixm.explosivesplus.common.items.components;

public class Fuselage extends ItemComponent {

	private final FuselageTiers tier;
	
	public Fuselage(String itemName, FuselageTiers tier) {
		super(itemName);
		this.tier = tier;
	}

	public FuselageTiers getTier() {
		return tier;
	}

	public enum FuselageTiers {
		NORMAL(250, 0.0F,5.0F), ENHANCED(1000,0.0F,10.0F), MASSIVE(5000,1.0F, 16.04F);

		public final int range;
		public final float fuseOffset;
		public final float warheadOffset;

		FuselageTiers(int range, float fuse, float war) {
			this.range = range;
			this.fuseOffset = fuse;
			this.warheadOffset = war;
		}

	}

}
