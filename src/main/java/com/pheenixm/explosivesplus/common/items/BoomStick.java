package com.pheenixm.explosivesplus.common.items;

import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.explosions.ExplosionClassic;
import com.pheenixm.explosivesplus.common.explosions.ExplosionClassicMulti;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;
import com.pheenixm.explosivesplus.common.explosions.ExplosionMega;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class BoomStick extends BeamWeapon {

	public BoomStick(String itemName) {
		super(itemName);
	}

	@Override
	public void playEffects(EntityPlayer player, RayTraceResult hitVector) {
		if(hitVector != null && hitVector.hitVec != null) {
			player.getEntityWorld().spawnParticle(EnumParticleTypes.EXPLOSION_HUGE, hitVector.hitVec.x, hitVector.hitVec.y, hitVector.hitVec.z, 1.0D, 0.0D, 0.0D);
		}
	}

	@Override
	public void damageItem(ItemStack itemstack, EntityPlayer player) {
		itemstack.damageItem(1, player);
	}

	@Override
	public int getUseDuration() {
		return -1;
	}

	@Override
	public void processHit(World world, EntityPlayer player,
			RayTraceResult ray) {
		if (ray != null) {
			//ExplosionClassic explosion = new ExplosionClassic(world, player,
					//ray.hitVec.x, ray.hitVec.y, ray.hitVec.z, 20, 16);
			//explosion.initiate(ExplosivesPlusHolder.MEGA.getDefaultState());
			ExplosionHelper.createExplosion(world, player, ray.hitVec.x, ray.hitVec.y, ray.hitVec.z, Explosions.CLASSIC, false);
		}

	}
	
	

}
