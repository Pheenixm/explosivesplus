package com.pheenixm.explosivesplus.common.items.components;

import java.util.List;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;

import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemComponent extends Item {

	public ItemComponent(String itemName) {
		setItemName(this, itemName);
		setCreativeTab(ExplosivesPlus.tab);
	}

	public static void setItemName(Item item, String itemName) {
		item.setRegistryName(ExplosivesPlus.MOD_ID, itemName);
		item.setTranslationKey(item.getRegistryName().toString());
	}

    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn)
    {
		tooltip.add(TextFormatting.WHITE + I18n.format("gui.tooltip." + stack.getTranslationKey()));	
    }
    
    public static float getOffset(ItemStack item, boolean warhead) {
    		if(!(item.getItem() instanceof ItemComponent)) {
    			return 0.0F;
    		}
    		Item it = item.getItem();
    		if(it instanceof Engine) {
    			return ((Engine)it).getTier().offset;
    		}
    		if(it instanceof Fuselage) {
    			if(warhead) {
    				return ((Fuselage)it).getTier().warheadOffset;
    			}
    			return ((Fuselage)it).getTier().fuseOffset;
    		}
    		if(it instanceof Warhead) {
    			return 16.04F;
    		}
    		return 0.0F;
    }

}
