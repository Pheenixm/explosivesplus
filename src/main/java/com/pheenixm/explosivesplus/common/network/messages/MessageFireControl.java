package com.pheenixm.explosivesplus.common.network.messages;

import com.pheenixm.explosivesplus.common.entities.EntityTargeter;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityControllable;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageFireControl implements IMessage, IMessageHandler<MessageFireControl, IMessage> { 
	
	private int x;
	private int y;
	private int z;
	private double x1;
	private double y1;
	private double z1;
	private int entityId;
	
	public MessageFireControl() {}

	public MessageFireControl(BlockPos target, Vec3d extraPos, EntityTargeter viewer) {
		this.x = target.getX();
		this.y = target.getY();
		this.z = target.getZ();
		
		this.x1 = extraPos.x;
		this.y1 = extraPos.y;
		this.z1 = extraPos.z;
		
		this.entityId = viewer.getEntityId();
	}
	
	
	@Override
	public IMessage onMessage(MessageFireControl message, MessageContext ctx) {
		EntityPlayerMP player = ctx.getServerHandler().player;
		World world = player.world;
		Entity uncastEnt = world.getEntityByID(message.entityId);
		if(uncastEnt instanceof EntityTargeter) {
			EntityTargeter fireControl = (EntityTargeter) uncastEnt;
			fireControl.receiveFireControl(message.x, message.y, message.z, message.x1, message.y1, message.z1);
		}
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.x = buf.readInt();
		this.y = buf.readInt();
		this.z = buf.readInt();
		this.x1 = buf.readDouble();
		this.y1 = buf.readDouble();
		this.z1 = buf.readDouble();
		this.entityId = buf.readInt();
	}
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(x);
		buf.writeInt(y);
		buf.writeInt(z);
		buf.writeDouble(x1);
		buf.writeDouble(y1);
		buf.writeDouble(z1);
		buf.writeInt(entityId);
		
	}

}
