package com.pheenixm.explosivesplus.common.network;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.network.messages.MessageAngleUpdate;
import com.pheenixm.explosivesplus.common.network.messages.MessageCameraReset;
import com.pheenixm.explosivesplus.common.network.messages.MessageEnergyUpdate;
import com.pheenixm.explosivesplus.common.network.messages.MessageFauxEntitySpawn;
import com.pheenixm.explosivesplus.common.network.messages.MessageFauxEntityUpdate;
import com.pheenixm.explosivesplus.common.network.messages.MessageFireControl;
import com.pheenixm.explosivesplus.common.network.messages.MessageGeneratorUpdate;
import com.pheenixm.explosivesplus.common.network.messages.MessageMouseInput;
import com.pheenixm.explosivesplus.common.network.messages.MessageProgressUpdate;
import com.pheenixm.explosivesplus.common.network.messages.MessageSeedUpdate;
import com.pheenixm.explosivesplus.common.network.messages.MessageTileReplaced;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;

public class PacketHandler {

	public PacketHandler() {
		// TODO Auto-generated constructor stub
	}

	public static final SimpleNetworkWrapper NETWORK = NetworkRegistry.INSTANCE.newSimpleChannel(ExplosivesPlus.MOD_ID);
    private static int messageId = 0;

    private static enum Side
    {
        CLIENT, SERVER, BOTH;
    }

    public static void init()
    {
		//REGISTER MESSAGES HERE
    		registerMessage(MessageMouseInput.class, Side.SERVER);
    		registerMessage(MessageFireControl.class, Side.SERVER);
    		registerMessage(MessageTileReplaced.class, Side.CLIENT);
    		registerMessage(MessageEnergyUpdate.class, Side.CLIENT);
    		registerMessage(MessageProgressUpdate.class, Side.CLIENT);
    		registerMessage(MessageSeedUpdate.class, Side.SERVER);
    		registerMessage(MessageGeneratorUpdate.class, Side.CLIENT);
    		registerMessage(MessageFauxEntityUpdate.class, Side.CLIENT);
    		registerMessage(MessageFauxEntitySpawn.class, Side.CLIENT);
    		registerMessage(MessageAngleUpdate.class, Side.SERVER);
    		registerMessage(MessageCameraReset.class, Side.CLIENT);

    }
    
    private static void registerMessage(Class packet, Side side)
    {
        if (side != Side.CLIENT) {
            registerMessage(packet, net.minecraftforge.fml.relauncher.Side.SERVER);
        }

        if (side != Side.SERVER) {
            registerMessage(packet, net.minecraftforge.fml.relauncher.Side.CLIENT);
        }
    }

    private static void registerMessage(Class packet, net.minecraftforge.fml.relauncher.Side side)
    {
        NETWORK.registerMessage(packet, packet, messageId++, side);
    }

	
}
