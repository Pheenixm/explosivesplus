package com.pheenixm.explosivesplus.common.network.messages;

import com.pheenixm.explosivesplus.common.entities.controllable.EntityControllable;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntityPluripotent;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageTileReplaced implements IMessage, IMessageHandler<MessageTileReplaced, IMessage> {

	private int x;
	private int y;
	private int z;
	private int masterX;
	private int masterY;
	private int masterZ;
	private int multiIndex;
	private int facing;

	public MessageTileReplaced() {

	}

	public MessageTileReplaced(BlockPos tilePos, BlockPos masterPos, MultiEnum multi, EnumFacing facing) {
		this.x = tilePos.getX();
		this.y = tilePos.getY();
		this.z = tilePos.getZ();
		this.masterX = masterPos.getX();
		this.masterY = masterPos.getY();
		this.masterZ = masterPos.getZ();
		this.multiIndex = multi.ordinal();
		this.facing = facing.getIndex();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(x);
		buf.writeInt(y);
		buf.writeInt(z);
		buf.writeInt(masterX);
		buf.writeInt(masterY);
		buf.writeInt(masterZ);
		buf.writeInt(multiIndex);
		buf.writeInt(facing);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.x = buf.readInt();
		this.y = buf.readInt();
		this.z = buf.readInt();
		this.masterX = buf.readInt();
		this.masterY = buf.readInt();
		this.masterZ = buf.readInt();
		this.multiIndex = buf.readInt();
		this.facing = buf.readInt();
	}

	@Override
	public IMessage onMessage(MessageTileReplaced message, MessageContext ctx) {	
		Minecraft.getMinecraft().addScheduledTask(() -> {
			World world = Minecraft.getMinecraft().world;
			BlockPos pos = new BlockPos(message.x, message.y, message.z);
			BlockPos masterPos = new BlockPos(message.masterX, message.masterY, message.masterZ);
			if(world.isBlockLoaded(pos)) {
				IBlockState blockState = world.getBlockState(pos);
				if(blockState.getBlock().hasTileEntity()) {
					TileEntity uncastTile = world.getTileEntity(pos);
					if(uncastTile instanceof TileEntityMultiblock) {
						if(uncastTile instanceof TileEntityPluripotent) {
							//Convert the tile at this location to what it should be
							TileEntityPluripotent pluri = (TileEntityPluripotent)uncastTile;
							MultiEnum multi = MultiEnum.get(message.multiIndex);
							EnumFacing facing = EnumFacing.byIndex(message.multiIndex);
							TileEntityMultiblock tile = MultiblockDict.getTileForStructure(multi);
							TileEntityMultiblock master = world.getTileEntity(masterPos) instanceof TileEntityMultiblock ? 
									(TileEntityMultiblock) world.getTileEntity(masterPos) : null;
							if(masterPos.equals(pos)) {
								//Block is master
								tile.setMaster(tile);
							}
							pluri.copyTileEntity(tile, pluri, null);
							tile.facing = EnumFacing.byIndex(message.facing);
							world.removeTileEntity(pos);
							world.setTileEntity(pos, tile);
						} else {
							//Reserved for future use, if I want to convert back
							//to pluripotent blocks
						}
					}
				}
			}
		});
		return null;
	}

}
