package com.pheenixm.explosivesplus.common.network.messages;

import com.pheenixm.explosivesplus.api.energy.EnergyBuffer;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityRegistry;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntitySpawnHandler.EnumFauxEntity;
import com.pheenixm.explosivesplus.common.gui.ContainerCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntityPluripotent;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageFauxEntitySpawn implements IMessage, IMessageHandler<MessageFauxEntitySpawn, IMessage> {

	private int dimID;
	private EnumFauxEntity faux;
	private int entityId;
	
	public MessageFauxEntitySpawn(EnumFauxEntity faux, int id, int entityId) {
		this.faux = faux;
		this.dimID = id;
		this.entityId = entityId;
	}
	
	
	public MessageFauxEntitySpawn() {}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.faux.ordinal());
		buf.writeInt(this.dimID);
		buf.writeInt(this.entityId);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.faux = EnumFauxEntity.get(buf.readInt());
		this.dimID = buf.readInt();
		this.entityId = buf.readInt();
	}

	@Override
	public IMessage onMessage(MessageFauxEntitySpawn message, MessageContext ctx) {		
        Minecraft.getMinecraft().addScheduledTask(() ->
        {
        		//TODO: Put this somewhere to be rendered
        		World world = Minecraft.getMinecraft().world;
        		if(world.provider.getDimension() == message.dimID) { 
        			FauxEntityRegistry.getRegistry(world).handleSpawn(message.faux, message.entityId);
        		}
        });
		return null;
	}


}
