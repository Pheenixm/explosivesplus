package com.pheenixm.explosivesplus.common.network.messages;

import com.pheenixm.explosivesplus.api.energy.EnergyBuffer;
import com.pheenixm.explosivesplus.common.gui.ContainerCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntityPluripotent;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageProgressUpdate implements IMessage, IMessageHandler<MessageProgressUpdate, IMessage> {
	private int windowId;
	private int ticksCompleted;
	private int ticksToComplete;
	
	public MessageProgressUpdate(int windowId, TileEntityCentrifuge tile) {
		this.windowId = windowId;
		this.ticksCompleted = tile.getCurrentRecipeTicksElapsed();
		this.ticksToComplete = tile.getCurrentRecipeTicks();
	}
	
	
	public MessageProgressUpdate() {}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(windowId);
		buf.writeInt(ticksCompleted);
		buf.writeInt(ticksToComplete);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.windowId = buf.readInt();
		this.ticksCompleted = buf.readInt();
		this.ticksToComplete = buf.readInt();
	}

	@Override
	public IMessage onMessage(MessageProgressUpdate message, MessageContext ctx) {		
        Minecraft.getMinecraft().addScheduledTask(() ->
        {
            EntityPlayer player = Minecraft.getMinecraft().player;

            if (message.windowId != -1)
            {
                if (message.windowId == player.openContainer.windowId)
                {
                    if ((player.openContainer instanceof ContainerCentrifuge))
                    {
                        ((ContainerCentrifuge) player.openContainer).updateProgress(message.ticksCompleted, message.ticksToComplete);
                    }
                }
            }
        });
		return null;
	}


}
