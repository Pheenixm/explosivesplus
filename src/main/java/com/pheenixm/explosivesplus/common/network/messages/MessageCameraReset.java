package com.pheenixm.explosivesplus.common.network.messages;

import com.pheenixm.explosivesplus.common.entities.EntityViewer;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityControllable;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageCameraReset implements IMessage, IMessageHandler<MessageCameraReset, IMessage> {
	
	public int id;
	
	public MessageCameraReset() {
		
	}
	
	
	public MessageCameraReset(int viewerId) {
		id = viewerId;
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(id);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.id = buf.readInt();
	}

	@Override
	public IMessage onMessage(MessageCameraReset message, MessageContext ctx) {		
		Minecraft.getMinecraft().addScheduledTask(() -> {
			int id = message.id;
			World world = Minecraft.getMinecraft().world;
			Entity ent = world.getEntityByID(id);
			if(ent instanceof EntityViewer) {
				EntityViewer view = (EntityViewer)ent;
				view.resetCamera();
				view.setDead();
			}
		});
		return null;
	}




}
