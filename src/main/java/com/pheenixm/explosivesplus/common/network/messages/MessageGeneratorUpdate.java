package com.pheenixm.explosivesplus.common.network.messages;

import com.pheenixm.explosivesplus.api.energy.EnergyBuffer;
import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityGenerator;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntityPluripotent;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageGeneratorUpdate implements IMessage, IMessageHandler<MessageGeneratorUpdate, IMessage> {

	private int x;
	private int y;
	private int z;
	private int energy;
	private int capacity;
	private boolean animate;
	private int mobId;
	
	public MessageGeneratorUpdate() {
		
	}
	
	
	public MessageGeneratorUpdate(EnergyBuffer buff, TileEntityBase base, int mobId, boolean animate) {
		this.x = base.getPos().getX();
		this.y = base.getPos().getY();
		this.z = base.getPos().getZ();
		this.energy = buff.getEnergyStored();
		this.capacity = buff.getMaxEnergyStored();
		this.animate = animate;
		this.mobId = mobId;
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(x);
		buf.writeInt(y);
		buf.writeInt(z);
		buf.writeInt(energy);
		buf.writeInt(capacity);
		buf.writeBoolean(animate);
		buf.writeInt(mobId);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.x = buf.readInt();
		this.y = buf.readInt();
		this.z = buf.readInt();
		this.energy = buf.readInt();
		this.capacity = buf.readInt();
		this.animate = buf.readBoolean();
		this.mobId = buf.readInt();
	}

	@Override
	public IMessage onMessage(MessageGeneratorUpdate message, MessageContext ctx) {		
		Minecraft.getMinecraft().addScheduledTask(() -> {
			World world = Minecraft.getMinecraft().world;
			BlockPos pos = new BlockPos(message.x, message.y, message.z);
			if(world.isBlockLoaded(pos)) {
				IBlockState blockState = world.getBlockState(pos);
				if(blockState.getBlock().hasTileEntity()) {
					TileEntity uncastTile = world.getTileEntity(pos);
					if(uncastTile instanceof TileEntityGenerator) {
						TileEntityGenerator<?> gen = (TileEntityGenerator<?>)uncastTile;
						gen.updateClient(message.energy, message.capacity, message.animate, world.getEntityByID(message.mobId).getPosition());
					}
				}
			}
		});
		return null;
	}


}
