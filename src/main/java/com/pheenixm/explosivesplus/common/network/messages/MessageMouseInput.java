package com.pheenixm.explosivesplus.common.network.messages;

import com.pheenixm.explosivesplus.common.entities.controllable.EntityControllable;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageMouseInput implements IMessage, IMessageHandler<MessageMouseInput, IMessage> {

	private int dx;
	private int dy;
	private int button;
	private int entityId;
	
	public MessageMouseInput() {
		
	}
	
	
	public MessageMouseInput(int dx, int dy, int button, int entityId) {
		this.dx = dx;
		this.dy = dy;
		this.button = button;
		this.entityId = entityId;
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(dx);
		buf.writeInt(dy);
		buf.writeInt(button);
		buf.writeInt(entityId);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.dx = buf.readInt();
		this.dy = buf.readInt();
		this.button = buf.readInt();
		this.entityId = buf.readInt();
	}

	@Override
	public IMessage onMessage(MessageMouseInput message, MessageContext ctx) {		
		EntityPlayerMP player = ctx.getServerHandler().player;
		World world = player.world;
		Entity uncastEnt = world.getEntityByID(message.entityId);
		if(uncastEnt instanceof EntityControllable) {
			EntityControllable control = (EntityControllable) world.getEntityByID(message.entityId);
			control.processMouseInput(message.dx, message.dy, message.button);
		}
		return null;
	}




}
