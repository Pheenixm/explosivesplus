package com.pheenixm.explosivesplus.common.network.messages;

import com.pheenixm.explosivesplus.common.entities.EntityViewer;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityControllable;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageAngleUpdate implements IMessage, IMessageHandler<MessageAngleUpdate, IMessage> {

	private float rotationYaw;
	private float rotationPitch;
	private int button;
	private int entityId;
	
	public MessageAngleUpdate() {
		
	}
	
	
	public MessageAngleUpdate(float rY, float rP, int button, int entityId) {
		this.rotationPitch = rP;
		this.rotationYaw = rY;
		this.button = button;
		this.entityId = entityId;
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeFloat(rotationYaw);
		buf.writeFloat(rotationPitch);
		buf.writeInt(button);
		buf.writeInt(entityId);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.rotationYaw = buf.readFloat();
		this.rotationPitch = buf.readFloat();
		this.button = buf.readInt();
		this.entityId = buf.readInt();
	}

	@Override
	public IMessage onMessage(MessageAngleUpdate message, MessageContext ctx) {		
		EntityPlayerMP player = ctx.getServerHandler().player;
		World world = player.world;
		Entity uncastEnt = world.getEntityByID(message.entityId);
		if(uncastEnt instanceof EntityControllable) {
			EntityControllable control = (EntityControllable) world.getEntityByID(message.entityId);
			control.updateAngles(message.rotationPitch, message.rotationYaw);
		}
		return null;
	}




}
