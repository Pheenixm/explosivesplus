package com.pheenixm.explosivesplus.common.network.messages;

import com.pheenixm.explosivesplus.api.energy.EnergyBuffer;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityRegistry;
import com.pheenixm.explosivesplus.common.gui.ContainerCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntityPluripotent;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageFauxEntityUpdate implements IMessage, IMessageHandler<MessageFauxEntityUpdate, IMessage> {

	private NBTTagCompound updateTag;
	private int dimID;
	
	public MessageFauxEntityUpdate(NBTTagCompound updateTag, int id) {
		this.updateTag = updateTag;
		this.dimID = id;
	}
	
	
	public MessageFauxEntityUpdate() {}
	
	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeTag(buf, this.updateTag);
		buf.writeInt(this.dimID);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.updateTag = ByteBufUtils.readTag(buf);
		this.dimID = buf.readInt();
	}

	@Override
	public IMessage onMessage(MessageFauxEntityUpdate message, MessageContext ctx) {		
        Minecraft.getMinecraft().addScheduledTask(() ->
        {
        		//TODO: Put this somewhere to be rendered
        		World world = Minecraft.getMinecraft().world;
        		//TODO: Remove the obvious sanity check
        		if(world.provider.getDimension() == message.dimID) { 
        			FauxEntityRegistry.getRegistry(world).handleUpdateTag(message.updateTag);
        		}
        });
		return null;
	}


}
