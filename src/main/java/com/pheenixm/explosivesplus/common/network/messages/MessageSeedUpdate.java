package com.pheenixm.explosivesplus.common.network.messages;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import com.pheenixm.explosivesplus.common.entities.controllable.EntityControllable;
import com.pheenixm.explosivesplus.common.tiles.TileEntityChaos;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSeedUpdate implements IMessage, IMessageHandler<MessageSeedUpdate, IMessage> {

	private String seed;
	private int x;
	private int y;
	private int z;

	public MessageSeedUpdate() {
		
	}
	
	
	public MessageSeedUpdate(String seed, BlockPos pos) {
		this.seed = seed;
		this.x = pos.getX();
		this.y = pos.getY();
		this.z = pos.getZ();
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, seed);
		buf.writeInt(x);
		buf.writeInt(y);
		buf.writeInt(z);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.seed = ByteBufUtils.readUTF8String(buf);
		this.x = buf.readInt();
		this.y = buf.readInt();
		this.z = buf.readInt();
	}

	@Override
	public IMessage onMessage(MessageSeedUpdate message, MessageContext ctx) {	
		World world = ctx.getServerHandler().player.world;
		BlockPos pos = new BlockPos(message.x, message.y, message.z);
		String messageSeed = message.seed;
		TileEntity t = world.getTileEntity(pos);
		if(t instanceof TileEntityChaos) {
			TileEntityChaos tile = (TileEntityChaos)t;
			if(tile.seed == null || (!tile.seed.equals(messageSeed) && !messageSeed.equals(""))) {
				tile.seed = messageSeed;
			}
		}
		
		return null;
	}




}
