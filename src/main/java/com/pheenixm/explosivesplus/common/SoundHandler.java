package com.pheenixm.explosivesplus.common;

import com.pheenixm.explosivesplus.ExplosivesPlus;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;

public class SoundHandler {

	public SoundHandler() {
		// TODO Auto-generated constructor stub
	}

	@ObjectHolder(ExplosivesPlus.MOD_ID + ":gun.fire")
	public static final SoundEvent GUN_FIRE = createSoundEvent("gun.fire");
	
	@ObjectHolder(ExplosivesPlus.MOD_ID + ":biome.bust")
	public static final SoundEvent BIOME_BUST = createSoundEvent("biome.bust");

	@ObjectHolder(ExplosivesPlus.MOD_ID + ":blackhole")
	public static final SoundEvent BLACK_HOLE = createSoundEvent("blackhole");

	
	
	private static SoundEvent createSoundEvent(String soundName) {
		final ResourceLocation soundID = new ResourceLocation(ExplosivesPlus.MOD_ID, soundName);
		return new SoundEvent(soundID).setRegistryName(soundID);
	}
	
}
