package com.pheenixm.explosivesplus.common;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.blocks.C4;
import com.pheenixm.explosivesplus.common.blocks.ComsCable;
import com.pheenixm.explosivesplus.common.blocks.CreeperBlock;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.blocks.NovaBomb;
import com.pheenixm.explosivesplus.common.blocks.PowerInjector;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.AccessoryBlock;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.StructuralBlock;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;
import com.pheenixm.explosivesplus.common.items.BeamWeapon;
import com.pheenixm.explosivesplus.common.items.components.Engine;
import com.pheenixm.explosivesplus.common.items.components.Fuselage;
import com.pheenixm.explosivesplus.common.items.components.ItemComponent;
import com.pheenixm.explosivesplus.common.items.components.ItemInvisibleComponent;
import com.pheenixm.explosivesplus.common.items.components.Warhead;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;

@ObjectHolder(ExplosivesPlus.MOD_ID)
public class ExplosivesPlusHolder {

	public static final ExplosivesBlock NOVA = null;
	public static final ExplosivesBlock BIOME = null;
	public static final ExplosivesBlock MEGA = null;
	public static final ExplosivesBlock RAIN = null;
	public static final ExplosivesBlock CHAOS = null;
	public static final ExplosivesBlock DISRUPT = null;
	public static final C4 C4 = null;
	
	public static final StructuralBlock CONCRETE = null;
	public static final AccessoryBlock IRON_SLAB = null;
	public static final StructuralBlock EM_BLOCK = null;
	public static final StructuralBlock LINEAR_EM_BLOCK = null;
	public static final AccessoryBlock LINEAR_EM_SLAB = null;
	public static final StructuralBlock IN_OUT_BLOCK = null;
	
	public static final CreeperBlock CREEPER = null;
	
	public static final ComsCable CABLE = null;
	public static final PowerInjector INJECTOR = null;
	public static final Block DEBUGGER_TILE = null;

	public static final Item DETONATOR = null;
	public static final BeamWeapon REPULSION = null;
	public static final BeamWeapon BOOMSTICK = null;
	public static final Item DEBUGGER = null;
	public static final Item PREDATOR = null;
	public static final Item STEALTH = null;

	public static final ItemComponent WARHEAD_NOVA = null;
	public static final ItemComponent WARHEAD_BIOME = null;
	public static final ItemComponent WARHEAD_MEGA = null;

	public static final ItemComponent FUSELAGE_NORMAL = null;
	
	public static final ItemComponent ENGINE_NORMAL = null;
	
	public static final ItemComponent REACTONITE = null;
	public static final ItemComponent REFINED_REACTONITE = null;

	public static final ItemComponent CIRCITIUM = null;
	public static final ItemComponent NANOTUBES = null;
	public static final ItemComponent GRAPHITE = null;
	public static final ItemComponent MARIUM = null;
	public static final ItemComponent INCENDIUM = null;
	public static final ItemComponent LIQUID_CRYSTAL = null;
	public static final ItemComponent BINDER = null;

	
			

}
