package com.pheenixm.explosivesplus.common;

import com.pheenixm.explosivesplus.client.gui.GuiCentrifuge;
import com.pheenixm.explosivesplus.client.gui.GuiChaosTheory;
import com.pheenixm.explosivesplus.client.gui.GuiGenerator;
import com.pheenixm.explosivesplus.client.gui.GuiPredatorMissile;
import com.pheenixm.explosivesplus.client.gui.GuiSilo;
import com.pheenixm.explosivesplus.client.gui.GuiStealthBomber;
import com.pheenixm.explosivesplus.common.entities.EntityTargeter;
import com.pheenixm.explosivesplus.common.entities.EntityTargeter.EnumTargetingType;
import com.pheenixm.explosivesplus.common.gui.ContainerCentrifuge;
import com.pheenixm.explosivesplus.common.gui.ContainerGenerator;
import com.pheenixm.explosivesplus.common.gui.ContainerSilo;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityChaos;
import com.pheenixm.explosivesplus.common.tiles.TileEntityGenerator;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GuiHandler implements IGuiHandler {
	
	@SideOnly(Side.CLIENT)
	private static Minecraft mc = Minecraft.getMinecraft();

	
	public GuiHandler() {
	}

	@Override
	public Container getServerGuiElement(int ID, EntityPlayer player, World world,
			int x, int y, int z) {
		switch(GuiEnum.get(ID)) {
			case SILO:
				return new ContainerSilo(player.inventory, ((TileEntitySilo)world.getTileEntity(new BlockPos(x, y, z))).getMaster());
			case CENTRI:
				return new ContainerCentrifuge(player.inventory, ((TileEntityCentrifuge)world.getTileEntity(new BlockPos(x, y, z))).getMaster());
			case GENERATOR:
				return new ContainerGenerator(player.inventory, ((TileEntityGenerator<?>)world.getTileEntity(new BlockPos(x, y, z))).getMaster());
			default:
				return null;
					
		}
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world,
			int x, int y, int z) {
		switch(GuiEnum.get(ID)) {
		case SILO:
			return new GuiSilo(getServerGuiElement(ID, player, world, x, y, z));
		case CENTRI: 
			return new GuiCentrifuge(getServerGuiElement(ID, player, world, x, y, z));
		case GENERATOR: 
			return new GuiGenerator(getServerGuiElement(ID, player, world, x, y, z));
		default:
			return null;
		}
	}
	
	public static void processTargetingGUI(EnumTargetingType targetType, EntityTargeter ent) {
		GuiScreen screen = null;
		switch(targetType) {
			case PREDATOR: {
				screen = new GuiPredatorMissile(ent);
				break;
			}
			case AC130: {
				
			}
			case STEALTH_BOMBER: {
				screen = new GuiStealthBomber(ent);
				break;
			}
			case AIRSTRIKE: {
				
			}
			default: {
				
			}
		}
		if(screen != null) {
			mc.displayGuiScreen(screen);
		}
	}

	public static void openChaos(TileEntityChaos tileEntity) {
		GuiChaosTheory chaos = new GuiChaosTheory(tileEntity);
		mc.displayGuiScreen(chaos);
	}

	public enum GuiEnum {
		SILO,
		CENTRI,
		GENERATOR;
		
	    public static GuiEnum get(int meta)
	    {
	        return meta >= 0 && meta < values().length ? values()[meta] : SILO;
	    }
	}

}
