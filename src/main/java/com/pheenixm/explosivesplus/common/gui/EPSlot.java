package com.pheenixm.explosivesplus.common.gui;

import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public abstract class EPSlot extends SlotItemHandler {

	final Container container;

	public EPSlot(Container conta, IItemHandler inventory, int index, int x,
			int y) {
		super(inventory, index, x, y);
		container = conta;
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		return validItem(stack);
	}

	public int getSlotStackLimit() {
		return getSlotLimit();
	}

	public abstract boolean validItem(ItemStack stack);
	public abstract int getSlotLimit();

	public static class SpecificSlot extends EPSlot {

	    private Class<? extends Item> item;
		private final int limit;

		/**
		 * Pass in -1 for limit to give default behavior
		 * @param conta
		 * @param inventory
		 * @param index
		 * @param x
		 * @param y
		 * @param clazz
		 * @param lim
		 */
		public SpecificSlot(Container conta, IItemHandler inventory, int index,
				int x, int y, Class<? extends Item> clazz, int lim) {
			super(conta, inventory, index, x, y);
			limit = lim;
			item = clazz;
		}
		
		@Override
		public void onSlotChanged() {
			if(container instanceof ContainerEPBase) {
				ContainerEPBase base = (ContainerEPBase) container;
				base.tile.markDirty();
			}
		}

		@Override
		public boolean validItem(ItemStack stack) {
	        return item.equals(stack.getItem().getClass()) || item.isInstance(stack.getItem());
		}

		@Override
		public int getSlotLimit() {
			return limit < 0 ? super.getSlotStackLimit() : limit;
		}
	}
	
	public static class Output extends EPSlot{

		public Output(Container conta, IItemHandler inventory, int index, int x, int y) {
			super(conta, inventory, index, x, y);
		}

		@Override
		public boolean validItem(ItemStack stack) {
			return false;
		}

		@Override
		public int getSlotLimit() {
			return super.getSlotStackLimit();
		}
		
		
	}
}
