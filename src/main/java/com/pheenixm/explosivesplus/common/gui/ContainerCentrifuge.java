package com.pheenixm.explosivesplus.common.gui;

import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageProgressUpdate;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IContainerListener;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerCentrifuge extends ContainerEPBase<TileEntityCentrifuge> {

	public int inputSlot;
	public int outputSlot;
	
	public ContainerCentrifuge(InventoryPlayer invPlay, TileEntityCentrifuge te) {
		super(invPlay, te, te.getInventory());
		bindPlayerInventory(invPlay);
	}

	@Override
	public void initSlots(IItemHandler[] inv) {
		inputSlot = addSlotToContainer(new SlotItemHandler(inv[0], 0, 50, 35)).slotNumber;
		outputSlot = addSlotToContainer(new EPSlot.Output(this, inv[1], 0, 115, 35)).slotNumber;
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}

	@Override
	public void closeContainer(EntityPlayer player) {
		tile.onInventoryClosed();
	}
	

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();

        if (tile.isProcessing())
        {
            for (IContainerListener watcher : this.listeners)
            {
                if (watcher instanceof EntityPlayerMP)
                {
                		MessageProgressUpdate message = new MessageProgressUpdate(windowId, tile);
                		PacketHandler.NETWORK.sendTo(message, (EntityPlayerMP)watcher);
                }	
            }

        }
    }

    public void updateProgress(int recipeCompleted, int recipeTime) {
    		tile.setCurrentRecipeTicksElapsed(recipeCompleted);
    		tile.setCurrentRecipeTicks(recipeTime);
    }

}
