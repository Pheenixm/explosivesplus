package com.pheenixm.explosivesplus.common.gui;

import java.lang.reflect.Array;

import com.pheenixm.explosivesplus.common.items.components.Engine;
import com.pheenixm.explosivesplus.common.items.components.Fuselage;
import com.pheenixm.explosivesplus.common.items.components.Warhead;
import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraftforge.items.IItemHandler;

public class ContainerSilo extends ContainerEPBase<TileEntitySilo> {

	public ContainerSilo(InventoryPlayer invPlay, TileEntitySilo te) {
		super(invPlay, te, te.getInventory());
        bindPlayerInventory(invPlay);
	}

	@Override
	public void initSlots(IItemHandler[] inv) {
		IItemHandler invin = inv[0];
		addSlotToContainer(new EPSlot.SpecificSlot(this, invin, 0, 80, 11, Warhead.class, 1));
		addSlotToContainer(new EPSlot.SpecificSlot(this, invin, 1, 80, 44, Fuselage.class, 1));
		addSlotToContainer(new EPSlot.SpecificSlot(this, invin, 2, 80, 77, Engine.class, 1));
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return tile.isLaunching ? false : super.canInteractWith(playerIn);
	}

	@Override
	public void closeContainer(EntityPlayer player) {
		tile.onInventoryClosed();
	}

}
