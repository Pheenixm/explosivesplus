package com.pheenixm.explosivesplus.common.gui;

import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;

public abstract class ContainerEPBase<T extends TileEntityBase>
		extends
			Container {

	public T tile;
	public IInventory invPlayer;
	public IItemHandler[] inventory;
	public int slots;

	public ContainerEPBase(InventoryPlayer invPlay, T te,
			IItemHandler... tileInv) {
		tile = te;
		invPlayer = invPlay;
		inventory = tileInv;
		initSlots(tileInv);
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return invPlayer.isUsableByPlayer(playerIn);
	}

	@Override
	protected boolean mergeItemStack(ItemStack stack, int startIndex,
			int endIndex, boolean reverseDirection) {
		return super.mergeItemStack(stack, startIndex, endIndex,
				reverseDirection);
	}
	
    public void onContainerClosed(EntityPlayer playerIn) {
    		super.onContainerClosed(playerIn);
    		closeContainer(playerIn);
    }
    
    public abstract void closeContainer(EntityPlayer player);

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			int containerSlots = inventorySlots.size()
					- player.inventory.mainInventory.size();

			if (index < containerSlots) {
				if (!this.mergeItemStack(itemstack1, containerSlots,
						inventorySlots.size(), true)) {
					return ItemStack.EMPTY;
				}
			} else if (!this.mergeItemStack(itemstack1, 0, containerSlots,
					false)) {
				return ItemStack.EMPTY;
			}

			if (itemstack1.getCount() == 0) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}

			slot.onTake(player, itemstack1);
		}

		return itemstack;
	}

	/**
	 * Courtesy of gigaherz
	 * @param playerInventory
	 */
	protected void bindPlayerInventory(InventoryPlayer playerInventory) {
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (int k = 0; k < 9; ++k)
        {
            this.addSlotToContainer(new Slot(playerInventory, k, 8 + k * 18, 142));
        }
	}

	public abstract void initSlots(IItemHandler[] tileInv);

}
