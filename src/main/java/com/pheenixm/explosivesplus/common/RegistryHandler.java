package com.pheenixm.explosivesplus.common;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.recipes.RecipeCentrifuge;
import com.pheenixm.explosivesplus.common.blocks.BiomeBuster;
import com.pheenixm.explosivesplus.common.blocks.C4;
import com.pheenixm.explosivesplus.common.blocks.ChaosTheory;
import com.pheenixm.explosivesplus.common.blocks.ComsCable;
import com.pheenixm.explosivesplus.common.blocks.CreeperBlock;
import com.pheenixm.explosivesplus.common.blocks.DebuggerTile;
import com.pheenixm.explosivesplus.common.blocks.MegaNuke;
import com.pheenixm.explosivesplus.common.blocks.MolecularDisruptor;
import com.pheenixm.explosivesplus.common.blocks.NovaBomb;
import com.pheenixm.explosivesplus.common.blocks.PowerInjector;
import com.pheenixm.explosivesplus.common.blocks.TNTRain;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.ConcreteBlock;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.IOBlock;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.RotationalEMBlock;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.LidBlock;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.LinearEMBlock;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.LinearEMSlab;
import com.pheenixm.explosivesplus.common.coms.tiles.TileEntityCable;
import com.pheenixm.explosivesplus.common.coms.tiles.TileEntityWireless;
import com.pheenixm.explosivesplus.common.entities.EntityBiomeBuster;
import com.pheenixm.explosivesplus.common.entities.EntityMegaNuke;
import com.pheenixm.explosivesplus.common.entities.EntityMolecularDisruptor;
import com.pheenixm.explosivesplus.common.entities.EntityNovaBomb;
import com.pheenixm.explosivesplus.common.entities.EntityNuke;
import com.pheenixm.explosivesplus.common.entities.EntityPhysicsBlock;
import com.pheenixm.explosivesplus.common.entities.EntityShockwave;
import com.pheenixm.explosivesplus.common.entities.EntityStealthBomber;
import com.pheenixm.explosivesplus.common.entities.EntityTNTRain;
import com.pheenixm.explosivesplus.common.entities.EntityTargeter;
import com.pheenixm.explosivesplus.common.entities.EntityTheoryOfChaos;
import com.pheenixm.explosivesplus.common.entities.EntityViewer;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityPredatorMissile;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityValkyrie;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityBatchExplosionController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTNTRainController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTheoryOfChaosController;
import com.pheenixm.explosivesplus.common.entities.missiles.EntityBallisticMissile;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;
import com.pheenixm.explosivesplus.common.items.BoomStick;
import com.pheenixm.explosivesplus.common.items.Detonator;
import com.pheenixm.explosivesplus.common.items.ItemDebugger;
import com.pheenixm.explosivesplus.common.items.PredatorMissile;
import com.pheenixm.explosivesplus.common.items.RepulsionBeam;
import com.pheenixm.explosivesplus.common.items.StealthBomber;
import com.pheenixm.explosivesplus.common.items.components.Engine;
import com.pheenixm.explosivesplus.common.items.components.Engine.EngineTiers;
import com.pheenixm.explosivesplus.common.items.components.Fuselage;
import com.pheenixm.explosivesplus.common.items.components.Fuselage.FuselageTiers;
import com.pheenixm.explosivesplus.common.items.components.ItemComponent;
import com.pheenixm.explosivesplus.common.items.components.ItemInvisibleComponent;
import com.pheenixm.explosivesplus.common.items.components.Warhead;
import com.pheenixm.explosivesplus.common.tiles.TileEntityC4;
import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityChaos;
import com.pheenixm.explosivesplus.common.tiles.TileEntityInjector;
import com.pheenixm.explosivesplus.common.tiles.TileEntityPluripotent;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.tiles.generator.Tier1Generator;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.common.registry.EntityEntryBuilder;
import net.minecraftforge.fml.common.registry.GameRegistry;

@EventBusSubscriber
public class RegistryHandler {

	private static Block[] registeredBlocks = null;
	private static int entityID = 0;
	public static final int TRACKING_RADIUS = 256;

	@SubscribeEvent
	public static void registerBlocks(Register<Block> event) {
		final Block[] blocks = {
				new NovaBomb("nova", "nova"),
				new BiomeBuster("biome", "biome"), 
				new MegaNuke("mega", "mega"),
				new	TNTRain("rain","rain"),
				new C4("c4", "c4"),
				new ChaosTheory("chaos","chaos"),
				new MolecularDisruptor("disrupt","disrupt"),
				
				new CreeperBlock("creeper", "creeper"),

				new ComsCable("cable"),
				new PowerInjector("injector"),
				new DebuggerTile("debugger_tile"),

				new LidBlock("iron_slab", "iron_slab"),
				new RotationalEMBlock("em_block","em_block"),
				new ConcreteBlock("concrete", "concrete"),
				new IOBlock("in_out_block","in_out_block"),
				new LinearEMBlock("linear_em_block","linear_em_block"),
				new LinearEMSlab("linear_em_slab", "linear_em_slab")
		
		};

		event.getRegistry().registerAll(blocks);
		registeredBlocks = blocks;

		// Register Tile Entities
		GameRegistry.registerTileEntity(TileEntityC4.class,
				new ResourceLocation(ExplosivesPlus.MOD_ID, "entity.c4"));
		GameRegistry.registerTileEntity(TileEntitySilo.class,
				new ResourceLocation(ExplosivesPlus.MOD_ID, "entity.silo"));
		GameRegistry.registerTileEntity(TileEntityPluripotent.class,
				new ResourceLocation(ExplosivesPlus.MOD_ID, "entity.pluri"));
		GameRegistry.registerTileEntity(TileEntityCentrifuge.class,
				new ResourceLocation(ExplosivesPlus.MOD_ID, "entity.centri"));
		GameRegistry.registerTileEntity(Tier1Generator.class,
				new ResourceLocation(ExplosivesPlus.MOD_ID, "entity.gen_tier1"));
		
		
		GameRegistry.registerTileEntity(TileEntityCable.class,
				new ResourceLocation(ExplosivesPlus.MOD_ID, "entity.cable"));
		GameRegistry.registerTileEntity(TileEntityInjector.class,
				new ResourceLocation(ExplosivesPlus.MOD_ID, "entity.power_injector"));
		GameRegistry.registerTileEntity(TileEntityWireless.class,
				new ResourceLocation(ExplosivesPlus.MOD_ID, "entity.wireless"));
		GameRegistry.registerTileEntity(TileEntityChaos.class,
				new ResourceLocation(ExplosivesPlus.MOD_ID, "entity.chaostile"));

	}

	@SubscribeEvent
	public static void registerItems(Register<Item> event) {
		final Item[] items = {new Detonator("detonator"),
				new RepulsionBeam("repulsion"), new BoomStick("boomstick"),
				new ItemDebugger("debugger"),
				new PredatorMissile("predator"),
				new StealthBomber("stealth"),

				new Warhead("warhead_nova", Explosions.NOVA),
				new Warhead("warhead_biome", Explosions.BIOME),
				new Warhead("warhead_mega", Explosions.MEGA),
				
				new Fuselage("fuselage_normal", FuselageTiers.MASSIVE),
				new Engine("engine_normal", EngineTiers.UNLIMITED),
				
				new ItemComponent("reactonite"),
				new ItemComponent("refined_reactonite"),
				new ItemComponent("circitium"),
				new ItemComponent("nanotubes"),
				new ItemComponent("graphite"),
				new ItemComponent("marium"),
				new ItemComponent("incendium"),
				new ItemComponent("liquid_crystal"),
				new ItemComponent("binder"),
				
		};

		final Item[] itemBlocks = {
				new ItemBlock(ExplosivesPlusHolder.NOVA).setRegistryName(
						ExplosivesPlusHolder.NOVA.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.C4).setRegistryName(
						ExplosivesPlusHolder.C4.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.BIOME).setRegistryName(
						ExplosivesPlusHolder.BIOME.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.CHAOS).setRegistryName(
						ExplosivesPlusHolder.CHAOS.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.MEGA).setRegistryName(
						ExplosivesPlusHolder.MEGA.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.RAIN).setRegistryName(
						ExplosivesPlusHolder.RAIN.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.DISRUPT).setRegistryName(
						ExplosivesPlusHolder.DISRUPT.getRegistryName()),

				new ItemBlock(ExplosivesPlusHolder.CONCRETE).setRegistryName(
						ExplosivesPlusHolder.CONCRETE.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.IRON_SLAB).setRegistryName(
						ExplosivesPlusHolder.IRON_SLAB.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.EM_BLOCK).setRegistryName(
						ExplosivesPlusHolder.EM_BLOCK.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.LINEAR_EM_BLOCK).setRegistryName(
						ExplosivesPlusHolder.LINEAR_EM_BLOCK.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.LINEAR_EM_SLAB).setRegistryName(
						ExplosivesPlusHolder.LINEAR_EM_SLAB.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.IN_OUT_BLOCK).setRegistryName(
						ExplosivesPlusHolder.IN_OUT_BLOCK.getRegistryName()),

				
				new ItemBlock(ExplosivesPlusHolder.CREEPER).setRegistryName(
						ExplosivesPlusHolder.CREEPER.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.INJECTOR).setRegistryName(
						ExplosivesPlusHolder.INJECTOR.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.DEBUGGER_TILE).setRegistryName(
						ExplosivesPlusHolder.DEBUGGER_TILE.getRegistryName()),
				new ItemBlock(ExplosivesPlusHolder.CABLE).setRegistryName(
						ExplosivesPlusHolder.CABLE.getRegistryName())

		};

		event.getRegistry().registerAll(items);
		event.getRegistry().registerAll(itemBlocks);
	}

	@SubscribeEvent
	public static void registerEntity(Register<EntityEntry> event) {
		event.getRegistry().register(buildEntityEntry(EntityNovaBomb.class,
				"nova.primed", TRACKING_RADIUS, 1, true));
		event.getRegistry().register(buildEntityEntry(EntityBiomeBuster.class,
				"biome.primed", TRACKING_RADIUS, 1, true));
		event.getRegistry().register(buildEntityEntry(EntityMegaNuke.class,
				"mega.primed", TRACKING_RADIUS, 1, true));
		event.getRegistry().register(buildEntityEntry(EntityTNTRain.class,
				"rain.primed", TRACKING_RADIUS, 1, true));
		event.getRegistry().register(buildEntityEntry(EntityNuke.class,
				"nuke.primed", TRACKING_RADIUS, 1, true));
		event.getRegistry().register(buildEntityEntry(EntityTheoryOfChaos.class,
				"chaos.primed", TRACKING_RADIUS, 1, true));
		event.getRegistry().register(buildEntityEntry(EntityMolecularDisruptor.class,
				"disrupt.primed", TRACKING_RADIUS, 1, true));

		event.getRegistry()
				.register(buildEntityEntry(EntityNovaController.class,
						"nova.active", TRACKING_RADIUS, 1, true));

		event.getRegistry()
				.register(buildEntityEntry(EntityExplosionController.class,
						"explosion", TRACKING_RADIUS, 1, false));
		event.getRegistry()
				.register(buildEntityEntry(EntityBatchExplosionController.class,
						"explosion.batch", TRACKING_RADIUS, 1, true));

		event.getRegistry()
				.register(buildEntityEntry(EntityTNTRainController.class,
						"rain.active", TRACKING_RADIUS, 1, false));

		event.getRegistry()
				.register(buildEntityEntry(EntityBallisticMissile.class,
						"ballistic", TRACKING_RADIUS, 1, true));
		
		event.getRegistry()
				.register(buildEntityEntry(EntityViewer.class,
						"viewer", TRACKING_RADIUS, 1, true));
		event.getRegistry()
		.register(buildEntityEntry(EntityTargeter.class,
				"viewer.targeting", TRACKING_RADIUS, 1, true));

		event.getRegistry()
				.register(buildEntityEntry(EntityPredatorMissile.class,
						"predator", TRACKING_RADIUS, 1, true));
		event.getRegistry()
		.register(buildEntityEntry(EntityValkyrie.class,
				"valkyrie", TRACKING_RADIUS, 1, true));
		
		
		event.getRegistry()
				.register(buildEntityEntry(EntityStealthBomber.class,
						"entity.stealth", TRACKING_RADIUS, 1, true));

		event.getRegistry()
				.register(buildEntityEntry(EntityShockwave.class,
						"shockwave", TRACKING_RADIUS, 1, false));
		event.getRegistry()
		.register(buildEntityEntry(EntityPhysicsBlock.class,
				"physics", TRACKING_RADIUS, 1, true));
		event.getRegistry()
		.register(buildEntityEntry(EntityTheoryOfChaosController.class,
				"chaos.theory", TRACKING_RADIUS, 1, true));

	}
	

	private static EntityEntry buildEntityEntry(
			Class<? extends Entity> entityClass, String entityName,
			int trackingRange, int updateFrequency, boolean sendVel) {
		EntityEntryBuilder builder = EntityEntryBuilder.create();
		builder.name(ExplosivesPlus.BASENAME + entityName);
		builder.id(new ResourceLocation(ExplosivesPlus.MOD_ID, entityName),
				entityID++);
		builder.tracker(trackingRange, updateFrequency, sendVel);
		builder.entity(entityClass);
		return builder.build();
	}

	public static Block[] getRegisteredBlocks() {
		return registeredBlocks;
	}
	
	@SubscribeEvent
	public static void registerSoundEvents(RegistryEvent.Register<SoundEvent> event) {
		event.getRegistry().registerAll(
				SoundHandler.GUN_FIRE,
				SoundHandler.BIOME_BUST
				);
	}
	
	public static void registerRecipes() {
		//TODO: Adjust ticks to process
		RecipeCentrifuge.addRecipe(new ItemStack(Items.GUNPOWDER, 4), new ItemStack(ExplosivesPlusHolder.REACTONITE, 1), 40);
		RecipeCentrifuge.addRecipe(new ItemStack(ExplosivesPlusHolder.REACTONITE, 16), new ItemStack(ExplosivesPlusHolder.REFINED_REACTONITE, 1), 160);
	}


}
