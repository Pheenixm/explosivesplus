package com.pheenixm.explosivesplus.common;

import java.io.*;
import java.util.*;
import java.util.logging.Level;


public class ConfigHandler {
	
	
	public static int MOLECULAR_DISRUPTOR_FUSE = 80;
	public static int DEFAULT_FUSE = 80;
	public static int NUKE_SIZE = 40;
	
	public static int MOLECULAR_DISRUPTOR_SIZE = 30;
	public static int MOLECULAR_DISRUPTOR_RANGE = 64;
	
	public static int TNT_RAIN_LAYERS = 8;
	public static int TNT_RAIN_SPACING = 5;
	public static int TNT_RAIN_BOMB_SIZE = 15;
	public static double TNT_RAIN_HEIGHT = 192;
	
	public static int SHOCKWAVE_DEFAULT_SIZE = 128;
	public static int SHOCKWAVE_DEFAULT_TIME = 20;
	
/*
	ConfigHandler(FMLPreInitializationEvent event){

		configuration_suggest = event.getSuggestedConfigurationFile();

		try
        {
            loadConfig();
            saveConfig();
        }
        catch(IOException e)
        {
        	FMLLog.log(Level.SEVERE, e, "ExplosivesPlus has a problem loading it's configuration");
        }
		
		Configuration cfg = new Configuration(configuration_suggest);
		cfg.load();
		
		
		nukeId = Integer.parseInt(cfg.getOrCreateBlockIdProperty("Main Block ID", 201).value);
        C4_Id = Integer.parseInt(cfg.getOrCreateBlockIdProperty("C4 ID", 202).value);
        beacon_Id = Integer.parseInt(cfg.getOrCreateBlockIdProperty("Beacon ID", 203).value);
        GenesisId = Integer.parseInt(cfg.getOrCreateBlockIdProperty("Genesis Bomb ID", 204).value);
        configuratorId = Integer.parseInt(cfg.getOrCreateBlockIdProperty("Configurator ID", 205).value);
        missileLauncherId = Integer.parseInt(cfg.getOrCreateBlockIdProperty("Missile Launcher ID", 206).value);

        
        FlareId = Integer.parseInt(cfg.getOrCreateIntProperty("Flare ID", Configuration.CATEGORY_ITEM, 1000).value);
        DetonatorId = Integer.parseInt(cfg.getOrCreateIntProperty("Detonator ID", Configuration.CATEGORY_ITEM, 1001).value);
        LaserDesignatorId = Integer.parseInt(cfg.getOrCreateIntProperty("Laser Designator ID", Configuration.CATEGORY_ITEM, 1003).value);
        PlasmaLanceId = Integer.parseInt(cfg.getOrCreateIntProperty("Plasma Lance ID", Configuration.CATEGORY_ITEM, 1004).value);
        BeamId = Integer.parseInt(cfg.getOrCreateIntProperty("Plasma Beam ID", Configuration.CATEGORY_ITEM, 1005).value);
        PreciseBeamId = Integer.parseInt(cfg.getOrCreateIntProperty("Precision Plasma Beam ID", Configuration.CATEGORY_ITEM, 1006).value);
        BoomStickId = Integer.parseInt(cfg.getOrCreateIntProperty("Boom Stick ID", Configuration.CATEGORY_ITEM, 1007).value);
        HellStickId = Integer.parseInt(cfg.getOrCreateIntProperty("Hell Stick ID", Configuration.CATEGORY_ITEM, 1008).value);
        WrenchId = Integer.parseInt(cfg.getOrCreateIntProperty("Wrench ID", Configuration.CATEGORY_ITEM, 1009).value);
        MissileId = Integer.parseInt(cfg.getOrCreateIntProperty("Predator Missile ID", Configuration.CATEGORY_ITEM, 1010).value);
        GunId = Integer.parseInt(cfg.getOrCreateIntProperty("Elephant Gun ID", Configuration.CATEGORY_ITEM, 1011).value);
        BomberId = Integer.parseInt(cfg.getOrCreateIntProperty("Stealth Bomber ID", Configuration.CATEGORY_ITEM, 1012).value);
        AC130Id = Integer.parseInt(cfg.getOrCreateIntProperty("AC130 ID", Configuration.CATEGORY_ITEM, 1013).value);
        ValkyrieId = Integer.parseInt(cfg.getOrCreateIntProperty("Valkyrie ID", Configuration.CATEGORY_ITEM, 1014).value);
        DynamiteId = Integer.parseInt(cfg.getOrCreateIntProperty("Dynamite ID", Configuration.CATEGORY_ITEM, 1015).value);
        suicideId = Integer.parseInt(cfg.getOrCreateIntProperty("Suicide Vest ID", Configuration.CATEGORY_ITEM, 1016).value);
        controlId = Integer.parseInt(cfg.getOrCreateIntProperty("Mind Control Helmet ID", Configuration.CATEGORY_ITEM, 1017).value);
        gravityId = Integer.parseInt(cfg.getOrCreateIntProperty("AntiGravity Laser ID", Configuration.CATEGORY_ITEM, 1002).value);
        repulsionId = Integer.parseInt(cfg.getOrCreateIntProperty("Repulsion Beam ID", Configuration.CATEGORY_ITEM, 1018).value);

        cfg.save();
        //setProperties();
        parse();
	}
	
	void parse(){

        nukePrimedID = Integer.parseInt(configuration.getProperty("entity.nukePrimed.id").trim());
        megaNukePrimedID = Integer.parseInt(configuration.getProperty("entity.megaNukePrimed.id").trim());
        napalmPrimedID = Integer.parseInt(configuration.getProperty("entity.napalmPrimed.id").trim());
        preciseTNTPrimedID = Integer.parseInt(configuration.getProperty("entity.preciseTNTPrimed.id").trim());
        stickyTNTPrimedID = Integer.parseInt(configuration.getProperty("entity.stickyTNTPrimed.id").trim());
        hBombPrimedID = Integer.parseInt(configuration.getProperty("entity.hBombPrimed.id").trim());
        plasmaRayID = Integer.parseInt(configuration.getProperty("entity.plasmaRay.id").trim());
        novaPrimedID = Integer.parseInt(configuration.getProperty("entity.novaPrimed.id").trim());
        fallingBlocksID = Integer.parseInt(configuration.getProperty("entity.fallingBlocks.id").trim());
        volcanoPrimedID = Integer.parseInt(configuration.getProperty("entity.volcanoPrimed.id").trim());
        volcanoPrePrimedID = Integer.parseInt(configuration.getProperty("entity.volcanoPrePrimed.id").trim());
        predatorMissileID = Integer.parseInt(configuration.getProperty("entity.predatorMissile.id").trim());
        novaPrePrimedID = Integer.parseInt(configuration.getProperty("entity.novaPrePrimed.id").trim());
        makeItRainPrimedID = Integer.parseInt(configuration.getProperty("entity.makeItRainPrimed.id").trim());
        TNTRainID = Integer.parseInt(configuration.getProperty("entity.TNTRain.id").trim());
        nuclearCreeperID = Integer.parseInt(configuration.getProperty("entity.nuclearCreeper.id").trim());
        EntityAC130ID = Integer.parseInt(configuration.getProperty("entity.AC130.id").trim());
        EntityAC130HelperID = Integer.parseInt(configuration.getProperty("entity.AC130Helper.id").trim());
        EntityGrenadeID = Integer.parseInt(configuration.getProperty("entity.grenade.id").trim());
        GreenID = Integer.parseInt(configuration.getProperty("entity.green.id").trim());
        RedID = Integer.parseInt(configuration.getProperty("entity.red.id").trim());
        EntityStealthID = Integer.parseInt(configuration.getProperty("entity.stealth.id").trim());
        EntityValkyrieID = Integer.parseInt(configuration.getProperty("entity.valkyrie.id").trim());

        configuration.setProperty("entity.nukePrimed.id", Integer.valueOf(nukePrimedID).toString());
        configuration.setProperty("entity.megaNukePrimed.id", Integer.valueOf(megaNukePrimedID).toString());
        configuration.setProperty("entity.napalmPrimed.id", Integer.valueOf(napalmPrimedID).toString());
        configuration.setProperty("entity.preciseTNTPrimed.id", Integer.valueOf(preciseTNTPrimedID).toString());
        configuration.setProperty("entity.stickyTNTPrimed.id", Integer.valueOf(stickyTNTPrimedID).toString());
        configuration.setProperty("entity.hBombPrimed.id", Integer.valueOf(hBombPrimedID).toString());
        configuration.setProperty("entity.plasmaRay.id", Integer.valueOf(plasmaRayID).toString());
        configuration.setProperty("entity.novaPrimed.id", Integer.valueOf(novaPrimedID).toString());
        configuration.setProperty("entity.fallingBlocks.id", Integer.valueOf(fallingBlocksID).toString());
        configuration.setProperty("entity.volcanoPrimed.id", Integer.valueOf(volcanoPrimedID).toString());
        configuration.setProperty("entity.volcanoPrePrimed.id", Integer.valueOf(volcanoPrePrimedID).toString());
        configuration.setProperty("entity.predatorMissile.id", Integer.valueOf(predatorMissileID).toString());
        configuration.setProperty("entity.novaPrePrimed.id", Integer.valueOf(novaPrePrimedID).toString());
        configuration.setProperty("entity.makeItRainPrimed.id", Integer.valueOf(makeItRainPrimedID).toString());
        configuration.setProperty("entity.TNTRain.id", Integer.valueOf(TNTRainID).toString());
        configuration.setProperty("entity.nuclearCreeper.id", Integer.valueOf(nuclearCreeperID).toString());
        configuration.setProperty("entity.AC130.id", Integer.valueOf(EntityAC130ID).toString());
        configuration.setProperty("entity.AC130Helper.id", Integer.valueOf(EntityAC130HelperID).toString());
        configuration.setProperty("entity.grenade.id", Integer.valueOf(EntityGrenadeID).toString());
        configuration.setProperty("entity.green.id", Integer.valueOf(GreenID).toString());
        configuration.setProperty("entity.red.id", Integer.valueOf(RedID).toString());
        configuration.setProperty("entity.stealth.id", Integer.valueOf(EntityStealthID).toString());
        configuration.setProperty("entity.valkyrie.id", Integer.valueOf(EntityValkyrieID).toString());
        
        keys.add("isNuclearCreeperEnabled");
        if(Boolean.parseBoolean(configuration.getProperty("Enable_Nuclear_Creeper").trim())){
        	values.put("isNuclearCreeperEnabled", 1F);
        }else{
        	values.put("isNuclearCreeperEnabled", 0F);
        }
        
        keys.add("nukeExplosiveStrength");
        values.put("nukeExplosiveStrength", Float.parseFloat(configuration.getProperty("Nuke_Explosive_Strength").trim()));
        keys.add("nukeFuse");
        values.put("nukeFuse", (float)Integer.parseInt(configuration.getProperty("Nuke_Fuse_Length").trim()));
        
        keys.add("megaNukeExplosiveStrength");
        values.put("megaNukeExplosiveStrength", Float.parseFloat(configuration.getProperty("MegaNuke_Explosive_Strength").trim()));
        keys.add("megaNukeFuse");
        values.put("megaNukeFuse", (float)Integer.parseInt(configuration.getProperty("MegaNuke_Fuse_Length").trim()));
        
        keys.add("napalmExplosiveStrength");
        values.put("napalmExplosiveStrength", Float.parseFloat(configuration.getProperty("Napalm_Explosive_Strength").trim()));
        keys.add("napalmFuse");
        values.put("napalmFuse", (float)Integer.parseInt(configuration.getProperty("Napalm_Fuse_Length").trim()));
        
        keys.add("preciseExplosiveStrength");
        values.put("preciseExplosiveStrength", Float.parseFloat(configuration.getProperty("PrecisionTNT_Explosive_Strength").trim()));
        keys.add("preciseFuse");
        values.put("preciseFuse", (float)Integer.parseInt(configuration.getProperty("PrecisionTNT_Fuse_Length").trim()));
        
        keys.add("stickyExplosiveStrength");
        values.put("stickyExplosiveStrength", Float.parseFloat(configuration.getProperty("StickyTNT_Explosive_Strength").trim()));
        keys.add("stickyFuse");
        values.put("stickyFuse", (float)Integer.parseInt(configuration.getProperty("StickyTNT_Fuse_Length").trim()));

        keys.add("C4ExplosiveStrength");
        values.put("C4ExplosiveStrength", Float.parseFloat(configuration.getProperty("C4_Explosive_Strength").trim()));

        keys.add("BiomeBusterStrength");
        values.put("BiomeBusterStrength", Float.parseFloat(configuration.getProperty("BiomeBuster_Explosive_Strength(Per_Explosion)").trim()));
        
        keys.add("HBombExplosiveStrength");
        values.put("HBombExplosiveStrength", Float.parseFloat(configuration.getProperty("Hydrogen_Bomb_Explosive_Strength").trim()));
        keys.add("HBombFuse");
        values.put("HBombFuse", (float)Integer.parseInt(configuration.getProperty("Hydrogen_Bomb_Fuse_Length").trim()));
        
        keys.add("changeSpeed");
        if(Boolean.parseBoolean(configuration.getProperty("Enable_Speed_Reduction_On_Plasma_Lance").trim())){
        	values.put("changeSpeed", 1F);
        }else{
        	values.put("changeSpeed", 0F);
        }
        keys.add("speedMultiplier");
        values.put("speedMultiplier", Float.parseFloat(configuration.getProperty("Speed_Reduction_Multiplier").trim()));
        
        keys.add("NukeCreeperExplosiveStrength");
        values.put("NukeCreeperExplosiveStrength", Float.parseFloat(configuration.getProperty("Nuclear_Creeper_Explosive_Strength").trim()));
        keys.add("ChargedNukeCreeperExplosiveStrength");
        values.put("ChargedNukeCreeperExplosiveStrength", Float.parseFloat(configuration.getProperty("Charged_Nuclear_Creeper_Explosive_Strength").trim()));
        keys.add("NukeCreeperRarity");
        values.put("NukeCreeperRarity", (float)Integer.parseInt(configuration.getProperty("Nuclear_Creeper_Rarity").trim()));
        keys.add("chainCreeper");
        if(Boolean.parseBoolean(configuration.getProperty("Enable_Chain_Explosions_On_Nuclear_Creepers").trim())){
        	values.put("chainCreeper", 1F);
        }else{
        	values.put("chainCreeper", 0F);
        }
        
        keys.add("FallingBlockSpeed");
        values.put("FallingBlockSpeed", (float)Double.parseDouble(configuration.getProperty("Falling_Block_Speed").trim()));
        
        keys.add("ExplosiveRadiusNuke");
        values.put("ExplosiveRadiusNuke", (float)Integer.parseInt(configuration.getProperty("Explosive_Radius_Nuke").trim()));
        keys.add("ExplosiveRadiusMegaNuke");
        values.put("ExplosiveRadiusMegaNuke", (float)Integer.parseInt(configuration.getProperty("Explosive_Radius_MegaNuke").trim()));
        keys.add("ExplosiveRadiusNapalm");
        values.put("ExplosiveRadiusNapalm", (float)Integer.parseInt(configuration.getProperty("Explosive_Radius_Napalm").trim()));
        keys.add("ExplosiveRadiusNukeCreeper");
        values.put("ExplosiveRadiusNukeCreeper", (float)Integer.parseInt(configuration.getProperty("Explosive_Radius_Nuclear_Creeper").trim()));
        keys.add("ExplosiveRadiusPreciseTNT");
        values.put("ExplosiveRadiusPreciseTNT", (float)Integer.parseInt(configuration.getProperty("Explosive_Radius_PreciseTNT").trim()));
        keys.add("ExplosiveRadiusNovaBomb");
        values.put("ExplosiveRadiusNovaBomb", (float)Integer.parseInt(configuration.getProperty("Explosive_Radius_Nova_Bomb").trim()));

        keys.add("NovaInitialExplosiveStrength");
        values.put("NovaInitialExplosiveStrength", Float.parseFloat(configuration.getProperty("Nova_Bomb_Initial_Explosive_Strength").trim()));
        keys.add("NovaBlastExplosiveStrength");
        values.put("NovaBlastExplosiveStrength", Float.parseFloat(configuration.getProperty("Nova_Bomb_Blast_Strength").trim()));
        keys.add("NovaFuse");
        values.put("NovaFuse", (float)Integer.parseInt(configuration.getProperty("Nova_Bomb_Fuse_Length").trim()));
        
        keys.add("EnhancedExplosions");
        if(Boolean.parseBoolean(configuration.getProperty("*****ENABLE_ENHANCED_EXPLOSIONS*****").trim())){
        	values.put("EnhancedExplosions", 1F);
        }else{
        	values.put("EnhancedExplosions", 0F);
        }
        
        keys.add("NapalmOutput");
        values.put("NapalmOutput", (float)Integer.parseInt(configuration.getProperty("Napalm_Explosion_Block_Output").trim()));
        
        keys.add("BoomStickPower");
        values.put("BoomStickPower", Float.parseFloat(configuration.getProperty("Boom_Stick_Power").trim()));
        
        keys.add("BiomeBusterExplosiveRadius");
        values.put("BiomeBusterExplosiveRadius", (float)Integer.parseInt(configuration.getProperty("Biome_Buster_Explosive_Radius_Per_Explosion").trim()));
        
        keys.add("HighYieldNova");
        if(Boolean.parseBoolean(configuration.getProperty("*****ENABLE_HIGH_YIELD_NOVAS*****").trim())){
            values.put("HighYieldNova", 1F);	
        }else{
        	values.put("HighYieldNova", 0F);
        }
        
        keys.add("PyramidSize");
        values.put("PyramidSize", (float)Integer.parseInt(configuration.getProperty("Volcano_Size").trim()));
        keys.add("VolcanoFuse");
        values.put("VolcanoFuse", (float)Integer.parseInt(configuration.getProperty("Volcano_Fuse").trim()));
        keys.add("MaterialType");
        if(Boolean.parseBoolean(configuration.getProperty("MaterialType").trim())){
        	values.put("MaterialType", 1F);
        }else{
        	values.put("MaterialType", 0F);
        }
        
        keys.add("rainSize");
        values.put("rainSize", (float)Integer.parseInt(configuration.getProperty("RainSize").trim()));
        keys.add("blockSpacing");
        values.put("blockSpacing", (float)Integer.parseInt(configuration.getProperty("BlockSpacing").trim()));
        keys.add("rainDuration");
        values.put("rainDuration", (float)Integer.parseInt(configuration.getProperty("RainDuration").trim()));
        
        keys.add("novaBombHole");
        if(Boolean.parseBoolean(configuration.getProperty("novaHole").trim())){
        	values.put("novaBombHole", 1F);
        }else{
        	values.put("novaBombHole", 0F);
        }
        keys.add("advancedFeatures");
        if(Boolean.parseBoolean(configuration.getProperty("advancedFeatures").trim())){
        	values.put("advancedFeatures", 1F);
        }else{
        	values.put("advancedFeatures", 0F);
        }
        
        keys.add("genesisCount");
        values.put("genesisCount", (float)Integer.parseInt(configuration.getProperty("genesisCount").trim()));
        keys.add("genesisRange");
        values.put("genesisRange", (float)Integer.parseInt(configuration.getProperty("genesisRange").trim()));
        
        keys.add("isForceGrenade");
        if(Boolean.parseBoolean(configuration.getProperty("isForceGrenade").trim())){
        	values.put("isForceGrenade", 1F);
        }else{
        	values.put("isForceGrenade", 0F);
        }
        keys.add("grenadeStrength");
        values.put("grenadeStrength", Float.parseFloat(configuration.getProperty("grenadeStrength").trim()));
        keys.add("lowYieldReturnRate");
        values.put("lowYieldReturnRate", (float)Double.parseDouble(configuration.getProperty("lowYieldReturnRate").trim()));
        keys.add("isPhase");
        if(Boolean.parseBoolean(configuration.getProperty("isPhase").trim())){
        	values.put("isPhase", 1F);
        }else{
        	values.put("isPhase", 0F);
        }
        keys.add("maxEntities");
        values.put("maxEntities", (float)Integer.parseInt(configuration.getProperty("maxEntities").trim()));
        keys.add("limitEntities");
        if(Boolean.parseBoolean(configuration.getProperty("limitEntities").trim())){
        	values.put("limitEntities", 1F);
        }else{
        	values.put("limitEntities", 0F);
        }
        keys.add("novaStrengthMultiplier");
        values.put("novaStrengthMultiplier", Float.parseFloat(configuration.getProperty("novaStrengthMultiplier").trim()));
        
        keys.add("bbChunkRadius");
        values.put("bbChunkRadius", (float)Integer.parseInt(configuration.getProperty("bbChunkRadius").trim()));
        keys.add("bbFuse");
        values.put("bbFuse", (float)Integer.parseInt(configuration.getProperty("bbFuse").trim()));
        
        keys.add("inverterRadius");
        values.put("inverterRadius", (float)Integer.parseInt(configuration.getProperty("inverterRadius").trim()));
        keys.add("inverterCeiling");
        values.put("inverterCeiling", (float)Integer.parseInt(configuration.getProperty("inverterCeiling").trim()));
        keys.add("timeLimitEnabled");
        if(Boolean.parseBoolean(configuration.getProperty("timeLimitEnabled").trim())){
        	values.put("timeLimitEnabled", 1F);
        }else{
        	values.put("timeLimitEnabled", 0F);
        }
        keys.add("timeLimit");
        values.put("timeLimit", (float)Integer.parseInt(configuration.getProperty("timeLimit").trim()));
        
        keys.add("goBig");
        if(Boolean.parseBoolean(configuration.getProperty("goBig").trim())){
        	values.put("goBig", 1F);
        }else{
        	values.put("goBig", 0F);
        }
        keys.add("missilePower");
        values.put("missilePower", Float.parseFloat(configuration.getProperty("missilePower").trim()));
        keys.add("missileRadius1");
        values.put("missileRadius1", (float)Integer.parseInt(configuration.getProperty("missileRadius1").trim()));
        keys.add("missileRadius2");
        values.put("missileRadius2", (float)Integer.parseInt(configuration.getProperty("missileRadius2").trim()));

        keys.add("blackHoleRadius");
        values.put("blackHoleRadius", Float.parseFloat(configuration.getProperty("blackHoleRadius").trim()));
        keys.add("blackHoleFuse");
        values.put("blackHoleFuse", (float)Integer.parseInt(configuration.getProperty("blackHoleFuse").trim()));
        keys.add("blackHoleCount");
        values.put("blackHoleCount", (float)Integer.parseInt(configuration.getProperty("blackHoleCount").trim()));
        keys.add("blackHoleChance");
        values.put("blackHoleChance", (float)Integer.parseInt(configuration.getProperty("blackHoleChance").trim()));

        keys.add("tntStrength");
        values.put("tntStrength", Float.parseFloat(configuration.getProperty("tntStrength").trim()));
        keys.add("tntRadius");
        values.put("tntRadius", (float)Byte.parseByte(configuration.getProperty("tntRadius").trim()));
        keys.add("tntFuse");
        values.put("tntFuse", (float)Integer.parseInt(configuration.getProperty("tntFuse").trim()));


	}

	
    
    
    static void saveConfig()
            throws IOException
        {
            configuration_dir.mkdir();
            if(!configuration_file.exists() && !configuration_file.createNewFile())
                return;
            if(configuration_file.canWrite())
            {
                FileOutputStream fileoutputstream = new FileOutputStream(configuration_file);
                configuration.store(fileoutputstream, "Explosives+ Configuration");
                fileoutputstream.close();
            }
        }

        public void loadConfig()
            throws IOException
        {
            configuration_dir.mkdir();
            if(!configuration_file.exists() && !configuration_file.createNewFile())
                return;
            if(configuration_file.canRead())
            {
                FileInputStream fileinputstream = new FileInputStream(configuration_file);
                configuration.load(fileinputstream);
                fileinputstream.close();
            }
        }
       
        
        public static Properties configuration;
        private static File configuration_dir;
        private static File configuration_file;
        private static File configuration_suggest;

        public static int nukePrimedID;
        public static int megaNukePrimedID;
        public static int napalmPrimedID;
        public static int preciseTNTPrimedID;
        public static int stickyTNTPrimedID;
        public static int hBombPrimedID;
        public static int plasmaRayID;
        public static int novaPrimedID;
        public static int fallingBlocksID;
        public static int volcanoPrimedID;
        public static int volcanoPrePrimedID;
        public static int predatorMissileID;
        public static int novaPrePrimedID;
        public static int makeItRainPrimedID;
        public static int TNTRainID;
        public static int nuclearCreeperID;
        public static int EntityAC130ID;
        public static int EntityAC130HelperID;
        public static int EntityGrenadeID;
        public static int GreenID;
        public static int RedID;
        public static int EntityStealthID;
        public static int EntityValkyrieID;
         
        public static List<String> keys = new LinkedList();
        public static HashMap<String, Float> values = new HashMap<String, Float>();
        public static HashMap<String, Float> remoteValue = new HashMap<String, Float>();
 		public static boolean soundsExist = true;
 		
         
         public static int nukeId;
     	 public static int C4_Id;// = 202;
     	 public static int beacon_Id;// = 203;
     	 public static int GenesisId; //= 204
     	 public static int configuratorId;
     	 public static int missileLauncherId;
         
     	 public static int FlareId;// = 1000;
     	 public static int DetonatorId;// = 1001;
           public static int LaserDesignatorId;// = 1003;
           public static int PlasmaLanceId;// = 1004;
           public static int BeamId;// = 1005;
           public static int PreciseBeamId;// = 1006;
           public static int BoomStickId;// = 1007;
           public static int HellStickId;// = 1008;
           public static int WrenchId;// = 1009;
            public static int MissileId;// = 1010;
            public static int GunId;// = 1011;
            public static int BomberId;// = 1012;
            public static int AC130Id;// = 1013;
            public static int ValkyrieId;// = 1014;
           public static int DynamiteId;// = 1015;	
           public static int suicideId;
           public static int controlId;
           public static int gravityId;
           public static int repulsionId;
           
           
           static {
               
               configuration = new Properties();
               configuration_dir = new File(Minecraft.getMinecraftDir(), "/config/");
               configuration_file = new File(configuration_dir, "Explosives+.cfg");

               configuration.setProperty("Enable_Nuclear_Creeper", "false");
               
               configuration.setProperty("Nuke_Explosive_Strength", "20F");
               configuration.setProperty("Nuke_Fuse_Length", "80");
               
               configuration.setProperty("MegaNuke_Explosive_Strength", "100F");
               configuration.setProperty("MegaNuke_Fuse_Length", "400");

               configuration.setProperty("Napalm_Explosive_Strength", "10F");
               configuration.setProperty("Napalm_Fuse_Length", "120");

               configuration.setProperty("PrecisionTNT_Explosive_Strength", "7F");
               configuration.setProperty("PrecisionTNT_Fuse_Length", "80");
               
               configuration.setProperty("StickyTNT_Explosive_Strength", "4F");
               configuration.setProperty("StickyTNT_Fuse_Length", "80");

               configuration.setProperty("C4_Explosive_Strength", "10F");

               configuration.setProperty("BiomeBuster_Explosive_Strength(Per_Explosion)", "75F");
               
               configuration.setProperty("Hydrogen_Bomb_Explosive_Strength", "50F");
               configuration.setProperty("Hydrogen_Bomb_Fuse_Length", "80");

               configuration.setProperty("Enable_Speed_Reduction_On_Plasma_Lance", "true");
               configuration.setProperty("Speed_Reduction_Multiplier", "0.01F");

               configuration.setProperty("Nuclear_Creeper_Explosive_Strength", "50F");
               configuration.setProperty("Charged_Nuclear_Creeper_Explosive_Strength", "150F");
               configuration.setProperty("Nuclear_Creeper_Rarity", "1");
               configuration.setProperty("Enable_Chain_Explosions_On_Nuclear_Creepers", "false");

               configuration.setProperty("Falling_Block_Speed", "0.039999999105930328D");

               configuration.setProperty("Explosive_Radius_Nuke", "32");
               configuration.setProperty("Explosive_Radius_MegaNuke", "64");
               configuration.setProperty("Explosive_Radius_Napalm", "32");
               configuration.setProperty("Explosive_Radius_Nuclear_Creeper", "32");
               configuration.setProperty("Explosive_Radius_PreciseTNT", "32");
               configuration.setProperty("Explosive_Radius_Nova_Bomb", "32");
               
               configuration.setProperty("Nova_Bomb_Initial_Explosive_Strength", "7.5F");
               configuration.setProperty("Nova_Bomb_Blast_Strength", "50F");
               configuration.setProperty("Nova_Bomb_Fuse_Length", "80");

               configuration.setProperty("*****ENABLE_ENHANCED_EXPLOSIONS*****", "false");
               configuration.setProperty("Napalm_Explosion_Block_Output", "10");
               
               configuration.setProperty("Boom_Stick_Power", "25F");

               configuration.setProperty("Biome_Buster_Explosive_Radius_Per_Explosion", "16");

               configuration.setProperty("*****ENABLE_HIGH_YIELD_NOVAS*****", "true");

               configuration.setProperty("Volcano_Size", "50");
               configuration.setProperty("Volcano_Fuse", "180");
               configuration.setProperty("MaterialType", "false");
               
               configuration.setProperty("RainSize", "5");
               configuration.setProperty("BlockSpacing", "5");
               configuration.setProperty("RainDuration", "80");

               configuration.setProperty("gui.biomeBuster", "218");
               configuration.setProperty("gui.c4", "219");
               configuration.setProperty("gui.hBomb", "220");
               configuration.setProperty("gui.megaNuke", "221");
               configuration.setProperty("gui.napalm", "222");
               configuration.setProperty("gui.nova", "223");
               configuration.setProperty("gui.nuke", "224");
               configuration.setProperty("gui.preciseTNT", "225");
               configuration.setProperty("gui.stickyTNT", "226");
               configuration.setProperty("gui.volcano", "227");
               configuration.setProperty("gui.creeper", "228");

               
               configuration.setProperty("entity.nukePrimed.id", "162");
               configuration.setProperty("entity.megaNukePrimed.id", "163");
               configuration.setProperty("entity.napalmPrimed.id", "164");
               configuration.setProperty("entity.preciseTNTPrimed.id", "165");
               configuration.setProperty("entity.stickyTNTPrimed.id", "166");
               configuration.setProperty("entity.hBombPrimed.id", "167");
               configuration.setProperty("entity.plasmaRay.id", "168");
               configuration.setProperty("entity.novaPrimed.id", "169");
               configuration.setProperty("entity.fallingBlocks.id", "170");
               configuration.setProperty("entity.volcanoPrimed.id", "171");
               configuration.setProperty("entity.volcanoPrePrimed.id", "172");
               configuration.setProperty("entity.predatorMissile.id", "173");
               configuration.setProperty("entity.novaPrePrimed.id", "174");
               configuration.setProperty("entity.makeItRainPrimed.id", "175");
               configuration.setProperty("entity.TNTRain.id", "178");
               configuration.setProperty("entity.nuclearCreeper.id", "66");
               
               configuration.setProperty("entity.AC130.id", "179");
               configuration.setProperty("entity.AC130Helper.id", "180");
               configuration.setProperty("entity.grenade.id", "186");
               configuration.setProperty("entity.red.id", "187");
               configuration.setProperty("entity.green.id", "188");
               configuration.setProperty("entity.stealth.id", "189");
               configuration.setProperty("entity.valkyrie.id", "191");

               configuration.setProperty("enable_sounds", "false");
               configuration.setProperty("novaHole", "false");
               configuration.setProperty("advancedFeatures", "true");

               configuration.setProperty("genesisCount", "200");
               configuration.setProperty("genesisRange", "5");

               configuration.setProperty("isForceGrenade", "false");
               configuration.setProperty("grenadeStrength", "10F");
               configuration.setProperty("lowYieldReturnRate", "0.85D");
               configuration.setProperty("isPhase", "false");
               configuration.setProperty("maxEntities", "1000");
               configuration.setProperty("limitEntities", "true");
               configuration.setProperty("novaStrengthMultiplier", "3F");
               
               configuration.setProperty("bbChunkRadius", "16");
               configuration.setProperty("bbFuse", "100");

               configuration.setProperty("inverterRadius", "15");
               configuration.setProperty("inverterCeiling", "128");
               configuration.setProperty("timeLimitEnabled", "false");
               configuration.setProperty("timeLimit", "80");

               configuration.setProperty("goBig", "false");
               configuration.setProperty("missilePower", "50F");
               configuration.setProperty("missileRadius1", "32");
               configuration.setProperty("missileRadius2", "100");
        
               configuration.setProperty("blackHoleRadius", "16F");
               configuration.setProperty("blackHoleFuse", "120");
               configuration.setProperty("blackHoleCount", "160");
               configuration.setProperty("blackHoleChance", "4");

               configuration.setProperty("tntStrength", "4.0F");
               configuration.setProperty("tntRadius", "16");
               configuration.setProperty("tntFuse", "80");
           
           }

      */     
           
}
