package com.pheenixm.explosivesplus.common.blocks;

import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;
import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.tiles.TileEntityC4;

import net.minecraft.block.Block;
import net.minecraft.block.BlockButton;
import net.minecraft.block.BlockDirectional;
import net.minecraft.block.BlockLever;
import net.minecraft.block.BlockTorch;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class C4 extends BlockDirectional {

	protected static final AxisAlignedBB NORTH_AABB = new AxisAlignedBB(.15f,
			.25f, 0.79f, .85f, .75f, 1);
	protected static final AxisAlignedBB SOUTH_AABB = new AxisAlignedBB(.15f,
			.25f, 0, .85f, .75f, 0.21f);
	protected static final AxisAlignedBB WEST_AABB = new AxisAlignedBB(0.79f,
			.25f, .15f, 1, .75f, .85f);
	protected static final AxisAlignedBB EAST_AABB = new AxisAlignedBB(0, .25f,
			.15f, 0.21f, .75f, .85f);
	protected static final AxisAlignedBB UP_AABB = new AxisAlignedBB(.15f, 0,
			0.25f, .85f, 0.21f, .75f);
	protected static final AxisAlignedBB DOWN_AABB = new AxisAlignedBB(.15f,
			0.75f, .21f, .85f, 1, .75f);

	public static final PropertyDirection FACING = PropertyDirection
			.create("facing");

	public C4(String unlocalizedName, String registryName) {
		super(Material.TNT);
		setTranslationKey(ExplosivesPlus.BASENAME + unlocalizedName);
		setRegistryName(registryName);
		setCreativeTab(ExplosivesPlus.tab);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING,
				EnumFacing.UP));
	}

	public IBlockState withRotation(IBlockState state, Rotation rot) {
		return state.withProperty(FACING,
				rot.rotate((EnumFacing) state.getValue(FACING)));
	}

    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn)
    {
		tooltip.add(TextFormatting.YELLOW + I18n.format("gui.tooltip." + stack.getTranslationKey()));	
    }

	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source,
			BlockPos pos) {
		switch (((EnumFacing) state.getValue(FACING))) {
			case UP :
			default :
				return UP_AABB;
			case DOWN :
				return DOWN_AABB;
			case NORTH :
				return NORTH_AABB;
			case SOUTH :
				return SOUTH_AABB;
			case EAST :
				return EAST_AABB;
			case WEST :
				return WEST_AABB;
		}
	}

	public IBlockState getStateForPlacement(World worldIn, BlockPos pos,
			EnumFacing facing, float hitX, float hitY, float hitZ, int meta,
			EntityLivingBase placer) {
		return this.getDefaultState().withProperty(FACING, facing);
	}

	/**
	 * Convert the given metadata into a BlockState for this Block
	 */
	public IBlockState getStateFromMeta(int meta) {
		IBlockState iblockstate = this.getDefaultState();
		iblockstate = iblockstate.withProperty(FACING,
				EnumFacing.byIndex(meta));
		return iblockstate;
	}

	public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn,
			IBlockState state, BlockPos pos, EnumFacing face) {
		return BlockFaceShape.UNDEFINED;
	}

	/**
	 * Convert the BlockState into the correct metadata value
	 */
	public int getMetaFromState(IBlockState state) {
		return ((EnumFacing) state.getValue(FACING)).getIndex();
	}

	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, new IProperty[]{FACING});
	}

	/**
	 * Used to determine ambient occlusion and culling when rebuilding chunks
	 * for render
	 */
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}

	public boolean isFullCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean canPlaceBlockOnSide(World worldIn, BlockPos pos,
			EnumFacing side) {
		return canAttachTo(worldIn, pos, side);
	}

	/**
	 * Checks if this block can be placed exactly at the given position.
	 */
	@Override
	public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
		for (EnumFacing enumfacing : EnumFacing.values()) {
			if (canAttachTo(worldIn, pos, enumfacing)) {
				return true;
			}
		}

		return false;
	}

	protected boolean canAttachTo(World worldIn, BlockPos p_181090_1_,
			EnumFacing p_181090_2_) {
		return canPlaceBlock(worldIn, p_181090_1_, p_181090_2_);
	}

	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos,
			Block blockIn, BlockPos fromPos) {
		if (this.checkCanSurvive(worldIn, pos, state) && !canAttachTo(worldIn,
				pos, (EnumFacing) state.getValue(FACING))) {
			this.dropBlockAsItem(worldIn, pos, state, 0);
			worldIn.setBlockToAir(pos);
		}
	}

	private boolean checkCanSurvive(World worldIn, BlockPos pos,
			IBlockState state) {
		if (this.canPlaceBlockAt(worldIn, pos)) {
			return true;
		} else {
			this.dropBlockAsItem(worldIn, pos, state, 0);
			worldIn.setBlockToAir(pos);
			return false;
		}
	}

	private boolean canPlaceBlock(World worldIn, BlockPos pos,
			EnumFacing direction) {
		BlockPos blockpos = pos.offset(direction.getOpposite());
		IBlockState iblockstate = worldIn.getBlockState(blockpos);
		boolean flag = iblockstate.getBlockFaceShape(worldIn, blockpos,
				direction) == BlockFaceShape.SOLID;
		Block block = iblockstate.getBlock();

		if (direction == EnumFacing.UP) {
			return getBlockFaceShape(worldIn, iblockstate, pos,
					EnumFacing.UP) == BlockFaceShape.SOLID
					|| !isExceptionBlockForAttaching(block) && flag;
		} else {
			return !isExceptBlockForAttachWithPiston(block) && flag;
		}
	}

	@Override
	public boolean hasTileEntity(IBlockState state) {
		return true;
	}

	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state,
			EntityLivingBase placer, ItemStack stack) {
		// worldIn.setBlockState(pos, state.withProperty(FACING,
		// placer.getHorizontalFacing().getOpposite()), 2);
		if (placer instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) placer;
			this.getTileEntity(worldIn, pos)
					.setUsername(player.getGameProfile().getName());
		}
	}

	public void detonate(World world, BlockPos pos) {
		if (!world.isRemote) {
			if (world.isAirBlock(pos)) {
				// Block has been destroyed by explosion, short circuit method
				world.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(),
						5F, true);
				return;
			}
			BlockPos attachedPos = pos.offset((EnumFacing) world
					.getBlockState(pos).getValue(FACING).getOpposite());
			IBlockState stateAtt = world.getBlockState(attachedPos);
			Block block = stateAtt.getBlock();
			world.setBlockToAir(pos);
			if (block instanceof ExplosivesBlock) {
				((ExplosivesBlock) block).detonate(world, attachedPos,
						world.getBlockState(attachedPos).withProperty(
								ExplosivesBlock.EXPLODE, Boolean.valueOf(true)),
						true);
			} else {
				world.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(),
						5F, true);
			}
		}
	}

	@Override
	public void onExplosionDestroy(World worldIn, BlockPos pos,
			Explosion explosionIn) {
		if (!worldIn.isRemote) {
			detonate(worldIn, pos);
		}
	}

	@Override
	public TileEntity createTileEntity(World world, IBlockState state) {
		return new TileEntityC4();
	}

	@Nullable
	public TileEntityC4 getTileEntity(IBlockAccess world, BlockPos pos) {
		return (TileEntityC4) world.getTileEntity(pos);
	}

}
