package com.pheenixm.explosivesplus.common.blocks;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.coms.INetworkComponent;
import com.pheenixm.explosivesplus.api.coms.INetworkedBlock;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.tiles.TileEntityCable;
import com.pheenixm.explosivesplus.common.coms.tiles.TileEntityWireless;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class DebuggerTile extends Block implements INetworkedBlock{

    public DebuggerTile(String unlocalizedName) {
		super(Material.GROUND);
		setTranslationKey(ExplosivesPlus.BASENAME + unlocalizedName);
		setRegistryName(unlocalizedName);
        setSoundType(SoundType.GROUND);
		setCreativeTab(ExplosivesPlus.tab);

	}

	@Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state)
    {
        return new TileEntityWireless();
    }
    
    
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		if (!worldIn.isRemote) {
			INetworkComponent comp = this.getTileEntity(worldIn, pos).getCapability(CapabilityNetwork.HANDLER, null);
			if(comp != null) {
				comp.onComponentDestroyed(pos);
			}
		}
		super.breakBlock(worldIn, pos, state);
	}



    @Override
    public void onNeighborChange(IBlockAccess world, BlockPos pos, BlockPos neighbor)
    {
        super.onNeighborChange(world, pos, neighbor);
        INetworkComponent comp = this.getTileEntity(world, pos).getCapability(CapabilityNetwork.HANDLER, null);
        if(comp != null) {
        		comp.updateNeighbors(world, pos);
        }
    }
    
    @Deprecated
    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos)
    {
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos);
        INetworkComponent comp = this.getTileEntity(worldIn, pos).getCapability(CapabilityNetwork.HANDLER, null);
        if(comp != null) {
        		comp.updateNeighbors(worldIn, pos);
        }    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
    		((TileEntityWireless)this.getTileEntity(worldIn, pos)).onPlacedBy(worldIn, pos, state, placer, stack);
    }
    
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
    		if(!worldIn.isRemote) {
    			System.out.println(((TileEntityWireless)this.getTileEntity(worldIn, pos)).wireless.getSharedUid().toString());
    		}
    		return false;
    }

}
