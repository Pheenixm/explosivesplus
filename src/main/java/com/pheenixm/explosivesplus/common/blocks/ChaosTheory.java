package com.pheenixm.explosivesplus.common.blocks;

import com.pheenixm.explosivesplus.client.gui.GuiChaosTheory;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.GuiHandler;
import com.pheenixm.explosivesplus.common.coms.tiles.TileEntityCable;
import com.pheenixm.explosivesplus.common.entities.EntityNovaBomb;
import com.pheenixm.explosivesplus.common.entities.EntityTheoryOfChaos;
import com.pheenixm.explosivesplus.common.tiles.TileEntityChaos;

import net.minecraft.block.BlockPlanks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ChaosTheory extends ExplosivesBlock {

	public ChaosTheory(String unlocalizedName, String registryName) {
		super(unlocalizedName, registryName);
	}

	@Override
	public void detonate(World world, BlockPos pos, IBlockState state,
			boolean shortfuse) {
		if (!world.isRemote) {
			if (((Boolean) state.getValue(EXPLODE)).booleanValue()) {
				TileEntityChaos tile = (TileEntityChaos) world.getTileEntity(pos);
				String seed = tile.seed;
				world.setBlockToAir(pos);
				int fuse = 80;
				if(shortfuse) {
					fuse = 0;
				}
				EntityTheoryOfChaos nova = new EntityTheoryOfChaos(world, pos.getX(), pos.getY(), pos.getZ(), null, seed);
				nova.setFuse(fuse);
				world.spawnEntity(nova);
			}
		}
	}
	
    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state)
    {
        return new TileEntityChaos();
    }

    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        ItemStack itemstack = playerIn.getHeldItem(hand);

        if (itemstack.isEmpty() || (itemstack.getItem() != Items.FLINT_AND_STEEL && itemstack.getItem() != Items.FIRE_CHARGE)) {	
        		if(worldIn.isRemote) {
        			GuiHandler.openChaos((TileEntityChaos)worldIn.getTileEntity(pos));
        		}
        		return true;
        		
        } else {
        		return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
        }
    }
}
