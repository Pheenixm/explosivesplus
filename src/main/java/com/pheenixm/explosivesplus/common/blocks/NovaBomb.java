package com.pheenixm.explosivesplus.common.blocks;

import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.entities.EntityNovaBomb;

import net.minecraft.block.BlockPlanks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class NovaBomb extends ExplosivesBlock {

	public NovaBomb(String unlocalizedName, String registryName) {
		super(unlocalizedName, registryName);
	}

	@Override
	public void detonate(World world, BlockPos pos, IBlockState state,
			boolean shortfuse) {
		if (!world.isRemote) {
			if (((Boolean) state.getValue(EXPLODE)).booleanValue()) {
				world.setBlockToAir(pos);
				int fuse = 80;
				if(shortfuse) {
					fuse = 0;
				}
				EntityNovaBomb nova = new EntityNovaBomb(world, pos.getX(), pos.getY(), pos.getZ(), null);
				nova.setFuse(fuse);
				world.spawnEntity(nova);
			}
		}
	}
}
