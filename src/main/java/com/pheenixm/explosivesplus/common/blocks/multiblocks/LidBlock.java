package com.pheenixm.explosivesplus.common.blocks.multiblocks;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.tiles.TileEntityPluripotent;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class LidBlock extends AccessoryBlock {

	public LidBlock(String unlocalizedName, String registryName) {
		super(unlocalizedName, registryName, Material.IRON);
	}

	protected static final AxisAlignedBB AABB = new AxisAlignedBB(0.0D, 0.0D,
			0.0D, 1.0D, 0.5D, 1.0D);

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source,
			BlockPos pos) {
		return AABB;
	}

	@Nullable
	public AxisAlignedBB getCollisionBoundingBox(IBlockState state,
			IBlockAccess worldIn, BlockPos pos) {
		switch (state.getValue(MULTI_BLOCK)) {
			case INVALID :
				return AABB;
			// TODO: REMOVE THIS LATER
			case STRUCTURE :
				return NULL_AABB;
			default :
				return AABB;
		}
	}

	@Override
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state) {
		return false;
	}

	@Override
	protected TileEntity getNewTile(World world, IBlockState state) {
		return MultiblockDict.getTileForStructure(state.getValue(MULTI_TILE));
	}

}
