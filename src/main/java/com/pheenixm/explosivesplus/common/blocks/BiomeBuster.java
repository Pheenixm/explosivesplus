package com.pheenixm.explosivesplus.common.blocks;

import com.pheenixm.explosivesplus.common.entities.EntityBiomeBuster;
import com.pheenixm.explosivesplus.common.entities.EntityNovaBomb;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.explosions.ExplosionBiomeBuster;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class BiomeBuster extends ExplosivesBlock {

	public BiomeBuster(String unlocalizedName, String registryName) {
		super(unlocalizedName, registryName);
	}

	@Override
	public void detonate(World world, BlockPos pos, IBlockState state,
			boolean shortfuse) {
		if (!world.isRemote) {
			if (((Boolean) state.getValue(EXPLODE)).booleanValue()) {
				world.setBlockToAir(pos);
				int fuse = 80;
				if(shortfuse) {
					fuse = 0;
				}
				EntityBiomeBuster biome = new EntityBiomeBuster(world, pos.getX(), pos.getY(), pos.getZ(), null);
				biome.setFuse(fuse);
				world.spawnEntity(biome);

			}
		}
	}

}
