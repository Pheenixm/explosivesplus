package com.pheenixm.explosivesplus.common.blocks.multiblocks;


import net.minecraft.block.material.Material;

public abstract class AccessoryBlock extends MultiblockComponent {
	
	public AccessoryBlock(String unlocalizedName, String registryName, Material mat) {
		super(unlocalizedName, registryName, mat);
	}

}
