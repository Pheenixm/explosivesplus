package com.pheenixm.explosivesplus.common.blocks;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.coms.INetworkComponent;
import com.pheenixm.explosivesplus.api.coms.INetworkedBlock;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.NetworkHelper;
import com.pheenixm.explosivesplus.common.coms.tiles.TileEntityCable;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.energy.CapabilityEnergy;

/**
 * Connection code courtesy of gigaherz
 * @author RobMontgomery
 *
 */
public class ComsCable extends Block implements INetworkedBlock
{

    public static final PropertyBool NORTH = PropertyBool.create("north");
    public static final PropertyBool SOUTH = PropertyBool.create("south");
    public static final PropertyBool WEST = PropertyBool.create("west");
    public static final PropertyBool EAST = PropertyBool.create("east");
    public static final PropertyBool UP = PropertyBool.create("up");
    public static final PropertyBool DOWN = PropertyBool.create("down");

    
    //TODO: Update these values
    private static final AxisAlignedBB BOUNDS = new AxisAlignedBB(5.75f / 16, 5.75f / 16, 5.75f / 16, 10.25f / 16, 10.25f / 16, 10.25f / 16);
    private static final AxisAlignedBB BOUNDS_NORTH = new AxisAlignedBB(6 / 16f, 6 / 16f, 0 / 16f, 10 / 16f, 10 / 16f, 10 / 16f);
    private static final AxisAlignedBB BOUNDS_SOUTH = new AxisAlignedBB(6 / 16f, 6 / 16f, 6 / 16f, 10 / 16f, 10 / 16f, 16 / 16f);
    private static final AxisAlignedBB BOUNDS_EAST = new AxisAlignedBB(6 / 16f, 6 / 16f, 6 / 16f, 16 / 16f, 10 / 16f, 10 / 16f);
    private static final AxisAlignedBB BOUNDS_WEST = new AxisAlignedBB(0 / 16f, 6 / 16f, 6 / 16f, 10 / 16f, 10 / 16f, 10 / 16f);
    private static final AxisAlignedBB BOUNDS_UP = new AxisAlignedBB(6 / 16f, 6 / 16f, 6 / 16f, 10 / 16f, 16 / 16f, 10 / 16f);
    private static final AxisAlignedBB BOUNDS_DOWN = new AxisAlignedBB(6 / 16f, 0 / 16f, 6 / 16f, 10 / 16f, 10 / 16f, 10 / 16f);

	public ComsCable(String unlocalizedName) {
		super(Material.TNT);
		setTranslationKey(ExplosivesPlus.BASENAME + unlocalizedName);
		setRegistryName(unlocalizedName);
        setSoundType(SoundType.CLOTH);
		setCreativeTab(ExplosivesPlus.tab);
        setDefaultState(blockState.getBaseState()
                .withProperty(NORTH, false)
                .withProperty(SOUTH, false)
                .withProperty(WEST, false)
                .withProperty(EAST, false)
                .withProperty(UP, false)
                .withProperty(DOWN, false));
	}
	
	
    @Override
    public int getMetaFromState(IBlockState state)
    {
        return 0;
    }

    @Deprecated
    @Override
    public IBlockState getStateFromMeta(int meta)
    {
        return getDefaultState();
    }
    
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
    		if(!worldIn.isRemote) {
        System.out.println(this.getComponentCapability(worldIn, pos, facing).getSharedUid());
    		}
    		return false;
    }


    @Deprecated
    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos)
    {
        return state
                .withProperty(NORTH, isConnectable(worldIn, pos, EnumFacing.NORTH))
                .withProperty(SOUTH, isConnectable(worldIn, pos, EnumFacing.SOUTH))
                .withProperty(WEST, isConnectable(worldIn, pos, EnumFacing.WEST))
                .withProperty(EAST, isConnectable(worldIn, pos, EnumFacing.EAST))
                .withProperty(UP, isConnectable(worldIn, pos, EnumFacing.UP))
                .withProperty(DOWN, isConnectable(worldIn, pos, EnumFacing.DOWN));
    }
    
    @Deprecated
    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        state = state.getActualState(source, pos);

        AxisAlignedBB bb = BOUNDS;
        if (state.getValue(NORTH)) bb = bb.union(BOUNDS_NORTH);
        if (state.getValue(SOUTH)) bb = bb.union(BOUNDS_SOUTH);
        if (state.getValue(EAST)) bb = bb.union(BOUNDS_EAST);
        if (state.getValue(WEST)) bb = bb.union(BOUNDS_WEST);
        if (state.getValue(UP)) bb = bb.union(BOUNDS_UP);
        if (state.getValue(DOWN)) bb = bb.union(BOUNDS_DOWN);
        return bb;
    }

    @Deprecated
    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Deprecated
    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    
    private boolean isConnectable(IBlockAccess worldIn, BlockPos pos, EnumFacing facing)
    {
        TileEntity tile = worldIn.getTileEntity(pos.offset(facing));
        if(tile != null) {
        		return tile.hasCapability(CapabilityEnergy.ENERGY, facing) || NetworkHelper.hasNetworkCapability(tile, facing);
        } else {
        		return false;
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //					TILE ENTITY CODE
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state)
    {
        return new TileEntityCable();
    }
    
    
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		if (!worldIn.isRemote) {
	        INetworkComponent comp = this.getComponentCapability(worldIn, pos, null);
			if(comp != null) {
				comp.onComponentDestroyed(pos);
			}
		}
		super.breakBlock(worldIn, pos, state);
	}



    @Override
    public void onNeighborChange(IBlockAccess world, BlockPos pos, BlockPos neighbor)
    {
        super.onNeighborChange(world, pos, neighbor);
        INetworkComponent comp = this.getComponentCapability(world, pos, null);
        if(comp != null) {
        		comp.updateNeighbors(world, pos);
        }
    }
    
    @Deprecated
    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos)
    {
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos);
        INetworkComponent comp = this.getComponentCapability(worldIn, pos, null);
        if(comp != null) {
        		comp.updateNeighbors(worldIn, pos);
        }    
        ((TileEntityCable)this.getTileEntity(worldIn, pos)).onNeighborChange(fromPos);
    }

    
    @Override
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, NORTH, SOUTH, WEST, EAST, UP, DOWN);
    }
    
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
    		((TileEntityCable)this.getTileEntity(worldIn, pos)).onPlacedBy(worldIn, pos, state, placer, stack);
    }


}