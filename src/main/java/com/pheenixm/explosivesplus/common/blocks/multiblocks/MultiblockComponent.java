package com.pheenixm.explosivesplus.common.blocks.multiblocks;

import java.util.List;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntityC4;
import com.pheenixm.explosivesplus.common.tiles.TileEntityMultiblock;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class MultiblockComponent extends Block {
	
	
	//TODO: Add stuff to this
	public MultiblockComponent(String unlocalizedName, String registryName,
			Material mat) {
		super(mat);
		setTranslationKey(ExplosivesPlus.BASENAME + unlocalizedName);
		setRegistryName(registryName);
		this.setDefaultState(this.blockState.getBaseState()
				.withProperty(MULTI_BLOCK, MultiTypeEnum.INVALID)
				.withProperty(MULTI_TILE, MultiEnum.PLURIPOTENT));
		setCreativeTab(ExplosivesPlus.tab);
	}

    public static final PropertyEnum<MultiTypeEnum> MULTI_BLOCK = PropertyEnum.create("multi", MultiTypeEnum.class);
    public static final PropertyEnum<MultiEnum> MULTI_TILE = PropertyEnum.create("multi_tile", MultiEnum.class);

	protected abstract TileEntity getNewTile(World world, IBlockState state);

	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos,
			Block blockIn, BlockPos fromPos) {
		// Some code here to update neighbors?
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		if (!worldIn.isRemote) {
			getTileEntity(worldIn, pos).onBlockDestroyed(pos);
		}
		super.breakBlock(worldIn, pos, state);
	}
	
	@Override
    public void onPlayerDestroy(World worldIn, BlockPos pos, IBlockState state)
    {
    }
	
    public void onExplosionDestroy(World worldIn, BlockPos pos, Explosion explosionIn)
    {
    }



	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state,
			EntityLivingBase placer, ItemStack stack) {
			TileEntity tile = worldIn.getTileEntity(pos);
			if (tile instanceof TileEntityBase) {
				TileEntityBase base = (TileEntityBase) tile;
				base.onPlacedBy(worldIn, pos, state, placer, stack);
			}
	}

	@Override
	public boolean hasTileEntity(IBlockState state) {
		return true;
	}

	@Override
	public TileEntity createTileEntity(World world, IBlockState state) {
		return getNewTile(world, state);
	}

	@Nullable
	public TileEntityMultiblock getTileEntity(IBlockAccess world,
			BlockPos pos) {
		return (TileEntityMultiblock) world.getTileEntity(pos);
	}
	
    @Override
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess worldIn, BlockPos pos, EnumFacing side) {
        return true;
    }
    
    @Override
    public EnumBlockRenderType getRenderType(IBlockState state)
    {
    		switch (state.getValue(MULTI_BLOCK)) {
    			case INVALID:
    				return super.getRenderType(state);
    			case CENTER:
    				return EnumBlockRenderType.INVISIBLE;
    			case STRUCTURE:
    		        return EnumBlockRenderType.INVISIBLE;
    			case RENDER:
    				return EnumBlockRenderType.MODEL;
    		}
            return EnumBlockRenderType.INVISIBLE;
    }


    @Override
    public boolean isBlockNormalCube(IBlockState blockState) {
        return blockState.getValue(MULTI_BLOCK) == MultiTypeEnum.INVALID;
    }

    @Override
    public boolean isOpaqueCube(IBlockState blockState) {
        return blockState.getValue(MULTI_BLOCK) == MultiTypeEnum.INVALID;
    }
    
    @Override
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, MULTI_BLOCK, MULTI_TILE);
    }
    
    @Override
    public int getMetaFromState(IBlockState state)
    {
        return MultiblockDict.mapPropertiesToValue(state.getValue(MULTI_BLOCK), state.getValue(MULTI_TILE));
    }

    @Override
    public IBlockState getStateFromMeta(int meta)
    {
        return this.getDefaultState()
        		.withProperty(MULTI_BLOCK, MultiblockDict.mapValueToTypeProperty(meta))
        		.withProperty(MULTI_TILE, MultiblockDict.mapValueToMultiProperty(meta));
    }
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
		TileEntityMultiblock tile = (TileEntityMultiblock)getTileEntity(world, pos);
		return tile.onMultiTileActivated(tile, player, hand, side, hitX, hitY, hitZ);
	}
	
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn)
    {
		tooltip.add(TextFormatting.GOLD + I18n.format("gui.tooltip." + stack.getTranslationKey()));	
    }

    
    public enum MultiTypeEnum implements IStringSerializable {
        INVALID("invalid"),
        CENTER("center"),
        STRUCTURE("structure"),
        RENDER("render"),
        DYNAMIC_RENDER("dynamic_render");
    		
    		public static final int SIZE = 4;
        private final String name;

        MultiTypeEnum(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return name;
        }

        @Override
        public String getName()
        {
            return name;
        }
        
        public static MultiTypeEnum get(int meta)
        {
            return meta >= 0 && meta < values().length ? values()[meta] : INVALID;
        }


        public static MultiTypeEnum[] values = values();

    }
}
