package com.pheenixm.explosivesplus.common.blocks;

import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.entities.EntityMegaNuke;
import com.pheenixm.explosivesplus.common.entities.EntityTNTRain;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TNTRain extends ExplosivesBlock {

	public TNTRain(String unlocalizedName, String registryName) {
		super(unlocalizedName, registryName);
	}

	@Override
	public void detonate(World world, BlockPos pos, IBlockState state, boolean shortfuse) {
		if (!world.isRemote) {
			if (((Boolean) state.getValue(EXPLODE)).booleanValue()) {
				world.setBlockToAir(pos);
				int fuse = ConfigHandler.DEFAULT_FUSE;
				if(shortfuse) {
					fuse = 0;
				}
				EntityTNTRain rain = new EntityTNTRain(world, pos.getX(), pos.getY(), pos.getZ(), null);
				rain.setFuse(fuse);
				world.spawnEntity(rain);

			}
		}
	}

}
