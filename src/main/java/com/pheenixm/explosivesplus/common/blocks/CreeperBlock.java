package com.pheenixm.explosivesplus.common.blocks;

import java.util.List;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class CreeperBlock extends Block {


	public CreeperBlock(String unlocalizedName, String registryName) {
		super(Material.GRASS);
		setTranslationKey(ExplosivesPlus.BASENAME + unlocalizedName);
		setRegistryName(registryName);
		this.setDefaultState(this.blockState.getBaseState());
		this.setSoundType(SoundType.GROUND);
		setCreativeTab(ExplosivesPlus.tab);
	}
	
	@Override
    public boolean canEntitySpawn(IBlockState state, Entity entityIn)
    {
        return true;
    }

    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn)
    {
		tooltip.add(TextFormatting.GREEN + I18n.format("gui.tooltip." + stack.getTranslationKey()));	
    }

	
}
