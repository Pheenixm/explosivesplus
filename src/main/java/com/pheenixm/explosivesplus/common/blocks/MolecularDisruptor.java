package com.pheenixm.explosivesplus.common.blocks;

import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.entities.EntityMegaNuke;
import com.pheenixm.explosivesplus.common.entities.EntityMolecularDisruptor;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class MolecularDisruptor extends ExplosivesBlock {

	public MolecularDisruptor(String unlocalizedName, String registryName) {
		super(unlocalizedName, registryName);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void detonate(World world, BlockPos pos, IBlockState state, boolean shortfuse) {
		if (!world.isRemote) {
			if (((Boolean) state.getValue(EXPLODE)).booleanValue()) {
				world.setBlockToAir(pos);
				int fuse = ConfigHandler.MOLECULAR_DISRUPTOR_FUSE;
				if(shortfuse) {
					fuse = 0;
				}
				EntityMolecularDisruptor mega = new EntityMolecularDisruptor(world, pos.getX(), pos.getY(), pos.getZ(), null);
				mega.setFuse(fuse);
				world.spawnEntity(mega);

			}
		}
	}

}
