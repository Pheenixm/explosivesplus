package com.pheenixm.explosivesplus.common.blocks.multiblocks;

import com.pheenixm.explosivesplus.common.tiles.TileEntityCentrifuge;
import com.pheenixm.explosivesplus.common.tiles.TileEntityPluripotent;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockStructure;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class IOBlock extends StructuralBlock {

	public IOBlock(String unlocalizedName, String registryName) {
		super(unlocalizedName, registryName, Material.ROCK);
	}

	@Override
	protected TileEntity getNewTile(World world, IBlockState state) {
		return MultiblockDict.getTileForStructure(state.getValue(MULTI_TILE));
	}

}
