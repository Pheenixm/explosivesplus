package com.pheenixm.explosivesplus.common.blocks;

import com.pheenixm.explosivesplus.common.entities.EntityMegaNuke;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class MegaNuke extends ExplosivesBlock {

	public MegaNuke(String unlocalizedName, String registryName) {
		super(unlocalizedName, registryName);
	}

	@Override
	public void detonate(World world, BlockPos pos, IBlockState state,
			boolean shortfuse) {
		if (!world.isRemote) {
			if (((Boolean) state.getValue(EXPLODE)).booleanValue()) {
				world.setBlockToAir(pos);
				int fuse = 80;
				if(shortfuse) {
					fuse = 0;
				}
				EntityMegaNuke mega = new EntityMegaNuke(world, pos.getX(), pos.getY(), pos.getZ(), null);
				mega.setFuse(fuse);
				world.spawnEntity(mega);

			}
		}
	}

}
