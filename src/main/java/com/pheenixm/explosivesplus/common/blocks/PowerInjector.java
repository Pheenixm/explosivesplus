package com.pheenixm.explosivesplus.common.blocks;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.coms.INetworkComponent;
import com.pheenixm.explosivesplus.common.coms.tiles.TileEntityCable;
import com.pheenixm.explosivesplus.common.tiles.TileEntityBase;
import com.pheenixm.explosivesplus.common.tiles.TileEntityC4;
import com.pheenixm.explosivesplus.common.tiles.TileEntityInjector;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDirectional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityHopper;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PowerInjector extends BlockDirectional {

	public PowerInjector(String unlocalizedName) {
		super(Material.IRON);
		setTranslationKey(ExplosivesPlus.BASENAME + unlocalizedName);
		setRegistryName(unlocalizedName);
        setSoundType(SoundType.METAL);
		setCreativeTab(ExplosivesPlus.tab);
		setDefaultState(blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH));
	}
	
    @Override
    public void onNeighborChange(IBlockAccess world, BlockPos pos, BlockPos neighbor)
    {
        super.onNeighborChange(world, pos, neighbor);
        TileEntityInjector tent = getTileEntity(world, pos);
        if(tent != null) {
        		tent.neighborChanged(world.getBlockState(pos), world, pos, world.getBlockState(pos).getBlock(), null);
        }
    }
    
    @Deprecated
    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos)
    {
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos);
        if(!worldIn.isRemote) {
            TileEntityInjector tent = getTileEntity(worldIn, pos);
            if(tent != null) {
            		tent.neighborChanged(worldIn.getBlockState(pos), worldIn, pos, worldIn.getBlockState(pos).getBlock(), null);
            }
        }
    }
    
	@Override
	public IBlockState withRotation(IBlockState state, Rotation rot) {
		return state.withProperty(FACING,
				rot.rotate((EnumFacing) state.getValue(FACING)));
	}

	@Override
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state) {
		return false;
	}

    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileEntityInjector();
    }
	
	public TileEntityInjector getTileEntity(IBlockAccess world, BlockPos pos) {
		return (TileEntityInjector)world.getTileEntity(pos);
	}

	@Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }
	
	@Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		TileEntity tile = worldIn.getTileEntity(pos);
		if (tile instanceof TileEntityBase) {
			TileEntityBase base = (TileEntityBase) tile;
			base.onPlacedBy(worldIn, pos, state, placer, stack);
		}
    }

    @Override
    protected BlockStateContainer createBlockState()
    {
		return new BlockStateContainer(this, new IProperty[]{FACING});
    }
    
    public IBlockState getStateFromMeta(int meta)
    {
        IBlockState iblockstate = this.getDefaultState();
        iblockstate = iblockstate.withProperty(FACING, EnumFacing.byIndex(meta));
        return iblockstate;
    }

    /**
     * Convert the BlockState into the correct metadata value
     */
    public int getMetaFromState(IBlockState state)
    {
        return ((EnumFacing)state.getValue(FACING)).getIndex();
    }
    
    //TODO: Fix?
	public IBlockState getStateForPlacement(World worldIn, BlockPos pos,
			EnumFacing facing, float hitX, float hitY, float hitZ, int meta,
			EntityLivingBase placer) {
		if(placer.isSneaking()) {
			return this.getDefaultState().withProperty(FACING, facing);
		}
		return this.getDefaultState().withProperty(FACING, facing.getOpposite());
	}





}
