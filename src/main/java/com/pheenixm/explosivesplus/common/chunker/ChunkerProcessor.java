package com.pheenixm.explosivesplus.common.chunker;

import java.util.Random;

import com.pheenixm.explosivesplus.common.explosions.Explosion;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public final class ChunkerProcessor extends Chunker {

	public ChunkerProcessor(World world, EntityPlayer player, ChunkPos chunkPos, Explosion ex) {
		super(world, player, chunkPos, ex);	
	}

	/**
	 * @Override
	 * @return the input chunk again for final processing
	 */
	protected final Chunk processChunk(Chunk inputChunk) {
		world.captureBlockSnapshots = true;
		
		explosion.processBlocks(inputChunk);
		explosion.removeAffectedChunk(inputChunk);
		
		world.captureBlockSnapshots = false;
		return inputChunk;
	}
	
	World worldObj;
	
}