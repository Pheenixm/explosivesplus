package com.pheenixm.explosivesplus.common.chunker;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import com.pheenixm.explosivesplus.common.explosions.Explosion;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ClassInheritanceMultiMap;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

abstract public class Chunker extends Thread {

	protected final World world;
	protected final EntityPlayer player;
	protected final Chunk chunk;
	protected Chunk processedChunk;
	protected final Explosion explosion;
	protected final ChunkPos chunkPos; 
	
	public Chunker(World world, EntityPlayer player, ChunkPos chunkPos, Explosion ex) {
		this.world = world;
		this.player = player;
		this.chunk = world.getChunk(chunkPos.x, chunkPos.z);
		this.chunkPos = chunkPos;
		this.setPriority(ThreadChunkProcessor.executionPriority);
		explosion = ex;
	}

	protected abstract Chunk processChunk(Chunk inputChunk);
	
	
	public final void run() {
		long time = System.currentTimeMillis();
		if (processedChunk == null) {
			Chunk newChunk = new Chunk(world, chunk.x, chunk.z);
			newChunk.setStorageArrays(chunk.getBlockStorageArray());
			newChunk.setBiomeArray(chunk.getBiomeArray());
			
			Map<BlockPos, TileEntity> oldTileEntities = (Map<BlockPos, TileEntity>) ChunkInjector.reflectObserve(Chunk.class, chunk, "tileEntities");
			Map<BlockPos, TileEntity> tileEntities = (Map<BlockPos, TileEntity>) ChunkInjector.reflectObserve(Chunk.class, newChunk, "tileEntities");
			tileEntities = oldTileEntities;

			ClassInheritanceMultiMap<Entity>[] oldEntityLists = (ClassInheritanceMultiMap<Entity>[]) ChunkInjector.reflectObserve(Chunk.class, chunk, "entityLists");
			ClassInheritanceMultiMap<Entity>[] entityLists = (ClassInheritanceMultiMap<Entity>[]) ChunkInjector.reflectObserve(Chunk.class, newChunk, "entityLists");
			entityLists = oldEntityLists;

		
			newChunk.setHasEntities((Boolean) ChunkInjector.reflectObserve(Chunk.class, chunk, "hasEntities"));
			
			newChunk.setModified(true);
			
			newChunk.setTerrainPopulated(chunk.isTerrainPopulated());
			newChunk.setLastSaveTime((Long) ChunkInjector.reflectObserve(Chunk.class, chunk, "lastSaveTime"));
			newChunk.setStorageArrays(chunk.getBlockStorageArray());
			processedChunk = processChunk(newChunk);
			
			java.lang.reflect.Method method = null;
			if(newChunk.getWorld().isRemote) {
				try {
					method = Chunk.class.getMethod("generateHeightMap");
				} catch (NoSuchMethodException | SecurityException e1) {
					e1.printStackTrace();
				}
				try {
					method.invoke(processedChunk);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
				}
			}
			processedChunk.generateSkylightMap();	
		}
		
		InjectionContainer container = new InjectionContainer(processedChunk, explosion);
		
		setPriority(10);
		int tick = 0;
		while (!explosion.getController().injector.offerActionToStack(container)) {
			try {
				sleep(50);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
