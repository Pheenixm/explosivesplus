package com.pheenixm.explosivesplus.common.chunker;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.explosions.Explosion;

import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.ChunkProviderServer;
import net.minecraftforge.common.util.ChunkCoordComparator;

public final class ChunkInjector {

	private Explosion explosion;

	public static final <T> Object reflectObserve(Class<T> class1,
			Object instance, String... fieldNames) {
		for (String fieldName : fieldNames) {
			try {
				Field field = class1.getDeclaredField(fieldName);
				field.setAccessible(true);
				if (field != null)
					return field.get(instance);
			} catch (Exception e) {
			}
		}
		return null;
	}

	public final ArrayBlockingQueue<InjectionContainer> queue = new ArrayBlockingQueue<InjectionContainer>(
			50000, true);

	public ChunkInjector(Explosion exp) {
		explosion = exp;
	}

	
	/**
	 * This may need to be synchronized
	 * @param unpack
	 */
	public void injectChunk(InjectionContainer unpack) {
		Chunk newChunk = unpack.getChunk();
		// Explosion explosion = unpack.getExplosion();
		Long2ObjectMap<Chunk> chunkMap = null;
		WorldServer ws = (WorldServer) newChunk.getWorld();
		chunkMap = ws.getChunkProvider().loadedChunks;

		long key = ChunkPos.asLong(newChunk.x, newChunk.z);

		if (chunkMap.get(key) == null) {
			System.out.println("Force loaded a chunk");
			//chunkMap.put(key, newChunk);
		} else {

			try {
				chunkMap.replace(key, newChunk);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (queue.isEmpty() && ThreadChunk.threadProcessor.queue.isEmpty()
				&& explosion.chunkTracker.isEmpty()
				&& explosion.getController() != null
				&& explosion.getController().ticksExisted > 3) {
			explosion.getController().doPostExplosion();
		}
	}

	public void injectNext() {
		try {
			if(ThreadChunk.threadProcessor.queue.isEmpty()
					&& explosion.initialInjectionComplete) {
				while(!queue.isEmpty()) {
					injectChunk(queue.take());
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean offerActionToStack(InjectionContainer container) {
		return queue.offer(container);
	}
}
