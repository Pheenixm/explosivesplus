package com.pheenixm.explosivesplus.common.chunker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.pheenixm.explosivesplus.common.explosions.Explosion;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
/**
 * 
 * Credit to Genuine for helping with Chunk Injection
 *
 */
public final class ThreadChunkProcessor extends Thread {

	public static volatile int executionPriority = 3;
	//protected Explosion currentExplosion;

	public static final void setExecutionPriority(int priority) {
		if (priority < 1) {
			priority = 1;
		}
		if (priority > 10) {
			priority = 10;
		}
		executionPriority = priority;
	}

	private volatile boolean shutdown = false;
	public final BlockingQueue<Chunker> queue = new ArrayBlockingQueue<Chunker>(5000, true);
	//Changed to 32 from 16
	private final ExecutorService pool = Executors.newFixedThreadPool(32);

	public ThreadChunkProcessor() {
		this.setName("Thread-Safe Chunk Processor");
		this.setPriority(10);
	}

	public final void cleanShutdown() {
		clear();
		shutdown = true;
	}

	public final void clear() {
		queue.clear();
		pool.shutdown();
		//pool.awaitTermination(30, TimeUnit.SECONDS);
	}

	public final boolean offerActionToStack(Chunker chunker) {
		return queue.offer(chunker);
	}
	
	public final void doExplosion(World world, EntityPlayer player, Explosion explosion) {
		HashMap<ChunkPos, HashMap<BlockPos, ArrayList<BlockPos>>> affected = explosion.getAffectedChunksAndBlocks();
		HashMap<ChunkPos, ArrayList<BlockPos>> affectedByChunk = explosion.toChunkMaps(affected);
		for(ChunkPos cPos : affectedByChunk.keySet()) {
			this.offerActionToStack(new ChunkerProcessor(world, player, cPos, explosion));
		}
		explosion.initialInjectionComplete = true;
	}
	
	

	@Override
	public final void run() {
		while (!shutdown) {
			try {
				if (!queue.isEmpty()) {
					pool.execute(queue.take());
				} else {
					sleep(500L);
				}
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
