package com.pheenixm.explosivesplus.common.chunker;

import com.pheenixm.explosivesplus.common.explosions.Explosion;

import net.minecraft.world.chunk.Chunk;

public class InjectionContainer {

	private Explosion explosion;
	private Chunk chunk;
	
	public InjectionContainer(Chunk chunk, Explosion explosion) {
		this.chunk = chunk;
		this.explosion = explosion;
	}

	public Explosion getExplosion() {
		return explosion;
	}

	public void setExplosion(Explosion explosion) {
		this.explosion = explosion;
	}

	public Chunk getChunk() {
		return chunk;
	}

	public void setChunk(Chunk chunk) {
		this.chunk = chunk;
	}

}
