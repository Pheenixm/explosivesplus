package com.pheenixm.explosivesplus.common.chunker;


public final class ThreadChunk {

	public static final ThreadChunkProcessor threadProcessor = new ThreadChunkProcessor();
//	public static final ChunkInjector threadInjector = new ChunkInjector();

	public static final void cleanShutdown() {
		threadProcessor.cleanShutdown();
	}

	public static final void clear() {
		threadProcessor.clear();
	}

	public static final void start() {
		threadProcessor.start();
	}
}
