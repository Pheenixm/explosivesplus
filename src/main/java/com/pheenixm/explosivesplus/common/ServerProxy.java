package com.pheenixm.explosivesplus.common;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

import com.google.common.base.Predicates;
import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.blocks.CreeperBlock;
import com.pheenixm.explosivesplus.common.chunker.ThreadChunk;
import com.pheenixm.explosivesplus.common.entities.EntityBiomeBuster;
import com.pheenixm.explosivesplus.common.entities.EntityViewer;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.radar.CapabilityRadar;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntitySkull;
import net.minecraft.util.ClassInheritanceMultiMap;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.PlaySoundAtEntityEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.terraingen.DecorateBiomeEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@EventBusSubscriber(Side.SERVER)
public class ServerProxy implements CommonProxy {

	public static boolean isBusting = false;

	@Override
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		System.out.println("Server proxy called");
	}

	@Override
	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		// TODO Auto-generated method stub

	}
		
	@SubscribeEvent
	public static void decorateEvent(DecorateBiomeEvent.Decorate decorate) {
		if (isBusting) {
			decorate.setResult(Result.DENY);
			return;
		}
	}
	
	@SubscribeEvent
	public static void onChunkUnload(ChunkEvent.Unload event) {
		for(ClassInheritanceMultiMap<Entity> list : event.getChunk().getEntityLists()) {
			if(list.contains(new EntityViewer(event.getWorld()))) { 
				list.getByClass(EntityViewer.class).forEach(entity -> entity.setDead());
				
			}
		}
	}
	
	@SubscribeEvent
	public static void onEntityJoinWorld(EntityJoinWorldEvent event) {
		Entity entity = event.getEntity();
		BlockPos blockPos = new BlockPos(entity.posX, entity.getEntityBoundingBox().minY, entity.posZ);
		IBlockState blockState = entity.world.getBlockState(blockPos.down());
		if(blockState.getBlock() instanceof CreeperBlock
				&& (entity instanceof EntityLiving)
				&& !(entity instanceof EntityCreeper) 
				&& !(entity instanceof EntityPlayer)) {
			event.setCanceled(true);
			if(!entity.world.isRemote) {
				EntityCreeper creeper = new EntityCreeper(entity.getEntityWorld());
				creeper.setPosition(entity.posX, entity.posY, entity.posZ);
				entity.world.spawnEntity(creeper);
			}
		}
		CapabilityRadar.checkForCap(event);
	}

	@SubscribeEvent public static void onBlockPacedEvent(BlockEvent.PlaceEvent event) {
		if(event.getPlacedBlock().getBlock() == Blocks.SKULL && !event.getWorld().isRemote) {
			World world = event.getWorld();
			BlockPos pos = event.getPos();
			TileEntity te = world.getTileEntity(pos);
			if(te instanceof TileEntitySkull) {
				TileEntitySkull tile = (TileEntitySkull)te;
				if(tile.getSkullType() == 4) {
					BlockPos d1 = pos.offset(EnumFacing.DOWN);
					BlockPos d2 = pos.offset(EnumFacing.DOWN, 2);
					Block down1 = world.getBlockState(d1).getBlock();
					Block down2 = world.getBlockState(d2).getBlock();
					if(down1 == Blocks.TNT && down2 == Blocks.TNT) {
						world.setBlockToAir(pos);
						world.setBlockToAir(d1);
						world.setBlockToAir(d2);
						EntityCreeper creeper = new EntityCreeper(world);
						creeper.setPosition(d2.getX() + 0.5, d2.getY(), d2.getZ() + 0.5);
						creeper.enablePersistence();
						creeper.setCustomNameTag("Creeper");
						//creeper.setGlowing(true); Cool effect
						world.spawnEntity(creeper);
					}
					if(down1 == ExplosivesPlusHolder.MEGA && down2 == ExplosivesPlusHolder.MEGA) {
						//TODO: Add nuclear creeper
					}
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onLivingDropEvent(LivingDropsEvent event) {
		if(event.getEntityLiving() instanceof EntityCreeper) {
			EntityCreeper creep = (EntityCreeper)event.getEntityLiving();
			if(creep.getPowered() && !creep.getLastDamageSource().getDamageType().equals("explosion")) {
				creep.incrementDroppedSkulls();
				creep.entityDropItem(new ItemStack(Items.SKULL, 1, 4), 0.0F);
			}
		}
	}


	/*
	 * public void onPacketData(NetworkManager manager, Packet250CustomPayload
	 * packet, Player player){ DataInputStream dataStream = new
	 * DataInputStream(new ByteArrayInputStream(packet.data)); EntityPlayerMP
	 * entityPlayer = (EntityPlayerMP)player;
	 * 
	 * byte id = 0; int entityID = 0; boolean dead = false; boolean launch =
	 * false; boolean jump = false; float deltaX = 0; float deltaY = 0; float
	 * deltaZ = 0; int caliber = 0;
	 * 
	 * int x = 0; int y = 0; int z = 0; float power = 0;
	 * 
	 * try{ id = dataStream.readByte(); if(id == 1){ entityID =
	 * dataStream.readInt(); dead = dataStream.readBoolean(); deltaY =
	 * dataStream.readFloat(); if(Math.abs(deltaY) > 3){ deltaY = 0; } launch =
	 * dataStream.readBoolean(); caliber = dataStream.readInt(); } if(id == 2){
	 * entityID = dataStream.readInt(); deltaX = dataStream.readFloat();
	 * if(Math.abs(deltaX) > 3){ deltaX = 0; } deltaY = dataStream.readFloat();
	 * if(Math.abs(deltaY) > 3){ deltaY = 0; } deltaZ = dataStream.readFloat();
	 * if(Math.abs(deltaZ) > 3){ deltaZ = 0; } dead = dataStream.readBoolean();
	 * launch = dataStream.readBoolean(); } if(id == 3){ entityID =
	 * dataStream.readInt(); launch = dataStream.readBoolean(); dead =
	 * dataStream.readBoolean(); jump = dataStream.readBoolean(); } if(id == 5){
	 * dead = dataStream.readBoolean(); deltaY = dataStream.readFloat();
	 * if(deltaY > 10){ deltaY = 10; } } if(id == 6){ x = dataStream.readInt();
	 * y = dataStream.readInt(); z = dataStream.readInt(); power =
	 * dataStream.readFloat(); } if(id == 7){ x = dataStream.readInt(); y =
	 * dataStream.readInt(); z = dataStream.readInt(); } if(id == 8){ x =
	 * dataStream.readInt(); y = dataStream.readInt(); z = dataStream.readInt();
	 * deltaX = dataStream.readFloat(); deltaY = dataStream.readFloat(); deltaZ
	 * = dataStream.readFloat(); } if(id == 9){ deltaX = dataStream.readFloat();
	 * } }catch(IOException e){ FMLLog.log(Level.SEVERE, e,
	 * "ExplosivesPlus has a problem reading a packet"); } //TODO: fixes....
	 * if(id == 1){ ExplosivesPlusEntityAC130 entityAc = null; for(Object obj :
	 * entityPlayer.worldObj.loadedEntityList){ if(((Entity)obj).entityId ==
	 * entityID){ entityAc = (ExplosivesPlusEntityAC130)obj; break; } }
	 * if(entityAc != null){ entityAc.shouldStayAlive = dead; entityAc.posY +=
	 * deltaY; entityAc.rotationPitch = entityPlayer.rotationPitch;
	 * entityAc.rotationYaw = entityPlayer.rotationYaw; if(launch){
	 * ExplosivesPlusEntityProjectile entityShell = new
	 * ExplosivesPlusEntityProjectile(entityAc.worldObj,
	 * (EntityLiving)(Entity)entityAc, 3.5F); entityShell.caliber = caliber;
	 * entityAc.worldObj.spawnEntityInWorld(entityShell); if(caliber == 105){
	 * entityAc.worldObj.playSoundEffect(entityAc.posX, entityAc.posY,
	 * entityAc.posZ, "nuke.shot", 5F, 1F); }else if(caliber == 20 || caliber ==
	 * 40){ entityAc.worldObj.playSoundEffect(entityAc.posX, entityAc.posY,
	 * entityAc.posZ, "nuke.rapids", 5F, 1F); } } } } if(id == 2){
	 * ExplosivesPlusEntityMissileTargeting entity = null; for(Object obj :
	 * entityPlayer.worldObj.loadedEntityList){ if(((Entity)obj).entityId ==
	 * entityID){ entity = (ExplosivesPlusEntityMissileTargeting)obj; break; } }
	 * if(entity != null){ entity.posX += deltaX; entity.posY += deltaY;
	 * entity.posZ += deltaZ; if(dead){ entity.setDead(); } if(launch){
	 * entity.launch(); } } } if(id == 3){ ExplosivesPlusEntityMobController
	 * entity = null; for(Object obj : entityPlayer.worldObj.loadedEntityList){
	 * if(((Entity)obj).entityId == entityID){ entity =
	 * (ExplosivesPlusEntityMobController)obj; break; } } if(entity != null){
	 * entity.rotationPitch = entityPlayer.rotationPitch; entity.rotationYaw =
	 * entityPlayer.rotationYaw; entity.targetEntity.rotationYawHead =
	 * entityPlayer.rotationYawHead; entity.targetEntity.motionX =
	 * entityPlayer.motionX * 1.01; entity.targetEntity.motionZ =
	 * entityPlayer.motionZ * 1.01; if(entityPlayer.motionX == 0){
	 * entity.targetEntity.motionX = 0; } if(entityPlayer.motionZ == 0){
	 * entity.targetEntity.motionZ = 0; } entity.targetEntity.rotationYaw =
	 * entityPlayer.rotationYaw; entity.targetEntity.rotationPitch =
	 * entityPlayer.rotationPitch; if(dead){ entity.setDead(); } if(jump){
	 * entity.targetEntity.jump(); } entity.detonate = launch; } } if(id == 6){
	 * --missileNumber; if(missileNumber < 0){ missileNumber = 0; }
	 * createExplosion(entityPlayer.worldObj, entityPlayer, x, y, z, power); }
	 * if(id == 7){ ExplosivesPlusTileEntityGenesisBomb bomb =
	 * (ExplosivesPlusTileEntityGenesisBomb)((NetServerHandler)network.
	 * getNetHandler()).getPlayerEntity().worldObj.getBlockTileEntity(x, y, z);
	 * if(bomb != null){ bomb.initGenesis = true; bomb.time =
	 * (int)(float)ExplosivesPlusConfigHandler.values.get("genesisCount"); } }
	 * if(id == 8){ ExplosivesPlusTileEntityLaunchMissile entity =
	 * (ExplosivesPlusTileEntityLaunchMissile)((NetServerHandler)network.
	 * getNetHandler()).getPlayerEntity().worldObj.getBlockTileEntity(x, y, z);
	 * if(entity != null){ entity.launch(deltaX, deltaY, deltaZ); entity.phase =
	 * ExplosivesPlusConfigHandler.values.get("isPhase") != 0F; } } if(id == 9){
	 * ExplosivesPlusConfigHandler.values.put("isPhase", deltaX);
	 * updateAllPlayers(); } }
	 */
}
