package com.pheenixm.explosivesplus.common;

import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;

public enum EnumTier {

	TIER_1,
	TIER_2,
	TIER_3;
	
	public static int getTrackingRangeFromTier(EnumTier tier) {
		switch(tier) {
			case TIER_1: {
				return 256;
			}
			case TIER_2: {
				return 512;
			}
			case TIER_3: {
				return 1024;
			}
		}
		return -1;
	}
	
	public static int getGeneratorPowerPerTickFromTier(EnumTier tier) {
		switch(tier) {
			case TIER_1: {
				return 16;
			}
			case TIER_2: {
				return 64;
			}
			case TIER_3: {
				return 128;
			}
		}
		return -1;
	}
	
    public static EnumTier get(int meta)
    {
        return meta >= 0 && meta < values().length ? values()[meta] : TIER_1;
    }

	
}
