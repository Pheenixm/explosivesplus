package com.pheenixm.explosivesplus.common.radar.caps;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.radar.IDetectable;
import com.pheenixm.explosivesplus.api.radar.ITrajectory;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

/**
 * Empty class used as placeholder for component storage
 * @author RobMontgomery
 *
 */
public class StorageTrajectory implements IStorage<ITrajectory> {

	public StorageTrajectory() {
	}

	@Override
	public NBTBase writeNBT(Capability<ITrajectory> capability, ITrajectory instance, EnumFacing side) {
		NBTTagCompound tag = new NBTTagCompound();
		return tag;
	}

	@Override
	public void readNBT(Capability<ITrajectory> capability, ITrajectory instance, EnumFacing side, NBTBase nbt) {	
	}

}
