package com.pheenixm.explosivesplus.common.radar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.radar.IDetectable;
import com.pheenixm.explosivesplus.api.radar.IRadarStation;
import com.pheenixm.explosivesplus.common.EnumTier;
import com.pheenixm.explosivesplus.common.util.Utils;

import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

//TODO: Subscribe to an OnTick method[?]
@Deprecated
public class RadarRegistry {
/*
	public static Map<Integer, RadarRegistry> RADARS = new HashMap<Integer, RadarRegistry>();
	
	//TODO: Change to BlockPos? 
	public Map<BlockPos, EnumTier> stationMap = new ConcurrentHashMap<>();
	public Map<IDetectable, Boolean> trackingMap = new ConcurrentHashMap<>();
	private World world;
	
	public RadarRegistry(World world) {
		this.world = world;
		storage = RadarSavedData.get(world);
		RADARS.put(world.provider.getDimension(), this);
	}
	

	
	//TODO: Increase the range that is force-loaded
	private void loadChunks(BlockPos nextPos) {
		if(!world.isBlockLoaded(nextPos)) {
			//If chunk is not loaded, need to load it
			if(!world.isRemote) {
				ChunkPos chunkPos = new ChunkPos(nextPos);
				ExplosivesPlus.instance.forceChunkLoad(world, chunkPos);
				chunks.add(chunkPos);
			}
		}
	}
	
	private void unloadChunks() {
		if(!world.isRemote) {
			for(ChunkPos chun : chunks) {
				ExplosivesPlus.instance.releaseChunkLoad(world, chun);
			}
		}
	}

	@SubscribeEvent 
	public static void onWorldLoad(WorldEvent.Load event) {
		RadarRegistry registry = new RadarRegistry(event.getWorld());
	}

	@SubscribeEvent 
	public static void onWorldUnload(WorldEvent.Unload event) {
		RadarRegistry registry = RadarRegistry.getRegistry(event.getWorld());
		registry.saveStationsToDisk();
		//Garbage clearnup
		registry.stationMap.clear();
		registry.trackingMap.clear();

	}
	*/
}
