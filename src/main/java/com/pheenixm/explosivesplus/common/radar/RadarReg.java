package com.pheenixm.explosivesplus.common.radar;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.radar.IDetectable;
import com.pheenixm.explosivesplus.api.radar.IRadarStation;
import com.pheenixm.explosivesplus.common.EnumTier;
import com.pheenixm.explosivesplus.common.radar.RadarRegistry;
import com.pheenixm.explosivesplus.common.radar.RadarSavedData;
import com.pheenixm.explosivesplus.common.util.Utils;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.storage.MapStorage;
import net.minecraft.world.storage.WorldSavedData;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

//TODO: Must add refences from receiver to transmitter and vice-versa
public class RadarReg extends WorldSavedData {

	public Map<BlockPos, EnumTier> stationMap = new ConcurrentHashMap<>();
	public Map<IDetectable, Boolean> trackingMap = new ConcurrentHashMap<>();
	public static Map<Integer, RadarReg> RADARS = new HashMap<>();
	
	private World world;
	
	private static final String DATA_NAME = ExplosivesPlus.MOD_ID + "_RADAR_Data";

	
	public RadarReg(String name) {
		super(name);
	}

	public static void load(World world) {
		if(world == null) {
			System.out.println("How was this called with a null world?");
		}
		int dim = world.provider.getDimension();
		MapStorage storage = world.getPerWorldStorage();
		RadarReg instance = (RadarReg) storage.getOrLoadData(RadarReg.class, DATA_NAME);
		if (instance == null) {
			instance = new RadarReg(DATA_NAME);
			storage.setData(DATA_NAME, instance);
		}
		instance.world = world;
		if(!RADARS.containsKey(dim)) {
			RADARS.put(dim, instance);
		}
		//Can iterate through list here if you so desire
		System.out.println("Created radar registry");
	}
	
	//TODO: Modify this
	public static RadarReg getRegistry(World world) {
		int dim = world.provider.getDimension();
		if(RADARS.containsKey(dim)) {
			return RADARS.get(dim);
		}
		MapStorage storage = world.getPerWorldStorage();
		RadarReg instance = (RadarReg) storage.getOrLoadData(RadarReg.class, DATA_NAME);
		if (instance == null) {
			instance = new RadarReg(DATA_NAME);
			storage.setData(DATA_NAME, instance);
			instance.world = world;
		}
		RADARS.put(dim, instance);
		return instance;
	}
			
	public boolean addStation(BlockPos station, EnumTier tier) {
		return stationMap.put(station, tier) == null;
	}
	
	public boolean addThreat(IDetectable threat) {
		return trackingMap.put(threat, true);
	}
	
	public boolean removeStation(BlockPos station) {
		return stationMap.remove(station) != null;
	}
	
	public boolean removeThreat(IDetectable threat) {
		return trackingMap.remove(threat);
	}
	
	public HashSet<IRadarStation> getStationsForMissile(Vec3d startPos, Vec3d endPos) {
		HashSet<IRadarStation> stations = new HashSet<IRadarStation>();
		Iterator<BlockPos> stats = stationMap.keySet().iterator();
		while(stats.hasNext()) {
			BlockPos stationPos = stats.next();
			EnumTier tier = stationMap.get(stationPos);
			int range = EnumTier.getTrackingRangeFromTier(tier);
			if(Utils.doesInterceptExist(startPos, endPos, new Vec3d(stationPos), range)) {
				IRadarStation station = getStationFromPos(stationPos);
				if(!stations.contains(station)) {
					stations.add(station);
				}
			}
		}
		return stations;
	}
	
	

	//Need to call on iTick
	//Maybe kill this method, since it is unnecessarily intensive (?)
	//Method may be useful for normal entities
	public void updateTick() {
		//GAME LOGIC HERE
		for(IDetectable threat : trackingMap.keySet()) {
			
			for(BlockPos pos : stationMap.keySet()) {
				EnumTier stationTier = stationMap.get(pos);
				double distance = new Vec3d(pos).distanceTo(threat.getPos());
				if(distance <= EnumTier.getTrackingRangeFromTier(stationTier)) {
					//Tracked object is in range

					//TODO: IMPLEMENT
						//Handle IFFs here
					IRadarStation station = getStationFromPos(pos);
					if(station.getSharedUid() != threat.getSharedUid()) {
						//The station and the threat have different groups
						//Need to activate the station to track this threat
					}
					
				}
			}
		}
	}
	
	//Will likely need a capability for this
	public void saveStationsToDisk() {
		markDirty();
	}
		
	public void sendInfoToClient() {}
	
	@Nullable
	private IRadarStation getStationFromPos(BlockPos pos) {
		if(!world.isAirBlock(pos)) {
			//Dealing with tile entity or block, likely
			TileEntity tile = world.getTileEntity(pos);
			if(tile != null) {
				//Tile entity
				if(tile instanceof IRadarStation) {
					IRadarStation stat = (IRadarStation)tile;
					return stat;
				}
			} else {
				//Block?
				if(world.getBlockState(pos).getBlock() instanceof IRadarStation) {
					//No, I'm not going to implement this
					//There's edge cases, and then there's stupidity
				}
			}
		} else {
			List<Entity> entities = world.getEntitiesWithinAABBExcludingEntity(null, new AxisAlignedBB(pos, pos.add(1, 1, 1)));
			for(Entity ent : entities) {
				if(ent instanceof IRadarStation) {
					IRadarStation unlikely = (IRadarStation)ent;
					return unlikely;
				}
			}
		}
		return null;
	}
	
	
		
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		NBTTagList list = nbt.getTagList("station_list", Constants.NBT.TAG_COMPOUND);
		for(NBTBase uncast : list) {
			NBTTagCompound tag = (NBTTagCompound)uncast;
			BlockPos pos = new BlockPos(Utils.arrayToVec3i(tag.getIntArray("pos")));
			int tier = tag.getInteger("tier");
			getRegistry(world).addStation(pos, EnumTier.get(tier));
		}
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		NBTTagList list = new NBTTagList();
		for(BlockPos pos : getRegistry(world).stationMap.keySet())	{
			NBTTagCompound tag = new NBTTagCompound();
			tag.setInteger("tier", getRegistry(world).stationMap.get(pos).ordinal());
			tag.setIntArray("pos", Utils.vec3iToArray(pos));
			list.appendTag(tag);
		}
		compound.setTag("station_list", list);
		return compound;
	}
	
	
	@SubscribeEvent 
	public static void onWorldLoaded(WorldEvent.Load event) {
		World world = event.getWorld();
		if(world != null && !world.isRemote) {
			RadarReg.load(world);
		}
	}

	@SubscribeEvent 
	public static void onWorldUnload(WorldEvent.Unload event) {
		RadarReg registry = getRegistry(event.getWorld());
		registry.saveStationsToDisk();
		//Garbage clearnup
		registry.stationMap.clear();
		registry.trackingMap.clear();
	}





}
