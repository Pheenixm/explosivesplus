package com.pheenixm.explosivesplus.common.radar;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.radar.IDetectable;
import com.pheenixm.explosivesplus.api.radar.ITrajectory;
import com.pheenixm.explosivesplus.common.radar.caps.StorageDetectable;
import com.pheenixm.explosivesplus.common.radar.caps.StorageTrajectory;
import com.pheenixm.explosivesplus.common.radar.caps.CapabilityRadarDetectable;
import com.pheenixm.explosivesplus.common.radar.caps.CapabilityRadarTrajectory;

import com.pheenixm.explosivesplus.common.coms.caps.*;

import net.minecraft.entity.Entity;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = ExplosivesPlus.MOD_ID)
public class CapabilityRadar {

	@CapabilityInject(IDetectable.class)
    public static Capability<IDetectable> DETECTABLE = null;
		
	@CapabilityInject(ITrajectory.class)
    public static Capability<ITrajectory> TRAJECTORY = null;

	
    public static void register() {
    		CapabilityManager.INSTANCE.register(IDetectable.class, new StorageDetectable(), () -> new CapabilityRadarDetectable());
    		CapabilityManager.INSTANCE.register(ITrajectory.class, new StorageTrajectory(), () -> new CapabilityRadarTrajectory());
    }


    /**
     * We may be able to simplify this method. Will depend on how trajectory differs from detectable
     * @param event
     */
	public static void checkForCap(EntityJoinWorldEvent event) {
		Entity entity = event.getEntity();
		if(entity.hasCapability(DETECTABLE, null)) {
			//Entity is a vanilla entity with our capability attached, call the capability's join command
			entity.getCapability(DETECTABLE, null).addToRadarRegistry(entity.world);

		} else {
			if(entity.hasCapability(TRAJECTORY, null)) {
				//Entity is one our entities, call the cap's join command
				entity.getCapability(TRAJECTORY, null).addToRadarRegistry(entity.world);
			}
		}
	}
    
    
	
}
