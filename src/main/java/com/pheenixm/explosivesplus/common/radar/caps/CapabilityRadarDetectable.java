package com.pheenixm.explosivesplus.common.radar.caps;

import java.util.UUID;

import com.pheenixm.explosivesplus.api.coms.IEncoder;
import com.pheenixm.explosivesplus.api.radar.IDetectable;
import com.pheenixm.explosivesplus.common.EnumTier;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

public class CapabilityRadarDetectable implements IDetectable {

	protected World world;
	protected Entity entity;
	
	public CapabilityRadarDetectable() {}
	
	public CapabilityRadarDetectable(World world, Entity ent) {
		this.world = world;
		this.entity = ent;
	}

	@Override
	public int getId() {
		return entity.getEntityId();
	}

	@Override
	public EnumTier getThreatTier() {
		return EnumTier.TIER_2;
	}

	@Override
	public Vec3d getPos() {
		return entity.getPositionVector();
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NBTTagCompound serializeNBT() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public UUID getSharedUid() {
		return null;
	}

}
