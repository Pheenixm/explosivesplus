package com.pheenixm.explosivesplus.common.radar.caps;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.radar.IDetectable;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

/**
 * Empty class used as placeholder for component storage
 * @author RobMontgomery
 *
 */
public class StorageDetectable implements IStorage<IDetectable> {

	public StorageDetectable() {
	}

	@Override
	public NBTBase writeNBT(Capability<IDetectable> capability, IDetectable instance, EnumFacing side) {
		NBTTagCompound tag = new NBTTagCompound();
		return tag;
	}

	@Override
	public void readNBT(Capability<IDetectable> capability, IDetectable instance, EnumFacing side, NBTBase nbt) {	
	}

}
