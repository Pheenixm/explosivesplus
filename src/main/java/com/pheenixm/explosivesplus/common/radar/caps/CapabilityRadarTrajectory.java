package com.pheenixm.explosivesplus.common.radar.caps;

import java.util.UUID;

import com.pheenixm.explosivesplus.api.coms.IEncoder;
import com.pheenixm.explosivesplus.api.radar.IDetectable;
import com.pheenixm.explosivesplus.api.radar.ITrajectory;
import com.pheenixm.explosivesplus.common.EnumTier;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.radar.CapabilityRadar;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;

public class CapabilityRadarTrajectory implements ITrajectory {

	protected World world;
	
	public CapabilityRadarTrajectory() {}
	
	public CapabilityRadarTrajectory(World world) {
		this.world = world;
	}

	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound tag = new NBTTagCompound();
		return tag;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == CapabilityRadar.TRAJECTORY;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		return capability == CapabilityRadar.TRAJECTORY ? CapabilityRadar.TRAJECTORY.cast(this) : null;
	}

	
	//~~~~~~~~~~~~~~~~~~~~~~~~~
	// FOR CHILDREN
	//~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	@Override
	public int getId() {
		return 0;
	}


	@Override
	public EnumTier getThreatTier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vec3d getPos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UUID getSharedUid() {
		// TODO Auto-generated method stub
		return null;
	}


}
