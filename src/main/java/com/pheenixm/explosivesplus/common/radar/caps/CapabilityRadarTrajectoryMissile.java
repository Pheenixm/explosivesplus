package com.pheenixm.explosivesplus.common.radar.caps;

import java.util.UUID;

import com.pheenixm.explosivesplus.api.coms.IEncoder;
import com.pheenixm.explosivesplus.common.EnumTier;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.entities.missiles.FauxEntityBallisticMissile;
import com.pheenixm.explosivesplus.common.items.components.Engine;
import com.pheenixm.explosivesplus.common.items.components.Fuselage;
import com.pheenixm.explosivesplus.common.items.components.Engine.EngineTiers;
import com.pheenixm.explosivesplus.common.items.components.Fuselage.FuselageTiers;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class CapabilityRadarTrajectoryMissile extends CapabilityRadarTrajectory {
	
	protected FauxEntityBallisticMissile missile;
	
	public CapabilityRadarTrajectoryMissile(World world) {
		super(world);
	}

	@Override
	public int getId() {
		return missile.entityId;
	}


	@Override
	/**
	 * TODO: CAREFULLY construct this formula to ensure proper balance
	 */
	public EnumTier getThreatTier() {
		EngineTiers engineTier = ((Engine) missile.engine.getItem()).getTier();
		FuselageTiers fuseTier = ((Fuselage) missile.fuselage.getItem()).getTier();
		return EnumTier.get(engineTier.ordinal() + fuseTier.ordinal() / 2);
	}

	@Override
	public Vec3d getPos() {
		double[] pos = missile.getPos();
		return new Vec3d(pos[0], pos[1], pos[2]);
	}

	@Override
	public UUID getSharedUid() {
		BlockPos origin = missile.origin;
		TileEntity tile = world.getTileEntity(origin);
		if(tile != null && tile.hasCapability(CapabilityNetwork.ENDPOINT, null)) {
			return tile.getCapability(CapabilityNetwork.ENDPOINT, null).getSharedUid();
		}
		return null;
	}

}
