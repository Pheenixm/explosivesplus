package com.pheenixm.explosivesplus.common.entities;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.MoverType;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;

public abstract class EntityFusedBomb extends Entity implements IEntityAdditionalSpawnData
{
    private static final DataParameter<Integer> FUSE = EntityDataManager.<Integer>createKey(EntityFusedBomb.class, DataSerializers.VARINT);
    @Nullable
	protected Entity bombPlacedBy;
    public IBlockState parentBlock; //used for rendering
    
   
    /** How long the fuse is */
    private int fuse;
    protected boolean useContactFuse;

    //TODO configure length of fuse
    public EntityFusedBomb(World world)
    {
        super(world);
        this.fuse = ConfigHandler.DEFAULT_FUSE;
        this.preventEntitySpawning = true;
        this.isImmuneToFire = true;
        this.setSize(0.98F, 0.98F);
    }

    public EntityFusedBomb(World world, double x, double y, double z, Entity igniter)
    {
        this(world);
        this.setPosition(x + 0.5D, y, z + 0.5D);
        /* Makes bombs unnecessarily move
        float f = (float)(Math.random() * (Math.PI * 2D));
        this.motionX = (double)(-((float)Math.sin((double)f)) * 0.02F);
        this.motionY = 0.20000000298023224D;
        this.motionZ = (double)(-((float)Math.cos((double)f)) * 0.02F);
        */
        this.setFuse(80);
        this.prevPosX = x;
        this.prevPosY = y;
        this.prevPosZ = z;
        this.bombPlacedBy = igniter;
    }

    protected void entityInit()
    {
        this.dataManager.register(FUSE, Integer.valueOf(80));
    }

    /**
     * returns if this entity triggers Block.onEntityWalking on the blocks they walk on. used for spiders and wolves to
     * prevent them from trampling crops
     */
    protected boolean canTriggerWalking()
    {
        return false;
    }

    /**
     * Returns true if other Entities should be prevented from moving through this Entity.
     */
    public boolean canBeCollidedWith()
    {
        return !this.isDead;
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;

        if (!this.hasNoGravity())
        {
            this.motionY -= 0.03999999910593033D;
        }

        this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);
        this.motionX *= 0.9800000190734863D;
        this.motionY *= 0.9800000190734863D;
        this.motionZ *= 0.9800000190734863D;

        if (this.onGround)
        {
            this.motionX *= 0.699999988079071D;
            this.motionZ *= 0.699999988079071D;
            this.motionY *= -0.5D;
            if(this.useContactFuse) {

                if (!this.world.isRemote)
                {
                    this.detonate();
                }
                this.setDead();
            }
        }

        --this.fuse;

        if (this.fuse <= 0)
        {

            if (!this.world.isRemote)
            {
                this.detonate();
            }
            this.setDead();

        }
        else
        {
            this.handleWaterMovement();
        }
        
		this.playEffects();
    }

    public abstract void detonate();

    public abstract void playEffects();
    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    @Override
    protected void writeEntityToNBT(NBTTagCompound compound)
    {
        compound.setShort("Fuse", (short)this.getFuse());
        compound.setInteger("explosive", Block.getStateId(parentBlock));
        compound.setBoolean("contact", useContactFuse);
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    @Override
    protected void readEntityFromNBT(NBTTagCompound compound)
    {
        this.setFuse(compound.getShort("Fuse"));
        parentBlock = Block.getStateById(compound.getInteger("explosive"));
        this.useContactFuse = compound.getBoolean("contact");
    }
    
    @Override
    public void writeSpawnData(ByteBuf data)
    {
        data.writeInt(Block.getStateId(parentBlock));
        data.writeInt(this.getFuse());

    }

    @Override
    public void readSpawnData(ByteBuf data)
    {
        parentBlock = Block.getStateById(data.readInt());
        this.fuse = data.readInt();
    }


    /**
     * returns null or the entityliving it was placed or ignited by
     */
    @Nullable
    public Entity getBombPlacedBy()
    {
        return this.bombPlacedBy;
    }

    public float getEyeHeight()
    {
        return 0.0F;
    }

    public void setFuse(int fuseIn)
    {
        this.dataManager.set(FUSE, Integer.valueOf(fuseIn));
        this.fuse = fuseIn;
    }

    public void notifyDataManagerChange(DataParameter<?> key)
    {
        if (FUSE.equals(key))
        {
            this.fuse = this.getFuseDataManager();
        }
    }

    /**
     * Gets the fuse from the data manager
     */
    public int getFuseDataManager()
    {
        return ((Integer)this.dataManager.get(FUSE)).intValue();
    }

    public int getFuse()
    {
        return this.fuse;
    }

	public boolean isContactFuse() {
		return useContactFuse;
	}

	public void setContactFuse(boolean useContactFuse) {
		this.useContactFuse = useContactFuse;
	}
}