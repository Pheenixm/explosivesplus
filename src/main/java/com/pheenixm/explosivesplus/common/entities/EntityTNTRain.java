package com.pheenixm.explosivesplus.common.entities;

import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.explosions.ExplosionTNTRain;

import net.minecraft.entity.Entity;
import net.minecraft.world.World;

public class EntityTNTRain extends EntityFusedBomb {

	public EntityTNTRain(World world, double x, double y, double z, Entity igniter) {
		super(world, x, y, z, igniter);
		this.parentBlock = ExplosivesPlusHolder.RAIN.getDefaultState().withProperty(ExplosivesBlock.EXPLODE, false);
	}
	
	public EntityTNTRain(World world) {
		super(world);
	}

	@Override
	public void detonate() {
		ExplosionTNTRain explosion = new ExplosionTNTRain(world, bombPlacedBy, posX, ConfigHandler.TNT_RAIN_HEIGHT, posZ, ConfigHandler.TNT_RAIN_LAYERS);
		explosion.initiate(parentBlock, false);
	}

	@Override
	public void playEffects() {
		// TODO Auto-generated method stub
		
	}

}
