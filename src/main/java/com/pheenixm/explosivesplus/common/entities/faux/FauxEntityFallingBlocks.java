package com.pheenixm.explosivesplus.common.entities.faux;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
//
public class FauxEntityFallingBlocks extends FauxEntity {

	public FauxEntityFallingBlocks(World world, double x, double y,
			double z, IBlockState state) {
		super(world, x, y, z);
		this.world = world;
		posX = x;
		posY = y;
		posZ = z;
		block = state;
		set = false;
		ticksExisted = 0;
		canLand = false;
		scale = 1F;
		decayMotion = true;
		hasGravity = true;
		inBlock = false;
	}
	
	public FauxEntityFallingBlocks(World world, BlockPos pos, IBlockState state) {
		this(world, pos.getX(), pos.getY(), pos.getZ(), state);
	}
	
	public static FauxEntityFallingBlocks getFallingBlockFromNBT(World world, NBTTagCompound compound) {
		FauxEntityFallingBlocks block = new FauxEntityFallingBlocks(
				world, compound.getDouble("posX"), compound.getDouble("posY"),
				compound.getDouble("posZ"),
				Block.getStateById(compound.getInteger("blockId")));
		block.motionX = compound.getDouble("motionX");
		block.motionY = compound.getDouble("motionY");
		block.motionZ = compound.getDouble("motionZ");
		block.prevPosX = compound.getDouble("pposX");
		block.prevPosY = compound.getDouble("pposY");
		block.prevPosZ = compound.getDouble("pposZ");
		block.inBlock = compound.getBoolean("inBlock");

		block.ticksExisted = compound.getInteger("ticksExisted");
		block.canLand = compound.getBoolean("canLand");
		
		block.scale = compound.getFloat("scale");
		block.decayMotion = compound.getBoolean("decayMotion");
		block.isDead = compound.getBoolean("isDead");

		return block;
	}

	public static FauxEntityFallingBlocks updateExistingBlockFromNBT(FauxEntityFallingBlocks block, NBTTagCompound compound) {
		
		block.posX = compound.getDouble("posX");
		block.posY = compound.getDouble("posY");
		block.posZ = compound.getDouble("posZ");
		block.prevPosX = compound.getDouble("pposX");
		block.prevPosY = compound.getDouble("pposY");
		block.prevPosZ = compound.getDouble("pposZ");
		block.inBlock = compound.getBoolean("inBlock");
		
		block.scale = compound.getFloat("scale");
		block.decayMotion = compound.getBoolean("decayMotion");

		block.ticksExisted = compound.getInteger("ticksExisted");
		block.canLand = compound.getBoolean("canLand");
		block.isDead = compound.getBoolean("isDead");
		return block;
	}
	
	public static NBTTagCompound getNBTFromFallingBlock(FauxEntityFallingBlocks block) {
		NBTTagCompound tag = new NBTTagCompound();
		tag.setDouble("posX", block.posX);
		tag.setDouble("posY", block.posY);
		tag.setDouble("posZ", block.posZ);
		tag.setInteger("blockId", Block.getStateId(block.block));
		tag.setBoolean("inBlock", block.inBlock);
		
		tag.setDouble("pposX", block.prevPosX);
		tag.setDouble("pposY", block.prevPosY);
		tag.setDouble("pposZ", block.prevPosZ);

		tag.setDouble("motionX", block.motionX);
		tag.setDouble("motionY", block.motionY);
		tag.setDouble("motionZ", block.motionZ);

		tag.setFloat("scale", block.scale);
		tag.setBoolean("decayMotion", block.decayMotion);
		tag.setInteger("ticksExisted", block.ticksExisted);
		tag.setBoolean("canLand", block.canLand);
		tag.setBoolean("isDead", block.isDead);
		return tag;
	}


	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;
		
		if(hasGravity) {
			motionY -= 0.039999999105930328D;
		}
		
		if(decayMotion) {
			motionX *= 0.98000001907348633D;
			motionY *= 0.98000001907348633D;
			motionZ *= 0.98000001907348633D;
		}

		posX += motionX;
		posY += motionY;
		posZ += motionZ;

		if (posY < -100) {
			isDead = true;
		}

		int i = (int) Math.floor(posX);
		int j = (int) Math.floor(posY);
		int k = (int) Math.floor(posZ);
		BlockPos blockposBelow = new BlockPos(i, j - 1, k);
		if(world.isAirBlock(blockposBelow)) {
			inBlock = false;
		} else {
			inBlock = true;
		}
		
		if (canLand && inBlock) {
			isDead = true;
			world.setBlockState(new BlockPos(i,j,k), block, 3);
		}

		ticksExisted++;
	}
	
	@Override 
	public boolean equals(Object o) {
		if(o instanceof FauxEntityFallingBlocks) {
			FauxEntityFallingBlocks block = (FauxEntityFallingBlocks)o;
			if(this.posX == block.posX && this.posY == block.posY && this.posZ == block.posZ) {
				return true;
			}
		}
		return false;
	}
	
	@Override public int hashCode() {
		return (int) (posX * posY * posZ);
	}

	public IBlockState block;
	int meta;
	public boolean set;
	public boolean canLand;
	public float scale;
	public boolean decayMotion;
	public boolean hasGravity;
    public boolean inBlock;
	@Override
	public NBTTagCompound getUpdateTag() {
		return getNBTFromFallingBlock(this);
	}

	@Override
	public void handleUpdateTag(NBTTagCompound tag) {
		FauxEntityFallingBlocks.updateExistingBlockFromNBT(this, tag);
	}

}
