package com.pheenixm.explosivesplus.common.entities.missiles;

import javax.annotation.Nullable;

import org.lwjgl.opengl.GL11;

import com.pheenixm.explosivesplus.common.entities.controllers.EntityExplosionController;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntity;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntitySpawnHandler.EnumFauxEntity;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;
import com.pheenixm.explosivesplus.common.items.components.Engine;
import com.pheenixm.explosivesplus.common.items.components.Fuselage;
import com.pheenixm.explosivesplus.common.items.components.ItemComponent;
import com.pheenixm.explosivesplus.common.items.components.Warhead;
import com.pheenixm.explosivesplus.common.entities.faux.FauxTickingEntity;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class FauxEntityBallisticMissile extends FauxTickingEntity {
	
	private static final float HEIGHT = 18.2F;
	private static final float WIDTH = 1.7F;

	// Give values even when NBT not initialized
	public ItemStack warhead = ItemStack.EMPTY;
	public ItemStack fuselage = ItemStack.EMPTY;
	public ItemStack engine = ItemStack.EMPTY;
	
	public ItemStack prevWarhead = ItemStack.EMPTY;
	public ItemStack prevFuselage = ItemStack.EMPTY;
	public ItemStack prevEngine = ItemStack.EMPTY;
	
	private MissileFlightHelper flight;
	public BlockPos origin;
	
	private Vec3d target;
	public float oldPitch;
	public float newYaw;
	public float newPitch;
	
	private int launchTimer;
	
	public boolean launched;
	public boolean isDetonating;
	public EntityExplosionController control;

	public FauxEntityBallisticMissile(World world, TileEntitySilo silo, Vec3d targ) {
		this(world);
		//TODO: Finish implementing
		BlockPos center = silo.getMaster().core.centerPos;
		setPosition(center.getX(), center.getY() + 1 + HEIGHT / 2, center.getZ());
		target = targ;
		prevRotationPitch = rotationPitch = 90;
		flight = new MissileFlightHelper(target, world);
		origin = silo.getMasterPos();
	}
	
	public FauxEntityBallisticMissile(World world) {
		super(world);
	}

	@Override
	public void onUpdate() {
		//System.out.println(world.provider.getDimension());
		if(isDetonating) {
			control.onUpdate();
			control.ticksExisted++;
			if(control.isDead) {
				control = null;
				this.setDead();
			}
		} else {
		
			if(flight == null) {
				return;
			}
			if(flight.missile == null) {
				flight.missile = this;
			}
			ticksExisted++;
			if(launched) {
				System.out.println(posX + " " + posY + " " + posZ	);
				if(flight.world == null) {
					flight.world = world;
				}
				launchTimer++;
				if (launchTimer <= 1) {
					motionY = 0;
					//TODO: We are initializing the capability of the missile here, note well that it works
					//TODO: IMPLEMENT CAPABILITIES IN FAUX ENTITIES
				} 
				if (launchTimer < MissileFlightHelper.LAUNCH_TICKS + MissileFlightHelper.TURN_TICKS) {
					flight.target = target;
					flight.handleLaunch(launchTimer);
				} else {
					if (launchTimer == MissileFlightHelper.LAUNCH_TICKS + MissileFlightHelper.TURN_TICKS) {
						// Launch animation complete, initialize launch
						flight.target = target;
						flight.launch();
					}
					getHeading();
				}
				flight.checkDetonation(launchTimer);
				flight.moveMissile();
			}
		}
	}
	
	@Override
	public void doClientUpdate() {
		// TODO Auto-generated method stub
		
	}

	public void premature() {
		this.setDead();
		world.createExplosion(null, posX, posY, posZ, 15F, true);
	}
	
	public void detonate(boolean forced) {
		System.out.println("Detonated");
		control = ExplosionHelper.createExplosion(world, null, posX, posY, posZ, getExplosionType(), !forced);
		if(control != null && !world.isRemote) {
			isDetonating = true;
		} else {
			this.setDead();
		}
		
	}
	
	@Nullable
	public Explosions getExplosionType() {
		if (warhead.getItem() instanceof Warhead) {
			Warhead item = (Warhead) warhead.getItem();
			return item.getExplosive();
		}
		return null;
	}


	@Override
	public NBTTagCompound getUpdateTag() {
		boolean flag = true;
		NBTTagCompound tag = this.updateTagHelper(flag);
		if(flag || !ItemStack.areItemStacksEqual(warhead, prevWarhead)) {
			tag.setTag("warhead", warhead.serializeNBT());
			prevWarhead = warhead.copy();
		}
		if(flag || !ItemStack.areItemStacksEqual(fuselage, prevFuselage)) {
			tag.setTag("fuselage", fuselage.serializeNBT());
			prevFuselage = fuselage.copy();
		}
		if(flag || !ItemStack.areItemStacksEqual(engine, prevEngine)) {
			tag.setTag("engine", engine.serializeNBT());
			prevEngine = engine.copy();
		}
		

		return tag;
	}

	@Override
	public void handleUpdateTag(NBTTagCompound tag) {
		this.handleUpdateHelper(tag);
		
		if(tag.hasKey("warhead")) {
			warhead = new ItemStack((NBTTagCompound)tag.getTag("warhead"));
		}
		if(tag.hasKey("fuselage")) {
			fuselage = new ItemStack((NBTTagCompound)tag.getTag("fuselage"));
		}
		if(tag.hasKey("engine")) {
			engine = new ItemStack((NBTTagCompound)tag.getTag("engine"));
		}

	}

	@Override
	public int getTrackingDistance() {
		return 100000;
	}

	@Override
	public EnumFauxEntity getEntityType() {
		return EnumFauxEntity.BALLISTIC;
	}

	@Override
	public NBTTagCompound serialize() {
		NBTTagCompound tag = this.updateTagHelper(true);
		tag.setTag("flight", flight.serialize());

		tag.setTag("warhead", warhead.serializeNBT());
		tag.setTag("fuselage", fuselage.serializeNBT());
		tag.setTag("engine", engine.serializeNBT());
		tag.setBoolean("launched", launched);
		tag.setInteger("launchTimer", launchTimer);
		tag.setFloat("oldPitch", oldPitch);
		tag.setFloat("newYaw", newYaw);
		tag.setFloat("newPitch", newPitch);
		
		tag.setDouble("tarX", target.x);
		tag.setDouble("tarY", target.y);
		tag.setDouble("tarZ", target.z);
		
		tag.setInteger("originX", origin.getX());
		tag.setInteger("originY", origin.getY());
		tag.setInteger("originZ", origin.getZ());
		
		return tag;
	}

	@Override
	public void deserialize(NBTTagCompound tag) {
		this.handleUpdateHelper(tag);
		
		warhead = new ItemStack((NBTTagCompound)tag.getTag("warhead"));
		fuselage = new ItemStack((NBTTagCompound)tag.getTag("fuselage"));
		engine = new ItemStack((NBTTagCompound)tag.getTag("engine"));
		double x = tag.getDouble("tarX");
		double y = tag.getDouble("tarY");
		double z = tag.getDouble("tarZ");
		
		target = new Vec3d(x, y, z);
		if(flight == null) {
			flight = new MissileFlightHelper(target, world);
		}
		flight.deserialize((NBTTagCompound)tag.getTag("flight"));
		
		launched = tag.getBoolean("launched");
		launchTimer = tag.getInteger("launchTimer");
		oldPitch = tag.getFloat("oldPitch");
		newYaw = tag.getFloat("newYaw");
		newPitch = tag.getFloat("newPitch");
		
		//Restore the original position of the missile
		int i = tag.getInteger("originX");
		int j = tag.getInteger("originY");
		int k = tag.getInteger("originZ");
		origin = new BlockPos(i, j, k);

	}
	
	public void getHeading() {
		float f3 = MathHelper.sqrt(motionX * motionX + motionZ * motionZ);
		prevRotationYaw = rotationYaw;
		prevRotationPitch = rotationPitch;
		rotationYaw = (float) ((Math.atan2(motionX, motionZ) * 180D) / 3.1415927410125732D);
		rotationPitch = (float) ((Math.atan2(motionY, f3) * 180D) / 3.1415927410125732D);
	}

	
	@Override
	@SideOnly(Side.CLIENT)
	public void render(double x, double y, double z, float partialTicks) {
		
        float yaw = prevRotationYaw + (rotationYaw - prevRotationYaw) * partialTicks - 90;
        float pitch = prevRotationPitch + (rotationPitch - prevRotationPitch) * partialTicks - 90;  
        float offset = 0.5F;
        GlStateManager.translate((float) x, (float) y, (float) z);
        GlStateManager.translate(offset, offset, offset);
        GlStateManager.translate(0, HEIGHT/2, 0);
        GlStateManager.rotate(yaw, 0.0F, 1.0F, 0.0F);
        GlStateManager.rotate(pitch, 0.0F, 0.0F, 1.0F);
                
        
		if(warhead.getItem() instanceof Warhead) {
			renderItem(warhead,x,y,z, yaw, pitch);
		}
		if(fuselage.getItem() instanceof Fuselage) {
			renderItem(fuselage,x,y,z, yaw, pitch);
		}
		if(engine.getItem() instanceof Engine) {
			renderItem(engine,x,y,z, yaw, pitch);
		}

	}
	@SideOnly(Side.CLIENT)
	private void renderItem(ItemStack stack, double x, double y, double z, float yaw, float pitch) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(0,ItemComponent.getOffset(stack, stack.getItem() instanceof Warhead) - HEIGHT,0);
        GlStateManager.rotate(-67.5F, 0, 1, 0);
        GlStateManager.enableRescaleNormal();

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder worldrenderer = tessellator.getBuffer();
        worldrenderer.begin(7, DefaultVertexFormats.ITEM);
        worldrenderer.setTranslation(-x, -y, -z);
        worldrenderer.color(255, 255, 255, 255);
        for (BakedQuad bakedquad : RenderUtils.getBakedModel(stack).getQuads(null, null, 0))
        {
            worldrenderer.addVertexData(bakedquad.getVertexData());
        }
        worldrenderer.setTranslation(0, 0, 0);
        tessellator.draw();
		
        
        GlStateManager.disableRescaleNormal();
        GlStateManager.enableLighting();
        GlStateManager.popMatrix();


	}

	
	@Override
	public boolean shouldRemove() {
		return isDead;
	}


	@Override
	public double[] getPos() {
		double[] pos = {posX, posY, posZ};
		return pos;
	}
	
	@Override
	public double getRenderDistance() {
		return 256;
	}

	public ItemStack getWarhead() {
		return warhead;
	}

	public void setWarhead(ItemStack warhead) {
		if (warhead.getItem() instanceof Warhead || warhead.isEmpty()) {
			prevWarhead = this.warhead.copy();
			this.warhead = warhead;
			System.out.println(warhead.getDisplayName());
		} else {
			this.warhead = ItemStack.EMPTY;
		}
	}

	public ItemStack getFuselage() {
		return fuselage;
	}

	public void setFuselage(ItemStack fuselage) {
		if (fuselage.getItem() instanceof Fuselage || fuselage.isEmpty()) {
			prevFuselage = this.fuselage.copy();
			this.fuselage = fuselage;
			System.out.println(fuselage.getDisplayName());
		} else {
			this.fuselage = ItemStack.EMPTY;
		}
	}

	public ItemStack getEngine() {
		return engine;
	}

	public void setEngine(ItemStack engine) {
		if (engine.getItem() instanceof Engine || engine.isEmpty()) {
			prevEngine = this.engine.copy();
			this.engine = engine;
			System.out.println(engine.getDisplayName());
		} else {
			this.engine = ItemStack.EMPTY;
		}
	}
	
	public void updateTarget(int x, int z) {
		BlockPos posToCheck = new BlockPos(x, 0, z);
		if (!world.isBlockLoaded(posToCheck)) {
			//this.loadChunks(posToCheck);
		}
		int y = world.getHeight(x, z);
		target = new Vec3d(x, y, z);
	}
	
	public boolean canLaunch() {
		return warhead.getItem() instanceof Warhead && fuselage.getItem() instanceof Fuselage
				&& engine.getItem() instanceof Engine;
	}


	

}
