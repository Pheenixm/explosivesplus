package com.pheenixm.explosivesplus.common.entities;



import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.explosions.ExplosionBiomeBuster;
import com.pheenixm.explosivesplus.common.explosions.ExplosionNova;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntityNovaBomb extends EntityFusedBomb
{

    public EntityNovaBomb(World world, double x, double y, double z, Entity igniter) {
		super(world, x, y, z, igniter);
		this.parentBlock = ExplosivesPlusHolder.NOVA.getDefaultState().withProperty(ExplosivesBlock.EXPLODE, false);
	}
    
    public EntityNovaBomb(World world) {
    		super(world);
    }

	@Override
	public void detonate() {
		ExplosionNova explosion = new ExplosionNova(world,
				null, posX, posY, posZ, EntityNovaController.DEFAULT_RADIUS);
		explosion.initiate(parentBlock, false);
	}

	@Override
	public void playEffects() {
		
	}

}
