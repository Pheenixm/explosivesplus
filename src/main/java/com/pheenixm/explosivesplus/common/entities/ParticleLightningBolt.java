package com.pheenixm.explosivesplus.common.entities;

import java.util.Iterator;

import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

import com.pheenixm.explosivesplus.common.util.WRVector3;

public class ParticleLightningBolt extends Particle
{

	protected ParticleLightningBolt(World worldIn, double posXIn, double posYIn, double posZIn) {
		super(worldIn, posXIn, posYIn, posZIn);
		// TODO Auto-generated constructor stub
	}
/**TYPE 0 = LIGHT PURPLE
 * TYPE 1 = YELLOW
 * TYPE 2 = BLUE LIGHTNING
 * TYPE 3 = GREEN
 * TYPE 4 = RED
 * TYPE 5 = DARK PURPLE/BLACK
 */

	/*
 public ParticleLightningBolt(World world, WRVector3 jammervec, WRVector3 targetvec, long seed)
 {
     super(world, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
     main = new LightningBoltCommon(world, jammervec, targetvec, seed);
     setupFromMain();
 }


 public ParticleLightningBolt(World world, Entity detonator, Entity target, long seed)
 {
     super(world, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
     main = new LightningBoltCommon(world, detonator, target, seed);
     setupFromMain();
 }
 
 public ParticleLightningBolt(World world, Entity detonator, Entity target, long seed, int speed)
 {
     super(world, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
     main = new LightningBoltCommon(world, detonator, target, seed, speed);
     setupFromMain();
 }

 public ParticleLightningBolt(World world, BlockPos detonator, Entity target, long seed)
 {
     super(world, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
     main = new LightningBoltCommon(world, detonator, target, seed);
     setupFromMain();
 }
 
 public ParticleLightningBolt(World world, double x1, double y1, double z1, double x, double y, double z, long seed, int duration, float multi)
 {
     super(world, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
     main = new LightningBoltCommon(world, x1,y1,z1, x,y,z, seed, duration, multi);  
     setupFromMain();
 }
 
 public ParticleLightningBolt(World world, double x1, double y1, double z1, double x, double y, double z, long seed, int duration, float multi, int speed)
 {
     super(world, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
     main = new LightningBoltCommon(world, x1,y1,z1, x,y,z, seed, duration, multi, speed);  
     setupFromMain();
 }
 
 public ParticleLightningBolt(World world, double x1, double y1, double z1, double x, double y, double z, long seed, int duration)
 {
     super(world, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
     main = new LightningBoltCommon(world, x1,y1,z1, x,y,z, seed, duration, 1f);  
     setupFromMain();
 }
 
 public ParticleLightningBolt(World world, BlockPos detonator, double x, double y, double z, long seed)
 {
     super(world, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
     main = new LightningBoltCommon(world, detonator, x,y,z, seed);
     setupFromMain();
 }

 private void setupFromMain()
 {
     main.setWrapper(this);
     this.particleAge = main.particleMaxAge;
     this.setPosition(main.start.x, main.start.y, main.start.z);
 }

 public void defaultFractal()
 {
     main.defaultFractal();
 }
 
 

 public void fractal(int splits, float amount, float splitchance, float splitlength, float splitangle)
 {
     main.fractal(splits, amount, splitchance, splitlength, splitangle);
 }

 public void finalizeBolt()
 {
     main.finalizeBolt();
     ModLoader.getMinecraftInstance().effectRenderer.addEffect(this);
 }

 public void setWrapper(Particle entity)
 {
	 main.wrapper = entity; 
 }
 
 public void setType(int type) {
	 this.type=type;
	 main.type=type;
 }
 public void setDamage(int dmg) {
	 main.damage=dmg;
 }
 
 public void setNonLethal() {
	 main.nonLethal=true;
 }
 
 public void setMultiplier(float m) {
	 main.multiplier = m;
 }
 
 private int type=0;
 
 @Override
public void onUpdate() {
	
     main.onUpdate();
     if(main.particleAge >= main.particleMaxAge)
         this.setExpired();
 }

 private static WRVector3 getRelativeViewVector(WRVector3 pos)
 {
     EntityPlayer renderentity = Minecraft.getMinecraft().player;
     return new WRVector3((float)renderentity.posX - pos.x, (float)renderentity.posY - pos.y, (float)renderentity.posZ - pos.z);
     
 }

 private void renderBolt(Tessellator tessellator, float partialframe, float cosyaw, float cospitch, 
		 				 float sinyaw, float cossinpitch, int pass)
 {
     WRVector3 playervec = new WRVector3(sinyaw * -cospitch, -cossinpitch / cosyaw, cosyaw * cospitch);
     float boltage = main.particleAge >= 0 ? (float)main.particleAge / (float)main.particleMaxAge : 0.0F;
     float mainalpha = 1.0F;
     if(pass == 0)
         mainalpha = (1.0F - boltage) * 0.4F;
     else
         mainalpha = 1.0F - boltage * 0.5F;
     int renderlength = (int)(((main.particleAge + partialframe + (int)(main.length * 3F)) / (int)(main.length * 3F)) * main.numsegments0);
     for(Iterator iterator = main.segments.iterator(); iterator.hasNext();)
     {
         LightningBoltCommon.Segment rendersegment = (LightningBoltCommon.Segment)iterator.next();
         if(rendersegment.segmentno <= renderlength)
         {
             float width = 0.03F * (getRelativeViewVector(rendersegment.startpoint.point).length() / 5F + 1.0F) * (1.0F + rendersegment.light) * 0.5F;
             WRVector3 diff1 = WRVector3.crossProduct(playervec, rendersegment.prevdiff).scale(width / rendersegment.sinprev);
             WRVector3 diff2 = WRVector3.crossProduct(playervec, rendersegment.nextdiff).scale(width / rendersegment.sinnext);
             WRVector3 startvec = rendersegment.startpoint.point;
             WRVector3 endvec = rendersegment.endpoint.point;
             float rx1 = (float)(startvec.x - interpPosX);
             float ry1 = (float)(startvec.y - interpPosY);
             float rz1 = (float)(startvec.z - interpPosZ);
             float rx2 = (float)(endvec.x - interpPosX);
             float ry2 = (float)(endvec.y - interpPosY);
             float rz2 = (float)(endvec.z - interpPosZ);
             tessellator.
             tessellator.setColorRGBA_F(particleRed, particleGreen, particleBlue, mainalpha * rendersegment.light);
             tessellator.addVertexWithUV(rx2 - diff2.x, ry2 - diff2.y, rz2 - diff2.z, 0.5D, 0.0D);
             tessellator.addVertexWithUV(rx1 - diff1.x, ry1 - diff1.y, rz1 - diff1.z, 0.5D, 0.0D);
             tessellator.addVertexWithUV(rx1 + diff1.x, ry1 + diff1.y, rz1 + diff1.z, 0.5D, 1.0D);
             tessellator.addVertexWithUV(rx2 + diff2.x, ry2 + diff2.y, rz2 + diff2.z, 0.5D, 1.0D);
             if(rendersegment.next == null)
             {
                 WRVector3 roundend = rendersegment.endpoint.point.copy().add(rendersegment.diff.copy().normalize().scale(width));
                 float rx3 = (float)(roundend.x - interpPosX);
                 float ry3 = (float)(roundend.y - interpPosY);
                 float rz3 = (float)(roundend.z - interpPosZ);
                 tessellator.addVertexWithUV(rx3 - diff2.x, ry3 - diff2.y, rz3 - diff2.z, 0.0D, 0.0D);
                 tessellator.addVertexWithUV(rx2 - diff2.x, ry2 - diff2.y, rz2 - diff2.z, 0.5D, 0.0D);
                 tessellator.addVertexWithUV(rx2 + diff2.x, ry2 + diff2.y, rz2 + diff2.z, 0.5D, 1.0D);
                 tessellator.addVertexWithUV(rx3 + diff2.x, ry3 + diff2.y, rz3 + diff2.z, 0.0D, 1.0D);
             }
             if(rendersegment.prev == null)
             {
                 WRVector3 roundend = rendersegment.startpoint.point.copy().sub(rendersegment.diff.copy().normalize().scale(width));
                 float rx3 = (float)(roundend.x - interpPosX);
                 float ry3 = (float)(roundend.y - interpPosY);
                 float rz3 = (float)(roundend.z - interpPosZ);
                 tessellator.addVertexWithUV(rx1 - diff1.x, ry1 - diff1.y, rz1 - diff1.z, 0.5D, 0.0D);
                 tessellator.addVertexWithUV(rx3 - diff1.x, ry3 - diff1.y, rz3 - diff1.z, 0.0D, 0.0D);
                 tessellator.addVertexWithUV(rx3 + diff1.x, ry3 + diff1.y, rz3 + diff1.z, 0.0D, 1.0D);
                 tessellator.addVertexWithUV(rx1 + diff1.x, ry1 + diff1.y, rz1 + diff1.z, 0.5D, 1.0D);
             }
         }
     }

 }

 
 @Override
public void renderParticle(Tessellator tessellator, float partialframe, float cosyaw, float cospitch, float sinyaw, float sinsinpitch, float cossinpitch)
 {
	 EntityPlayer renderentity = ModLoader.getMinecraftInstance().thePlayer;
	 int visibleDistance = 100;
	 if (!ModLoader.getMinecraftInstance().gameSettings.fancyGraphics) visibleDistance=50; 
	 if (renderentity.getDistance(posX, posY, posZ)>visibleDistance) return;
	 
	 tessellator.draw();
	 GL11.glPushMatrix();
	 
     GL11.glDepthMask(false);
     GL11.glEnable(3042);
	 
	 particleRed = particleGreen = particleBlue = 1f;
	 
	 switch(type) {
		 case 0: particleRed = 0.6f; particleGreen = 0.3f; particleBlue = 0.6f; GL11.glBlendFunc(770, 1); break;
		 case 1: particleRed = 0.6f; particleGreen = 0.6f; particleBlue = 0.1f; GL11.glBlendFunc(770, 1); break;
		 case 2: particleRed = 0.1f; particleGreen = 0.1f; particleBlue = 0.6f; GL11.glBlendFunc(770, 1); break;
		 case 3: particleRed = 0.1f; particleGreen = 1f; particleBlue = 0.1f; GL11.glBlendFunc(770, 1); break;
		 case 4: particleRed = 0.6f; particleGreen = 0.1f; particleBlue = 0.1f; GL11.glBlendFunc(770, 1); break;
		 case 5: particleRed = 0.6f; particleGreen = 0.2f; particleBlue = 0.6f; GL11.glBlendFunc(770, 771); break;
	 }
     
     MinecraftForgeClient.bindTexture("/nuke/p_large.png"); 
     tessellator.startDrawingQuads();
     tessellator.setBrightness(0xf000f0);
     renderBolt(tessellator, partialframe, cosyaw, cospitch, sinyaw, cossinpitch, 0);
     tessellator.draw();
     
     switch(type) {
	     case 0: particleRed = 1f; particleGreen = 0.6f; particleBlue = 1f; break;
		 case 1: particleRed = 1f; particleGreen = 1f; particleBlue = 0.1f; break;
		 case 2: particleRed = 0.1f; particleGreen = 0.1f; particleBlue = 1f; break;
		 case 3: particleRed = 0.1f; particleGreen = 0.6f; particleBlue = 0.1f; break;
		 case 4: particleRed = 1f; particleGreen = 0.1f; particleBlue = 0.1f; break;
		 case 5: particleRed = 0f; particleGreen = 0f; particleBlue = 0f; GL11.glBlendFunc(770, 771);  break;
     }
     
	 MinecraftForgeClient.bindTexture("/nuke/p_small.png");
     tessellator.startDrawingQuads();
     tessellator.setBrightness(0xf000f0);
     renderBolt(tessellator, partialframe, cosyaw, cospitch, sinyaw, cossinpitch, 1);
     tessellator.draw();
     
     GL11.glDisable(3042);
     GL11.glDepthMask(true);
     GL11.glPopMatrix();
     
     GL11.glBindTexture(3553 /*GL_TEXTURE_2D*/ /* GL_TEXTURE_2D ,
    		 ModLoader.getMinecraftInstance().renderEngine.getTexture("/particles.png"));
     
     tessellator.startDrawingQuads();
 }

 public int getRenderPass()
 {
     return 2;
 }

 private LightningBoltCommon main;

*/

}
