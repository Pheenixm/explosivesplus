package com.pheenixm.explosivesplus.common.entities;

import com.pheenixm.explosivesplus.client.gui.GuiPredatorMissile;
import com.pheenixm.explosivesplus.common.GuiHandler;
import com.pheenixm.explosivesplus.common.entities.controllable.EntityPredatorMissile;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageFireControl;
import com.pheenixm.explosivesplus.common.tiles.multiblock.MultiblockDict.MultiEnum;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityTargeter extends EntityViewer implements IEntityAdditionalSpawnData {

	protected EnumTargetingType targetType;
	protected BlockPos target;
	protected Vec3d extraVector;
	public static final int Y_OFFSET = 88;
	
	
	@Deprecated
	private static final DataParameter<Boolean> FIRE = EntityDataManager
			.createKey(EntityTargeter.class, DataSerializers.BOOLEAN);
	@Deprecated
	private static final DataParameter<BlockPos> TARGET = EntityDataManager
			.createKey(EntityTargeter.class, DataSerializers.BLOCK_POS);

	private boolean shouldFire;
	
	
	public EntityTargeter(World world, double x, double y, double z, EntityPlayer player, EnumTargetingType type) {
		super(world, x, y, z, player, true);
		targetType = type;
		this.rotationYaw = 0;
		this.rotationPitch = 90;
	}

	public EntityTargeter(World world) {
		super(world);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onUpdate() {
		super.onUpdate();
		if(!world.isRemote) {
			if(shouldFire) {
				//Begin firing sequence
				fire();
				//Reset camera, just in case...
				
				this.resetCamera();
				this.setDead();
			}
		} else {
			if(shouldFire) {
				//this.resetCamera();
				this.setDead();
			}
		}
	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound compound) {
		
	}
	
	@Override
	protected void writeEntityToNBT(NBTTagCompound compound) {
		
	}
	
	@Override
	protected void entityInit() {
		super.entityInit();
		dataManager.register(FIRE, false);
		dataManager.register(TARGET, new BlockPos(0,0,0));
	}

	
	
	
	
	@Override
	public void writeSpawnData(ByteBuf buffer) {
		super.writeSpawnData(buffer);
		buffer.writeInt(targetType.ordinal());
	}

	@Override
	/**
	 * Automatically sets the viewport var to this entity for the player
	 * who spawned it
	 */
	public void readSpawnData(ByteBuf buffer) {
		super.readSpawnData(buffer);
		if(mc.player.getEntityId() == playerId) {
			targetType = EnumTargetingType.get(buffer.readInt());
		}
		if(world.isRemote) {
			GuiHandler.processTargetingGUI(targetType, this);
			initFramebuffer();
		}
	}
	
	public void transmitFireControl(BlockPos targetPos, Vec3d extraInfo) {
		this.setFire(true);
		this.target = targetPos;
		RenderUtils.isRenderingViewport = false;
		MessageFireControl message = new MessageFireControl(target, extraInfo, this);
		PacketHandler.NETWORK.sendToServer(message);
	}
	
	public void receiveFireControl(int x, int y, int z, double d1, double d2, double d3) {
		BlockPos posIn = new BlockPos(x,y,z);
		extraVector = new Vec3d(d1,d2,d3);
		if(posIn.equals(new BlockPos(0,-1,0))) {
			this.setDead();
		} else {
			this.setTarget(posIn);
			this.setFire(true);
		}
	}
	
	public void fire() {
		switch(targetType) {
			case PREDATOR: {
				EntityPredatorMissile predator = new EntityPredatorMissile(world, controller.get(), 
						target.getX(), 
						target.getY() + EntityPredatorMissile.VERTICAL_OFFSET, 
						target.getZ());
				world.spawnEntity(predator); 
				break;
			}
			case AC130: {
				
			}
			case STEALTH_BOMBER: {
				EntityStealthBomber stealth = new EntityStealthBomber(world, target.getX(), (target.getY() + EntityStealthBomber.Y_OFFSET), target.getZ(), 
						(float) ((Math.atan2(extraVector.z, extraVector.x) * 180D) / 3.1415927410125732D) - 90F);
				world.spawnEntity(stealth);
				break;
			}
			case AIRSTRIKE: {
				
			}
			default: {
				
			}
		}
	}

	@Override
	public void notifyDataManagerChange(DataParameter<?> key) {
		super.notifyDataManagerChange(key);
		if(FIRE.equals(key)) {
			this.shouldFire = this.getFireDataManager();
		}
		if(TARGET.equals(key)) {
			this.target = this.getTargetDataManager();
		}
	}

	private boolean getFireDataManager() {
		return (Boolean) this.dataManager.get(FIRE);
	}
	
	private BlockPos getTargetDataManager() {
		return (BlockPos) this.dataManager.get(TARGET);
	}


	public boolean getShouldFire() {
		return this.shouldFire;
	}

	public void setFire(boolean fire) {
		this.dataManager.set(FIRE, fire);
		this.dataManager.setDirty(FIRE);
		this.shouldFire = fire;
	}

	public BlockPos getTarget() {
		return this.target;
	}
	
	public void setTarget(BlockPos t) {
		this.dataManager.set(TARGET, t);
		this.dataManager.setDirty(TARGET);
		this.target = t;
	}


	public enum EnumTargetingType {
		PREDATOR,
		AC130,
		STEALTH_BOMBER,
		AIRSTRIKE;
		
        public static EnumTargetingType get(int meta)
        {
            return meta >= 0 && meta < values().length ? values()[meta] : PREDATOR;
        }

		
        public static EnumTargetingType[] values = values();

	}
}
