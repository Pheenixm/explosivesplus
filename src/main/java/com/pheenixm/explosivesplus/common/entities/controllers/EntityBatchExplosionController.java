package com.pheenixm.explosivesplus.common.entities.controllers;

import java.util.ArrayDeque;
import java.util.Queue;

import org.apache.commons.lang3.tuple.Pair;

import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EntityBatchExplosionController extends Entity {

	protected Queue<Pair<BlockPos, Explosions>> queue;
	private int size;
	
	public EntityBatchExplosionController(World worldIn) {
		super(worldIn);
		queue = new ArrayDeque<Pair<BlockPos, Explosions>>(128);
	}
	
	public EntityBatchExplosionController(World world, double x, double y, double z, int size) {
		this(world);
		setPosition(x,y,z);
		this.size = size;
	}
	
	public void addExplosionAtPoint(BlockPos pos, Explosions exp) {
		queue.add(Pair.of(pos, exp));
	}
	
	@Override
	public void onUpdate() {
		if(!world.isRemote) {
			if(queue == null || queue.isEmpty()) {
				this.setDead();
			} else {
				Pair<BlockPos, Explosions> pair = queue.poll();
				BlockPos pos = pair.getLeft();
				ExplosionHelper.createExplosion(world, null, pos.getX(), pos.getY(), pos.getZ(), pair.getRight(), false, size);
			}
		}
	}

	@Override
	protected void entityInit() {}
	
	@Override
	protected void readEntityFromNBT(NBTTagCompound compound) {
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound compound) {
	}

}
