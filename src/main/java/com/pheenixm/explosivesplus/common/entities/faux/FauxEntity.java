package com.pheenixm.explosivesplus.common.entities.faux;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public abstract class FauxEntity {

	
	public FauxEntity(World worl, double x, double y, double z){
		this(worl);
		posX = x;
		posY = y;
		posZ = z;
		motionX = 0;
		motionY = 0;
		motionZ = 0;
		isDead = false;
		
	}
	
	public FauxEntity(World world2) {
		this.world = world2;
	}

	public abstract void onUpdate();

	public abstract NBTTagCompound getUpdateTag();
	
	public abstract void handleUpdateTag(NBTTagCompound tag);
	
    public double getDistance(double par1, double par3, double par5)
    {
        double var7 = this.posX - par1;
        double var9 = this.posY - par3;
        double var11 = this.posZ - par5;
        return (double)Math.sqrt(var7 * var7 + var9 * var9 + var11 * var11);
    }
    
    public double getDistance(Vec3d vec) {
    		return Math.sqrt(vec.squareDistanceTo(posX, posY, posZ));
    }
    
    public BlockPos getBlockPos() {
    		return new BlockPos(posX, posY, posZ);
    }
    
    public void setPositionAndRotation(double x, double y, double z, float yaw, float pitch) {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;
		
		prevRotationYaw = prevRotationYaw == rotationYaw ? prevRotationYaw : rotationYaw;
		prevRotationPitch = rotationPitch == rotationPitch ? prevRotationPitch : rotationPitch;
		
		posX = x;
		posY = y;
		posZ = z;
		rotationYaw = yaw;
		rotationPitch = pitch;
    }
    
	public void setPosition(double x, double y, double z) {
		setPositionAndRotation(x, y, z, rotationYaw, rotationPitch);
	}
	
	public void setDead() {
		isDead = true;
	}


	
	
    public World world;
    public double prevPosX;
    public double prevPosY;
    public double prevPosZ;
	
    
	public double posX;
	public double posY;
	public double posZ;
	
	public double motionX;
	public double motionY;
	public double motionZ;
	
    public float rotationYaw;
    public float rotationPitch;
    
    public float prevRotationYaw;
    public float prevRotationPitch;

	public boolean isDead;
	public int ticksExisted;
	
}
