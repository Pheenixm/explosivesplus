package com.pheenixm.explosivesplus.common.entities;



import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTheoryOfChaosController;
import com.pheenixm.explosivesplus.common.explosions.ExplosionBiomeBuster;
import com.pheenixm.explosivesplus.common.explosions.ExplosionChaos;
import com.pheenixm.explosivesplus.common.explosions.ExplosionNova;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntityTheoryOfChaos extends EntityFusedBomb
{

	private String seed;
	
    public EntityTheoryOfChaos(World world, double x, double y, double z, Entity igniter, String seed) {
		super(world, x, y, z, igniter);
		this.parentBlock = ExplosivesPlusHolder.CHAOS.getDefaultState().withProperty(ExplosivesBlock.EXPLODE, false);
		this.seed = seed;
	}
    
    public EntityTheoryOfChaos(World world) {
    		super(world);
    }

	@Override
	public void detonate() {
		ExplosionChaos explosion = new ExplosionChaos(world, bombPlacedBy, posX, EntityTheoryOfChaosController.HEIGHT, posZ, seed);
		explosion.initiate(parentBlock, false);
	}

	@Override
	public void playEffects() {
		
	}

}
