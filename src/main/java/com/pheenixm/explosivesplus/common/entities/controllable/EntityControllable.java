package com.pheenixm.explosivesplus.common.entities.controllable;

import com.pheenixm.explosivesplus.common.entities.EntityViewer;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageAngleUpdate;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

 
public abstract class EntityControllable extends Entity implements IEntityAdditionalSpawnData{

	public EntityViewer camera;
	private int cameraId;
	public float nextPitch;
	public float nextYaw;
	
	//Client-side variables
	public int mouseButton;
	
	private static final DataParameter<Integer> CAMERA = EntityDataManager.createKey(EntityControllable.class, DataSerializers.VARINT);

	
	public EntityControllable(World world) {
		super(world);	
	}
	
	public EntityControllable(World world, EntityPlayer player, double x, double y, double z) {
		this(world);
		this.setPosition(x, y, z);
		if(!world.isRemote) {
			camera = initializeCamera(player);
			System.out.println("Spawned camera");
		} 
		camera.startRiding(this, true);
	}
	
	protected EntityViewer initializeCamera(EntityPlayer player) {
		EntityViewer view = new EntityViewer(world, posX, posY, posZ, player, false);
		view.setAngleLocked(getLockAngle());
		world.spawnEntity(view);
		this.setCameraId(view.getEntityId());
		return view;
	}
	
	@Override
    public double getMountedYOffset() {
    		return getCameraY();
    }
	
	@Override
    public void updatePassenger(Entity passenger) {
    		super.updatePassenger(passenger);
    		passenger.setPosition(posX, posY + getMountedYOffset(), posZ);
    }

	
	@Override
	public void onUpdate() {
		super.onUpdate();
		if(world.isRemote) {
			onClientUpdate();
			if(!getLockAngle()) {
				MessageAngleUpdate angle = new MessageAngleUpdate(camera.rotationYaw, camera.rotationPitch, getMouseButton(), getEntityId());
				PacketHandler.NETWORK.sendToServer(angle);
			}
		}
		if(!world.isRemote) {
			onServerUpdate();
		}
		updateMovement();
	}
	

	@SideOnly(Side.CLIENT) 
	public static int getMouseButton() {
		GameSettings set = Minecraft.getMinecraft().gameSettings;
		if(set.keyBindAttack.isKeyDown()) {
			return 0;
		}
		if(set.keyBindUseItem.isKeyDown()) {
			return 1;
		}
		return -1;
	}

	///DATA MANGERS
	
	@Override
	protected void entityInit() {
		dataManager.register(CAMERA, -1);
	}
	
	@Override
	public void notifyDataManagerChange(DataParameter<?> key) {
		if (CAMERA.equals(key)) {
			this.cameraId = this.getCameraDataManager();
		}
	}
	
	private int getCameraDataManager() {
		return (Integer)this.dataManager.get(CAMERA);
	}

	//SERIALIZERS

	@Override
	protected void readEntityFromNBT(NBTTagCompound compound) {
		Entity ent = world.getEntityByID(compound.getInteger("id"));
		if(ent instanceof EntityViewer) {
			camera = (EntityViewer) ent;
		}
		deserialize(compound);
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound compound) {
		compound.setInteger("id", camera.getEntityId());
		serialize(compound);
	}
	
	@Override
	public void writeSpawnData(ByteBuf buffer) {
		if(camera != null) {
			buffer.writeInt(camera.getEntityId());
		} else {
			System.out.println("Camera should never be null!");
		}
	}

	@Override
	public void readSpawnData(ByteBuf additionalData) {
		Entity uncast = world.getEntityByID(additionalData.readInt());
		if(uncast instanceof EntityViewer) {
			camera = (EntityViewer)uncast;
			System.out.println("Read");
		} else {
			System.out.println("Could not find entity viewer");
		}
		camera.setupCamera();
		camera.startRiding(this, true);
	}
	
	//GETTERS AND SETTERS

	public int getCameraId() {
		return cameraId;
	}

	public void setCameraId(int cameraId) {
		this.dataManager.set(CAMERA, cameraId);
		this.dataManager.setDirty(CAMERA);
		this.cameraId = cameraId;
	}
	

	
	public abstract void onClientUpdate();
	public abstract void onServerUpdate();
	public abstract void updateMovement();
	public abstract void processMouseInput(int dx, int dy, int button);
	public abstract void serialize(NBTTagCompound tag);
	public abstract void deserialize(NBTTagCompound tag);
	
	public abstract double getCameraY();
	public abstract boolean getLockAngle();
	
	public abstract float getLockPitch();
	public abstract float getLockYaw();

	public void updateAngles(float rP, float rY) {
		nextPitch = rP;
		nextYaw = rY;
	}

	

}
