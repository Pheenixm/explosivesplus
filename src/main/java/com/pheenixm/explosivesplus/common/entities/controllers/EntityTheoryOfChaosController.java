package com.pheenixm.explosivesplus.common.entities.controllers;

import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.pheenixm.explosivesplus.api.entities.ICustomRender;
import com.pheenixm.explosivesplus.common.SoundHandler;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntity;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityFallingBlocks;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper;
import com.pheenixm.explosivesplus.common.util.RenderUtils;
import com.pheenixm.explosivesplus.common.util.Utils;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.EntitySelectors;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * TODO: Allow for chunk size to be configured
 * @author RobMontgomery
 *
 */
public class EntityTheoryOfChaosController extends Entity implements ICustomRender {

	public static final float COMP_RAD = 6;
	public static final float LETHAL_RAD = 3; 
	public static final int CHUNK_RANGE = 7;
	public static final int HEIGHT = 96;
	
	private static final Predicate<Entity> TARGETS = Predicates.and(EntitySelectors.NOT_SPECTATING,
			EntitySelectors.IS_ALIVE, new Predicate<Entity>() {
				public boolean apply(@Nullable Entity p_apply_1_) {
					return p_apply_1_.canBeCollidedWith();
				}
			});

	public HashMap<Integer, FauxEntityFallingBlocks> fallingBlocks;
	public HashSet<Entity> entities;
	

	private static final DataParameter<NBTTagCompound> ENTITIES = EntityDataManager.<NBTTagCompound>createKey(EntityTheoryOfChaosController.class, DataSerializers.COMPOUND_TAG);

	
	private Queue<BlockPos> blockQ;
	private ArrayList<BlockPos> blockQR;
	private int level;
	private int size;
	private double bVal;
	private boolean exploded;

	public static final double PARAM_A = 0.95;
	public static final double PARAM_B = 0.7;
	public static final double PARAM_C = 0.6;
	public static final double PARAM_D = 3.5;
	public static final double PARAM_E = 0.25;
	public static final double PARAM_F = 0.1;
	
	public static final double PARAM_TB = 0.19;
	
	public static final double PARAM_LM2A = 0.9;
	public static final double PARAM_LM2B = 5;
	public static final double PARAM_LM2C = 9.9;
	public static final double PARAM_LM2D = 1D;
	
	public static final double PARAM_TSA = 40.0;
	public static final double PARAM_TSB = 0.833;
	public static final double PARAM_TSC = 20.0;
	public static final double PARAM_TSD = 0.5;
	public static final double PARAM_TSE = 0.65;

	public static final double PARAM_DA = 40.0;
	public static final double PARAM_DB = 1.833;
	public static final double PARAM_DC = 0.16;
	public static final double PARAM_DD = 0.65;
	public static final double PARAM_DE = 55.0;
	public static final double PARAM_DF = 20.0;


	public EntityTheoryOfChaosController(World world) {
		super(world);
		fallingBlocks = new HashMap<Integer, FauxEntityFallingBlocks>();
		blockQ = new ArrayDeque<BlockPos>(2304);
		blockQR = new ArrayList<BlockPos>(2304);
		entities = new HashSet<Entity>();
		this.ignoreFrustumCheck = true;
		setSize(256, 256);

	}

	public EntityTheoryOfChaosController(World world, double x, double y, double z) {
		this(world, x, y, z, null);
	}
	
	public EntityTheoryOfChaosController(World world, double x, double y, double z, String seed) {
		this(world);
		setPosition(x, y, z);
		level = getHighest();
		if(seed != null) {
			rand = Utils.getRandSeed(seed);
		}
		bVal = (double)rand.nextInt(208186) / 1000000D;
	}
	
	@Override
    public AxisAlignedBB getEntityBoundingBox()
    {
        return new AxisAlignedBB(posX - 2 * (CHUNK_RANGE * 16 + 8), 0, posZ - 2 * (CHUNK_RANGE * 16 + 8), posX + 2 * (CHUNK_RANGE * 16 + 8), 256, posZ+2 * (CHUNK_RANGE * 16 + 8));
    }
	
	@Override
    public AxisAlignedBB getRenderBoundingBox()
    {
        return new AxisAlignedBB(posX - 1, 0, posZ - 1, posX + 1, 256, posZ + 1);
    }



	@Override
	public void onUpdate() {
		this.notifyDataManagerChange(ENTITIES);
		if (!world.isRemote) {
			if(ticksExisted % 720 == 0) {
				playSound(SoundHandler.BLACK_HOLE, 10, (1.0F + (this.world.rand.nextFloat() - this.world.rand.nextFloat()) * 0.2F) * 0.7F);
			}
			int tempSize = 0;
			if (fallingBlocks != null && !fallingBlocks.isEmpty()) {
				//Update motion in falling blocks
				Iterator<Integer> iter = fallingBlocks.keySet().iterator();
				while(iter.hasNext()) {
					tempSize++;
					FauxEntityFallingBlocks block = fallingBlocks.get(iter.next());
					if(block.ticksExisted > 0 && !exploded) {
						//TODO: Allow for cases of different attractors
						applyThomas(block);// So far, best one
						//applyLorentzMod2(block); //Bad
						//applyThreeScroll(block); //MEh
						//applyAizawa(block);
						//applyDequan(block);
						
						double dist = block.getDistance(posX, posY, posZ);
						if(dist <= COMP_RAD + LETHAL_RAD) {
							block.scale = (float)dist/COMP_RAD;
							if(dist < LETHAL_RAD) {
								//block.isDead = true;
								//System.out.println("Kill");
							}
						}
						if(block.ticksExisted >= 1200) {
							block.isDead = true;
						}
					}
					if(exploded && !block.hasGravity) {
						block.hasGravity = true;
					}
					block.onUpdate();
					
					if (block.isDead) {
						iter.remove();
					}

				}

			} else {
				if(fallingBlocks != null && ticksExisted > 10) {
					this.setDead();
				}
			}
			
			/*Debugging
			if (ticksExisted > 600 && !exploded) {
				explode();
			}
			*/
			
			if(!exploded) {
				// Block consumption code
				consumeBlocks();
	
				//Convert 10 blocks from BlockQ
				convertBlocks(tempSize);
				
				
				List<Entity> ents = world.getEntitiesWithinAABBExcludingEntity(this, new AxisAlignedBB(posX - 2*(CHUNK_RANGE/2 * 16 - 8), 0, posZ - 2*(CHUNK_RANGE/2 * 16 - 8), 
						posX + 2*(CHUNK_RANGE/2 * 16 + 8), world.getHeight(), posZ + 2*(CHUNK_RANGE/2 * 16 + 8)));
				for(Entity ent : ents) {
					if(world.canSeeSky(ent.getPosition())) {
						applyThomas(ent);
						ent.noClip = true;
					}
				}
			}
		}

	}

	protected void consumeBlocks() {
		// TODO: Modify the consumption conditions
		if (blockQ.isEmpty()) {
			int chunk = CHUNK_RANGE/2 * 16 + 8;
			for (int i = -chunk; i <= chunk; i++) {
				for (int k = -chunk; k <= chunk; k++) {
					BlockPos bPos = new BlockPos(posX + i, level, posZ + k);
					if (!world.isAirBlock(bPos)) {
						blockQR.add(bPos);
					}
				}
			}
			Collections.shuffle(blockQR);
			blockQ.addAll(blockQR);
			blockQR.clear();
			level--;
			if (level < 0) {
				// We've reached the bottom
				// Time to explode and start the death timer
				explode();
				return;
			}
		}
	}
	
	protected void explode() {
		ExplosionHelper.spawnShockwave(world, posX, posY, posZ, 128, 30);
		exploded = true;
		if (fallingBlocks != null && !fallingBlocks.isEmpty()) {
			float explosionSize = 512F;

			for (FauxEntityFallingBlocks entity : fallingBlocks.values()) {
				double distance = entity.getDistance(posX, posY, posZ);
				if (distance < 256) {

					double d4 = entity.getDistance(posX, posY, posZ)
							/ (double) explosionSize;
					double d6 = entity.posX - posX;
					double d8 = entity.posY - posY;
					double d10 = entity.posZ - posZ;
					double d11 = MathHelper.sqrt(d6 * d6 + d8 * d8 + d10 * d10);
					d6 /= d11;
					d8 /= d11;
					d10 /= d11;
					double d12 = 1D;
					double d13 = (0.7D - d4) * d12;
					double d14 = d13;


					if (entity.posY > posY - 32D) {
						entity.motionX += d6 * d14 * 6D;
						entity.motionY += d8 * d14 * 4D;
						entity.motionZ += d10 * d14 * 6D;
					} else {
						entity.motionX -= d6 * d14 * 6D;
						entity.motionY -= d8 * d14 * 4D;
						entity.motionZ -= d10 * d14 * 6D;
					}
					entity.canLand = true;
					entity.decayMotion = true; 
				}
			}
		}
		
	}

	protected void convertBlocks(int s) {
		for (int i = 0; i < 32; i++) {
			//TODO: Add small explosions on the ground, just showy, but still dangerous to players
			// TODO: Change this condition to throttle explosion progression
			BlockPos targetPos = blockQ.poll();
			if(targetPos == null) {
				return;
			}
			IBlockState state = world.getBlockState(targetPos);
			world.setBlockState(targetPos, Blocks.AIR.getDefaultState(), 2);
			if (!blockQ.isEmpty() && s < 3600 && i < 2) {
				//Only create block entities every other tick
				FauxEntityFallingBlocks block = new FauxEntityFallingBlocks(world, targetPos, state);
				block.decayMotion = false;
				block.hasGravity = false;
				block.motionY = 0.4;
				block.ticksExisted = - (rand.nextInt(100) + 50);
				fallingBlocks.put(size, block);
				size++;
				
				world.markBlockRangeForRenderUpdate(targetPos, targetPos.add(1, 1, 1));

			}
		}
	}
	
	protected void applyAizawa(FauxEntityFallingBlocks block) {
		double dx = (block.posX - posX)/2D;
		double dy = (block.posY - posY)/2D;
		double dz = (block.posZ - posZ)/2D;
		
		
		//double d = Math.sqrt(dx*dx + dy*dy + dz*dz);
		

		double x1 = (dz - PARAM_B) * dx - PARAM_D * dy;

		double y1 = PARAM_D * dx + (dz - PARAM_B) * dy;
		
		double z1 = PARAM_C + PARAM_A * dz - ((dz * dz * dz) / 3D)
				- ((dx * dx) + (dy * dy)) * (1 + PARAM_E * dz)
				+ PARAM_F * dz * (dx * dx * dx);

		/*
		if(x1 * x1 > 100) {
			x1 *= 0.1;
		}
		if(y1 * y1 > 100) {
			y1 *= 0.1;
		}
		if(z1 * z1 > 100) {
			z1 *= 0.1;
		}*/
		
		block.motionX = x1 * 0.005;
		block.motionY = y1 * 0.005;
		block.motionZ = z1 * 0.005;
		
	}
	
	protected void applyThomas(FauxEntityFallingBlocks block) {
		double dx = block.posX - posX;
		double dy = block.posY - posY;
		double dz = block.posZ - posZ;
		
		//double b = 0.1313;////0.1998; //0.158883
		
		dx/=10D;
		dy/=10D;
		dz/=10D;

		double x1 = (-bVal * dx + Math.sin(dy));
		double y1 = (-bVal * dy + Math.sin(dz));
		double z1 = (-bVal * dz + Math.sin(dx));
		
		block.motionX = x1; //* 0.1;
		block.motionY = y1; //* 0.1;
		block.motionZ = z1;//* 0.1;
	}
	
	protected void applyThomas(Entity ent) {
		double dx = ent.posX - posX;
		double dy = ent.posY - posY;
		double dz = ent.posZ - posZ;
		
		//double b = 0.1313;////0.1998;
		
		dx/=10D;
		dy/=10D;
		dz/=10D;

		double x1 = (-bVal * dx + Math.sin(dy));
		double y1 = (-bVal * dy + Math.sin(dz));
		double z1 = (-bVal * dz + Math.sin(dx));
		
		ent.setVelocity(x1, y1, z1);
	}

	
	protected void applyLorentzMod2(FauxEntityFallingBlocks block) {
		double dx = block.posX - posX;
		double dy = block.posY - posY;
		double dz = block.posZ - posZ;
		
		dx/=100D;
		dy/=100D;
		dz/=100D;

		double x1 = (-PARAM_LM2A*dx+ dy*dy - dz*dz + PARAM_LM2A * PARAM_LM2C);
		double y1 = (dx*(dy-PARAM_LM2B*dz)+PARAM_LM2D);
		double z1 = (-dz + dx*(PARAM_LM2B*dy +dz));
		
		block.motionX = x1 * .01;
		block.motionY = y1 * .01;
		block.motionZ = z1 * .01;
	}
	
	protected void applyThreeScroll(FauxEntityFallingBlocks block) {
		double dx = block.posX - posX;
		double dy = block.posY - posY;
		double dz = block.posZ - posZ;
		
		dx/=1000D;
		dy/=1000D;
		dz/=1000D;

		double x1 = (PARAM_TSA*(dy-dx) + PARAM_TSD*dx*dz);
		double y1 = (PARAM_TSC*dy - dx*dz );
		double z1 = (PARAM_TSB*dz + dx*dy - PARAM_TSE*dx*dx);
		
		block.motionX = x1 * 0.1;
		block.motionY = y1 * 0.1;
		block.motionZ = z1 * 0.01;
	}
	
	protected void applyDequan(FauxEntityFallingBlocks block) {
		double dx = block.posX - posX;
		double dy = block.posY - posY;
		double dz = block.posZ - posZ;
		
		dx/=5D;
		dy/=5D;
		dz/=5D;

		double x1 = (PARAM_DA*(dy-dx) + PARAM_DC*dx*dz);
		double y1 = (PARAM_DE*dx + PARAM_DF*dy - dx*dz );
		double z1 = (PARAM_DB*dz + dx*dy - PARAM_DD*dx*dx);
		
		block.motionX = x1 * 0.01;
		block.motionY = y1 * 0.01;
		block.motionZ = z1 * 0.01;
	}


	private void buildListsFromNBT() {
		NBTTagList falling = this.dataManager.get(ENTITIES).getTagList("fallingBlocks", Constants.NBT.TAG_COMPOUND);

		fallingBlocks = new HashMap<Integer, FauxEntityFallingBlocks>();
		

		for (NBTBase block1 : falling) {
			NBTTagCompound blockCompound = (NBTTagCompound) block1;
			int id = blockCompound.getInteger("id");
			if (!fallingBlocks.containsKey(id)) {
				FauxEntityFallingBlocks block = FauxEntityFallingBlocks.getFallingBlockFromNBT(world,
						blockCompound);
				fallingBlocks.put(id, block);
			} else {
				FauxEntityFallingBlocks existingBlock = fallingBlocks.get(id);
				fallingBlocks.replace(id, FauxEntityFallingBlocks.updateExistingBlockFromNBT(existingBlock, blockCompound));
			}
		}
	}
	
	private NBTTagCompound getNBTFromLists() {
		// Write fallingBlocks to NBT
		NBTTagCompound compound = new NBTTagCompound();
		if (fallingBlocks == null) {
			return compound;
		}
		NBTTagList falling = new NBTTagList();
		
		for(int i : fallingBlocks.keySet()) {
			if (fallingBlocks.containsKey(i)) {
				FauxEntityFallingBlocks block = fallingBlocks.get(i);
				NBTTagCompound tag = FauxEntityFallingBlocks.getNBTFromFallingBlock(block);
				tag.setInteger("id", i);
				falling.appendTag(tag);
			}
		}
		
		compound.setTag("fallingBlocks", falling);
		return compound;
	}


	public int getHighest() {
		int highest = 0;
		int chunk = CHUNK_RANGE / 2;
		BlockPos pos = new BlockPos(posX, posY, posZ);
		for (int i = -chunk; i <= chunk; i++) {
			for (int j = -chunk; j <= chunk; j++) {
				for (int h : world.getChunk(pos.add(16 * i, 0, 16 * j)).getHeightMap()) {
					if (h > highest) {
						highest = h;
					}
				}
			}
		}
		return highest;
	}

	@Override
	protected void entityInit() {
		NBTTagCompound compound = this.getNBTFromLists();
		this.dataManager.register(ENTITIES, compound);
		if(world.isRemote && !RenderUtils.customRender.contains(new WeakReference<Entity>(this))) {
			RenderUtils.customRender.add(new WeakReference<ICustomRender>(this));
		}
	}
	
	public void setEntities() {
		NBTTagCompound compound = this.getNBTFromLists();
		this.dataManager.set(ENTITIES, compound);
	}
	
	@Override
	public void notifyDataManagerChange(DataParameter<?> key) {
		if (ENTITIES.equals(key)) {
			if (world.isRemote) {
				this.buildListsFromNBT();
			} else {
				this.setEntities();
			}
		}

	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound tag) {
		this.dataManager.set(ENTITIES, tag.getCompoundTag("lists"));
		this.buildListsFromNBT();
		this.bVal = tag.getDouble("b");
		this.level = tag.getInteger("level") + 1;
		if(world.isRemote && !RenderUtils.fallingControllers.contains(new WeakReference<Entity>(this))) {
			RenderUtils.fallingControllers.add(new WeakReference<Entity>(this));
		}

	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound tag) {
		tag.setTag("lists", this.getNBTFromLists());
		tag.setDouble("b", bVal);
		tag.setInteger("level", level);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void render(double x, double y, double z, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, z);
		RenderUtils.renderBlackhole(6);
		GlStateManager.popMatrix();

		
		for(FauxEntityFallingBlocks block : fallingBlocks.values()) {
			if(!block.inBlock || block.ticksExisted < 30) {
				float diffX = (float) (block.posX - posX);
				float diffY = (float) (block.posY - posY);
				float diffZ = (float) (block.posZ - posZ);

				float blockX = (float) x + diffX;
				float blockY = (float) y + diffY;
				float blockZ = (float) z + diffZ;
				if(block.block.getBlock() instanceof BlockLiquid) {
					//renderFluid(blockX, blockY, blockZ, block);
				} else {
					RenderUtils.renderBlock(blockX, blockY, blockZ, block, this, Tessellator.getInstance().getBuffer());
				}
			}
		}
	}
	

	@Override
	public double[] getPos() {
		double[] pos	 = {posX, posY, posZ};
		return pos;
	}

	@Override
	public boolean shouldRemove() {
		return isDead;
	}

	@Override
	public double getRenderDistance() {
		return 256D;
	}


}
