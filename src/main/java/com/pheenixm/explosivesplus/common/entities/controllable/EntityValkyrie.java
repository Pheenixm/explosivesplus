package com.pheenixm.explosivesplus.common.entities.controllable;

import java.util.Iterator;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.pheenixm.explosivesplus.common.entities.missiles.EntityBallisticMissile;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageMouseInput;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class EntityValkyrie extends EntityControllable {

	public static final double MAX_VELOCITY = 2.0D;
	public static final int VERTICAL_OFFSET = 150;

	boolean rightButtonDown; // is right mouse button down.

	public static double Speed = 10.0D;
	public static boolean missileSpawned = false;

	private int newmouseX;
	private int newmouseY;
	private int button;
	
	private float speed;
	
	private boolean detonateNextTick;

	private static final float YAW = 0F;
	private static final float PITCH = 0F;
	private static final double CAM_OFFSET = 7;
	
	private static final DataParameter<Boolean> DETONATE = EntityDataManager.createKey(EntityValkyrie.class, DataSerializers.BOOLEAN);


	public EntityValkyrie(World world) {
		super(world);
		setSize(1.0F, 1.0F);
		
	}
	
	
	public EntityValkyrie(World world, EntityPlayer entityplayer,
			double x, double y, double z) {
		super(world, entityplayer, x, y, z);
		motionX = 0.0D; // Sets velocity in X direction 0
		motionY = 0.0D; // Sets velocity in Y direction 0
		motionZ = 0.0D; // Sets velocity in Z direction 0
		detonateNextTick = false;
        speed = 0.75F;


		/*
		 * if(mod_ExplosivesPlus.advancedFeatures){ AxisAlignedBB boundingBox =
		 * AxisAlignedBB.getBoundingBox(-500.0D, -500.0D, -500.0D, 500.0D,
		 * 500.0D, 500.0D); AxisAlignedBB axisalignedbb = boundingBox.expand(4D,
		 * 2D, 4D); List list1 =
		 * worldObj.getEntitiesWithinAABB(EntityLiving.class, axisalignedbb);
		 * 
		 * for (int k = 0; k < list1.size(); k++) { EntityLiving entity =
		 * (EntityLiving)list1.get(k); ExplosivesPlusEntityRedSquare target =
		 * new ExplosivesPlusEntityRedSquare(worldObj, entity, this,
		 * entity.posX, entity.posY, entity.posZ);
		 * worldObj.spawnEntityInWorld(target); } }
		 */
	}
	
	@Override
	protected void entityInit() {
		super.entityInit();
		dataManager.register(DETONATE, false);
	}

	@Override
	public void onClientUpdate() {
		nextPitch = camera.rotationPitch;
		nextYaw = camera.rotationYaw;
		if(detonateNextTick) {
			camera.doReset();
		}
	}

	@Override
	public void updateMovement() {
		if(detonateNextTick || isDead) {
			//Call this method twice to ensure it is processed
			//Detonate
			detonate();
			camera.doReset();
		}
		if (collidedHorizontally || collidedVertically || posY < 0) {
			System.out.println("Set dead");
			camera.doReset();
			setDetonateNextTick(true);
		} else {

			prevRotationYaw = rotationYaw;
			prevRotationPitch = rotationPitch;
			
			rotationPitch = nextPitch;
			rotationYaw = nextYaw;
			adjustMotion();
			setValkryieHeading(motionX, motionY, motionZ, speed, 0.0F);

			if (button == 1) {
				motionY = -4D;
			}
		}
	}
	
    public void setValkryieHeading(double d, double d1, double d2, float f,
            float f1)
    {
        float f2 = MathHelper.sqrt(d * d + d1 * d1 + d2 * d2);
        d /= f2;
        d1 /= f2;
        d2 /= f2;
        d *= f;
        d1 *= f;
        d2 *= f;
        motionX = d;
        motionY = d1;
        motionZ = d2;
        float f3 = MathHelper.sqrt(d * d + d2 * d2);
//        prevRotationYaw = rotationYaw = (float)((Math.atan2(d, d2) * 180D) / 3.1415927410125732D);
//        prevRotationPitch = rotationPitch = (float)((Math.atan2(d1, f3) * 180D) / 3.1415927410125732D);
        this.move(MoverType.SELF, -motionX, -motionY, motionZ);
    	

    }

	
    private void adjustMotion() {
        float f = 0.4F;
        motionX = MathHelper.sin((rotationYaw / 180F) * 3.141593F) * MathHelper.cos((rotationPitch / 180F) * 3.141593F) * f;
        motionZ = MathHelper.cos((rotationYaw / 180F) * 3.141593F) * MathHelper.cos((rotationPitch / 180F) * 3.141593F) * f;
        motionY = MathHelper.sin(((rotationPitch) / 90F) * 3.141593F) * f;

        }


	@Override
	public void onServerUpdate() {}

	private void detonate() {
		setDead();
		ExplosionHelper.createExplosion(world, null, posX, posY, posZ,
				Explosions.CLASSIC,false, 7);
	}

	@Override
	public void serialize(NBTTagCompound tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deserialize(NBTTagCompound tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getCameraY() {
		return 0;
	}

	@Override
	public boolean getLockAngle() {
		return false;
	}

	@Override
	public float getLockPitch() {
		return PITCH;
	}

	@Override
	public float getLockYaw() {
		return YAW;
	}

	public boolean isDetonateNextTick() {
		return detonateNextTick;
	}

	public void setDetonateNextTick(boolean detonateNextTick) {
		this.dataManager.set(DETONATE, detonateNextTick);
		this.getDataManager().setDirty(DETONATE);
		this.detonateNextTick = detonateNextTick;
	}
	
	public boolean getDetonateDataManager() {
		return (Boolean) this.dataManager.get(DETONATE).booleanValue();
	}

	
	@Override
	public void notifyDataManagerChange(DataParameter<?> key) {
		if(key.equals(DETONATE))	{
			
		}
	}

	@Override
	public void processMouseInput(int dx, int dy, int button) {}
}