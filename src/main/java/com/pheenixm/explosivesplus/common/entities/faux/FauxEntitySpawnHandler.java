package com.pheenixm.explosivesplus.common.entities.faux;

import com.pheenixm.explosivesplus.common.EnumTier;
import com.pheenixm.explosivesplus.common.entities.missiles.FauxEntityBallisticMissile;

import net.minecraft.world.World;

public class FauxEntitySpawnHandler {

	public static FauxTickingEntity getEntityFromEnum(World world, EnumFauxEntity faux) {
		switch(faux) {
			case BALLISTIC: {
				//TODO: add constructor
				FauxEntityBallisticMissile missile = new FauxEntityBallisticMissile(world);
				missile.rotationPitch = missile.prevRotationPitch = 90;
				return missile;
			}
			default: {
				return new FauxEntityBallisticMissile(world);
			}
		}
	}
	
	public enum EnumFauxEntity {
		BALLISTIC;
		
	    public static EnumFauxEntity get(int meta)
	    {
	        return meta >= 0 && meta < values().length ? values()[meta] : BALLISTIC;
	    }

	}
}
