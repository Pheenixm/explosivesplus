package com.pheenixm.explosivesplus.common.entities.missiles;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import com.mojang.authlib.GameProfile;
import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent;
import com.pheenixm.explosivesplus.common.blocks.multiblocks.MultiblockComponent.MultiTypeEnum;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;
import com.pheenixm.explosivesplus.common.items.components.Engine;
import com.pheenixm.explosivesplus.common.items.components.Engine.EngineTiers;
import com.pheenixm.explosivesplus.common.items.components.Fuselage;
import com.pheenixm.explosivesplus.common.items.components.Fuselage.FuselageTiers;
import com.pheenixm.explosivesplus.common.items.components.Warhead;
import com.pheenixm.explosivesplus.common.tiles.TileEntitySilo;
import com.pheenixm.explosivesplus.common.util.Utils;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ServerTickEvent;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * TODO: Make the missiles render the lid animations?
 * 
 * @author RobMontgomery
 *
 */
public class EntityBallisticMissile extends Entity implements IEntityAdditionalSpawnData {

	// DO NOT CHANGE THIS
	public static final double Y_CONSTANT = -0.01;
	public static final double ARMING_DISTANCE = 15;
	public static final double MIN_TICKS = 100;
	
	public static final int CHUNK_RANGE = 5;
	// Previously 150
	public static final double Y_MAX = 256;
	public static final double LAUNCH_TICKS = 80;
	private static final double TURN_TICKS = 10;

	private static final float HEIGHT = 18.2F;
	private static final float WIDTH = 1.7F;

	private static final DataParameter<ItemStack> WARHEAD = EntityDataManager.createKey(EntityBallisticMissile.class,
			DataSerializers.ITEM_STACK);
	private static final DataParameter<ItemStack> FUSELAGE = EntityDataManager.createKey(EntityBallisticMissile.class,
			DataSerializers.ITEM_STACK);
	private static final DataParameter<ItemStack> ENGINE = EntityDataManager.createKey(EntityBallisticMissile.class,
			DataSerializers.ITEM_STACK);
	private static final DataParameter<Boolean> LAUNCHED = EntityDataManager.createKey(EntityBallisticMissile.class,
			DataSerializers.BOOLEAN);
	private static final DataParameter<Boolean> RENDER = EntityDataManager.createKey(EntityBallisticMissile.class,
			DataSerializers.BOOLEAN);

	private boolean launched;

	// Give values even when NBT not initialized
	private ItemStack warhead = ItemStack.EMPTY;
	private ItemStack fuselage = ItemStack.EMPTY;
	private ItemStack engine = ItemStack.EMPTY;

	private double xVel;
	private double zVel;
	private double initialYVelocity;
	private double yDecrement;
	private double yDiff;
	private Vec3d target;
	private HashSet<ChunkPos> chunks;

	private double flightTime;

	private float oldPitch;
	private float newYaw;
	private float newPitch;

	private boolean isArmed;
	private boolean shouldRender;
	private int launchTimer = 0;
	
	private double x;
	private double y;
	private double z;

	public EntityBallisticMissile(World world, TileEntitySilo silo, Vec3d targ) {
		this(world);
		this.setSize(HEIGHT, WIDTH);
		this.ignoreFrustumCheck = true;
		BlockPos center = silo.getMaster().centerPosition;
		setPosition(center.getX(), center.getY() + 1 + HEIGHT / 2, center.getZ());
		x = posX;
		y = posY;
		z = posZ;
		setTarget(targ);
		this.rotationPitch = 90;
		chunks = new HashSet<ChunkPos>();
		System.out.println("Missile at " + posX + " " + posY + " " + posZ);
		yDecrement = 0;
		yDiff = 0;
		flightTime = 1;
		setShouldRender(true);
	}

	public EntityBallisticMissile(World world) {
		super(world);
	}

	@Override
	protected void entityInit() {
		dataManager.register(WARHEAD, ItemStack.EMPTY);
		dataManager.register(FUSELAGE, ItemStack.EMPTY);
		dataManager.register(ENGINE, ItemStack.EMPTY);
		dataManager.register(LAUNCHED, false);
		dataManager.register(RENDER, true);
	}
		
	@Override
	public void onUpdate() {
		super.onUpdate();


		if (world.isRemote) {
			// Call client-side update
			doClientUpdate();
		} else {
			//loadChunks(new BlockPos(target));
			// Perform server-side update
			double distance = Math.sqrt(target.squareDistanceTo(posX, posY, posZ));

			// Check if entity is within range to its target
			if (distance < ARMING_DISTANCE) {
				isArmed = true;
			}

			if (launched) {
				launchTimer++;
				if (launchTimer <= 1) {
					motionY = 0;
				}
				if (launchTimer < LAUNCH_TICKS + TURN_TICKS) {
					// Perform launching animations
					if (launchTimer < LAUNCH_TICKS) {
						motionY = 25D / LAUNCH_TICKS;
					}
					if (launchTimer == LAUNCH_TICKS) {
						beginTurningAnimation();
						oldPitch = rotationPitch;
						rotationYaw = -newYaw - 90;
					}
					// TODO: Make this animation configurable
					if (launchTimer > LAUNCH_TICKS) {
						motionY -= motionY / TURN_TICKS;

						double animTicks = launchTimer - LAUNCH_TICKS;
						double pitchConst = (newPitch - oldPitch) / TURN_TICKS;
						rotationPitch = (float) (oldPitch - pitchConst * animTicks);
					}
				} else {
					if (launchTimer == LAUNCH_TICKS + TURN_TICKS) {
						// Launch animation complete, initialize launch
						launch();
					}

					// Check to see if entity has impacted a block or entity
					checkForCollision();
					getHeading();

					// Perform normal flight activities

				}
			}
			motionY += yDecrement;
			flyMissile();
			
		}
	}

	public void doClientUpdate() {
		if (!launched && rotationPitch != 90) {
			prevRotationPitch = rotationPitch = 90;
		}
		this.ignoreFrustumCheck = true;
	}
	
	public void flyMissile() {
		//Check if next true position is loaded
		if(world.isBlockLoaded(new BlockPos(x + motionX, y + motionY + yDiff/flightTime, z + motionZ))) {
			if(!shouldRender) {
				//Phase missile to correct position
				setShouldRender(true);
				posX = x;
				posY = y;
				posZ = z;
			}
			
			//Update apparent position markers
			posX += xVel;
			posY += motionY + yDiff/flightTime;
			posZ += zVel;
			
			//Update true position markers
			x = posX;
			y = posY;
			z = posZ;
			this.setPositionAndUpdate(posX, posY, posZ);
		} else {
			//Block is not loaded, we must simulate motion
			if(shouldRender) {
				setShouldRender(false);
			}
			
			x += xVel;
			y += motionY + yDiff/flightTime;
			z += zVel;
		}
	}

	/**
	 * Calculate initial momentum values
	 */
	public void calculateInitialValues() {
		Vec3d start = new Vec3d(posX, posY, posZ);
		Vec3d end = target;
		Vec3d line = end.subtract(start);

		flightTime = this.calculateTimeByDistance(line);

		setxVel(line.x / flightTime);
		setzVel(line.z / flightTime);

		double b = getInitY(flightTime, Y_MAX);
		double a = 1 / (flightTime) * b;

		setInitialYVelocity(b);
		setyDecrement(-2 * a);

		motionY = this.initialYVelocity;

		setyDiff(line.y);
	}

	private double getInitY(double time, double yMax) {
		double t_c = time / 2;
		return (yMax - (Y_CONSTANT * t_c * t_c)) / (t_c);
	}

	/*
	 * This should only ever be called during the launch command Calculates the
	 * distance to the target, uses the engine tier to calculate the time to
	 * reach it and if it can reach it
	 * 
	 * @returns time to reach target in ticks, -1 if target out of range
	 */
	public double calculateTimeByDistance(Vec3d pos) {
		if (target == null) {
			System.out.println("Target should be set in the constructor");
			return -1;
		}
		// Base value, fastest allowed time
		double time = MIN_TICKS;

		double distance = Math.sqrt(pos.lengthSquared());

		FuselageTiers fuseTier = ((Fuselage) fuselage.getItem()).getTier();
		if (fuseTier.range < distance) {
			// Target is out of range
			return -1;
		}

		EngineTiers engineTier = ((Engine) engine.getItem()).getTier();
		int ticks = 0;
		double speed = engineTier.acceleration;
		// TODO: Come up with a formula for non-uniform acceleration
		while (distance > 0) {
			distance -= speed;
			if (speed < engineTier.velocity) {
				speed += engineTier.acceleration;
			}
			ticks++;
		}
		if (ticks >= time) {
			// Don't want to cut into the base time
			time = ticks;
		}

		return Math.floor(time);
	}

	private void loadChunks(BlockPos tarPos) {
		if (chunks.isEmpty()) {
			//Load roughly 
			if (!world.isRemote) {
				for(int i = -CHUNK_RANGE; i <= CHUNK_RANGE; i++) {
					for(int j = -CHUNK_RANGE; j <= CHUNK_RANGE; j++) {
						ChunkPos chunkPos = new ChunkPos(new BlockPos(tarPos.getX() + i*16, 0, tarPos.getY() + j * 16));
						ExplosivesPlus.instance.forceChunkLoad(world, chunkPos);
						chunks.add(chunkPos);
					}
				}
			}
		}
	}

	private void unloadChunks() {
		if (!world.isRemote) {
			for (ChunkPos chun : chunks) {
				ExplosivesPlus.instance.releaseChunkLoad(world, chun);
			}
		}
	}

	/**
	 * Updates the target coords to the highest y pos in the x,z column
	 * 
	 * @param x
	 *            the x coord to target
	 * @param z
	 *            the z coord to target
	 */
	public void updateTarget(int x, int z) {
		BlockPos posToCheck = new BlockPos(x, 0, z);
		if (!world.isBlockLoaded(posToCheck)) {
			this.loadChunks(posToCheck);
		}
		int y = world.getHeight(x, z);
		Vec3d targ = new Vec3d(x, y, z);
		this.setTarget(targ);
		System.out.println("Target set " + targ);
	}

	@Override
	public AxisAlignedBB getEntityBoundingBox() {
		AxisAlignedBB axisBB = new AxisAlignedBB(this.posX - WIDTH / 2, this.posY - HEIGHT / 2, this.posZ - WIDTH / 2,
				this.posX + WIDTH / 2, this.posY + HEIGHT / 2, this.posZ + WIDTH / 2);
		return axisBB;
	}

	public void launch() {
		this.calculateInitialValues();
		motionX = xVel;
		motionZ = zVel;
		motionY = initialYVelocity;
	}

	public void beginTurningAnimation() {
		calculateInitialValues();
		float f3 = MathHelper.sqrt(xVel * xVel + zVel * zVel);
		newYaw = (float) ((Math.atan2(xVel, zVel) * 180D) / 3.1415927410125732D);
		newPitch = (float) ((Math.atan2(initialYVelocity, f3) * 180D) / 3.1415927410125732D);
		yDecrement = 0;
	}

	private void checkForCollision() {
		Vec3d position = new Vec3d(posX, posY, posZ);
		Vec3d motion = new Vec3d(posX + motionX, posY + motionY, posZ + motionZ);
		RayTraceResult rayT = world.rayTraceBlocks(position, motion);
		if (rayT != null) {
			motion = rayT.hitVec;
		}

		Entity entity = null;
		List<Entity> list = this.world.getEntitiesWithinAABBExcludingEntity(this,
				this.getEntityBoundingBox().expand(this.motionX, this.motionY, this.motionZ).grow(1.0D));

		double distanceToEntity = 0.0D;
		for (int i = 0; i < list.size(); ++i) {
			Entity entity1 = list.get(i);

			if (entity1.canBeCollidedWith()) {
				AxisAlignedBB axisalignedbb = entity1.getEntityBoundingBox().grow(0.30000001192092896D);
				RayTraceResult entityIntercept = axisalignedbb.calculateIntercept(position, motion);
				if (entityIntercept != null) {
					double shortDist = position.squareDistanceTo(entityIntercept.hitVec);
					if (shortDist < distanceToEntity || distanceToEntity == 0.0D) {
						entity = entity1;
						distanceToEntity = shortDist;
					}
				}

			}
		}

		// If entity has run into another entity, we want to do something about
		// it
		if (entity != null) {
			rayT = new RayTraceResult(entity);
		}

		// Triggering stuff in here
		if (rayT != null) {
			// Give this forge hook just because
			if (!net.minecraftforge.event.ForgeEventFactory.onProjectileImpact(this, rayT)) {
				if (isArmed) {
					this.detonate();
					System.out.println("Detonated");
				} else {
					if (launchTimer > LAUNCH_TICKS + 40) {
						this.impactedPrematurely();
						System.out.println("Premature");
					}
				}
			}
		}
	}

	private void detonate() {
		this.setDead();
		ExplosionHelper.createExplosion(world, null, posX, posY, posZ, getExplosionType(), false);
		this.unloadChunks();
	}

	private void impactedPrematurely() {
		this.setDead();
		world.createExplosion(this, posX, posY, posZ, 15F, true);
	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound tag) {
		this.setLaunched(tag.getBoolean("launched"));
		this.setShouldRender(tag.getBoolean("shouldRender"));
		this.setWarhead(new ItemStack(tag.getCompoundTag("warhead")));
		this.setFuselage(new ItemStack(tag.getCompoundTag("fuselage")));
		this.setEngine(new ItemStack(tag.getCompoundTag("engine")));
		this.setTarget(new Vec3d(Utils.arrayToVec3i(tag.getIntArray("target"))));

		this.setxVel(tag.getDouble("xVel"));
		this.setzVel(tag.getDouble("zVel"));
		this.setInitialYVelocity(tag.getDouble("initialYVelocity"));
		this.setyDecrement(tag.getDouble("yConstant"));
		this.setyDiff(tag.getDouble("yDiff"));
		this.isArmed = tag.getBoolean("armed");

		if (chunks == null) {
			chunks = new HashSet<ChunkPos>();
		}
		NBTTagList chunkList = tag.getTagList("chunkList", Constants.NBT.TAG_COMPOUND);
		if (!chunkList.isEmpty()) {
			for (NBTBase block1 : chunkList) {
				NBTTagCompound chunks = (NBTTagCompound) block1;
				BlockPos pos = new BlockPos(Utils.arrayToVec3i(chunks.getIntArray("chunk")));
				this.loadChunks(pos);
			}
		}
		x = tag.getDouble("x");
		y = tag.getDouble("y");
		z = tag.getDouble("z");

	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound tag) {
		tag.setBoolean("launched", launched);
		tag.setBoolean("shouldRender", shouldRender);
		if (this.getWarhead() != null) {
			tag.setTag("warhead", this.getWarhead().serializeNBT());
		}
		if (this.getFuselage() != null) {
			tag.setTag("fuselage", this.getFuselage().serializeNBT());
		}
		if (this.getEngine() != null) {
			tag.setTag("engine", this.getEngine().serializeNBT());
		}

		NBTTagList chunkList = new NBTTagList();
		if (!chunks.isEmpty()) {
			for (ChunkPos chunk : chunks) {
				BlockPos pos = new BlockPos(chunk.x, 0, chunk.z);
				NBTTagCompound comp = new NBTTagCompound();
				comp.setIntArray("chunk", Utils.vec3iToArray(pos));
				chunkList.appendTag(comp);
			}
		}
		tag.setTag("chunkList", chunkList);

		tag.setIntArray("target", Utils.vec3iToArray(new BlockPos(target)));
		tag.setDouble("xVel", xVel);
		tag.setDouble("zVel", zVel);
		tag.setDouble("initialYVelocity", initialYVelocity);
		tag.setDouble("yConstant", yDecrement);
		tag.setDouble("yDiff", yDiff);
		tag.setBoolean("armed", isArmed);

		tag.setDouble("x", x);
		tag.setDouble("y", y);
		tag.setDouble("z", z);
	}

	@Override
	public void notifyDataManagerChange(DataParameter<?> key) {
		if (WARHEAD.equals(key)) {
			this.warhead = this.getWarheadDataManager();
		}
		if (FUSELAGE.equals(key)) {
			this.fuselage = this.getFuselageDataManager();
		}
		if (ENGINE.equals(key)) {
			this.engine = this.getEngineDataManager();
		}
		if (LAUNCHED.equals(key)) {
			this.launched = this.getLaunchedDataManager();
		}
		if (RENDER.equals(key)) {
			this.shouldRender = this.getRenderDataManager();
		}
	}

	public ItemStack getWarheadDataManager() {
		return ((ItemStack) this.dataManager.get(WARHEAD));
	}
	public ItemStack getFuselageDataManager() {
		return ((ItemStack) this.dataManager.get(FUSELAGE));
	}
	public ItemStack getEngineDataManager() {
		return ((ItemStack) this.dataManager.get(ENGINE));
	}
	public boolean getLaunchedDataManager() {
		return (Boolean) this.dataManager.get(LAUNCHED).booleanValue();
	}
	public boolean getRenderDataManager() {
		return (Boolean) this.dataManager.get(RENDER).booleanValue();
	}

	@Override
	public boolean shouldRenderInPass(int pass) {
		return true;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public net.minecraft.util.math.AxisAlignedBB getRenderBoundingBox() {
		return net.minecraft.tileentity.TileEntity.INFINITE_EXTENT_AABB;
	}

	@Override
	public void setVelocity(double d, double d1, double d2) {
		motionX = d;
		motionY = d1;
		motionZ = d2;
		if (prevRotationPitch == 0.0F && prevRotationYaw == 0.0F) {
			float f = MathHelper.sqrt(d * d + d2 * d2);
			prevRotationYaw = rotationYaw = (float) ((Math.atan2(d, d2) * 180D) / 3.1415927410125732D);
			prevRotationPitch = rotationPitch = (float) ((Math.atan2(d1, f) * 180D) / 3.1415927410125732D);
		}
	}

	public void getHeading() {
		float f3 = MathHelper.sqrt(motionX * motionX + motionZ * motionZ);
		prevRotationYaw = rotationYaw;
		prevRotationPitch = rotationPitch;
		rotationYaw = (float) ((Math.atan2(motionX, motionZ) * 180D) / 3.1415927410125732D);
		rotationPitch = (float) ((Math.atan2(motionY, f3) * 180D) / 3.1415927410125732D);
	}

	@Override
	public void writeSpawnData(ByteBuf buffer) {
		this.ignoreFrustumCheck = true;

	}

	@Override
	public void readSpawnData(ByteBuf additionalData) {
		this.setSize(WIDTH, HEIGHT);
		this.ignoreFrustumCheck = true;
		this.rotationPitch = 90;
	}

	public boolean canLaunch() {
		return warhead.getItem() instanceof Warhead && fuselage.getItem() instanceof Fuselage
				&& engine.getItem() instanceof Engine;
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// GETTERS AND SETTERS //
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	public ItemStack getWarhead() {
		return warhead;
	}

	public void setWarhead(ItemStack warhead) {
		if (warhead.getItem() instanceof Warhead || warhead.isEmpty()) {
			this.dataManager.set(WARHEAD, warhead);
			this.getDataManager().setDirty(WARHEAD);
			this.warhead = warhead;
			System.out.println(warhead.getDisplayName());
		} else {
			this.warhead = ItemStack.EMPTY;
		}
	}

	public ItemStack getFuselage() {
		return fuselage;
	}

	public void setFuselage(ItemStack fuselage) {
		if (fuselage.getItem() instanceof Fuselage || fuselage.isEmpty()) {
			this.dataManager.set(FUSELAGE, fuselage);
			this.getDataManager().setDirty(FUSELAGE);
			this.fuselage = fuselage;
			System.out.println(fuselage.getDisplayName());
		} else {
			this.fuselage = ItemStack.EMPTY;
		}
	}

	public ItemStack getEngine() {
		return engine;
	}

	public void setEngine(ItemStack engine) {
		if (engine.getItem() instanceof Engine || engine.isEmpty()) {
			this.dataManager.set(ENGINE, engine);
			this.getDataManager().setDirty(ENGINE);
			this.engine = engine;
			System.out.println(engine.getDisplayName());
		} else {
			this.engine = ItemStack.EMPTY;
		}
	}

	public boolean isLaunched() {
		return launched;
	}

	public void setLaunched(boolean launched) {
		this.dataManager.set(LAUNCHED, launched);
		this.getDataManager().setDirty(LAUNCHED);
		this.launched = launched;
	}

	public boolean isShouldRender() {
		return shouldRender;
	}

	public void setShouldRender(boolean shouldRender) {
		this.dataManager.set(RENDER, shouldRender);
		this.getDataManager().setDirty(RENDER);
		this.shouldRender = shouldRender;
	}

	public Vec3d getTarget() {
		return target;
	}

	public void setTarget(Vec3d target) {
		this.target = target;
	}

	public double getxVel() {
		return xVel;
	}

	public void setxVel(double xVel) {
		this.xVel = xVel;
	}

	public double getzVel() {
		return zVel;
	}

	public void setzVel(double zVel) {
		this.zVel = zVel;
	}

	public double getInitialYVelocity() {
		return initialYVelocity;
	}

	public void setInitialYVelocity(double initialYVelocity) {
		this.initialYVelocity = initialYVelocity;
	}

	public double getyDecrement() {
		return yDecrement;
	}

	public void setyDecrement(double yConstant) {
		this.yDecrement = yConstant;
	}

	public double getyDiff() {
		return yDiff;
	}

	public void setyDiff(double yDiff) {
		this.yDiff = yDiff;
	}

	@Nullable
	public Explosions getExplosionType() {
		if (warhead.getItem() instanceof Warhead) {
			Warhead item = (Warhead) warhead.getItem();
			return item.getExplosive();
		}
		return null;
	}

}
