package com.pheenixm.explosivesplus.common.entities;

import java.util.*;

import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTNTRainController;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;
import com.pheenixm.explosivesplus.common.util.Utils;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class EntityStealthBomber extends Entity {

	private static final double SPEED = 0.4D;
	public static final int Y_OFFSET = 88;
	public static final int RANGE = 128;
	public static final int NUMBER_VOLLEYS = 8;
	
	private double xSpeed;
	private double ySpeed;
	
	private Vec3d goalPos;
	private Vec3d origin;
	private boolean inGround;
	private int ticksInGround;
	
	public EntityStealthBomber(World world) {
		super(world);
		inGround = false;
		preventEntitySpawning = true;
		setSize(16F, 1F);
		this.ignoreFrustumCheck = true;
	}

	protected void entityInit() {
	}

	public EntityStealthBomber(World world, double x1, double y1, double z1, float angle) {
		this(world);
		setSize(20F, 10F);
		double y = Utils.getAverageHeight(world, (int)Math.floor(x1), (int)Math.floor(z1), 128, 128);
		setPosition(x1, y1, z1);
		initVals(x1,y1,z1,angle);
	}

	private void initVals(double x1, double y1, double z1, float angle) {
		Vec3d directionVec = Vec3d.fromPitchYaw(0, angle).normalize();
		origin = new Vec3d(x1,y1,z1);
		xSpeed = directionVec.x * SPEED;
		motionY = 0;
		ySpeed = directionVec.z * SPEED;
		
		prevRotationYaw = rotationYaw = angle;
		prevRotationPitch = rotationPitch = 0;
		
		goalPos = directionVec.scale(RANGE);
	}

	

	private void explode() {
		for (int i = -EntityTNTRainController.SPREAD + 2; i <= EntityTNTRainController.SPREAD - 2; i++) {
			for (int j = -EntityTNTRainController.SPREAD + 2; j < EntityTNTRainController.SPREAD - 2; j++) {
				EntityNuke nuke = new EntityNuke(world, posX + i * ConfigHandler.TNT_RAIN_SPACING, posY,
						posZ + j * ConfigHandler.TNT_RAIN_SPACING, this, false);
				nuke.setContactFuse(true);
				nuke.setFuse(EntityTNTRainController.LONG_FUSE);
				nuke.setSize(ConfigHandler.TNT_RAIN_BOMB_SIZE);
				world.spawnEntity(nuke);

			}
		}
	}

	public void onUpdate() {
		if(!world.isRemote) {
			super.onUpdate();
	
			lastTickPosX = posX;
			posY = lastTickPosY;
			lastTickPosZ = posZ;
	
			if (inGround) {
				if (world.isAirBlock(this.getPosition())) {
					inGround = false;
					ticksInGround = 0;
				} else {
					ticksInGround++;
					if (ticksInGround == 12) {
						if (!world.isRemote) {
							setDead();
							ExplosionHelper.createExplosion(world, null, posX, posY, posZ, Explosions.CLASSIC_MULTI, false, 40);
							System.out.println("Steal Bomber is Done");
						}
					}
					return;
				}
			} else {
	
			}
			Vec3d positionVec = new Vec3d(posX, posY, posZ);
			Vec3d vec3d1 = new Vec3d(posX + motionX, posY + motionY, posZ + motionZ);
	
			Entity entity = null;
			List<Entity> list = world.getEntitiesWithinAABBExcludingEntity(this,
					getEntityBoundingBox().offset(motionX, motionY, motionZ).expand(1.0D, 1.0D, 1.0D));
			double d = 0.0D;
			for (int i1 = 0; i1 < list.size(); i1++) {
				Entity entity1 = (Entity) list.get(i1);
				if (!entity1.canBeCollidedWith()) {
					continue;
				}
				float f4 = 0.3F;
				AxisAlignedBB axisalignedbb = entity1.getEntityBoundingBox().expand(f4, f4, f4);
				RayTraceResult movingobjectposition1 = axisalignedbb.calculateIntercept(positionVec, vec3d1);
				if (movingobjectposition1 == null) {
					continue;
				}
				double d1 = positionVec.distanceTo(movingobjectposition1.hitVec);
				if (d1 < d || d == 0.0D) {
					entity = entity1;
					d = d1;
				}
			}
	
			posX += xSpeed;
			posZ += ySpeed;
			setPosition(posX, posY, posZ);
			
			if(goalPos == null) {
				this.setDead();
				return;
			}
			double dist = positionVec.distanceTo(goalPos);
			int v1 = (int) (RANGE/SPEED);
			int v2 = v1 / NUMBER_VOLLEYS;
			if(ticksExisted % v2 == 0) {
				if (ticksExisted == v1) {
					setDead();
				}
				explode();
			}
		}
	}

	public void writeEntityToNBT(NBTTagCompound tag) {
		tag.setByte("inGround", (byte) (inGround ? 1 : 0));
		//TODO: Implement serialization
	}

	public void readEntityFromNBT(NBTTagCompound nbttagcompound) {
		inGround = nbttagcompound.getByte("inGround") == 1;

	}

	public boolean canBeCollidedWith() {
		return !isDead;
	}

	public float getShadowSize() {
		return 1.0F;
	}
}