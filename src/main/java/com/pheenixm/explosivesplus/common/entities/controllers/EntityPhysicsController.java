package com.pheenixm.explosivesplus.common.entities.controllers;

import java.util.HashMap;
import java.util.List;

import com.google.common.collect.Lists;
import com.pheenixm.explosivesplus.common.entities.physics.PhysicsBlockEntity;
import com.pheenixm.explosivesplus.common.explosions.Explosion;
import com.pheenixm.explosivesplus.common.util.Position;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.world.NoteBlockEvent.Play;

public class EntityPhysicsController extends Entity {
	
	public HashMap<BlockPos, PhysicsBlockEntity> blocks;
	public BlockPos centerBlock;
	public BlockPos centerPos;
	public BlockPos centerMass;
	
	
	public EntityPhysicsController(World world, BlockPos pos) {
		this(world);
		centerBlock = new BlockPos(pos);
		centerPos = new BlockPos((double)pos.getX(), (double) pos.getY(), (double) pos.getZ());
		centerMass = new BlockPos((double)pos.getX(), (double) pos.getY(), (double) pos.getZ());
	}
	
	public EntityPhysicsController(World world) {
		super(world);
	}
	
	@Override
	public void onUpdate() {
		super.onUpdate();
	}
	
	public boolean addBlockToEntity(BlockPos pos) {
		PhysicsBlockEntity block = new PhysicsBlockEntity(world, world.getBlockState(pos));
		
		if(block.hasTileEntity()) {
			//Tile entity handling
			//FALSE IS PLACEHOLDER
			return false;
		} else {
			BlockPos relativePos = pos.subtract(centerBlock);
			blocks.put(relativePos, block);
		}
		this.centerMass = recalculateCenterMass();
		return world.setBlockToAir(pos);
	}
	
	private BlockPos recalculateCenterMass() {
		//TODO: Give different blocks different weight		
		BlockPos center = new BlockPos(0,0,0);
		int count = blocks.keySet().size();
		for(BlockPos pos : blocks.keySet()) {
			center.add(pos);
		}
		return new BlockPos(center.getX() / count, center.getY() / count, center.getZ() / count);
	}
	
    private List<AxisAlignedBB> getCollisionBoxList(IBlockState bstate) {
        List<AxisAlignedBB> list = Lists.<AxisAlignedBB>newArrayList();
        	AxisAlignedBB center = new AxisAlignedBB(centerPos);
        	list.add(center);
        	
        	for(BlockPos relPos : blocks.keySet()) {
        		AxisAlignedBB nextBlock = center.offset(relPos);
        		list.add(nextBlock);
        	}
        	
        return list;
    }
 

	@Override
	protected void entityInit() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound compound) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound compound) {
		// TODO Auto-generated method stub
		
	}
	
	

}
