package com.pheenixm.explosivesplus.common.entities.controllable;

import java.util.Iterator;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageMouseInput;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;

public class EntityPredatorMissile extends EntityControllable {

	public static final double MAX_VELOCITY = 2.0D;
	public static final int VERTICAL_OFFSET = 150;

	boolean rightButtonDown; // is right mouse button down.

	public static double Speed = 10.0D;
	public static boolean missileSpawned = false;

	private int oldmouseX;
	private int oldmouseY;
	private int newmouseX;
	private int newmouseY;
	private int button;
	
	private boolean detonateNextTick;

	private static final float YAW = 0F;
	private static final float PITCH = 90F;
	private static final double CAM_OFFSET = 7;
	private static final double SENS = 0.03D;
	

	public EntityPredatorMissile(World world) {
		super(world);
		setSize(1.0F, 1.0F);
		
	}

	public EntityPredatorMissile(World world, EntityPlayer entityplayer,
			double x, double y, double z) {
		super(world, entityplayer, x, y, z);
		motionX = 0.0D; // Sets velocity in X direction 0
		motionY = 0.0D; // Sets velocity in Y direction 0
		motionZ = 0.0D; // Sets velocity in Z direction 0
		detonateNextTick = false;

		/*
		 * if(mod_ExplosivesPlus.advancedFeatures){ AxisAlignedBB boundingBox =
		 * AxisAlignedBB.getBoundingBox(-500.0D, -500.0D, -500.0D, 500.0D,
		 * 500.0D, 500.0D); AxisAlignedBB axisalignedbb = boundingBox.expand(4D,
		 * 2D, 4D); List list1 =
		 * worldObj.getEntitiesWithinAABB(EntityLiving.class, axisalignedbb);
		 * 
		 * for (int k = 0; k < list1.size(); k++) { EntityLiving entity =
		 * (EntityLiving)list1.get(k); ExplosivesPlusEntityRedSquare target =
		 * new ExplosivesPlusEntityRedSquare(worldObj, entity, this,
		 * entity.posX, entity.posY, entity.posZ);
		 * worldObj.spawnEntityInWorld(target); } }
		 */
	}

	@Override
	public void processMouseInput(int dx, int dy, int b) {
		oldmouseX = newmouseX;
		oldmouseY = newmouseY;

		newmouseX = dx;
		newmouseY = dy;
		button = b;
	}

	@Override
	public void onClientUpdate() {
		int newmouseX = Minecraft.getMinecraft().mouseHelper.deltaX;
		int newmouseY = Minecraft.getMinecraft().mouseHelper.deltaY;
		int buttonDown = 0;
		if(this.ticksExisted > 10) { 
			buttonDown = getMouseButton();
		}
		MessageMouseInput mouse = new MessageMouseInput(newmouseX, newmouseY,
				buttonDown, this.getEntityId());
		PacketHandler.NETWORK.sendToServer(mouse);

		this.newmouseX = newmouseX;
		this.newmouseY = newmouseY;
	}

	@Override
	public void updateMovement() {
		if(detonateNextTick || isDead) {
			//Call this method twice to ensure it is processed
			camera.doReset();
			//Detonate
			detonate();
		}
		if (collidedVertically || posY < 0) {
			System.out.println("Set dead");
			camera.doReset();
			detonateNextTick = !detonateNextTick;
		} else {
			// determines your mouse movement and set it as change in x-Z
			// velocity.
			motionX -= (newmouseX) * SENS;
			// 0.03D sets the mouse sensitivity
			motionZ += (newmouseY) * SENS;
			// sets the maximum velocity limits. //was 4
			if (motionX > MAX_VELOCITY) {
				motionX = MAX_VELOCITY;
			}
			if (motionX < -MAX_VELOCITY) {
				motionX = -MAX_VELOCITY;
			}
			if (motionZ > MAX_VELOCITY) {
				motionZ = MAX_VELOCITY;
			}
			if (motionZ < -MAX_VELOCITY) {
				motionZ = -MAX_VELOCITY;
			}
			if (button == 1) {
				motionY = -4D;
			}

			// makes the missile fly down
			motionY += -10.0D * 0.005D; // 10.0D sets how fast it's falling
			this.move(MoverType.SELF, motionX, motionY, motionZ);
		}
	}

	@Override
	public void onServerUpdate() {}

	private void detonate() {
		ExplosionHelper.createExplosion(world, null, posX, posY, posZ,
				Explosions.CLASSIC,false, 20);
		this.setDead();
		/*
		 * AxisAlignedBB boundingBox = AxisAlignedBB.getBoundingBox(-500.0D,
		 * -500.0D, -500.0D, 500.0D, 500.0D, 500.0D); AxisAlignedBB
		 * axisalignedbb = boundingBox.expand(4D, 2D, 4D); List list1 =
		 * worldObj.getEntitiesWithinAABB(ExplosivesPlusEntityRedSquare. class,
		 * axisalignedbb); for (int k = 0; k < list1.size(); k++) {
		 * ExplosivesPlusEntityRedSquare entity =
		 * (ExplosivesPlusEntityRedSquare)list1.get(k); entity.setDead(); }
		 */
	}

	@Override
	public void serialize(NBTTagCompound tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deserialize(NBTTagCompound tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getCameraY() {
		return CAM_OFFSET;
	}

	@Override
	public boolean getLockAngle() {
		return true;
	}

	@Override
	public float getLockPitch() {
		return PITCH;
	}

	@Override
	public float getLockYaw() {
		return YAW;
	}
}