package com.pheenixm.explosivesplus.common.entities.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.entities.EntityNuke;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityFallingBlocks;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;

public class EntityTNTRainController extends Entity {

	private int stormTimer;
	public static final int SPREAD = 5;
	public static final int TIME_BETWEEN_LAYERS = 80;
	public static final int LONG_FUSE = 300;

	public EntityTNTRainController(World world, double x, double y, double z, int layers) {
		this(world);
		setPosition(x, y, z);
		stormTimer = layers * TIME_BETWEEN_LAYERS;
		ticksExisted = 0;
		this.ignoreFrustumCheck = true;
	}

	public EntityTNTRainController(World world) {
		super(world);
	}

	public void onUpdate() {
		if (!world.isRemote) {

			prevPosX = posX;
			prevPosY = posY;
			prevPosZ = posZ;

			if (stormTimer % TIME_BETWEEN_LAYERS == 0) {
				System.out.println(stormTimer);
				for (int i = -SPREAD; i <= SPREAD; i++) {
					for (int j = -SPREAD; j < SPREAD; j++) {
						EntityNuke nuke = new EntityNuke(world, posX + i * ConfigHandler.TNT_RAIN_SPACING, posY,
								posZ + j * ConfigHandler.TNT_RAIN_SPACING, this, true);
						nuke.setContactFuse(true);
						nuke.setFuse(LONG_FUSE);
						nuke.setSize(ConfigHandler.TNT_RAIN_BOMB_SIZE);
						world.spawnEntity(nuke);

					}
				}
			}
			stormTimer--;

			if (stormTimer <= 0) {
				this.setDead();
			}
		}
	}

	@Override
	protected void entityInit() {

	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound compound) {
		setStormTimer(compound.getInteger("fuse"));
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound compound) {
		compound.setInteger("fuse", getStormTimer());
	}

	public int getStormTimer() {
		return stormTimer;
	}

	public void setStormTimer(int stormTimer) {
		this.stormTimer = stormTimer;
	}

}
