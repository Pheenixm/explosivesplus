package com.pheenixm.explosivesplus.common.entities;

import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.explosions.ExplosionBiomeBuster;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;

public class EntityBiomeBuster extends EntityFusedBomb {

	public EntityBiomeBuster(World world) {
		super(world);
		// TODO Auto-generated constructor stub
	}

	public EntityBiomeBuster(World world, double x, double y, double z,
			Entity igniter) {
		super(world, x, y, z, igniter);
		this.parentBlock = ExplosivesPlusHolder.BIOME.getDefaultState().withProperty(ExplosivesBlock.EXPLODE, false);
	}

	@Override
	public void detonate() {
		if (!world.isRemote) {
			ExplosionBiomeBuster explosion = new ExplosionBiomeBuster(world,
					null, posX, posY, posZ);
			explosion.initiate(parentBlock, false);
		}
	}

	@Override
	public void playEffects() {
        this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.posX, this.posY + 0.5D, this.posZ, 0.0D, 1.0D, 0.0D);		
	}

}
