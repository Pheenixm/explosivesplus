package com.pheenixm.explosivesplus.common.entities;

import java.lang.ref.WeakReference;

import com.pheenixm.explosivesplus.api.entities.IViewableTile;
import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.entities.missiles.EntityBallisticMissile;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageCameraReset;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

//TODO: Break this into interfaces for simplicity
//TODO: Fix the newer components of this
public class EntityViewer extends Entity implements IEntityAdditionalSpawnData {

	public WeakReference<EntityPlayer> controller;

	private int thirdPersonBackup;
	private boolean hideGuiBackup;
	private int particleBackup;
	private float FOVBackup;
	private int mipmapBackup;
	private int widthBackup;
	private int heightBackup;
	
	@SideOnly(Side.CLIENT)
	public Framebuffer framebuffer;

	@SideOnly(Side.CLIENT)
	protected Minecraft mc = Minecraft.getMinecraft();

	/**
	 * Controls whether this camera
	 * should be attached to another 
	 * entity, or simply exists in free space
	 */
	protected boolean freeCam;

	protected WeakReference<Entity> ride;
	
	//TODO: Make this more abstract
	protected int renderHeight;
	protected int renderWidth;
	protected boolean shouldRender;
	
	/**Null for all cameras that have no viewport
	* TODO: Implement system for this
	*/
	public IViewableTile viewport = null;
	

	// Variables to be sent between client/server
	protected boolean isAngleLocked;
	protected int playerId;


	
	
	private static final DataParameter<Boolean> ANGLE_LOCK = EntityDataManager
			.createKey(EntityViewer.class, DataSerializers.BOOLEAN);
	private static final DataParameter<Integer> PLAYER = EntityDataManager
			.createKey(EntityViewer.class, DataSerializers.VARINT);

	public EntityViewer(World world, double x, double y, double z,
			EntityPlayer player, boolean freeCam) {
		this(world);
		this.setPosition(x, y, z);
		controller = new WeakReference<EntityPlayer>(player);
		this.setPlayerId(controller.get().getEntityId());
		this.freeCam = freeCam;
	}

	public EntityViewer(World world) {
		super(world);
	}
	
	
	@Override
	public void onUpdate() {
		super.onUpdate();
		if (world.isRemote) {
			doClientUpdate();
		} else {
			if (controller == null) {
				this.setDead();
				System.out.println("Null controller");
				return;
			}

		}
		
		if(this.isRiding()) {
			if(ride == null && !this.getRidingEntity().isDead) {
				ride = new WeakReference<Entity>(this.getRidingEntity());
			}
			if(ride.get() != null && ride.get().isDead) {
				System.out.println("Tried to reset camera");
				resetCamera();
				this.setDead();
			}
		}
		if(ride != null && (ride.get() == null || ride.get().isDead)) {
			System.out.println("Hard reset attemtped");
			resetCamera();
			this.setDead();

		}
		if (this.isDead) {
			resetCamera();
			System.out.println("Tried to reset camera");
		}

	}
	
	public void doReset() {
		if(!world.isRemote) {
			MessageCameraReset reset = new MessageCameraReset(this.getEntityId());
			PacketHandler.NETWORK.sendTo(reset, (EntityPlayerMP)controller.get());
		}
		resetCamera();
		Minecraft.getMinecraft().gameSettings.hideGUI = hideGuiBackup;
		this.setDead();
	}

	private void doClientUpdate() {
		
		if(!freeCam && !this.isDead) {
			Minecraft mc = Minecraft.getMinecraft();
			GameSettings settings = mc.gameSettings;
	
			//TODO: Reenable this if an obvious reason shows up
			settings.hideGUI = true;
		}
		// settings.fovSetting = 0F;

		/*
		//FIXER METHOD IS UNNECESSARY
		if(mc.player.getEntityId() == playerId) {
			if(mc.getRenderViewEntity() != this) {
				if(!(this instanceof EntityTargeter)) {
					this.setupCamera();
				}
			}
		}
		*/
	}

	public void resetCamera() {
		if (world.isRemote) {
			Minecraft mc = Minecraft.getMinecraft();
			GameSettings settings = mc.gameSettings;
			settings.thirdPersonView = thirdPersonBackup;
			settings.hideGUI = hideGuiBackup;
			settings.particleSetting = particleBackup;
			settings.fovSetting = FOVBackup;

			mc.setRenderViewEntity(controller.get());
            settings.mipmapLevels = mipmapBackup;
            mc.displayWidth = widthBackup;
            mc.displayHeight = heightBackup;
            
		} 
		else {}
	}

	public void setupCamera() {
		if (world.isRemote) {
			Minecraft mc = Minecraft.getMinecraft();
			GameSettings settings = mc.gameSettings;

			if(this.controller == null) {
				controller = new WeakReference<EntityPlayer>(mc.player);
			}
			
			thirdPersonBackup = settings.thirdPersonView;
			hideGuiBackup = settings.hideGUI;
			particleBackup = settings.particleSetting;
			FOVBackup = settings.fovSetting;
			mipmapBackup = settings.mipmapLevels;
			widthBackup = mc.displayWidth;
			heightBackup = mc.displayHeight;

			mc.setRenderViewEntity(this);
		}
	}
	
	public void setWidthAndHeight(int w, int h) {
		setWidth(w);
		setHeight(h);
	}
	
	@SideOnly(Side.CLIENT)
	public void initFramebuffer() {
		this.framebuffer = new Framebuffer(renderWidth, renderHeight, true);
	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound compound) {
		this.setPlayerId(compound.getInteger("id"));
		Entity ent = world.getEntityByID(playerId);
		if (ent instanceof EntityPlayer) {
			controller = new WeakReference<EntityPlayer>((EntityPlayer) ent);
		}
		this.setAngleLocked(compound.getBoolean("angle_lock"));
		thirdPersonBackup = compound.getInteger("3pb");
		hideGuiBackup = compound.getBoolean("hideGui");
		particleBackup = compound.getInteger("part");
		FOVBackup = compound.getFloat("fov");
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound compound) {
		compound.setInteger("id", playerId);
		compound.setBoolean("angle_lock", getAngleDataManager());
		compound.setInteger("3pb", thirdPersonBackup);
		compound.setInteger("part", particleBackup);
		compound.setBoolean("hideGui", hideGuiBackup);
		compound.setFloat("fov", FOVBackup);
	}

	@Override
	protected void entityInit() {
		dataManager.register(ANGLE_LOCK, false);
		dataManager.register(PLAYER, -1);
	}

	@Override
	public void notifyDataManagerChange(DataParameter<?> key) {
		if (ANGLE_LOCK.equals(key)) {
			this.isAngleLocked = this.getAngleDataManager();
		}
		if (PLAYER.equals(key)) {
			this.setPlayerId(this.getPlayerDataManager());
		}
	}

	private boolean getAngleDataManager() {
		return (Boolean) this.dataManager.get(ANGLE_LOCK);
	}

	private int getPlayerDataManager() {
		return (Integer) this.dataManager.get(PLAYER);
	}

	public boolean isAngleLocked() {
		return isAngleLocked;
	}

	public void setAngleLocked(boolean isAngleLocked) {
		this.dataManager.set(ANGLE_LOCK, isAngleLocked);
		this.dataManager.setDirty(ANGLE_LOCK);
		this.isAngleLocked = isAngleLocked;
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.dataManager.set(PLAYER, playerId);
		this.dataManager.setDirty(PLAYER);
		this.playerId = playerId;
	}

	@Override
	public void writeSpawnData(ByteBuf buffer) {
		buffer.writeBoolean(freeCam);
		buffer.writeInt(playerId);
		buffer.writeInt(renderHeight);
		buffer.writeInt(renderWidth);
	}

	@Override
	/**
	 * Automatically sets the viewport var to this entity for the player
	 * who spawned it
	 */
	public void readSpawnData(ByteBuf buffer) {
		freeCam = buffer.readBoolean();
		playerId = buffer.readInt();
		setHeight(buffer.readInt());
		setWidth(buffer.readInt());
		if(freeCam) {
			if(Minecraft.getMinecraft().player.getEntityId() == playerId) {
				//TODO: Something with this method, like adding the camera to the maps(?)
			}
		}
	}

	public int getHeight() {
		return renderHeight;
	}

	public void setHeight(int height) {
		this.renderHeight = height;
	}

	public int getWidth() {
		return renderWidth;
	}

	public void setWidth(int width) {
		this.renderWidth = width;
	}

	public boolean shouldRender() {
		return shouldRender;
	}

	public void setShouldRender(boolean shouldRender) {
		this.shouldRender = shouldRender;
	}

	public boolean isFreeCam() {
		return freeCam;
	}

	public void setFreeCam(boolean freeCam) {
		this.freeCam = freeCam;
	}

}
