package com.pheenixm.explosivesplus.common.entities;



import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityNovaController;
import com.pheenixm.explosivesplus.common.entities.controllers.EntityTheoryOfChaosController;
import com.pheenixm.explosivesplus.common.explosions.ExplosionBiomeBuster;
import com.pheenixm.explosivesplus.common.explosions.ExplosionChaos;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper;
import com.pheenixm.explosivesplus.common.explosions.ExplosionNova;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntityMolecularDisruptor extends EntityFusedBomb
{
	
    public EntityMolecularDisruptor(World world, double x, double y, double z, Entity igniter) {
		super(world, x, y, z, igniter);
		this.parentBlock = ExplosivesPlusHolder.DISRUPT.getDefaultState().withProperty(ExplosivesBlock.EXPLODE, false);
	}
    
    public EntityMolecularDisruptor(World world) {
    		super(world);
    }

	@Override
	public void detonate() {
		ExplosionHelper.createExplosion(world, null, posX, posY, posZ, Explosions.DISRUPTOR, false);
	}

	@Override
	public void playEffects() {
		
	}

}
