package com.pheenixm.explosivesplus.common.entities;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFalling;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.item.EntityFallingBlock;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;

public class EntityPhysicsBlock extends EntityFallingBlock implements IEntityAdditionalSpawnData {

	private IBlockState fallTile;
	public float scale;

	public EntityPhysicsBlock(World world) {
		super(world);
	}

	public EntityPhysicsBlock(World world, double x, double y, double z, IBlockState state) {
		super(world, x, y, z, state);
		fallTile = state;
		fallTime = -1000;
		scale = 1;
	}

	@Override
	public void onUpdate() {
		Block block = this.fallTile.getBlock();

		if (this.fallTile.getMaterial() == Material.AIR) {
			this.setDead();
		} else {
			this.prevPosX = this.posX;
			this.prevPosY = this.posY;
			this.prevPosZ = this.posZ;

			if (this.fallTime++ == 0) {
				BlockPos blockpos = new BlockPos(this);

				if (this.world.getBlockState(blockpos).getBlock() == block) {
					this.world.setBlockToAir(blockpos);
				} else if (!this.world.isRemote) {
					this.setDead();
					return;
				}
			}

			if (!this.hasNoGravity()) {
				this.motionY -= 0.03999999910593033D;
			}

			this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);

			if (!this.world.isRemote) {
				BlockPos blockpos1 = new BlockPos(this);

				double d0 = this.motionX * this.motionX + this.motionY * this.motionY + this.motionZ * this.motionZ;

				if (!this.onGround) {
					if (this.fallTime > 100 && !this.world.isRemote && (blockpos1.getY() < 1 || blockpos1.getY() > 256)
							|| this.fallTime > 600) {
						if (this.shouldDropItem && this.world.getGameRules().getBoolean("doEntityDrops")) {
							this.entityDropItem(new ItemStack(block, 1, block.damageDropped(this.fallTile)), 0.0F);
						}

						this.setDead();
					}
				} else {
					IBlockState iblockstate = this.world.getBlockState(blockpos1);

					if (this.world.isAirBlock(new BlockPos(this.posX, this.posY - 0.009999999776482582D, this.posZ))) // Forge:
					if (BlockFalling.canFallThrough(this.world.getBlockState(new BlockPos(this.posX, this.posY - 0.009999999776482582D, this.posZ)))) {
						this.onGround = false;
						return;
					}

					this.motionX *= 0.699999988079071D;
					this.motionZ *= 0.699999988079071D;
					this.motionY *= -0.5D;

					if (iblockstate.getBlock() != Blocks.PISTON_EXTENSION) {
						this.setDead();

						if (this.world.mayPlace(block, blockpos1, true, EnumFacing.UP, (Entity) null)
								&& !BlockFalling.canFallThrough(this.world.getBlockState(blockpos1.down()))
								&& this.world.setBlockState(blockpos1, this.fallTile, 3)) {
							if (block instanceof BlockFalling) {
								((BlockFalling) block).onEndFalling(this.world, blockpos1, this.fallTile, iblockstate);
							}

							//Copy tile data
							if (this.tileEntityData != null && block.hasTileEntity(this.fallTile)) {
								TileEntity tileentity = this.world.getTileEntity(blockpos1);

								if (tileentity != null) {
									NBTTagCompound nbttagcompound = tileentity.writeToNBT(new NBTTagCompound());

									for (String s : this.tileEntityData.getKeySet()) {
										NBTBase nbtbase = this.tileEntityData.getTag(s);

										if (!"x".equals(s) && !"y".equals(s) && !"z".equals(s)) {
											nbttagcompound.setTag(s, nbtbase.copy());
										}
									}

									tileentity.readFromNBT(nbttagcompound);
									tileentity.markDirty();
								}
							}
						} else if (this.shouldDropItem && this.world.getGameRules().getBoolean("doEntityDrops")) {
							this.entityDropItem(new ItemStack(block, 1, block.damageDropped(this.fallTile)), 0.0F);
						}

					}
				}
			}

			//Causes decay in motion
			this.motionX *= 0.9800000190734863D;
			//this.motionY *= 0.9800000190734863D;
			this.motionZ *= 0.9800000190734863D;
            //world.spawnParticle(EnumParticleTypes.BLOCK_DUST, posX, posY + 1, posZ, 2, 2, 2, Block.getStateId(fallTile));
            //scale -= 0.00125;

		}
	}
	
	public IBlockState getBlock() {
		return fallTile;
	}

	@Override
	public void writeSpawnData(ByteBuf buffer) {
		buffer.writeInt(Block.getStateId(fallTile));
	}

	@Override
	public void readSpawnData(ByteBuf buffer) {
		fallTile = Block.getStateById(buffer.readInt());
		scale = 1;
	}
	
    protected void writeEntityToNBT(NBTTagCompound tag)
    {
    		super.writeEntityToNBT(tag);
    		tag.setInteger("tile", Block.getStateId(fallTile));
    }

    protected void readEntityFromNBT(NBTTagCompound tag) {
    		super.readEntityFromNBT(tag);
    		fallTile = Block.getStateById(tag.getInteger("tile"));
    }
}
