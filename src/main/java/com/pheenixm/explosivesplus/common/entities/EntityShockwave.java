package com.pheenixm.explosivesplus.common.entities;

import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;

public class EntityShockwave extends Entity implements IEntityAdditionalSpawnData {

	private int size;
	private int time; 
	public float radius;
	public float prevRadius;
	private float factor;
	private float f2;
		
	
	public EntityShockwave(World world) {
		super(world);
	}
	
	public EntityShockwave(World world, double x, double y, double z) {
		this(world);
		setPosition(x,y,z);
		size = ConfigHandler.SHOCKWAVE_DEFAULT_SIZE;
		time = ConfigHandler.SHOCKWAVE_DEFAULT_TIME;
		motionX = motionY = motionZ = 0;
	}
	
	public void setSizeAndTime(int size, int time) {
		this.size = size;
		this.time = time;
	}
	
	@Override
	public void onUpdate() {
		super.onUpdate();
		motionX = motionY = motionZ = 0;
		factor = 1F;
		f2 = (float)((float)7F/time)*time;
		if(ticksExisted + f2 >= time) {
			//factor = (float)(Math.pow(Math.E, (6 - (time - ticksExisted))/2)/6);
			//factor = (float)(Math.pow(Math.E, (f2 - (time - ticksExisted))/Math.E)/(f2));
			factor = 1F + ((float)(f2 - (time - ticksExisted)))/100F * 5F;
		}
		if(ticksExisted++ >= time) {
			this.setDead();
			return;
		}
		prevRadius = radius;
		float scale = (float)ticksExisted/time;
		radius = (float)	size * scale * factor;
	}
	

	@Override
	protected void entityInit() {
	}

    public boolean isImmuneToExplosions()
    {
        return true;
    }

	@Override
	protected void readEntityFromNBT(NBTTagCompound tag) {
		this.time = tag.getInteger("time");
		this.size = tag.getInteger("size");
		ticksExisted = tag.getInteger("ticks");
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound tag) {
		tag.setInteger("time", time);
		tag.setInteger("size", size);
		tag.setInteger("ticks", ticksExisted);
	}

	@Override
	public void writeSpawnData(ByteBuf buffer) {
		buffer.writeInt(size);
		buffer.writeInt(time);
		buffer.writeInt(ticksExisted);
		if(!world.isRemote) {
			buffer.writeInt(this.getEntityId());
		}
	}

	@Override
	public void readSpawnData(ByteBuf buffer) {
		size = buffer.readInt();
		time = buffer.readInt();
		ticksExisted = buffer.readInt();
		if(world.isRemote) {
			RenderUtils.shockwaves.add(buffer.readInt());
		}
	}

}
