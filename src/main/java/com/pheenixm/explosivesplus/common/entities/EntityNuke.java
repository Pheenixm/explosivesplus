package com.pheenixm.explosivesplus.common.entities;

import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper;
import com.pheenixm.explosivesplus.common.explosions.ExplosionHelper.Explosions;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;

public class EntityNuke extends EntityFusedBomb {
	
	private int size;
	private boolean classic;

    public EntityNuke(World world, double x, double y, double z, Entity igniter, boolean classic) {
		super(world, x, y, z, igniter);
		this.parentBlock = ExplosivesPlusHolder.MEGA.getDefaultState().withProperty(ExplosivesBlock.EXPLODE, false);
		size = ConfigHandler.NUKE_SIZE;
		this.classic = classic;
	}
    
    public EntityNuke(World world) {
    		super(world);
    }

	
	@Override
	public void detonate() {
		if(classic) {
			ExplosionHelper.createExplosion(world, null, posX, posY, posZ, Explosions.CLASSIC, false, size);
		} else {
			ExplosionHelper.createExplosion(world, null, posX, posY, posZ, Explosions.CLASSIC_MULTI, false, size);
		}
	}

	@Override
	public void playEffects() {
        this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, this.posX, this.posY + 0.5D, this.posZ, 0.0D, 1.0D, 0.0D);		
	}
	
	public void setSize(int s) {
		this.size = s;
	}

}
