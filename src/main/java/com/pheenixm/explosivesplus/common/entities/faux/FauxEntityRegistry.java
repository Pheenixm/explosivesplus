package com.pheenixm.explosivesplus.common.entities.faux;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.base.Predicates;
import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntitySpawnHandler.EnumFauxEntity;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.network.messages.MessageFauxEntitySpawn;
import com.pheenixm.explosivesplus.common.network.messages.MessageFauxEntityUpdate;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.storage.MapStorage;
import net.minecraft.world.storage.WorldSavedData;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

//TODO: I'm not entirely sure why this class works, but it does
public class FauxEntityRegistry extends WorldSavedData {

	
	private static final String DATA_NAME = ExplosivesPlus.MOD_ID + "_ENTITY_Data";
	public static final int KEEP_CHUNKS_LOADED = 1200;

	/**
	 * On initializing, each transmitter grabs its reference from this map and connects to the receivers on its list
	 */
	public Map<Integer, FauxTickingEntity> entityMap = new ConcurrentHashMap<>();

	private World world;
	private HashMap<EntityPlayerMP, NBTTagList> updateMap = new HashMap<>();
	private Map<int[], NBTTagCompound> spawnList = new HashMap<>();
	private Map<int[], Integer> chunkMap = new HashMap<>();
	public int entityId = 0;
	private boolean firstTick = true;
	
	public FauxEntityRegistry(String name) {
		super(name);
	}
	
	

	public static void load(World world) {
		if(world == null) {
			System.out.println("How was this called with a null world?");
		}
		MapStorage storage = world.getPerWorldStorage();
		FauxEntityRegistry instance = (FauxEntityRegistry) storage.getOrLoadData(FauxEntityRegistry.class, DATA_NAME);
		if (instance == null) {
			instance = new FauxEntityRegistry(DATA_NAME);
			storage.setData(DATA_NAME, instance);
		}
		instance.world = world;
		instance.spawnAllEnts();
		//Can iterate through list here if you so desire
		System.out.println("Created faux entity registry");
	}
	
	public static FauxEntityRegistry getRegistry(World world) {
		MapStorage storage = world.getPerWorldStorage();
		FauxEntityRegistry instance = (FauxEntityRegistry) storage.getOrLoadData(FauxEntityRegistry.class, DATA_NAME);
		if (instance == null) {
			instance = new FauxEntityRegistry(DATA_NAME);
			storage.setData(DATA_NAME, instance);
			instance.world = world;
			FMLCommonHandler.instance().bus().register(instance);
		}
		return instance;
	}

	@SubscribeEvent
	public void onTickInGame(WorldTickEvent event) {
		if(event.phase.equals(Phase.END)) {
			if(firstTick) {
				firstTick = false;
				FauxEntityRegistry.load(event.world);
			}
			int realDim = world.provider.getDimension();
			int eventDim = event.world.provider.getDimension();
			//System.out.println("Dimension correct " + (realDim == eventDim));
			//System.out.println("World is server " + !world.isRemote);

			if(realDim == eventDim) {
				getRegistry(event.world).updateEntities();
				
			}
		}
	}

	public <T extends FauxTickingEntity> T getEntity(int id) {
		return (T)entityMap.get(id);
	}
	
	public int spawnEntity(FauxTickingEntity ent, int... id) {
		int entId = 0;
		if(id.length > 0) {
			entId = id[0];
		} else {
			entityId++;
			entId = entityId;
		}
		while(entityMap.containsKey(entId)) {
			entityId++;
			entId = entityId;
		}
		ent.entityId = entId;
		entityMap.put(entId, ent);
		MessageFauxEntitySpawn spawn = new MessageFauxEntitySpawn(ent.getEntityType(), world.provider.getDimension(), entId);
		PacketHandler.NETWORK.sendToAll(spawn);
		this.markDirty();
		return entId;
	}
	
	public void forceChunks(ChunkPos pos, int... flightTime) {
		if(world == null || !world.isRemote) {
			return;
		}
		ExplosivesPlus.instance.forceChunkLoad(world, pos);
		int time = KEEP_CHUNKS_LOADED;
		if(flightTime.length > 0) {
			time = flightTime[0];
		}
		int[] posit = {pos.x, pos.z};
		chunkMap.put(posit, time);
	}
	
	public void handleSpawn(EnumFauxEntity enumFaux, int id) {
		FauxTickingEntity ent = FauxEntitySpawnHandler.getEntityFromEnum(world, enumFaux);
		entityMap.put(id, ent);
	}
	
	/**
	 * Sends updates to players on a per-player basis
	 * determines whether updates should be sent based on a distance calculation
	 */
	public void updateEntities() {
		Iterator<int[]> chunkIter = chunkMap.keySet().iterator();
		while(chunkIter.hasNext()) {
			int[] chunkPos = chunkIter.next();
			int timer = chunkMap.get(chunkPos);
			timer--;
			if(timer <= 0) {
				ExplosivesPlus.instance.releaseChunkLoad(world, new ChunkPos(chunkPos[0], chunkPos[1]));
				chunkIter.remove();
			} else {
				chunkMap.put(chunkPos, timer);
			}
		}
		
		Iterator<Integer> iter = entityMap.keySet().iterator();
		WorldServer ws = (WorldServer)world;
		while(iter.hasNext()) {
			int id = iter.next();
			FauxTickingEntity ent = entityMap.get(id);
			if(ent == null || ent.isDead) {
				iter.remove();
			} else {
				ent.onUpdate();
			}
			if(ent != null) {
				//Send an update tag, even if ent is dead
				NBTTagCompound updateTag = ent.getUpdateTag();
				updateTag = updateTag == null ? new NBTTagCompound() : updateTag;
				updateTag.setInteger("entityId", id);
				updateTag.setBoolean("isDead", ent.isDead);
				for(EntityPlayerMP player : ws.getPlayers(EntityPlayerMP.class, Predicates.alwaysTrue())) {
					if(ent.getDistance(player.posX, player.posY, player.posZ) <= ent.getTrackingDistance()) {
						if(!updateMap.containsKey(player)) {
							updateMap.put(player, new NBTTagList());
						}
						updateMap.get(player).appendTag(updateTag);
					}
				}
			}
		}
		for(EntityPlayerMP player : updateMap.keySet()) {
			if(player != null) {
				NBTTagCompound update = new NBTTagCompound();
				update.setTag("updates", updateMap.get(player));
				MessageFauxEntityUpdate updatePacket = new MessageFauxEntityUpdate(update, world.provider.getDimension());
				PacketHandler.NETWORK.sendTo(updatePacket, player);
				updateMap.put(player, new NBTTagList());
			}
		}
	}
	
	//TODO: Add custom rendering method (more generic than BatchRender<?>) in RenderUtils
	public void handleUpdateTag(NBTTagCompound updateTag) {
		NBTTagList list = updateTag.getTagList("updates", Constants.NBT.TAG_COMPOUND);
		for(NBTBase uncast : list) {
			NBTTagCompound tag = (NBTTagCompound)uncast;
			int id = tag.getInteger("entityId");
			if(entityMap.containsKey(id)) {
				if(tag.getBoolean("isDead")) {
					entityMap.remove(id);
				} else {
					FauxTickingEntity ent = entityMap.get(id);
					if(ent != null) {
						ent.handleUpdateTag(tag);
					}
				}
			}
		}
	}
	
	public void spawnAllEnts() {
		for(int[] pair : spawnList.keySet()) {
			int id = pair[0];
			int typeId = pair[1];
			EnumFauxEntity entType = EnumFauxEntity.get(typeId);
			 if(!entityMap.containsKey(id)) {
				 if(world == null) { 
					 System.out.println("World is null");
				 }
				 FauxTickingEntity ent = FauxEntitySpawnHandler.getEntityFromEnum(world, entType);
				 ent.readFromNBT(spawnList.get(pair));
				 this.spawnEntity(ent, id);
			 }
		}
	}
		
	@Override
	public void readFromNBT(NBTTagCompound com) {
		NBTTagList list = com.getTagList("entityList", Constants.NBT.TAG_COMPOUND);
		for(NBTBase uncast : list) {
			NBTTagCompound tag = (NBTTagCompound)uncast;
			int id = tag.getInteger("entityId");
			int typeId = tag.getInteger("type");
			int[] pair = {id, typeId};
			spawnList.put(pair, tag);
		}
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound tag) {
		NBTTagList fullMap = new NBTTagList();
		for(int id : entityMap.keySet()) {
			FauxTickingEntity ent = entityMap.get(id);
			NBTTagCompound saveTag = ent.writeToNBT();
			saveTag.setInteger("entityId", id);
			saveTag.setInteger("type", ent.getEntityType().ordinal());
			fullMap.appendTag(saveTag);
		}
		tag.setTag("entityList", fullMap);
		return tag;
	}	
	
	
	@SubscribeEvent 
	public static void onWorldLoaded(WorldEvent.Load event) {
		World world = event.getWorld();
		if(world != null && !world.isRemote) {
			FauxEntityRegistry.load(world);
		}
	}

}
