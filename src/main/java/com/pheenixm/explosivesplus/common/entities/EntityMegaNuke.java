package com.pheenixm.explosivesplus.common.entities;

import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.explosions.ExplosionMega;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;

public class EntityMegaNuke extends EntityFusedBomb {

	public EntityMegaNuke(World world) {
		super(world);
	}

	public EntityMegaNuke(World world, double x, double y, double z,
			Entity igniter) {
		super(world, x, y, z, igniter);
		this.parentBlock = ExplosivesPlusHolder.MEGA.getDefaultState().withProperty(ExplosivesBlock.EXPLODE, false);
	}

	@Override
	public void detonate() {
		if(!world.isRemote) {
			ExplosionMega explosion = new ExplosionMega(world,
					null, posX, posY, posZ, ExplosionMega.DEFAULT_SIZE);
			explosion.initiate(parentBlock, false);
		}
	}

	@Override
	public void playEffects() {
		
	}

}
