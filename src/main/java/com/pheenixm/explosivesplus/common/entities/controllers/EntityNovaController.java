package com.pheenixm.explosivesplus.common.entities.controllers;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SPacketChunkData;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.lwjgl.opengl.GL11;

import com.google.common.base.Predicates;
import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.api.entities.ICustomRender;
import com.pheenixm.explosivesplus.common.RegistryHandler;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntity;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityFallingBlocks;
import com.pheenixm.explosivesplus.common.util.Position;
import com.pheenixm.explosivesplus.common.util.RenderUtils;

import io.netty.buffer.ByteBuf;

//TODO: Implement a fix to the problem of entity rendering
//Implement faux entities as particles to solve problem?
public class EntityNovaController extends Entity implements ICustomRender {

	private int deathTimer = 0;
	public static final int DEATH_TIME = 100;
	public static final int CONVERT_TICKS = 5;
	
	public EntityNovaController(World world, double x, double y, double z, int size) {
		super(world);
		this.world = world;
		posX = x;
		posY = y;
		posZ = z;
		setSize(1, 256);
		radius = size;
		this.setFuse(80);
		explosionSize = 7.5F;
		ticksExisted = 0;
		fallingBlocks = new HashMap<Integer, FauxEntityFallingBlocks>();
		attractors = new ArrayList<EntityNovaController>(10);
		exploded = false;
		attractors.add(this);
		chunks = new HashMap<ChunkPos, Integer>();
		this.ignoreFrustumCheck = true;
	}

	public EntityNovaController(World world) {
		super(world);
	}
	
    @SideOnly(Side.CLIENT)
    public boolean isInRangeToRender3d(double x, double y, double z)
    {
    		return true;
    }

	/**
	 * This will not do ANYTHING on the logical client, nor should it
	 */
	@Override
	public void onUpdate() {
		this.notifyDataManagerChange(ENTITIES);
		if (!world.isRemote) {
			fuse--;
			if (fuse <= 0) {
				if (!exploded) {
					explode();
					exploded = true;
				}
				if (fallingBlocks != null && fallingBlocks.isEmpty()) {
					deathTimer++;
					if(deathTimer >= DEATH_TIME) {
						this.setDead();
					}
				}
			}

			if (ticksExisted < radius) {
				convertBlocks(ticksExisted);
			} else {
				if (ticksExisted == radius) {
					List<EntityNovaController> nearby = this.findNearbyNovas();
					attractors.addAll(nearby);
					System.out.println("Registered nearby nova" + nearby.size()
							+ " # of attractors = " + attractors.size());
					for (EntityNovaController controller : nearby) {
						if (!controller.attractors.contains(this)) {
							controller.attractors.add(this);
						}
					}
				}
			}

			if (fallingBlocks != null && !fallingBlocks.isEmpty()) {
				Iterator<EntityNovaController> iterPos = attractors.iterator();
				while (iterPos.hasNext()) {
					EntityNovaController nearbyNova = iterPos.next();
					if (nearbyNova.isDead) {
						iterPos.remove();
					} else {
						Iterator<FauxEntityFallingBlocks> iterator = nearbyNova.fallingBlocks
								.values().iterator();
						while (iterator.hasNext()) {
							FauxEntityFallingBlocks entity = iterator
									.next();
							double distance = entity.getDistance(posX, posY,
									posZ);
							if (distance < 256) {
								entity.onUpdate();
								if (!exploded) {
									double d4 = distance
											/ (double) explosionSize;
									double d6 = entity.posX - posX;
									double d8 = entity.posY - posY;
									double d10 = entity.posZ - posZ;
									double d11 = MathHelper.sqrt(
											d6 * d6 + d8 * d8 + d10 * d10);
									d6 /= d11;
									d8 /= d11;
									d10 /= d11;
									double d12 = 1D;
									double d13 = (0.7D - d4) * d12;
									double d14 = d13;

									//Originally 2D
									entity.motionX -= d6 * d14 / 3D;
									entity.motionY -= d8 * d14 / 3D;
									entity.motionZ -= d10 * d14 / 3D;
								}
							}
							if (entity.isDead) {
								iterator.remove();
							}
						}
					}
				}
			}
		}
	}

	public void convertBlocks(int r) {
		
		//double ratio = (double) ticksExisted/CONVERT_TICKS;
		//r *= ratio;
	
		int i = MathHelper.floor(posX);
		int j = MathHelper.floor(posY);
		int k = MathHelper.floor(posZ);

		explosionSize *= 3F;
		// Set radius here

		for (int x = -r; x < r; x++) {
			for (int y = -r; y < r; y++) { // pos2
				for (int z = -r; z < r; z++) {
					double dist = MathHelper.sqrt((x * x + y * y + z * z));
					if (dist <= r) {
						BlockPos position = new BlockPos(i + x, j + y, k + z);
						ChunkPos chunkPos = new ChunkPos(position);
						IBlockState state = world.getBlockState(position);
						if (!world.isAirBlock(position)) {
							
							world.setBlockState(position, Blocks.AIR.getDefaultState(), 2);
							FauxEntityFallingBlocks block = new FauxEntityFallingBlocks(
									world, i + x, j + y, k + z, state);
							fallingBlocks.put(fallingBlocks.size(), block);
			                world.markBlockRangeForRenderUpdate(i+x, j+y, k+z, i+ x + 1, j+ y + 1, k+z + 1);
						}
					}
				}
			}
		}
		

	}

	public void explode() {
		if (fallingBlocks != null && !fallingBlocks.isEmpty()) {
			for (Object entity1 : fallingBlocks.values()) {
				FauxEntity entity = (FauxEntity) entity1;
				double distance = entity.getDistance(posX, posY, posZ);
				if (distance < 256) {
					explosionSize = 7.5F;
					explosionSize *= 3F;

					double d4 = entity.getDistance(posX, posY, posZ)
							/ (double) explosionSize;
					double d6 = entity.posX - posX;
					double d8 = entity.posY - posY;
					double d10 = entity.posZ - posZ;
					double d11 = MathHelper.sqrt(d6 * d6 + d8 * d8 + d10 * d10);
					d6 /= d11;
					d8 /= d11;
					d10 /= d11;
					double d12 = 1D;
					double d13 = (0.7D - d4) * d12;
					double d14 = d13;

					if (attractors.size() > 1) {
						d8 *= 3;
					}

					if (entity.posY > posY) {
						entity.motionX += d6 * d14 * 3D;
						entity.motionY += d8 * d14 * 2D;
						entity.motionZ += d10 * d14 * 3D;
					} else {
						entity.motionX -= d6 * d14 * 4D;
						entity.motionY -= d8 * d14 * 2D;
						entity.motionZ -= d10 * d14 * 4D;
					}
					if (entity instanceof FauxEntityFallingBlocks) {
						FauxEntityFallingBlocks block = (FauxEntityFallingBlocks) entity;
						block.canLand = true;
					}
				}
			}
		}
	}

	public List<EntityNovaController> findNearbyNovas() {
		List<EntityNovaController> inter = world.getEntities(
				EntityNovaController.class,
				Predicates.instanceOf(EntityNovaController.class));
		List<EntityNovaController> list = new ArrayList<EntityNovaController>();
		for (EntityNovaController ent : inter) {
			if (ent != this && !attractors.contains(ent)) {
				list.add(ent);
			}
		}
		return list;
	}

	private NBTTagCompound getNBTFromLists() {
		// Write fallingBlocks to NBT
		NBTTagCompound compound = new NBTTagCompound();
		if (fallingBlocks == null || attractors == null) {
			return compound;
		}
		NBTTagList falling = new NBTTagList();

		for (int i = 0; i < fallingBlocks.size(); i++) {
			if (fallingBlocks.containsKey(i)) {
				FauxEntityFallingBlocks block = fallingBlocks.get(i);
				NBTTagCompound tag = FauxEntityFallingBlocks.getNBTFromFallingBlock(block);
				tag.setInteger("id", i);
				falling.appendTag(tag);
			}
		}

		NBTTagList novas = new NBTTagList();
		for (EntityNovaController nova : attractors) {
			NBTTagCompound tag = new NBTTagCompound();
			tag.setInteger("id", nova.getEntityId());
			novas.appendTag(tag);
		}

		compound.setTag("fallingBlocks", falling);
		compound.setTag("attractors", novas);

		return compound;
	}


	private EntityNovaController getNovaControllerFromNBT(
			NBTTagCompound compound) {
		return (EntityNovaController) world
				.getEntityByID(compound.getInteger("id"));
	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound compound) {
		this.dataManager.set(ENTITIES, compound.getCompoundTag("lists"));
		this.buildListsFromNBT();
		this.setFuse(compound.getInteger("fuse"));
		this.setExploded(compound.getBoolean("exploded"));
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound compound) {
		compound.setTag("lists", this.getNBTFromLists());
		compound.setInteger("fuse", this.getFuse());
		compound.setBoolean("exploded", this.getExploded());
	}

	// DATA MANAGER METHODS

	protected void entityInit() {
		this.dataManager.register(FUSE, Integer.valueOf(80));
		this.dataManager.register(EXPLODED, false);

		NBTTagCompound compound = this.getNBTFromLists();
		this.dataManager.register(ENTITIES, compound);
		if(world.isRemote && !RenderUtils.customRender.contains(new WeakReference<Entity>(this))) {
			RenderUtils.customRender.add(new WeakReference<ICustomRender>(this));
		}
	}

	public void setFuse(int fuseIn) {
		this.dataManager.set(FUSE, Integer.valueOf(fuseIn));
		this.fuse = fuseIn;
	}

	public int getFuse() {
		return this.fuse;
	}

	public void setExploded(boolean ex) {
		this.dataManager.set(EXPLODED, ex);
		this.exploded = ex;
	}

	public boolean getExploded() {
		return this.exploded;
	}

	public void setEntities() {
		NBTTagCompound compound = this.getNBTFromLists();
		this.dataManager.set(ENTITIES, compound);
		// this.buildListsFromNBT();
	}

	@Override
	public void notifyDataManagerChange(DataParameter<?> key) {
		if (FUSE.equals(key)) {
			this.fuse = this.getFuseDataManager();
		}
		if (EXPLODED.equals(key)) {
			this.exploded = this.getExplodedDataManager();
		}
		if (ENTITIES.equals(key)) {
			if (world.isRemote) {
				this.buildListsFromNBT();
			} else {
				this.setEntities();
			}
		}
	}

	/**
	 * Gets the fuse from the data manager
	 */
	public int getFuseDataManager() {
		return ((Integer) this.dataManager.get(FUSE)).intValue();
	}

	public boolean getExplodedDataManager() {
		return this.dataManager.get(EXPLODED).booleanValue();
	}

	private void buildListsFromNBT() {
		NBTTagList falling = this.dataManager.get(ENTITIES)
				.getTagList("fallingBlocks", Constants.NBT.TAG_COMPOUND);
		NBTTagList novas = this.dataManager.get(ENTITIES)
				.getTagList("attractors", Constants.NBT.TAG_COMPOUND);

		if (attractors == null) {
			attractors = new ArrayList<EntityNovaController>(10);
		}
		fallingBlocks = new HashMap<Integer, FauxEntityFallingBlocks>();

		for (NBTBase block1 : falling) {
			NBTTagCompound blockCompound = (NBTTagCompound) block1;
			int id = blockCompound.getInteger("id");
			if (!fallingBlocks.containsKey(id)) {
				FauxEntityFallingBlocks block = FauxEntityFallingBlocks.getFallingBlockFromNBT(world, blockCompound);
				fallingBlocks.put(id, block);
			} else {
				FauxEntityFallingBlocks existingBlock = fallingBlocks
						.get(id);
				fallingBlocks.replace(id, FauxEntityFallingBlocks.updateExistingBlockFromNBT(existingBlock, blockCompound));
			}
		}
		for (NBTBase att1 : novas) {
			NBTTagCompound novaCompound = (NBTTagCompound) att1;
			EntityNovaController attractor = getNovaControllerFromNBT(
					novaCompound);
			if (!attractors.contains(attractor)) {
				attractors.add(attractor);
			}
		}
	}
	
	@Override
    public AxisAlignedBB getRenderBoundingBox()
    {
        return new AxisAlignedBB(posX - 0.5D, 0, posZ - 0.5D, posX + 0.5D, world.getActualHeight(), posZ+0.5D);
    }

	@Override
	public boolean equals(Object o) {
		if (o instanceof EntityNovaController) {
			EntityNovaController nova = (EntityNovaController) o;
			return this.getEntityId() == nova.getEntityId();
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getEntityId();
	}

	private float explosionSize;

	private int fuse;
	private boolean exploded;
	public HashMap<Integer, FauxEntityFallingBlocks> fallingBlocks;
	private ArrayList<EntityNovaController> attractors;
	private HashMap<ChunkPos, Integer> chunks;

	//TODO: Make this configurable
	private int radius = DEFAULT_RADIUS;
	public static final int DEFAULT_RADIUS = 13;
	
	private static final DataParameter<Integer> FUSE = EntityDataManager
			.<Integer> createKey(EntityNovaController.class,
					DataSerializers.VARINT);
	private static final DataParameter<Boolean> EXPLODED = EntityDataManager
			.<Boolean> createKey(EntityNovaController.class,
					DataSerializers.BOOLEAN);
	private static final DataParameter<NBTTagCompound> ENTITIES = EntityDataManager
			.<NBTTagCompound> createKey(EntityNovaController.class,
					DataSerializers.COMPOUND_TAG);


	@Override
	@SideOnly(Side.CLIENT)
	public void render(double x, double y, double z, float partialTicks) {		
		for(FauxEntityFallingBlocks block : fallingBlocks.values()) {
			if(!block.inBlock || block.ticksExisted < 30) {
				double x1 = block.posX;//block.prevPosX + (block.posX - block.prevPosX) * (double)partialTicks;
				double y1 = block.posY;//block.prevPosY + (block.posY - block.prevPosY) * (double)partialTicks;
				double z1 = block.posZ;//block.prevPosZ + (block.posZ - block.prevPosZ) * (double)partialTicks;

				float diffX = (float) (x1 - posX);
				float diffY = (float) (y1 - posY);
				float diffZ = (float) (z1 - posZ);
				
				float blockX = (float) x + diffX;
				float blockY = (float) y + diffY;
				float blockZ = (float) z + diffZ;
				if(block.block.getBlock() instanceof BlockLiquid) {
					//renderFluid(blockX, blockY, blockZ, block);
				} else {
					RenderUtils.renderBlock(blockX, blockY, blockZ, block, this, Tessellator.getInstance().getBuffer());
				}
			}
		}

	}
	
	@Override
	public double[] getPos()	 {
		double[] pos = {posX, posY, posZ};
		return pos;
	}
	
	@Override
	public boolean shouldRemove() {
		return isDead;
	}

	@Override
	public double getRenderDistance() {
		// TODO Auto-generated method stub
		return 256D;
	}
	
}
