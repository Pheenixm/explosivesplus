package com.pheenixm.explosivesplus.common.entities.faux;

import com.pheenixm.explosivesplus.api.entities.ICustomRender;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntitySpawnHandler.EnumFauxEntity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public abstract class FauxTickingEntity extends FauxEntity implements ICustomRender{

	public int entityId;
	
	public FauxTickingEntity(World world) {
		super(world);
	}
	
	public abstract void doClientUpdate();
	
	public abstract int getTrackingDistance();
	
	public abstract EnumFauxEntity getEntityType();
	
	public abstract NBTTagCompound serialize();
	
	public abstract void deserialize(NBTTagCompound tag);

	public NBTTagCompound writeToNBT() {
		NBTTagCompound tag = this.serialize();
		tag.setDouble("prevPosX", prevPosX);
		tag.setDouble("prevPosY", prevPosY);
		tag.setDouble("prevPosZ", prevPosZ);
		
		tag.setDouble("posX", posX);
		tag.setDouble("posY", posY);
		tag.setDouble("posZ", posZ);
		
		tag.setDouble("motionX", motionX);
		tag.setDouble("motionY", motionY);
		tag.setDouble("motionZ", motionZ);
		
		tag.setFloat("rotationPitch", rotationPitch);
		tag.setFloat("rotationYaw", rotationYaw);
		tag.setFloat("prevRotationPitch", prevRotationPitch);
		tag.setFloat("prevRotationYaw", prevRotationYaw);
		
		tag.setBoolean("isDead", isDead);
		
		return tag;
	}

	public void readFromNBT(NBTTagCompound tag) {
		this.deserialize(tag);
		
		this.prevPosX = tag.getDouble("prevPosX");
		this.prevPosY = tag.getDouble("prevPosY");
		this.prevPosZ = tag.getDouble("prevPosZ");
		
		this.posX = tag.getDouble("posX");
		this.posY = tag.getDouble("posY");
		this.posZ = tag.getDouble("posZ");

		this.motionX = tag.getDouble("motionX");
		this.motionY = tag.getDouble("motionY");
		this.motionZ = tag.getDouble("motionZ");
		
		this.rotationPitch = tag.getFloat("rotationPitch");
		this.prevRotationPitch = tag.getFloat("prevRotationPitch");
		this.rotationYaw = tag.getFloat("rotationYaw");
		this.prevRotationYaw = tag.getFloat("prevRotationYaw");
		
		this.isDead = tag.getBoolean("isDead");

	}
	
	public void handleUpdateHelper(NBTTagCompound tag) {
		if(tag.hasKey("posX")) {
			posX = tag.getDouble("posX");
			prevPosX = tag.getDouble("prevPosX");
		}
		if(tag.hasKey("posY")) {
			posY = tag.getDouble("posY");
			prevPosY = tag.getDouble("prevPosY");
		}
		if(tag.hasKey("posZ")) {
			posZ = tag.getDouble("posZ");
			prevPosZ = tag.getDouble("prevPosZ");
		}
		if(tag.hasKey("rotationPitch")) {
			rotationPitch = tag.getFloat("rotationPitch");
			prevRotationPitch = tag.getFloat("prevRotationPitch");
		}
		if(tag.hasKey("rotationYaw")) {
			rotationYaw = tag.getFloat("rotationYaw");
			prevRotationYaw = tag.getFloat("prevRotationYaw");
		}

		isDead = tag.getBoolean("isDead");
	}
	
	public NBTTagCompound updateTagHelper(boolean flag) {
		NBTTagCompound tag = new NBTTagCompound();
		if(flag || prevPosX != posX) {
			tag.setDouble("prevPosX", prevPosX);
			tag.setDouble("posX", posX);
			prevPosX = posX;
		}
		if(flag || prevPosY != posY) {
			tag.setDouble("prevPosY", prevPosY);
			tag.setDouble("posY", posY);
			prevPosY = posY;
		}
		if(flag || prevPosZ != posZ) {
			tag.setDouble("prevPosZ", prevPosZ);
			tag.setDouble("posZ", posZ);
			prevPosZ = posZ;
		}
		
		if(flag || rotationPitch != prevRotationPitch) {
			tag.setFloat("rotationPitch", rotationPitch);
			tag.setFloat("prevRotationPitch", prevRotationPitch);
			prevRotationPitch = rotationPitch;
		}
		if(flag || rotationYaw != prevRotationYaw) {
			tag.setFloat("rotationYaw", rotationYaw);
			tag.setFloat("prevRotationYaw", prevRotationYaw);
			prevRotationYaw = rotationYaw;
		}

		tag.setBoolean("isDead", isDead);
		return tag;
	}

}
