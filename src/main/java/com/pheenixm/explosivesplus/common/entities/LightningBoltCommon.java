package com.pheenixm.explosivesplus.common.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.pheenixm.explosivesplus.common.util.WRVector3;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.particle.Particle;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.potion.Potion;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

public class LightningBoltCommon {
/*
	public class BoltPoint {

		WRVector3 point;
		WRVector3 basepoint;
		WRVector3 offsetvec;
		final LightningBoltCommon this$0;

		public BoltPoint(WRVector3 basepoint, WRVector3 offsetvec) {
			super();
			this$0 = LightningBoltCommon.this;
			point = basepoint.copy().add(offsetvec);
			this.basepoint = basepoint;
			this.offsetvec = offsetvec;
		}
	}

	public class Segment {

		public void calcDiff() {
			diff = endpoint.point.copy().sub(startpoint.point);
		}

		public void calcEndDiffs() {
			if (prev != null) {
				WRVector3 prevdiffnorm = prev.diff.copy().normalize();
				WRVector3 thisdiffnorm = diff.copy().normalize();
				prevdiff = thisdiffnorm.add(prevdiffnorm).normalize();
				sinprev = (float) Math.sin(WRVector3.anglePreNorm(thisdiffnorm, prevdiffnorm.scale(-1F)) / 2.0F);
			} else {
				prevdiff = diff.copy().normalize();
				sinprev = 1.0F;
			}
			if (next != null) {
				WRVector3 nextdiffnorm = next.diff.copy().normalize();
				WRVector3 thisdiffnorm = diff.copy().normalize();
				nextdiff = thisdiffnorm.add(nextdiffnorm).normalize();
				sinnext = (float) Math.sin(WRVector3.anglePreNorm(thisdiffnorm, nextdiffnorm.scale(-1F)) / 2.0F);
			} else {
				nextdiff = diff.copy().normalize();
				sinnext = 1.0F;
			}
		}

		@Override
		public String toString() {
			return (new StringBuilder(String.valueOf(startpoint.point.toString()))).append(" ")
					.append(endpoint.point.toString()).toString();
		}

		public BoltPoint startpoint;
		public BoltPoint endpoint;
		public WRVector3 diff;
		public Segment prev;
		public Segment next;
		public WRVector3 nextdiff;
		public WRVector3 prevdiff;
		public float sinprev;
		public float sinnext;
		public float light;
		public int segmentno;
		public int splitno;
		final LightningBoltCommon this$0;

		public Segment(BoltPoint start, BoltPoint end, float light, int segmentnumber, int splitnumber) {
			super();
			this$0 = LightningBoltCommon.this;
			startpoint = start;
			endpoint = end;
			this.light = light;
			segmentno = segmentnumber;
			splitno = splitnumber;
			calcDiff();
		}

		public Segment(WRVector3 start, WRVector3 end) {
			this(new BoltPoint(start, new WRVector3(0.0D, 0.0D, 0.0D)),
					new BoltPoint(end, new WRVector3(0.0D, 0.0D, 0.0D)), 1.0F, 0, 0);
		}
	}

	public class SegmentLightSorter implements Comparator {
		public int compare(Segment o1, Segment o2) {
			return Float.compare(o2.light, o1.light);
		}

		@Override
		public int compare(Object obj, Object obj1) {
			return compare((Segment) obj, (Segment) obj1);
		}

		final LightningBoltCommon this$0;

		public SegmentLightSorter() {
			super();
			this$0 = LightningBoltCommon.this;
		}
	}

	public class SegmentSorter implements Comparator {

		public int compare(Segment o1, Segment o2) {
			int comp = Integer.valueOf(o1.splitno).compareTo(Integer.valueOf(o2.splitno));
			if (comp == 0)
				return Integer.valueOf(o1.segmentno).compareTo(Integer.valueOf(o2.segmentno));
			else
				return comp;
		}

		@Override
		public int compare(Object obj, Object obj1) {
			return compare((Segment) obj, (Segment) obj1);
		}

		final LightningBoltCommon this$0;

		public SegmentSorter() {
			super();
			this$0 = LightningBoltCommon.this;
		}
	}

	public LightningBoltCommon(World world, WRVector3 jammervec, WRVector3 targetvec, long seed) {
		segments = new ArrayList();
		splitparents = new HashMap();
		canhittarget = true;
		this.world = world;
		start = jammervec;
		end = targetvec;
		this.seed = seed;
		rand = new Random(seed);
		numsegments0 = 1;
		increment = 1;
		length = end.copy().sub(start).length();
		particleMaxAge = (3 + rand.nextInt(3)) - 1;
		multiplier = 1f;
		particleAge = -(int) (length * 3F);
		boundingBox = new AxisAlignedBB(Math.min(start.x, end.x), Math.min(start.y, end.y), Math.min(start.z, end.z),
				Math.max(start.x, end.x), Math.max(start.y, end.y), Math.max(start.z, end.z)).expand(length / 2,
						length / 2, length / 2);
		segments.add(new Segment(start, end));

	}

	public LightningBoltCommon(World world, Entity detonator, Entity target, long seed) {
		this(world, new WRVector3(detonator), new WRVector3(target), seed);

	}

	public LightningBoltCommon(World world, Entity detonator, Entity target, long seed, int speed) {
		this(world, new WRVector3(detonator), new WRVector3(target.posX,
				target.posY + (double) target.getEyeHeight() - 0.69999998807907104D, target.posZ), seed);
		increment = speed;
		multiplier = .4f;
	}

	public LightningBoltCommon(World world, BlockPos detonator, Entity target, long seed) {
		this(world, new WRVector3(detonator), new WRVector3(target), seed);
	}

	public LightningBoltCommon(World world, BlockPos detonator, double x, double y, double z, long seed) {
		this(world, new WRVector3(detonator), new WRVector3(x, y, z), seed);
	}

	public LightningBoltCommon(World world, double x1, double y1, double z1, double x, double y, double z, long seed,
			int duration, float multi) {
		this(world, new WRVector3(x1, y1, z1), new WRVector3(x, y, z), seed);
		particleMaxAge = (duration + rand.nextInt(duration)) - (duration / 2);
		multiplier = multi;
	}

	public LightningBoltCommon(World world, double x1, double y1, double z1, double x, double y, double z, long seed,
			int duration, float multi, int speed) {
		this(world, new WRVector3(x1, y1, z1), new WRVector3(x, y, z), seed);
		particleMaxAge = (duration + rand.nextInt(duration)) - (duration / 2);
		multiplier = multi;
		increment = speed;
	}

	public void setWrapper(Particle entity) {
		wrapper = entity;
	}

	public void setMultiplier(float m) {
		multiplier = m;
	}

	public void fractal(int splits, float amount, float splitchance, float splitlength, float splitangle) {
		if (finalized)
			return;
		ArrayList oldsegments = segments;
		segments = new ArrayList();
		Segment prev = null;
		for (Iterator iterator = oldsegments.iterator(); iterator.hasNext();) {
			Segment segment = (Segment) iterator.next();
			prev = segment.prev;
			WRVector3 subsegment = segment.diff.copy().scale(1.0F / splits);
			BoltPoint newpoints[] = new BoltPoint[splits + 1];
			WRVector3 startpoint = segment.startpoint.point;
			newpoints[0] = segment.startpoint;
			newpoints[splits] = segment.endpoint;
			for (int i = 1; i < splits; i++) {
				WRVector3 randoff = WRVector3.getPerpendicular(segment.diff).rotate(rand.nextFloat() * 360F,
						segment.diff);
				randoff.scale((rand.nextFloat() - 0.5F) * amount);
				WRVector3 basepoint = startpoint.copy().add(subsegment.copy().scale(i));
				newpoints[i] = new BoltPoint(basepoint, randoff);
			}

			for (int i = 0; i < splits; i++) {
				Segment next = new Segment(newpoints[i], newpoints[i + 1], segment.light,
						segment.segmentno * splits + i, segment.splitno);
				next.prev = prev;
				if (prev != null)
					prev.next = next;
				if (i != 0 && rand.nextFloat() < splitchance) {
					WRVector3 splitrot = WRVector3.xCrossProduct(next.diff).rotate(rand.nextFloat() * 360F, next.diff);
					WRVector3 diff = next.diff.copy().rotate((rand.nextFloat() * 0.66F + 0.33F) * splitangle, splitrot)
							.scale(splitlength);
					numsplits++;
					splitparents.put(Integer.valueOf(numsplits), Integer.valueOf(next.splitno));
					Segment split = new Segment(newpoints[i],
							new BoltPoint(newpoints[i + 1].basepoint, newpoints[i + 1].offsetvec.copy().add(diff)),
							segment.light / 2.0F, next.segmentno, numsplits);
					split.prev = prev;
					segments.add(split);
				}
				prev = next;
				segments.add(next);
			}

			if (segment.next != null)
				segment.next.prev = prev;
		}

		numsegments0 *= splits;
	}

	public void defaultFractal() {
		fractal(2, length * multiplier / 8F, 0.7F, 0.1F, 45F);
		fractal(2, length * multiplier / 12F, 0.5F, 0.1F, 50F);
		fractal(2, length * multiplier / 17F, 0.5F, 0.1F, 55F);
		fractal(2, length * multiplier / 23F, 0.5F, 0.1F, 60F);
		fractal(2, length * multiplier / 30F, 0.0F, 0.0F, 0.0F);
		fractal(2, length * multiplier / 34F, 0.0F, 0.0F, 0.0F);
		fractal(2, length * multiplier / 40F, 0.0F, 0.0F, 0.0F);
	}

	private void vecBBDamageSegment(WRVector3 start, WRVector3 end, ArrayList entitylist) {
		Vec3d start3D = start.toVec3D();
		Vec3d end3D = end.toVec3D();
		try {
			for (Iterator iterator = entitylist.iterator(); iterator.hasNext();) {

				Entity entity = (Entity) iterator.next();
				if ((entity instanceof EntityLiving) && (entity.getEntityBoundingBox()
						.contract((entity.getEntityBoundingBox().maxX - entity.getEntityBoundingBox().minX) / 2.6,
								(entity.getEntityBoundingBox().maxY - entity.getEntityBoundingBox().minY) / 2.6,
								(entity.getEntityBoundingBox().maxZ - entity.getEntityBoundingBox().minZ) / 2.6)
						.contains(start3D)
						|| entity.getEntityBoundingBox()
								.contract(
										(entity.getEntityBoundingBox().maxX - entity.getEntityBoundingBox().minX) / 2.6,
										(entity.getEntityBoundingBox().maxY - entity.getEntityBoundingBox().minY) / 2.6,
										(entity.getEntityBoundingBox().maxZ - entity.getEntityBoundingBox().minZ) / 2.6)
								.contains(end3D))) {
					if (wrapper == null)// || !(wrapper instanceof EntityLiving))
						switch (type) {
							case 0 :
								entity.attackEntityFrom(DamageSource.MAGIC, damage);
								//poisonBolt(entity, Potion.getPotionById(), 2);
								break;
							case 1 :
								entity.attackEntityFrom(DamageSource.MAGIC, damage);
								break;
							case 2 :
								entity.attackEntityFrom(DamageSource.MAGIC, damage);
								//poisonBolt(entity, Potion.moveSlowdown, 2);
								break;
							case 3 :
								entity.attackEntityFrom(DamageSource.MAGIC, damage);
								//poisonBolt(entity, Potion.poison, 1);
								break;
							case 4 :
								entity.attackEntityFrom(DamageSource.MAGIC, damage);
								entity.setFire(2);
								break;
							case 5 :
								entity.attackEntityFrom(DamageSource.MAGIC, damage);
								//poisonBolt(entity, Potion.blindness, 1);
								break;
						}
					else
						switch (type) {
							case 0 :
								entity.attackEntityFrom(DamageSource.causeIndirectMagicDamage(wrapper, wrapper),
										damage);
								//poisonBolt(entity, Potion.confusion, 2);
								break;
							case 1 :
								entity.attackEntityFrom(DamageSource.causeIndirectMagicDamage(wrapper, wrapper),
										damage);
								break;
							case 2 :
								entity.attackEntityFrom(DamageSource.causeIndirectMagicDamage(wrapper, wrapper),
										damage);
								//poisonBolt(entity, Potion.moveSlowdown, 2);
								break;
							case 3 :
								entity.attackEntityFrom(DamageSource.causeIndirectMagicDamage(wrapper, wrapper),
										damage);
								//poisonBolt(entity, Potion.poison, 1);
								break;
							case 4 :
								entity.attackEntityFrom(DamageSource.causeIndirectMagicDamage(wrapper, wrapper),
										damage);
								entity.setFire(2);
								break;
							case 5 :
								entity.attackEntityFrom(DamageSource.causeIndirectMagicDamage(wrapper, wrapper),
										damage);
								//poisonBolt(entity, Potion.blindness, 1);
								break;
						}
				}
			}
		} catch (Exception e) {
		}
	}

	private void poisonBolt(Entity entity, Potion poison, int durmod) {
		byte byte0 = 0;
	    if(world.getDifficulty() != EnumDifficulty.PEACEFUL)
	    {
	        if(world.getDifficulty() == EnumDifficulty.NORMAL)
	        {
	            byte0 = 3;
	        } else
	        if(world.getDifficulty() == EnumDifficulty.HARD)
	        {
	            byte0 = 6;
	        }
	    }
	    if(byte0 > 0)
	    {
	       // ((EntityLiving)entity).addPotionEffect(new PotionEffect(poison.id, byte0 * 20 * durmod, 0));
	    }
	}

	private void bbTestEntityDamage() {
		if (nonLethal)
			return;
		List nearentities = world.getEntitiesWithinAABBExcludingEntity(wrapper, boundingBox);
		if (nearentities.size() == 0)
			return;
		for (Iterator iterator = segments.iterator(); iterator.hasNext();) {
			Segment segment = (Segment) iterator.next();
			vecBBDamageSegment(segment.startpoint.point, segment.endpoint.point, (ArrayList) nearentities);
		}

	}

	private float rayTraceResistance(WRVector3 start, WRVector3 end, float prevresistance) {
		RayTraceResult mop = world.rayTraceBlocks(start.toVec3D(), end.toVec3D());
		if (mop == null)
			return prevresistance;
		if (mop.typeOfHit == RayTraceResult.Type.BLOCK) {
			BlockPos hit = new BlockPos(mop.hitVec);
			IBlockState state = world.getBlockState(hit);
			if (world.isAirBlock(hit)) {
				return prevresistance;
			}
			return prevresistance + (state.getBlock().getExplosionResistance(null) + 0.3F);
		} else {
			return prevresistance;
		}
	}

	private void calculateCollisionAndDiffs() {
		HashMap lastactivesegment = new HashMap();
		Collections.sort(segments, new SegmentSorter());
		int lastsplitcalc = 0;
		int lastactiveseg = 0;
		float splitresistance = 0.0F;
		for (Iterator iterator = segments.iterator(); iterator.hasNext();) {
			Segment segment = (Segment) iterator.next();
			if (segment.splitno > lastsplitcalc) {
				lastactivesegment.put(Integer.valueOf(lastsplitcalc), Integer.valueOf(lastactiveseg));
				lastsplitcalc = segment.splitno;
				lastactiveseg = ((Integer) lastactivesegment.get(splitparents.get(Integer.valueOf(segment.splitno))))
						.intValue();
				splitresistance = lastactiveseg >= segment.segmentno ? 0 : 50;
			}
			if (splitresistance < 40F * segment.light) {
				// splitresistance =
				// rayTraceResistance(segment.startpoint.point,
				// segment.endpoint.point, splitresistance);
				lastactiveseg = segment.segmentno;
			}
		}

		lastactivesegment.put(Integer.valueOf(lastsplitcalc), Integer.valueOf(lastactiveseg));
		lastsplitcalc = 0;
		lastactiveseg = ((Integer) lastactivesegment.get(Integer.valueOf(0))).intValue();
		Segment segment;
		for (Iterator iterator = segments.iterator(); iterator.hasNext(); segment.calcEndDiffs()) {
			segment = (Segment) iterator.next();
			if (lastsplitcalc != segment.splitno) {
				lastsplitcalc = segment.splitno;
				lastactiveseg = ((Integer) lastactivesegment.get(Integer.valueOf(segment.splitno))).intValue();
			}
			if (segment.segmentno > lastactiveseg)
				iterator.remove();
		}

		if (((Integer) lastactivesegment.get(Integer.valueOf(0))).intValue() + 1 < numsegments0)
			canhittarget = false;
	}

	public void finalizeBolt() {
		if (finalized) {
			return;
		} else {
			finalized = true;
			calculateCollisionAndDiffs();
			Collections.sort(segments, new SegmentLightSorter());
			return;
		}
	}

	public void onUpdate() {
		particleAge += increment;
		if (particleAge > particleMaxAge)
			particleAge = particleMaxAge;
		bbTestEntityDamage();
	}

	ArrayList segments;
	WRVector3 start;
	WRVector3 end;
	HashMap splitparents;
	public float multiplier;
	public float length;
	public int numsegments0;
	public int increment;
	public int type = 0;
	public boolean nonLethal = false;
	private int numsplits;
	private boolean finalized;
	private boolean canhittarget;
	private Random rand;
	public long seed;
	public int particleAge;
	public int particleMaxAge;
	private AxisAlignedBB boundingBox;
	private World world;
	public Particle wrapper;
	public static final float speed = 3F;
	public static final int fadetime = 20;
	public static int damage;*/
}
