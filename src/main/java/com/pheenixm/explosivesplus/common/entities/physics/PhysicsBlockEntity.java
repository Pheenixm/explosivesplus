package com.pheenixm.explosivesplus.common.entities.physics;

import net.minecraft.block.state.IBlockState;
import net.minecraft.world.World;

public class PhysicsBlockEntity {

	public IBlockState blockState;
	public World world;
	
	
	public PhysicsBlockEntity(World w, IBlockState block) {
		blockState = block;
		world = w;
		
	}

	public boolean hasTileEntity() {
		return blockState.getBlock().hasTileEntity(blockState);
	}
}
