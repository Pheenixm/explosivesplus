package com.pheenixm.explosivesplus.common.entities.missiles;

import java.util.HashSet;
import java.util.Set;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityRegistry;
import com.pheenixm.explosivesplus.common.items.components.Engine;
import com.pheenixm.explosivesplus.common.items.components.Fuselage;
import com.pheenixm.explosivesplus.common.items.components.Engine.EngineTiers;
import com.pheenixm.explosivesplus.common.items.components.Fuselage.FuselageTiers;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class MissileFlightHelper {

	public static final double Y_CONSTANT = -0.01;
	public static final double ARMING_DISTANCE = 15;
	public static final double MIN_TICKS = 100;
	
	public static final int CHUNK_RANGE = 5;
	// Previously 150
	public static final double Y_MAX = 256;
	public static final double LAUNCH_TICKS = 80;
	public static final double TURN_TICKS = 10;

	
	public FauxEntityBallisticMissile missile;
	public World world;
	public Vec3d start;
	public Vec3d target;
	
	private double flightTime;
	private double xVel;
	private double zVel;
	private double yDecrement;
	private double yDiff;
	private boolean chunkLoaded;
	private boolean forcedChunks;


	
	public MissileFlightHelper(Vec3d tar, World world) {
		target = tar;
		yDecrement = 0;
		flightTime = 1;
	}
	
	//TODO: Improve impact detection in unloaded chunks
	public void checkDetonation(int launchTimer) {
		if(launchTimer > LAUNCH_TICKS + TURN_TICKS) {
			if( (launchTimer - LAUNCH_TICKS - TURN_TICKS) < flightTime/2 && !world.isAirBlock(missile.getBlockPos())) {
				missile.premature();
				return;
			}

			if(launchTimer >= flightTime + TURN_TICKS + LAUNCH_TICKS) {
				missile.detonate(forcedChunks);
			}
		}
	}
	
	public void handleLaunch(int launchTimer) {
		// Perform launching animations
		if (launchTimer < LAUNCH_TICKS) {
			missile.motionY = 25D / LAUNCH_TICKS;
		}
		if (launchTimer == LAUNCH_TICKS) {
			beginTurningAnimation();
			missile.oldPitch = missile.rotationPitch;
			missile.prevRotationYaw = missile.rotationYaw;
			missile.rotationYaw = -missile.newYaw - 90;
		}
		// TODO: Make this animation configurable
		if (launchTimer > LAUNCH_TICKS) {
			missile.motionY -= missile.motionY / TURN_TICKS;
			double animTicks = launchTimer - LAUNCH_TICKS;
			double pitchConst = (missile.newPitch - missile.oldPitch) / TURN_TICKS;
			missile.prevRotationPitch = missile.rotationPitch;
			missile.rotationPitch = (float) (missile.oldPitch - pitchConst * animTicks);
		}
	}
	
	public void moveMissile() {
		if(!chunkLoaded) {
			BlockPos targ = new BlockPos(target);
			if(target == null) {
				missile.setDead();
				return;
			}
			if(!world.isBlockLoaded(targ)) {
				loadChunks(targ);
				forcedChunks = true;
			}
			chunkLoaded = true;
		}
		missile.motionY += yDecrement;
		
		missile.prevPosX = missile.posX;
		missile.prevPosY = missile.posY;
		missile.prevPosZ = missile.posZ;
		
		missile.posX += xVel;
		missile.posY += missile.motionY + yDiff;
		missile.posZ += zVel;
		
	}
	/**
	 * Calculate initial momentum values
	 */
	public void calculateInitialValues() {
		Vec3d start = new Vec3d(missile.posX, missile.posY, missile.posZ);
		Vec3d end = target;
		Vec3d line = end.subtract(start);

		flightTime = this.calculateTimeByDistance(line);

		xVel = line.x / flightTime;
		zVel = line.z / flightTime;

		double b = getInitB(flightTime, Y_MAX);
		double a = 1 / (flightTime) * b;

		//Modified from original
		yDecrement = -2*a;

		missile.motionY = b;

		yDiff = line.y / flightTime;
	}

	
	private double getInitB(double time, double yMax) {
		double t_c = time / 2;
		return (yMax - (Y_CONSTANT * t_c * t_c)) / (t_c);
	}

	/*
	 * This should only ever be called during the launch command Calculates the
	 * distance to the target, uses the engine tier to calculate the time to
	 * reach it and if it can reach it
	 * 
	 * @returns time to reach target in ticks, -1 if target out of range
	 */
	public double calculateTimeByDistance(Vec3d pos) {
		if (target == null) {
			System.out.println("Target should be set in the constructor");
			return -1;
		}
		// Base value, fastest allowed time
		double time = MIN_TICKS;

		double distance = Math.sqrt(pos.lengthSquared());

		FuselageTiers fuseTier = ((Fuselage) missile.fuselage.getItem()).getTier();
		if (fuseTier.range < distance) {
			// Target is out of range
			return -1;
		}

		EngineTiers engineTier = ((Engine) missile.engine.getItem()).getTier();
		int ticks = 0;
		double speed = engineTier.acceleration;
		// TODO: Come up with a formula for non-uniform acceleration
		while (distance > 0) {
			distance -= speed;
			if (speed < engineTier.velocity) {
				speed += engineTier.acceleration;
			}
			ticks++;
		}
		if (ticks >= time) {
			// Don't want to cut into the base time
			time = ticks;
		}

		return Math.floor(time);
	}
	
	public void beginTurningAnimation() {
		calculateInitialValues();
		float f3 = MathHelper.sqrt(xVel * xVel + zVel * zVel);
		missile.newYaw = (float) ((Math.atan2(xVel, zVel) * 180D) / 3.1415927410125732D);
		missile.newPitch = (float) ((Math.atan2(missile.motionY, f3) * 180D) / 3.1415927410125732D);
		yDecrement = 0;
	}
	
	public void launch() {
		this.calculateInitialValues();
		missile.motionX = xVel;
		missile.motionZ = zVel;
		missile.getHeading();
	}
	
	public void updateTarget(Vec3d target) {
		this.target = target;
	}
	
	private void loadChunks(BlockPos tarPos) {
		if (!world.isRemote) {
			for(int i = -CHUNK_RANGE; i <= CHUNK_RANGE; i++) {
				for(int j = -CHUNK_RANGE; j <= CHUNK_RANGE; j++) {
					ChunkPos chunkPos = new ChunkPos(new BlockPos(tarPos.getX() + i*16, 0, tarPos.getY() + j * 16));
					FauxEntityRegistry.getRegistry(world).forceChunks(chunkPos, (int)flightTime * 2);
					System.out.println("Loaded chunks");
				}
			}
		}
	}

	
	public NBTTagCompound serialize() {
		NBTTagCompound tag = new NBTTagCompound();
		tag.setDouble("flightTime", flightTime);
		tag.setDouble("xVel", xVel);
		tag.setDouble("zVel", zVel);
		tag.setDouble("yDecrement", yDecrement);
		return tag;
	}

	public void deserialize(NBTTagCompound tag) {
		flightTime = tag.getDouble("flightTime");
		xVel = tag.getDouble("xVel");
		zVel = tag.getDouble("zVel");
		yDecrement = tag.getDouble("yDecrement");
	}



}
