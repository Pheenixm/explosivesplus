package com.pheenixm.explosivesplus.common.entities.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import com.pheenixm.explosivesplus.ExplosivesPlus;
import com.pheenixm.explosivesplus.common.chunker.ChunkInjector;
import com.pheenixm.explosivesplus.common.chunker.ThreadChunk;
import com.pheenixm.explosivesplus.common.explosions.Explosion;
import com.pheenixm.explosivesplus.common.explosions.ExplosionBiomeBuster;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.multiplayer.ChunkProviderClient;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.SPacketChunkData;
import net.minecraft.server.management.PlayerChunkMap;
import net.minecraft.server.management.PlayerChunkMapEntry;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;

public class EntityExplosionController extends Entity implements IEntityAdditionalSpawnData {

	private EntityPlayer player;
	private boolean explosionDone = false;
	private boolean endExplosion = false;
	public Explosion explosion;
	private double oldX;
	private double oldZ;
	private double oldY;
	private int ticks;
	private static final int TICKS_PER_SECOND = 20;
	public ChunkInjector injector;
	public IBlockState parentBlock;
	private boolean classicExplosion;

	public EntityExplosionController(World world, Entity entity, double x,
			double y, double z, Explosion explosion, IBlockState parentBlock, boolean isClassic) {
		this(world);
		this.setPosition(x, y, z);
		ticks = 0;
		if(entity instanceof EntityPlayer) {
			this.player = (EntityPlayer) entity;
		}
		this.explosion = explosion;
		this.parentBlock = parentBlock;
		this.classicExplosion = isClassic;
		if (!world.isRemote && !isClassic) {
			injector = new ChunkInjector(explosion);
			ThreadChunk.threadProcessor.doExplosion(world, player, explosion);
			System.out.println("Calculation fin");
		} 
	}

	public EntityExplosionController(World world) {
		super(world);
	}

	@Override
	protected void entityInit() {
	}

	@Override
	public void onUpdate() {
		if(classicExplosion) {
			//Allows for use of more complex queuing and ticking methods
			if(explosion.doPostExplosion()) {
				this.setDead();
			}
		} else {
			if (explosionDone) {
				
				if(explosion != null) {
					endExplosion = explosion.doPostExplosion();
				}
				
				if (endExplosion) {
					if (!world.isRemote) {
						for (ChunkPos pos : explosion.getAffectedBlocksByChunk()
								.keySet()) {
							WorldServer ws = (WorldServer) world;
							Chunk chunk = ws.getChunk(pos.x, pos.z);
							SPacketChunkData packet = new SPacketChunkData(chunk,
									65535);

							for (EntityPlayer play : ws.playerEntities) {
								EntityPlayerMP entityplayermp = (EntityPlayerMP) play;
								entityplayermp.connection.sendPacket(packet);
								ws.getEntityTracker().sendLeashedEntitiesInChunk(
										entityplayermp, chunk);
							}
							if (ExplosivesPlus.ticketList != null
									&& ExplosivesPlus.ticketList.containsKey(pos)) {
								ExplosivesPlus.instance.releaseChunkLoad(this.world,
										pos);
							}
						}
						this.setDead();
					}
				}
			} else {
				if (injector != null) {
					injector.injectNext();
				}
			}
		}

	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound compound) {
        parentBlock = Block.getStateById(compound.getInteger("explosive"));
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound compound) {
        compound.setInteger("explosive", Block.getStateId(parentBlock));
	}

	public void doPostExplosion() {
		System.out.println("Post explosion called");
		explosionDone = true;
	}

	@Override
	public void writeSpawnData(ByteBuf buffer) {
        buffer.writeInt(Block.getStateId(parentBlock));
	}

	@Override
	public void readSpawnData(ByteBuf additionalData) {
        parentBlock = Block.getStateById(additionalData.readInt());		
	}

}
