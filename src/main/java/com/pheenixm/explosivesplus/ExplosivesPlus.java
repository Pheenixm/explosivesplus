package com.pheenixm.explosivesplus;

import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Queues;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableFutureTask;
import com.pheenixm.explosivesplus.common.CommonProxy;
import com.pheenixm.explosivesplus.common.ConfigHandler;
import com.pheenixm.explosivesplus.common.ExplosivesPlusHolder;
import com.pheenixm.explosivesplus.common.GuiHandler;
import com.pheenixm.explosivesplus.common.RegistryHandler;
import com.pheenixm.explosivesplus.common.ServerProxy;
import com.pheenixm.explosivesplus.common.blocks.ExplosivesBlock;
import com.pheenixm.explosivesplus.common.chunker.ThreadChunk;
import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;
import com.pheenixm.explosivesplus.common.coms.WirelessRegistry;
import com.pheenixm.explosivesplus.common.entities.faux.FauxEntityRegistry;
import com.pheenixm.explosivesplus.common.network.PacketHandler;
import com.pheenixm.explosivesplus.common.radar.RadarRegistry;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Util;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.ForgeChunkManager.LoadingCallback;
import net.minecraftforge.common.ForgeChunkManager.Ticket;
import net.minecraftforge.common.ForgeChunkManager.Type;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

/**
 * @author Pheenixm
 * 
 *TODO: Fix nova bomb saving
 *TODO: Fix nova bomb culling
 *TODO: Batch TNTRain Explosions to one explosion/layer
 *TODO: Determine optimal methodology for radar
 *
 *TODO: Do the trig needed for perfect predator missile targeting
 *
 *
 *TODO: Implement orbital strikes
 *TODO: Implement the good old AC-130
 *TODO: Charged creeper generators/explosive generators
 *TODO: Supernova - blocks move towards "sun"
 *TODO: Anti-mob turrets
 *TODO: Implement generators
 *TODO: Use AbstractHorse for Mind-Control Movemenet
 */
@Mod(modid = ExplosivesPlus.MOD_ID, name = ExplosivesPlus.MOD_NAME, version = ExplosivesPlus.VERSION, acceptedMinecraftVersions = ExplosivesPlus.MC_VERSION)
public final class ExplosivesPlus {
	
	public static final 	String MOD_ID		= "explosivesplus";
	public static final	String MOD_NAME 		= "Explosives+";
	public static final	String 	VERSION 		= "0.1";
	public static final String 	MC_VERSION 	= "[1.12.2]";
	public static final String BASENAME 	= MOD_ID + ":";
	
    private static final String CLIENT 		= "com.pheenixm.explosivesplus.client.ClientProxy";
    private static final String SERVER	 	= "com.pheenixm.explosivesplus.common.ServerProxy";

	public static HashMap<ChunkPos, Integer> ticketList;
	private static Ticket chunkLoaderTicket;
    private final Queue < FutureTask<? >> scheduledTasks = Queues. < FutureTask<? >> newArrayDeque();

	@Instance("explosivesplus")
	public static ExplosivesPlus instance;

	
    @SidedProxy(clientSide = ExplosivesPlus.CLIENT, serverSide = ExplosivesPlus.SERVER)
	public static CommonProxy proxy;

	public static Logger LOGGER;
	
	public static ConfigHandler config;
	
	public static boolean icbmClassicLoaded;


    public static CreativeTabs tab = new CreativeTabs("explosivesplus") {


		@Override
		public ItemStack createIcon() {
            return new ItemStack(ExplosivesPlusHolder.NOVA, 1, 0);
		}
		
    };

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event){
		icbmClassicLoaded = Loader.isModLoaded("icbmclassic");
		
		LOGGER = event.getModLog();
		proxy.preInit(event);
		instance = this;
		MinecraftForge.EVENT_BUS.register(RegistryHandler.class);
	    MinecraftForge.TERRAIN_GEN_BUS.register(ServerProxy.class);
	    MinecraftForge.EVENT_BUS.register(ServerProxy.class);
	    MinecraftForge.EVENT_BUS.register(RadarRegistry.class);
	    MinecraftForge.EVENT_BUS.register(WirelessRegistry.class);
	    MinecraftForge.EVENT_BUS.register(FauxEntityRegistry.class);
	    

		NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());

        PacketHandler.init();
        
        CapabilityNetwork.register();

        
	}
	
	
	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		ThreadChunk.start();
		proxy.init(event);
		RegistryHandler.registerRecipes();
	}
	
	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
		ForgeChunkManager.setForcedChunkLoadingCallback(this, new LoadingCallback() {
			@Override
			public void ticketsLoaded(List<Ticket> tickets, World world) {
			}
		});
    }
	
	@Mod.EventHandler
	public static void onServerStartEvent(FMLServerStartingEvent event) {
		//ThreadChunk.start();
	}
	 
	@Mod.EventHandler
	public static void onServerStopEvent(FMLServerStoppingEvent event) {
		//Wierd bug
		//ThreadChunk.cleanShutdown();
	}
	
	public void forceChunkLoad(World w, ChunkPos pos) {
		if(ticketList == null) {
			ticketList = new HashMap<ChunkPos, Integer>();
		}
		if(pos == null) {
			return;
		}
		if(!ticketList.containsKey(pos)) {
			if(chunkLoaderTicket == null) {
				chunkLoaderTicket = ForgeChunkManager.requestTicket(this, w, Type.NORMAL);
			}
			ticketList.put(pos, 1);
			ForgeChunkManager.forceChunk(chunkLoaderTicket, pos);
		}
		else {
			ticketList.put(pos, ticketList.get(pos)+1);
		}
	}
	
	public void releaseChunkLoad(World w, ChunkPos pos) {
		if(!ticketList.containsKey(pos) || chunkLoaderTicket == null) {
			return;
		}
		else {
			int num = ticketList.get(pos)-1;
			if(num > 0)
				ticketList.put(pos, num);
			else {
				ForgeChunkManager.unforceChunk(chunkLoaderTicket, pos);
				ticketList.remove(pos);
			}
		}
	}
	
	
	/* Scheduler implementation, may be useful in the future
    public ListenableFuture<Object> addScheduledTask(Runnable runnableToSchedule)
    {
        Validate.notNull(runnableToSchedule);
        return this.<Object>addScheduledTask(Executors.callable(runnableToSchedule));
    }

    public <V> ListenableFuture<V> addScheduledTask(Callable<V> callableToSchedule)
    {
        Validate.notNull(callableToSchedule);
        ListenableFutureTask<V> listenablefuturetask = ListenableFutureTask.<V>create(callableToSchedule);

        synchronized (this.scheduledTasks)
        {
            this.scheduledTasks.add(listenablefuturetask);
            return listenablefuturetask;
        }
    
    }
    
    	public void onTick() {
            synchronized (this.scheduledTasks)
            {
                while (!this.scheduledTasks.isEmpty())
                {
                    Util.runTask(this.scheduledTasks.poll(), LOGGER);
                }
            }

    	}
	*/
	
	
	
}
