package com.pheenixm.explosivesplus.api.energy;

public interface IEnergyConsumer {

	public EnergyBuffer getEnergyBuffer();
}
