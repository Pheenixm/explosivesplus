package com.pheenixm.explosivesplus.api.energy;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.energy.EnergyStorage;

public class EnergyBuffer extends EnergyStorage implements INBTSerializable<NBTTagCompound> {

	public EnergyBuffer(int capacity) {
		super(capacity);
	}
	
	public EnergyBuffer(int c, int e) {
		this(c);
		energy = e;
	}

	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound compound = new NBTTagCompound();
		compound.setInteger("energy", energy);
		compound.setInteger("capacity", capacity);
		return compound;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		energy = nbt.getInteger("energy");
		capacity = nbt.getInteger("capacity");
	}
	
	public void setEnergy(int e) {
		this.extractEnergy(energy - e, false);
	}
	
	public void setCapacity(int c) {
		this.capacity = c;
	}

}
