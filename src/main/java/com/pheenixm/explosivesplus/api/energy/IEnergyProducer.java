package com.pheenixm.explosivesplus.api.energy;

public interface IEnergyProducer {

	public void pushEnergy();
}
