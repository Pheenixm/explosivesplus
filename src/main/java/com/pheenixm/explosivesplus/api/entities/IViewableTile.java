package com.pheenixm.explosivesplus.api.entities;

import com.pheenixm.explosivesplus.common.entities.EntityViewer;

import net.minecraft.entity.Entity;

public interface IViewableTile<T extends Entity> {

	/**
	 * Used to create an entity for detecting
	 * if the player is looking at this tile
	 */
	public void createDetectionEntity();
	
	public void createCameraEntity();
	
	public T getDetectionEntity(); 
	
	public EntityViewer getCameraEntity();

}
