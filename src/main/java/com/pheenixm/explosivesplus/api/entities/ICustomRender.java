package com.pheenixm.explosivesplus.api.entities;

import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface ICustomRender {
	
	@SideOnly(Side.CLIENT)
	public void render(double x, double y, double z, float partialTicks);

	public double[] getPos();
	
	public boolean shouldRemove();
	
	public double getRenderDistance();
}
