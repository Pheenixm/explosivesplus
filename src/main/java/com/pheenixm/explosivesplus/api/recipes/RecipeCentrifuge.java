package com.pheenixm.explosivesplus.api.recipes;

import java.util.HashMap;

import it.unimi.dsi.fastutil.ints.IntStack;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class RecipeCentrifuge {

	public static HashMap<Item, RecipeCentrifuge> recipeList = new HashMap<Item, RecipeCentrifuge>();
	
	private ItemStack input;
	private ItemStack output;
	private int ticks;
	
	public RecipeCentrifuge(ItemStack input, ItemStack output, int ticksToProcess) {
		this.input = input;
		this.output = output;
		this.ticks = ticksToProcess;
	}
	
	public static void addRecipe(ItemStack in, ItemStack out, int ticksToProcess) {
		RecipeCentrifuge centri = new RecipeCentrifuge(in, out, ticksToProcess);
		if(!recipeList.containsKey(in.getItem())) {
			recipeList.put(in.getItem(), centri);
		}
	}
	
	
	public static ItemStack getCentrifugeOutput(ItemStack input) {
		if(recipeList.isEmpty() || !recipeList.containsKey(input.getItem())) {
			return ItemStack.EMPTY;
		}
		return recipeList.get(input.getItem()).output.copy();
	}
	
	//TODO: Add removal method
	
	public static int getCentrifugeTicks(ItemStack input) {
		if(recipeList.isEmpty() || !recipeList.containsKey(input.getItem())) {
			return -1;
		}
		return recipeList.get(input.getItem()).ticks;

	}
	
	public static boolean hasRecipe(ItemStack in) {
		return recipeList.containsKey(in.getItem());
	}

	public static int getInputCount(ItemStack in) {
		return recipeList.get(in.getItem()).input.getCount();
	}

	@Override
	public int hashCode() {
		return (Item.getIdFromItem(input.getItem()) * Item.getIdFromItem(output.getItem())) * ticks;
	}
}
