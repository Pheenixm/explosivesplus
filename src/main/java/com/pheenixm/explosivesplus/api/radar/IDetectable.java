package com.pheenixm.explosivesplus.api.radar;

import com.pheenixm.explosivesplus.api.coms.IEncoder;
import com.pheenixm.explosivesplus.common.EnumTier;
import com.pheenixm.explosivesplus.common.radar.RadarReg;
import com.pheenixm.explosivesplus.common.radar.RadarRegistry;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public interface IDetectable extends IEncoder, ICapabilitySerializable<NBTTagCompound> {

	/**
	 * Needs to be called whenever the entity implementing this
	 * is loaded from disk or spawned into the world
	 */
	public default boolean addToRadarRegistry(World world) {
		return RadarReg.getRegistry(world).addThreat(this);
	}
	
	/**
	 * Gets the id of the threat-entity, used for tracking the entity
	 * @return
	 */
	public int getId();
		
	
	public EnumTier getThreatTier();
	
	/**
	 * Gets the current position of the threat entity 
	 * @return a position representing the position of the entity
	 */
	public Vec3d getPos();
	
	
}
