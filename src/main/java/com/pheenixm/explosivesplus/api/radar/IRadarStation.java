package com.pheenixm.explosivesplus.api.radar;

import com.pheenixm.explosivesplus.api.coms.IEncoder;
import com.pheenixm.explosivesplus.common.EnumTier;
import com.pheenixm.explosivesplus.common.radar.RadarReg;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public interface IRadarStation extends IEncoder {

	/**
	 * Get the location of this radar station
	 * @return the location of this radar station
	 */
	public BlockPos getPos();

	public EnumTier getDetectionTier();
	
	/**
	 * Needs to be called whenever the tile implementing this
	 * is spawned into the world
	 */
	public default boolean addToRadarRegistry(World world) {
		return RadarReg.getRegistry(world).addStation(this.getPos(), this.getDetectionTier());
	}
	
	
	/**
	 * Wakes up station, passing it an IDetectable to track
	 * @param threat
	 */
	public void wakeUpStation(IDetectable threat);
	

}
