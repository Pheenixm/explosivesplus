package com.pheenixm.explosivesplus.api.coms;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public interface INetworkEndpoint extends INetworkComponent, ICapabilitySerializable<NBTTagCompound> {
	
	/**
	 * Call to send a command to this endpoint
	 * @param command a string representing a command to handle
	 */
	public void handleMessage(EnumCommand command, Object[] args);
	
	/**
	 * Get the status of this network endpoint
	 * @return a string containing the status of the endpoint
	 */
	public String getStatus();
}
