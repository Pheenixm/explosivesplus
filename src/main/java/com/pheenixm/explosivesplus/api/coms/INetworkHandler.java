package com.pheenixm.explosivesplus.api.coms;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;

public interface INetworkHandler extends INetworkComponent, ICapabilitySerializable<NBTTagCompound> {

	/**
	 * Used to save the current list of machines/network handlers on this network
	 * Allows for checking the status of each component with its previous state
	 * on the initialization of the graph
	 * Additionally saves the whitelist
	 * @param tag
	 * @return
	 */
	public NBTTagCompound encodeNodeList();
	
	/**
	 * Used to decode the previous list of machines/network handlers on this network
	 * Allows for checking the status of each component with its previous state
	 * upon the init of the graph
	 * Additionally reinitializes the whitelist
	 * @param tag
	 */
	public void decodeNodeList(NBTTagCompound tag);
	
	
	/**
	 * Used by wireless components to join the network this handler is on
	 * Particularly of use to entities joining the network they were
	 * created on
	 * @return
	 */
	public boolean joinExternalToHandlerNetwork();
	
	/**
	 * Used to join this handler to a remote network
	 * Primarily used by receiver type components
	 * Needs to draw edges from one to the other, 
	 * and vice-versa
	 * Should check if entry is already in registry list, 
	 * make call to add to registry list if not
	 * @return the success of this operation
	 */
	public boolean joinHandlerToExternalNetwork(BlockPos transmitter);
	
	/**
	 * Gets a copy of the player whitelist for this network
	 * @return a HashSet containing the UUIDs of all whitelisted players
	 */
	public Set<UUID> getPlayerWhitelist(boolean update); 

	public Set<UUID> getPlayerBlacklist(boolean update);
	
	/**
	 * Update the whitelist reference in network shared data with the passed in sets
	 * Also recommended that this method mark the entity dirty
	 * @param updateList A HashSet containing all players whitelisted on this network
	 */
	public boolean setPlayerWhitelists(HashSet<UUID> whitelist, HashSet<UUID> blacklist);
	
	/**
	 * Gets the wireless range for this network handler, returns -1 if no wireless available
	 * @return the wireless range of this handler, -1 if the handler has none
	 */
	public int getWirelessRange();
	
	/**
	 * Iterate over the entire graph and update this handler's list of endpoints
	 * If this handler does not need an endpoint list, do nothing
	 */
	public void updateEndpointList();
	
	/**
	 * Called when the block is placed
	 * @param worldIn
	 * @param pos
	 * @param state
	 * @param placer
	 * @param stack
	 */
	public void onPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack);
	
	/**
	 * Called when the entity this cap is attached to is actually loaded
	 */
	public void init(World world);


}
