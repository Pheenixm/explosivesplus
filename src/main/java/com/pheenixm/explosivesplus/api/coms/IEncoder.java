package com.pheenixm.explosivesplus.api.coms;

import java.util.UUID;

public interface IEncoder {
	
	/**
	 * Get
	 * 
	 * @return
	 */
	public UUID getSharedUid();
}
