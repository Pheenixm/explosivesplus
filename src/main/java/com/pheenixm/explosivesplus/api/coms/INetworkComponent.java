package com.pheenixm.explosivesplus.api.coms;

import java.util.List;

import com.google.common.collect.Lists;

import gigaherz.graph2.Graph;
import gigaherz.graph2.GraphObject;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public interface INetworkComponent extends IEncoder {

	/**
	 * Get the network handler that controls the graph this vertex is on
	 * 
	 * @return a graph object holding
	 */
	public GraphObject getNetworkHandler();


	/**
	 * Get a list of neighboring graphs, if any exist
	 * 
	 * @param world
	 *            World to check in
	 * @param pos
	 *            Pos to check from
	 * @return a list of neighboring graphs
	 */
	public List<GraphObject> getNeighbors(IBlockAccess world, BlockPos pos);

	/**
	 * Update nieghboring graphs
	 */
	public void updateNeighbors(IBlockAccess world, BlockPos pos);

	public void onComponentDestroyed(BlockPos pos);

}
