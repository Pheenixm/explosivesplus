package com.pheenixm.explosivesplus.api.coms;

import javax.annotation.Nullable;

import com.pheenixm.explosivesplus.common.coms.CapabilityNetwork;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public interface INetworkedBlock {

	/**
	 * Needs to be called whenever a neighboring block changes
	 * @param world The world the change occurs in
	 * @param pos the position of this block
	 *TODO: This method either needs a serious reworking, or removal
	 *@deprecated
	 */
	public default void onBlockChange(World world, BlockPos pos) {
		INetworkComponent tile = (INetworkComponent) world.getTileEntity(pos);
		tile.updateNeighbors(world, pos);
	}
	
	@Nullable
	public default TileEntity getTileEntity(IBlockAccess world, BlockPos pos) {
		TileEntity tile = world.getTileEntity(pos);
		return tile;
	}
	
	@Nullable
	public default INetworkComponent getComponentCapability(IBlockAccess world, BlockPos pos, @Nullable EnumFacing facing) {
		TileEntity tile = world.getTileEntity(pos);
		if(tile != null && tile.hasCapability(CapabilityNetwork.COMPONENT, facing)) {
			return tile.getCapability(CapabilityNetwork.COMPONENT, facing);
		} else {
			return null;
		}
	}
	
}
