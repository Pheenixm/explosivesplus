uniform sampler2D texture0;

varying vec2 uvVarying;

const int WaveMaxID = 10;

uniform vec2 agk_resolution;
uniform float waveTime[WaveMaxID]; // effect elapsed time
uniform vec2 waveCenter[WaveMaxID]; // Mouse position
uniform vec3 shockParams; // 10.0, 0.8, 0.1

void main()
{
    float aspectRatio = agk_resolution.x/agk_resolution.y;
    vec2 uv = uvVarying * vec2(aspectRatio,1.0);
    vec2 finalCoord = uvVarying;
    for (int i = 0; i < WaveMaxID; i++)
    {
      vec2 center = waveCenter[i]/agk_resolution * vec2(aspectRatio,1.0);
      vec2 texCoord = vec2(0.0);
      float dist = distance(uv, center);
      if ((dist >= (waveTime[i] - shockParams.z)) && (dist <= (waveTime[i] + shockParams.z))) 
      {
        float diff = (dist - waveTime[i]); 
        float powDiff = 1.0 - pow(abs(diff*shockParams.x), shockParams.y); 
        float diffTime = diff * powDiff;
        vec2 diffUV = normalize(uv - center); 
        texCoord = (diffUV * diffTime);
        }
        finalCoord += texCoord;
        }
        gl_FragColor = texture2D(texture0, finalCoord);
}